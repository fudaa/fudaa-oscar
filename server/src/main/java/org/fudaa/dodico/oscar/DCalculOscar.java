/**
 * @file         DCalculOscar.java
 * @creation     1997-12-10
 * @modification $Date: 2006-09-19 14:44:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.oscar;

import java.io.File;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.oscar.ICalculOscar;
import org.fudaa.dodico.corba.oscar.ICalculOscarOperations;
import org.fudaa.dodico.corba.oscar.IParametresOscar;
import org.fudaa.dodico.corba.oscar.IParametresOscarHelper;
import org.fudaa.dodico.corba.oscar.IResultatsOscar;
import org.fudaa.dodico.corba.oscar.IResultatsOscarHelper;
import org.fudaa.dodico.corba.oscar.SParametresOscar;
import org.fudaa.dodico.corba.oscar.SResultatsOscar;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CExec;

/**
 * Gestion du lancement du code de calcul <code>Oscar</code>.
 * 
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:44:23 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class DCalculOscar extends DCalcul implements ICalculOscar, ICalculOscarOperations {
  /**
   * Initialisation des extensions des fichiers.
   */
  public DCalculOscar() {
    super();
    // setFichiersExtensions(new String[] {".dia"});
  }

  /**
   * cette m�thode n'est pas impl�ment�e compl�tement.
   * 
   * @return <code>new DCalculOscar()</code>.
   */
  public final Object clone() throws CloneNotSupportedException {
    return new DCalculOscar();
  }

  public String toString() {
    return "DCalculOscar()";
  }

  /**
   * Description du serveur de calcul <code>Oscar</code>.
   */
  public String description() {
    return "Oscar, serveur de calcul: " + super.description();
  }

  /**
   * lance le code de calcul Oscar : verifie la non-nullite des interfaces parametres et resultats,lance l'executable et
   * finalement lit les fichiers de resultats pour completer l'interface IResultats.
   * 
   * @param _c la connexion utilisee.
   */
  public void calcul(final IConnexion _c) {
    try {
      if (!verifieConnexion(_c)) {
        throw new Exception();
      }
    } catch (final Exception _e1) {
      System.out.println("Connexion non valide, execution du code de calcul impossible");
      return;
    }
    final IParametresOscar params = IParametresOscarHelper.narrow(parametres(_c));
    try {
      if (params == null) {
        throw new Exception();
      }
    } catch (final Exception _e2) {
      System.out.println("Aucun param�tre d'entr�e pour le code de calcul");
      return;
    }
    final IResultatsOscar results = IResultatsOscarHelper.narrow(resultats(_c));
    try {
      if (results == null) {
        throw new Exception();
      }
    } catch (final Exception _e3) {
      System.out.println("Aucun r�sultat renvoy� par le code de calcul");
      return;
    }
    final String os = System.getProperty("os.name");
    final String path = cheminServeur();
    final SParametresOscar _p = params.parametresOscar();
    String timestamp = String.valueOf(System.currentTimeMillis());
    String etude = "e" + timestamp.substring(timestamp.length() - 8, timestamp.length());
    while ((new File(etude + ".dat")).exists()) {
      timestamp = String.valueOf(System.currentTimeMillis());
      etude = "e" + timestamp.substring(timestamp.length() - 8, timestamp.length());
    }
    final String _mode = _p.parametresGeneraux.choixCalculSol;
    SResultatsOscar oscarResults = null;
    try {
      DParametresOscar.ecritParametresOscar(_p, etude + ".dat");
      String[] cmd;
      cmd = new String[4];
      cmd[0] = path + "bin" + ((os.startsWith("Windows")) ? "\\win\\oscar_win.bat" : "/linux/oscar_linux");
      cmd[1] = path + "bin" + ((os.startsWith("Windows")) ? "\\win\\" : "/linux/");
      if (_mode.equals("L")) {
        cmd[2] = "all_ce";
        // execute xpsol_ce et xplan � la suite
      } else {
        cmd[2] = "all_ct";
      }
      // execute xpsol_ct et xplan � la suite
      cmd[3] = etude;
      System.out.println(">>> Execute : [" + cmd[0] + " " + cmd[1] + " " + cmd[2] + " " + cmd[3] + "]");
      try {
        final CExec ex = new CExec();
        ex.setCommand(cmd);
        /* ex.setExecDirectory(new File(path)); */
        ex.setOutStream(System.out);
        ex.setErrStream(System.err);
        ex.exec();
      } catch (final Throwable _e1) {
        System.out.println("Erreur rencontr�e lors de l'execution du code de calcul");
        _e1.printStackTrace();
      }
      oscarResults = DResultatsOscar.resultatsOscar(etude);
      results.resultatsOscar(oscarResults);
    } catch (final Exception ex) {
      System.out.println("Erreur lors de l'execution du code de calcul");
    }
    String property = System.getProperty("java.io.tmpdir") + System.getProperty("file.separator");
    // On efface les fichiers temporaires utilis�s par le code de calcul
    File  tmp = new File(property + "oscar.dat");
    if (tmp.exists()) {
      tmp.delete();
    }
    File tmpo = new File(etude + ".dat");
    tmpo.renameTo(tmp);
    tmpo.delete();
    tmp = new File(property + "oscar.res");
    if (tmp.exists()) {
      tmp.delete();
    }
    tmpo = new File(etude + ".res");
    tmpo.renameTo(tmp);
    tmpo.delete();
    tmp = new File(property + "oscar.rid");
    if (tmp.exists()) {
      tmp.delete();
    }
    tmpo = new File(etude + ".rid");
    tmpo.renameTo(tmp);
    tmpo.delete();
    tmp = new File(property + "oscar.diag");
    if (tmp.exists()) {
      tmp.delete();
    }
    tmpo = new File(etude + ".diag");
    tmpo.renameTo(tmp);
    tmpo.delete();
    tmp = new File(property + "oscar.err");
    if (tmp.exists()) {
      tmp.delete();
    }
    tmpo = new File(etude + ".err");
    tmpo.renameTo(tmp);
    tmpo.delete();
  }
}
