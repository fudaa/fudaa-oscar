/**
 * @creation 2000-09-21
 * @modification $Date: 2006-09-19 14:44:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.oscar;

import java.io.FileWriter;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.corba.oscar.IParametresOscar;
import org.fudaa.dodico.corba.oscar.IParametresOscarOperations;
import org.fudaa.dodico.corba.oscar.SFichierDiagramme;
import org.fudaa.dodico.corba.oscar.SParametresOscar;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;
import org.fudaa.dodico.fortran.FortranWriter;
/**
 * Classe impl�mentant l'interface IParametresOscar g�rant les parametres du code de calcul oscar.
 * @version $Revision: 1.13 $ $Date: 2006-09-19 14:44:23 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class DParametresOscar extends DParametres implements IParametresOscar,
    IParametresOscarOperations {

  /**
   * mettre 10.0 pour "unit� en Tonne" et 1.0 pour "unit� en kN".
   */
  static double coefconv_ = 10.0;
  /**
   * structure decrivant les parametres du calcul.
   */
  private SParametresOscar parametreOscar_ ;

  /**
   * constructeur vide.
   */
  public DParametresOscar() {
    super();
  }

  /**
   * cette m�thode n'est pas implant�e compl�tement.
   * @return <code>DParametresOscar()</code>
   */
  public final Object clone() throws CloneNotSupportedException{
    return new DParametresOscar();
  }

  /**
   * cha�ne descriptive de l'objet.
   * @return "DParametresOscar"
   */
  public String toString(){
    return "DParametresOscar";
  }

  /**
   * retourne la structure utilisee pour d�crire les parametres.
   */
  public SParametresOscar parametresOscar(){
    return parametreOscar_;
  }

  /**
   * modifie la structure des parametres.
   * @param _p la nouvelle structure.
   */
  public void parametresOscar(final SParametresOscar _p){
    parametreOscar_ = _p;
  }

  /**
   * Ecrit les parametres <code>_params</code> dans le fichier de nom <code>_nomfichier</code>,
   * d'extension ".dia" et de chemin <code>_path</code>. Le format "fortran" est utilise.
   * @param _nomFichier le fichier destination(avec extension).
   * @param _params les parametres a ecrire dans le fichier.
   * @throws IOException
   */
  public static void ecrisParametresFichierDiagrammeSup(final SFichierDiagramme _params,final String _nomFichier)
      throws IOException{
    int i = 0;
    int[] fmtI;
    double v;
    final FortranWriter _fw = new FortranWriter(new FileWriter(_nomFichier));
    if (System.getProperty("os.name").startsWith("Windows")) {
      _fw.setLineSeparator("\r\n");
    }
    else {
      _fw.setLineSeparator("\n");
    }
    fmtI = new int[] { 80};
    // Titre
    _fw.stringField(0, "Fichier de diagramme suppl�mentaires " + _nomFichier);
    _fw.writeFields(fmtI);
    fmtI = new int[] { 15, 15};
    // Points
    System.out.println("nb points = " + _params.pointsCotePression.length);
    for (i = 0; i < _params.pointsCotePression.length; i++) {
      v = _params.pointsCotePression[i].cote;
      System.out.println("i=" + i + " | _v=" + v);
      _fw.doubleField(0, (v == VALEUR_NULLE.value) ? 0.0 : v);
      v = _params.pointsCotePression[i].valeur;
      _fw.doubleField(1, (v == VALEUR_NULLE.value) ? 0.0 : v);
      _fw.writeFields(fmtI);
    }
    _fw.flush();
    _fw.close();
  }

  /**
   * Ecrit les parametres <code>_params</code> dans le fichier de nom <code>_nomfichier</code>,
   * d'extension ".dia" et de chemin <code>_path</code>. Le format "fortran" est utilise.
   * @param _nomFichier le fichier destination(avec extension).
   * @param _params les parametres a ecrire dans le fichier.
   * @throws IOException
   */
  public static void ecritParametresOscar(final SParametresOscar _params,final String _nomFichier)
      throws IOException{
    int i = 0;
    int[] fmtI;
    double v;
    String t;
    Double tc;
    final FortranWriter fw = new FortranWriter(new FileWriter(_nomFichier));
    fw.setLineSeparator(CtuluLibString.LINE_SEP);
    fmtI = new int[] { 80};
    // Titre
    fw.stringField(0, _params.commentairesOscar.titre);
    fw.writeFields(fmtI);
    fw.stringField(0, "");
    fw.writeFields(fmtI);
    // Sol
    fw.stringField(0, "SOL");
    fw.writeFields(fmtI);
    fw.stringField(0, "");
    fw.writeFields(fmtI);
    writePoussee(_params, fmtI, fw);
    fw.stringField(0, "");
    fw.writeFields(fmtI);
    //// But�e
    fw.stringField(0, "BUTEE");
    fw.writeFields(fmtI);
    ////// Cote du Fond de Souille
    v = _params.parametresGeneraux.coteTerrePleinButee;
    t = String.valueOf(v);
    fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
    fw.writeFields(fmtI);
    ////// Nombre de couches en but�e
    fw.stringField(0, String.valueOf(_params.sol.nombreCouchesButee));
    fw.writeFields(fmtI);
    ////// Couches
    for (i = 0; i < _params.sol.nombreCouchesButee; i++) {
      //////// Epaisseur
      v = _params.sol.couchesButee[i].epaisseur;
      t = String.valueOf(v);
      fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      fw.writeFields(fmtI);
      //////// Poids volumique humide
      v = _params.sol.couchesButee[i].poidsVolumiqueHumide;
      t = String.valueOf(v / coefconv_);
      fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      fw.writeFields(fmtI);
      //////// Poids volumique satur�
      v = _params.sol.couchesButee[i].poidsVolumique;
      if (_params.parametresGeneraux.choixCalculSol.equals("L") && (v != VALEUR_NULLE.value)) {
        v += 10.0;
      }
      t = String.valueOf(v / coefconv_);
      fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      fw.writeFields(fmtI);
      //////// Coefficient de but�e
      v = _params.sol.couchesButee[i].coefficient;
      t = String.valueOf(v);
      fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      fw.writeFields(fmtI);
      //////// Terme de coh�sion dra�n�e
      if ((_params.sol.couchesButee[i].angleFrottement != VALEUR_NULLE.value)
          && (_params.sol.couchesButee[i].coefficient != VALEUR_NULLE.value)
          && (_params.sol.couchesButee[i].cohesion != VALEUR_NULLE.value)) {
        if (_params.sol.couchesButee[i].angleFrottement != 0.0) {
          tc = new Double(
              -((1 - _params.sol.couchesButee[i].coefficient) / Math
                  .tan(Math.toRadians(_params.sol.couchesButee[i].angleFrottement)))
                  * _params.sol.couchesButee[i].cohesion);
        } else {
          tc = new Double(2 * _params.sol.couchesButee[i].cohesion);
        }
      }
      else {
        tc = null;
      }
      fw.stringField(0, (tc == null) ? "" : String.valueOf(tc.doubleValue() / coefconv_));
      fw.writeFields(fmtI);
    }
    writeEaux(_params, fmtI, fw);
    fw.stringField(0, "");
    fw.writeFields(fmtI);
    // Surcharges
    writeSurcharge(_params, fmtI, fw);
    //// Surcharge Lin�ique
    writeSurchargeLineique(_params, fmtI, fw);
    //// Surcharge Uniforme en Bande
    writeBande(_params, fmtI, fw);
    fw.stringField(0, "");
    fw.writeFields(fmtI);
    writeEfforts(_params, fmtI, fw);
    fw.stringField(0, "");
    fw.writeFields(fmtI);
    writeOuvrages(_params, fmtI, fw);
    fw.flush();
    fw.close();
  }

  private static void writePoussee(final SParametresOscar _params, int[] _fmtI, final FortranWriter _fw) throws IOException {
    int i;
    double v;
    String t;
    Double tc;
    //// Pouss�e
    _fw.stringField(0, "POUSSEE");
    _fw.writeFields(_fmtI);
    ////// Cote du Terre-Plein
    v = _params.parametresGeneraux.coteTerrePleinPoussee;
    t = String.valueOf(v);
    _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
    _fw.writeFields(_fmtI);
    ////// Nombre de couches en pouss�e
    _fw.stringField(0, String.valueOf(_params.sol.nombreCouchesPoussee));
    _fw.writeFields(_fmtI);
    ////// Couches
    for (i = 0; i < _params.sol.nombreCouchesPoussee; i++) {
      //////// Epaisseur
      v = _params.sol.couchesPoussee[i].epaisseur;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      //////// Poids volumique humide
      v = _params.sol.couchesPoussee[i].poidsVolumiqueHumide;
      t = String.valueOf(v / coefconv_);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      //////// Poids volumique
      v = _params.sol.couchesPoussee[i].poidsVolumique;
      if (_params.parametresGeneraux.choixCalculSol.equals("L") && (v != VALEUR_NULLE.value)) {
        v += 10.0;
      }
      t = String.valueOf(v / coefconv_);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      //////// Coefficient de pouss�e
      v = _params.sol.couchesPoussee[i].coefficient;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      //////// Terme de coh�sion dra�n�e
      if ((_params.sol.couchesPoussee[i].angleFrottement != VALEUR_NULLE.value)
          && (_params.sol.couchesPoussee[i].coefficient != VALEUR_NULLE.value)
          && (_params.sol.couchesPoussee[i].cohesion != VALEUR_NULLE.value)) {
        if (_params.sol.couchesPoussee[i].angleFrottement != 0.0) {
          tc = new Double(
              ((1 - _params.sol.couchesPoussee[i].coefficient) / Math
                  .tan(Math.toRadians(_params.sol.couchesPoussee[i].angleFrottement)))
                  * _params.sol.couchesPoussee[i].cohesion);
        } else {
          tc = new Double(2 * _params.sol.couchesPoussee[i].cohesion);
        }
      }
      else {
        tc = null;
      }
      _fw.stringField(0, (tc == null) ? "" : String.valueOf(tc.doubleValue() / coefconv_));
      _fw.writeFields(_fmtI);
    }
  }

  private static void writeOuvrages(final SParametresOscar _params, int[] _fmtI, final FortranWriter _fw) throws IOException {
    double v;
    String t;
    // OUVRAGE
    _fw.stringField(0, "OUVRAGE");
    _fw.writeFields(_fmtI);
    _fw.stringField(0, "");
    _fw.writeFields(_fmtI);
    //// Tirants
    _fw.stringField(0, "TIRANTS");
    _fw.writeFields(_fmtI);
    //// Pr�sence d'une nappe de tirants
    _fw.stringField(0, _params.ouvrage.presenceNappeTirants);
    _fw.writeFields(_fmtI);
    //// Informations si tirants pr�sents
    if (_params.ouvrage.presenceNappeTirants.equals("O")) {
      ////// Cote de la nappe de tirants
      v = _params.ouvrage.coteNappeTirants;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      ////// Pourcentage d'encastrement de la nappe de tirants
      v = _params.ouvrage.pourcentageEncastrementNappe;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      ////// Espacement entre deux tirants
      v = _params.ouvrage.espaceEntreDeuxTirants;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      ////// Section d'un tirant
      v = _params.ouvrage.sectionTirants;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      ////// Limite �lastique de l'acier
      v = _params.ouvrage.limiteElastiqueAcierTirants;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
    _fw.stringField(0, "");
    _fw.writeFields(_fmtI);
    //// Palplanches
    _fw.stringField(0, "PALPLANCHES");
    _fw.writeFields(_fmtI);
    ////// Module d'Young de l'acier
    v = _params.ouvrage.moduleYoungAcier;
    t = String.valueOf(v);
    _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
    _fw.writeFields(_fmtI);
    ////// Limite �lastique de l'acier
    v = _params.ouvrage.limiteElastiqueAcier;
    t = String.valueOf(v);
    _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
    _fw.writeFields(_fmtI);
    ////// Inertie Palplanches
    v = _params.ouvrage.inertiePalplanches;
    t = String.valueOf(v);
    _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
    _fw.writeFields(_fmtI);
    ////// Demi Hauteur
    v = _params.ouvrage.demieHauteur;
    t = String.valueOf(v);
    _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
    _fw.writeFields(_fmtI);
  }

  private static void writeEfforts(final SParametresOscar _params, int[] _fmtI, final FortranWriter _fw) throws IOException {
    int i;
    double v;
    String t;
    // Efforts
    _fw.stringField(0, "EFFORTS SUPPLEMENTAIRES");
    _fw.writeFields(_fmtI);
    _fw.stringField(0, "");
    _fw.writeFields(_fmtI);
    //// Efforts en t�te
    _fw.stringField(0, "EFFORTS EN TETE");
    _fw.writeFields(_fmtI);
    v = _params.effort.effortEnTeteDeRideau.valeurForceEnTete / coefconv_;
    t = String.valueOf(v);
    _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
    _fw.writeFields(_fmtI);
    // effort en t�te de rideau n�gatif pour coh�rence avec diagramme pr�sent�
    v = -(_params.effort.effortEnTeteDeRideau.valeurMomentEnTete / coefconv_);
    t = String.valueOf(v);
    _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
    _fw.writeFields(_fmtI);
    _fw.stringField(0, "");
    _fw.writeFields(_fmtI);
    //// Contraintes Verticales Suppl�mentaires
    _fw.stringField(0, "CVS");
    _fw.writeFields(_fmtI);
    ////// Nombre de contraintes
    _fw.stringField(0, String.valueOf(_params.effort.nombreContraintesVerticales));
    _fw.writeFields(_fmtI);
    ////// Contraintes
    for (i = 0; i < _params.effort.nombreContraintesVerticales; i++) {
      //////// Cote de la contrainte
      v = _params.effort.contraintesVerticales[i].cote;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      //// Valeur de la contrainte
      v = _params.effort.contraintesVerticales[i].valeur / coefconv_;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
    _fw.stringField(0, "");
    _fw.writeFields(_fmtI);
    //// Contraintes Horizontales Suppl�mentaires
    _fw.stringField(0, "CHS");
    _fw.writeFields(_fmtI);
    ////// Nombre de contraintes
    _fw.stringField(0, String.valueOf(_params.effort.nombreContraintesHorizontales));
    _fw.writeFields(_fmtI);
    ////// Contraintes
    for (i = 0; i < _params.effort.nombreContraintesHorizontales; i++) {
      //////// Cote de la contrainte
      v = _params.effort.contraintesHorizontales[i].cote;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      //// Valeur de la contrainte
      v = _params.effort.contraintesHorizontales[i].valeur / coefconv_;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
    _fw.stringField(0, "");
    _fw.writeFields(_fmtI);
    //// Fichiers de diagramme suppl�mentaires
    _fw.stringField(0, "FDS");
    _fw.writeFields(_fmtI);
    ////// Nombre de fichiers
    _fw.stringField(0, String.valueOf(_params.effort.fichiersDeDiagramme.length));
    _fw.writeFields(_fmtI);
    ////// Fichiers
    for (i = 0; i < _params.effort.fichiersDeDiagramme.length; i++) {
      //////// Chemin du fichier : fds0.diag, fds1.diag, ...
      try {
        t = String.valueOf(System.currentTimeMillis());
        // �criture sur disque des fichiers temporaires de diagrammes
        ecrisParametresFichierDiagrammeSup(_params.effort.fichiersDeDiagramme[i], "fds" + t + "-"
            + i + ".diag");
        _fw.stringField(0, "fds" + t + "-" + i + ".diag");
        _fw.writeFields(_fmtI);
      }
      catch (final Exception _e1) {
        _fw.stringField(0, "");
        _fw.writeFields(_fmtI);
      }
    }
  }

  private static void writeBande(final SParametresOscar _params, int[] _fmtI, final FortranWriter _fw) throws IOException {
    double bandeValue;
    String t;
    _fw.stringField(0, "UNIFORME BANDE");
    _fw.writeFields(_fmtI);
    ////// Valeur de la surcharge
    if (_params.surcharge.typeSurcharge.indexOf("B") > -1) {
      bandeValue = 0.0;
      t = String.valueOf(bandeValue);
      _fw.stringField(0, (bandeValue == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      bandeValue = 0.0 / coefconv_;
      t = String.valueOf(bandeValue);
      _fw.stringField(0, (bandeValue == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      bandeValue = 0.0;
      t = String.valueOf(bandeValue);
      _fw.stringField(0, (bandeValue == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      bandeValue = 0.0 / coefconv_;
      t = String.valueOf(bandeValue);
      _fw.stringField(0, (bandeValue == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
    else {
      bandeValue = 0.0;
      t = String.valueOf(bandeValue);
      _fw.stringField(0, (bandeValue == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      bandeValue = 0.0;
      t = String.valueOf(bandeValue);
      _fw.stringField(0, (bandeValue == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      bandeValue = 0.0;
      t = String.valueOf(bandeValue);
      _fw.stringField(0, (bandeValue == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      bandeValue = 0.0;
      t = String.valueOf(bandeValue);
      _fw.stringField(0, (bandeValue == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
  }

  private static void writeSurchargeLineique(final SParametresOscar _params, int[] _fmtI, final FortranWriter _fw) throws IOException {
    double v;
    String t;
    _fw.stringField(0, "LINEIQUE");
    _fw.writeFields(_fmtI);
    ////// Valeur de la surcharge
    if (_params.surcharge.typeSurcharge.indexOf("L") > -1) {
      v = 0.0;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      v = 0.0 / coefconv_;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
    else {
      v = 0.0;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
      v = 0.0;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
  }

  private static void writeSurcharge(final SParametresOscar _params, int[] _fmtI, final FortranWriter _fw) throws IOException {
    double v;
    String t;
    _fw.stringField(0, "SURCHARGES");
    _fw.writeFields(_fmtI);
    _fw.stringField(0, "");
    _fw.writeFields(_fmtI);
    //// Surcharge Uniforme Semi-Infinie
    _fw.stringField(0, "UNIFORME SEMI-INFINIE");
    _fw.writeFields(_fmtI);
    ////// Valeur de la surcharge
    if (_params.surcharge.typeSurcharge.indexOf("S") > -1) {
      v = _params.surcharge.valeurSurchargeUniformeSemiInfinie / coefconv_;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
    else {
      v = 0.0;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
  }

  private static void writeEaux(final SParametresOscar _params, int[] _fmtI, final FortranWriter _fw) throws IOException {
    double v;
    String t;
    _fw.stringField(0, "");
    _fw.writeFields(_fmtI);
    // Eau
    _fw.stringField(0, "EAU");
    _fw.writeFields(_fmtI);
    //// Type de calcul
    _fw.stringField(0, _params.eau.typeCalculEau);
    _fw.writeFields(_fmtI);
    //// Pr�sence d'une nappe d'eau dans le terre-plein
    _fw.stringField(0, (_params.eau.presenceNappeEauCotePoussee) ? "O" : "N");
    _fw.writeFields(_fmtI);
    ////// Cote Nappe d'eau si pr�sente
    if (_params.eau.presenceNappeEauCotePoussee) {
      v = _params.eau.coteNappeEauCotePoussee;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
    //// Pr�sence d'une nappe d'eau dans le bassin
    _fw.stringField(0, (_params.eau.presenceNappeEauCoteButee) ? "O" : "N");
    _fw.writeFields(_fmtI);
    ////// Cote Nappe d'eau si pr�sente
    if (_params.eau.presenceNappeEauCoteButee) {
      v = _params.eau.coteNappeEauCoteButee;
      t = String.valueOf(v);
      _fw.stringField(0, (v == VALEUR_NULLE.value) ? "" : t);
      _fw.writeFields(_fmtI);
    }
  }

  /**
   * Construit une structure <code>SParametresOscar</code> a partir des donnees du fichier
   * <code>_nomFichier</code>.
   * @param _nomFichier le nom du fichier a lire.
   * @return la structure construite a partir des donnees du fichier.
   * @throws IOException
   */
  public static SParametresOscar litParametresOscar(final String _nomFichier) throws IOException{
    final SParametresOscar _p = new SParametresOscar();
    return _p;
  }
}