/**
 * @creation     2000-10-07
 * @modification $Date: 2006-09-19 14:44:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.oscar;
import java.io.File;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IOrganisme;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.corba.oscar.ICalculOscar;
import org.fudaa.dodico.corba.oscar.IParametresOscar;
import org.fudaa.dodico.corba.oscar.IParametresOscarHelper;
import org.fudaa.dodico.corba.usine.IUsine;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Une classe cliente pour Oscar?.
 *
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 14:44:23 $ by $Author: deniger $
 * @author       Olivier Hoareau
 */
public final class OscarClient {
  private OscarClient(){
    
  }
  /**
   * @param _args serveur,fichier entree
   *
   */
  public static void main(final String[] _args) {
    // V�rifie si l'appel en ligne de commande du programme est correct (2 arguments)
    if (_args.length != 2) {
      System.out.println("Utilisation: OscarClient <serveur> <fichier_entree>");
      System.exit(1);
    }
    // R�cup�re les informations sur le serveur
    System.out.println("serveur " + _args[0]);
    System.setProperty("FUDAA_SERVEUR", _args[0]);
    // V�rifie si le fichier d'entr�e existe
    final File entree= new File(_args[1] + ".dat");
    if (!entree.exists()) {
      System.out.println(entree.getAbsolutePath() + " non trouve");
      System.exit(1);
    }
    // Cr�ation du serveur
    System.out.println("creation du serveur");
    UsineLib.setAllLocal(true);
    final IUsine usine= UsineLib.findUsine();
    final IPersonne p= usine.creeObjetPersonne();
    final IOrganisme o= usine.creeObjetOrganisme();
    o.intitule("sans");
    p.organisme(o);
    p.nom("test");
    final ICalculOscar calcul= usine.creeOscarCalculOscar();
    final IConnexion connexion= calcul.connexion(p);
    final IParametresOscar params=
      IParametresOscarHelper.narrow(calcul.parametres(connexion));
    // Lecture des param�tres et lancement du calcul
    System.out.println("lecture des parametres");
    try {
      params.parametresOscar(DParametresOscar.litParametresOscar(_args[1]));
      System.out.println("lancement calcul");
      calcul.calcul(connexion);
      System.out.println("calcul termine");
    } catch (final Exception e) {
      System.out.println(e);
    }
  }
}
