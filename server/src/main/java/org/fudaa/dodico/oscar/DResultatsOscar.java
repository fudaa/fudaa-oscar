/**
 * @creation 1998-03-12
 * @modification $Date: 2006-09-19 14:44:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.oscar;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.fudaa.ctulu.fileformat.FortranLib;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.corba.oscar.IResultatsOscar;
import org.fudaa.dodico.corba.oscar.IResultatsOscarOperations;
import org.fudaa.dodico.corba.oscar.SPointCoteValeur;
import org.fudaa.dodico.corba.oscar.SResultatsDeformees;
import org.fudaa.dodico.corba.oscar.SResultatsEffortsTranchants;
import org.fudaa.dodico.corba.oscar.SResultatsErreursXplan;
import org.fudaa.dodico.corba.oscar.SResultatsMomentsFlechissants;
import org.fudaa.dodico.corba.oscar.SResultatsOscar;
import org.fudaa.dodico.corba.oscar.SResultatsPredimensionnement;
import org.fudaa.dodico.corba.oscar.SResultatsPressions;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;
import org.fudaa.dodico.fortran.FortranReader;
/**
 * Les resultats Oscar.
 * @version $Revision: 1.12 $ $Date: 2006-09-19 14:44:23 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class DResultatsOscar extends DResultats implements IResultatsOscar,
    IResultatsOscarOperations {

  /**
   * mettre 10.0 pour "unit� en Tonne" et 1.0 pour "unit� en kN".
   */
  static double coefconv_ = 10.0;

  /**
   * @param _etude
   * @return une structure de donn�es contenant les informations r�sultats de l'�tude
   * <code>_etude</code>. Cette m�thode r�cup�re les donn�es des fichiers <code>_etude</code>
   * .rid, <code>_etude</code> .res et <code>_etude</code> .err
   */
  public static SResultatsOscar resultatsOscar(final String _etude){
    Object[] valeurs;
    // Initialisation
    final SResultatsOscar _r = new SResultatsOscar();
    try {
      _r.erreurs = resultatsOscarErreursXplan(_etude + ".err");
    }
    catch (final Throwable _e) {
      _r.erreurs = null;
    }
    try {
      _r.predimensionnement = resultatsOscarPredimensionnement(_etude + ".rid");
    }
    catch (final Throwable _e) {
      _r.predimensionnement = null;
    }
    try {
      _r.pressions = resultatsOscarPressions(_etude + ".diag");
    }
    catch (final Throwable _e1) {
      _r.pressions =  null;
    }
    try {
      valeurs = resultatsOscarResultatsXplan(_etude + ".res");
    }
    catch (final Throwable _e) {
      valeurs = null;
    }
    if(valeurs==null) return _r;
    try {
      _r.deformees = (SResultatsDeformees) valeurs[0];
    }
    catch (final Throwable _e) {
      _r.deformees = null;
    }
    try {
      _r.efforts = (SResultatsEffortsTranchants) valeurs[1];
    }
    catch (final Throwable _e) {
      _r.efforts = null;
    }
    try {
      _r.moments = (SResultatsMomentsFlechissants) valeurs[2];
    }
    catch (final Throwable _e) {
      _r.moments = null;
    }
    return _r;
  }

  /**
   * lecture du fichier d'erreurs Xplan, et r�cup�ration des erreurs sous forme de chaine de
   * caract�res.
   * @param _nomFichier
   * @return les resultats
   */
  public static SResultatsErreursXplan resultatsOscarErreursXplan(final String _nomFichier){
    int i = 0;
    int nblines = 0;
    // Initialisation
    final SResultatsErreursXplan e = new SResultatsErreursXplan();
    try {
      FortranReader fr = new FortranReader(new FileReader(_nomFichier));
      String t;
      try {
        while (true) {
          t = fr.readLine();
          nblines++;
        }
      }
      catch (final EOFException _e3) {}
      fr.close();
      fr = new FortranReader(new FileReader(_nomFichier));
      try {
        e.messages = new String[nblines];
        for (i = 0; i < nblines; i++) {
          t = fr.readLine();
          e.messages[i] = t;
        }
      }
      catch (final Exception _e3) {
        System.out.println("impossible de r�cup�rer tous les messages du fichier d'erreur Xplan.");
      }
      fr.close();
    }
    catch (final FileNotFoundException _e2) {
      System.out.println("le fichier d'erreurs Xplan est introuvable.");
    }
    catch (final IOException _e1) {
      System.out.println("erreur(s) rencontr�es lors de la lecture du fichier d'erreurs Xplan.");
    }
    return e;
  }

  /**
   * lecture du fichier r�sultats du pr�dimensionnement, et r�cup�ration des r�sultats pour un
   * rideau ancr� ou non ancr�.
   * @param _nomFichier
   * @return les resultats du pr�dimensionnemen
   */
  public static SResultatsPredimensionnement resultatsOscarPredimensionnement(final String _nomFichier){
    int[] fmtI;
    // Initialisation
    final SResultatsPredimensionnement _r = new SResultatsPredimensionnement();
    _r.momentMaximum = new SPointCoteValeur();
    _r.momentMaximum.cote = VALEUR_NULLE.value;
    _r.momentMinimum = new SPointCoteValeur();
    _r.momentMaximum.valeur = VALEUR_NULLE.value;
    try {
      final FortranReader _fr = new FortranReader(new FileReader(_nomFichier));
      fmtI = new int[] { 13};
      //// Cote de pression nulle
      _fr.readFields(fmtI);
      _r.cotePressionNull = _fr.doubleField(0);
      //// Point but�e simple
      _fr.readFields(fmtI);
      _r.pointButeeSimple = _fr.doubleField(0);
      //// Pied 20% contre-but�e
      _fr.readFields(fmtI);
      _r.pied20Pourcent = _fr.doubleField(0);
      //// Pied contre-but�e
      _fr.readFields(fmtI);
      _r.piedContreButee = _fr.doubleField(0);
      //// Valeur du moment maximum
      _fr.readFields(fmtI);
      _r.momentMaximum.valeur = _fr.doubleField(0) * coefconv_;
      //// Cote du moment maximum
      _fr.readFields(fmtI);
      _r.momentMaximum.cote = _fr.doubleField(0);
      //// Valeur du moment minimum
      _fr.readFields(fmtI);
      _r.momentMinimum.valeur = _fr.doubleField(0) * coefconv_;
      //// Cote du moment minimum
      _fr.readFields(fmtI);
      _r.momentMinimum.cote = _fr.doubleField(0);
      try {
        // Le rideau est ancr� ?
        //// Encastrement
        _fr.readFields(fmtI);
        _r.encastrement = _fr.doubleField(0);
        //// R�action
        _fr.readFields(fmtI);
        _r.reactionAncrage = _fr.doubleField(0) * coefconv_;
        //// Niveau
        _fr.readFields(fmtI);
        _r.niveauAncrage = _fr.doubleField(0);
      }
      catch (final EOFException _e3) {
        // Le rideau n'est pas ancr�
        _r.encastrement = VALEUR_NULLE.value;
        _r.reactionAncrage = VALEUR_NULLE.value;
        _r.niveauAncrage = VALEUR_NULLE.value;
      }
    }
    catch (final FileNotFoundException _e2) {
      System.out.println("le fichier r�sultats de pr�dimensionnement Xplan est introuvable.");
    }
    catch (final IOException _e1) {
      System.out
          .println("erreur(s) rencontr�es lors de la lecture du fichier r�sultats de pr�dimensionnement Xplan.");
    }
    return _r;
  }

  /**
   * renvoie une structure de donn�es contenant les informations du fichier r�sultats de Oscar
   * <code>_nomFichier</code>. Ce fichier contient le diagramme de pressions.
   * @param _nomFichier
   * @return les resultats de pression
   */
  public static SResultatsPressions resultatsOscarPressions(final String _nomFichier){
    int i = 0;
    SPointCoteValeur[] r;
    //String _titre;
    int[] fmtI;
    int nbpoints = 0;
    final SResultatsPressions _rp = new SResultatsPressions();
    FortranReader fr=null;
    try {
      fr = new FortranReader(new FileReader(_nomFichier));
      // Initialisation
      // Lecture de la premi�re ligne
      fmtI = new int[] { 80};
      //// Titre du fichier de diagramme
      fr.readFields(fmtI);
      //_titre= _fr.stringField(0);
      // Lecture des points cote / pression
      fmtI = new int[] { 15, 15};
      try {
        while (true) {
          fr.readFields(fmtI);
          nbpoints++;
        }
      }
      catch (final EOFException _e3) {}
      fr.close();
      fr = new FortranReader(new FileReader(_nomFichier));
      // Initialisation
      // Lecture de la premi�re ligne
      fmtI = new int[] { 80};
      //// Titre du fichier de diagramme
      fr.readFields(fmtI);
      // Lecture des points cote / pression
      fmtI = new int[] { 15, 15};
      r = new SPointCoteValeur[nbpoints];
      try {
        for (i = 0; i < nbpoints; i++) {
          fr.readFields(fmtI);
          r[i] = new SPointCoteValeur();
          r[i].cote = fr.doubleField(0);
          r[i].valeur = fr.doubleField(1) * coefconv_;
        }
      }
      catch (final EOFException _e3) {
        r = null;
      }
      _rp.pointsCotePression = r;
     
    }
    catch (final FileNotFoundException _e2) {
      System.out.println("le fichier r�sultats Oscar est introuvable.");
    }
    catch (final IOException _e1) {
      System.out.println("erreur(s) rencontr�es lors de la lecture du fichier r�sultats Oscar.");
    }
    FortranLib.close( fr);
    return _rp;
  }

  /**
   * lecture du fichier r�sultats Xplan, et r�cup�ration des r�sultats concernant la d�form�e, les
   * efforts tranchants et les moments fl�chissants.
   * @param _nomFichier
   * @return <code>object[0]</code>= SResultatsDeformees <code>object[1]</code>=
   *         SResultatsEffortsTranchants <code>object[2]</code>= SResultatsMomentsFlechissants
   */
  public static Object[] resultatsOscarResultatsXplan(final String _nomFichier){
    int i = 0;
    int[] fmtI;
    int nbpoints = 0;
    // Initialisation
    final SResultatsDeformees _d = new SResultatsDeformees();
    final SResultatsEffortsTranchants _e = new SResultatsEffortsTranchants();
    final SResultatsMomentsFlechissants _m = new SResultatsMomentsFlechissants();
    FortranReader fr=null ;
    try {
       fr=new FortranReader(new FileReader(_nomFichier));
      fmtI = new int[] { 11, 15, 15, 15, 15, 15};
      // Lecture du nombre de points dans le fichier
      try {
        while (true) {
          fr.readFields(fmtI);
          nbpoints++;
        }
      }
      catch (final EOFException _e3) {}
      fr.close();
      // R�cup�ration des points
      fr = new FortranReader(new FileReader(_nomFichier));
      try {
        _d.pointsCoteDeformee = new SPointCoteValeur[nbpoints];
        _e.pointsCoteEffort = new SPointCoteValeur[nbpoints];
        _m.pointsCoteMoment = new SPointCoteValeur[nbpoints];
        for (i = 0; i < nbpoints; i++) {
          fr.readFields(fmtI);
          _d.pointsCoteDeformee[i] = new SPointCoteValeur();
          _e.pointsCoteEffort[i] = new SPointCoteValeur();
          _m.pointsCoteMoment[i] = new SPointCoteValeur();
          //// Cote
          _d.pointsCoteDeformee[i].cote = fr.doubleField(0);
          _e.pointsCoteEffort[i].cote = fr.doubleField(0);
          _m.pointsCoteMoment[i].cote = fr.doubleField(0);
          //// D�form�e
          _d.pointsCoteDeformee[i].valeur = fr.doubleField(5);
          //// Effort tranchant
          _e.pointsCoteEffort[i].valeur = fr.doubleField(2) * coefconv_;
          //// Moment Fl�chissant
          _m.pointsCoteMoment[i].valeur = fr.doubleField(3) * coefconv_;
        }
      }
      catch (final EOFException _e3) {
        System.out.println("Erreur de lecture, des points ont �t� effac� dans le fichier.");
      }
    }
    catch (final FileNotFoundException _e2) {
      System.out.println("le fichier r�sultats de pr�dimensionnement Xplan est introuvable.");
    }
    catch (final IOException _e1) {
      System.out
          .println("erreur(s) rencontr�es lors de la lecture du fichier r�sultats de pr�dimensionnement Xplan.");
    }
    FortranLib.close(fr);
    return new Object[] { _d, _e, _m};
  }
  /**
   * nom du fichier r�sultats contenant les donn�es import�es.
   */
  private String nomFichier_;
  /**
   * structure de donn�es contenant les r�sultats import�s.
   */
  private SResultatsOscar resultatsOscar_;

  /**
   * constructeur vide.
   */
  public DResultatsOscar() {
    super();
  }

  /**
   * renvoie une copie de l'objet. attention la structure de donn�es est vide, lors de l'appel de la
   * fonction <code>resultatsOscar()</code> sur ce nouvel objet, une relecture compl�te du fichier
   * sera efectu�e.
   */
  public Object clone(){
    final DResultatsOscar _r = new DResultatsOscar();
    _r.setFichier(nomFichier_);
    return _r;
  }

  /**
   * renvoie une structure de donn�es contenant les informations du fichier r�sultats.
   * <code>nomFichier_</code>
   */
  public SResultatsOscar resultatsOscar(){
    final SResultatsOscar _r = resultatsOscar_;
    return _r;
  }

  /**
   * stocke la structure de donn�es r�sultats <code>_resultatsOscar</code>.
   */
  public void resultatsOscar(final SResultatsOscar _resultatsOscar){
    resultatsOscar_ = _resultatsOscar;
  }

  /**
   * sauvegarde le nom du fichier contenant les donn�es. si <code>_nomFichier</code> est diff�rent
   * de <code>nomFichier_</code> alors la structure de donn�es <code>resultatsOscar_</code> est
   * r�initialis�e (vid�e).
   * @param _nomFichier
   */
  public void setFichier(final String _nomFichier){
    try {
      if (!_nomFichier.equals(nomFichier_)) {
        nomFichier_ = _nomFichier;
        resultatsOscar_ = null;
      }
    }
    catch (final NullPointerException _e1) {
      System.out.println("Impossible de charger un fichier sans nom");
    }
  }

  /**
   * renvoie une r�pr�sentation cha�ne de caract�res de l'objet.
   */
  public String toString(){
    String s = "Object DResultatsOscar | ";
    s += "nomFichier_:" + nomFichier_;
    s += ", ";
    try {
      s += "resultatsOscar_:" + resultatsOscar_.toString();
    }
    catch (final NullPointerException _e1) {}
    return s;
  }
}