/**
 * @creation 2000-10-07
 * @modification $Date: 2006-09-19 14:44:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.oscar;

import java.util.Date;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Une classe serveur pour Oscar.
 * @version $Revision: 1.12 $ $Date: 2006-09-19 14:44:23 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public final class ServeurOscar {

  /**
   * @param _args si non null le nom sera egale a args[0]
   */
  public static void main(final String[] _args){
    final String nom = (_args.length > 0 ? _args[0] : CDodico.generateName("::oscar::ICalculOscar"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculOscar.class));
    System.out.println("Oscar server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }

  private ServeurOscar() {

  }
}