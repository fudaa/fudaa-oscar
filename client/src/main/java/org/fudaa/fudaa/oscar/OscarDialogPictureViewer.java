/**
 * @file         OscarDialogPictureViewer.java
 * @creation     2002-12-03
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;

import org.fudaa.ctulu.gif.AcmeGifEncoder;
import org.fudaa.ebli.graphe.BGraphe;


/**
 * Boite de dialogue affichant un graphique.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
class OscarDialogPictureViewer extends BuDialog {
  JComponent graphic_;

  /**
   *
   */
  public OscarDialogPictureViewer(final BuCommonInterface _app, final String _titre, final JComponent _bg) {
    this(_app, _titre, _bg, new Dimension(700, 400), new Point(100, 100));
    graphic_ = _bg;
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(final WindowEvent _e) {
        fermer();
      }
    });
  }

  /**
   *
   */
  public OscarDialogPictureViewer(final BuCommonInterface _app, final String _titre, final JComponent _bg,
      final Dimension _dim, final Point _pos) {
    super(_app, _app.getInformationsSoftware(), _titre);
    final JPanel _buttons = new JPanel();
    _buttons.setLayout(new FlowLayout());
    _buttons.setBorder(new EmptyBorder(5, 5, 5, 5));
    final JButton _fermer = OscarLib.Button("Fermer", "ANNULER", "FERMER");
    final JButton _exporter = OscarLib.Button("Exporter", "ENREGISTRER", "EXPORTER");
    _fermer.addActionListener(this);
    _exporter.addActionListener(this);
    _buttons.add(_exporter);
    _buttons.add(_fermer);
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(_bg, BorderLayout.CENTER);
    getContentPane().add(_buttons, BorderLayout.SOUTH);
    setSize(_dim.width, _dim.height);
    setLocation(_pos.x, _pos.y);
  }

  /**
   *
   */
  public JComponent getComponent() {
    return new JPanel();
  }

  /**
   *
   */
  public void actionPerformed(final ActionEvent _evt) {
    final String _action = _evt.getActionCommand();
    if (_action.equals("FERMER")) {
      fermer();
    } else if (_action.equals("EXPORTER")) {
      exporter();
    }
  }

  /**
   *
   */
  protected void fermer() {
    setVisible(false);
  }

  /**
   *
   */
  protected void exporter() {
    if (graphic_ instanceof BGraphe) {
      final String _f = OscarLib.getFileChoosenByUser(graphic_, "Exporter l'image", "Exporter", OscarLib.USER_HOME
          + "image.gif");
      if (_f == null) {
        return;
      }
      if ((new File(_f)).exists()) {
        final int _i = OscarLib.DialogConfirmation((BuCommonInterface) null,
            OscarImplementation.informationsSoftware(), "Le fichier cible existe d�j�. Voulez-vous l'�craser ?");
        if (_i != JOptionPane.YES_OPTION) {
          return;
        }
      }
      try {
        final AcmeGifEncoder _age = new AcmeGifEncoder(((BGraphe) graphic_).getImageCache(), new FileOutputStream(_f));
        _age.encode();
      } catch (final Exception _e1) {
        WSpy.Error("Erreur d'entr�e/sortie. Impossible d'�crire le fichier image sur le disque dur");
      }
    }
  }
}
