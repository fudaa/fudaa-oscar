/*
 * @file         OscarSurchargesParametresSurchargeLineique.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;

/**
 * Description du sous-onglet 'Surcharge Lin�ique' de l'onglet des parametres de Surcharges.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarSurchargesParametresSurchargeLineique extends OscarAbstractOnglet {
  /**
   *
   */
  public OscarSurchargesParametresSurchargeLineique(final OscarSurchargesParametres _surcharges, final String _helpfile) {
    super(_surcharges.getApplication(), _surcharges, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    return _p;
  }

  /**
   *
   */
  public Double getValeurSurcharge() {
    return null;
  }

  /**
   *
   */
  public synchronized void setValeurSurcharge(final double _p) {}

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _e) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT008);
    _mv.addMessages(validation().getMessages());
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    // ...
    return _vm;
  }
}
