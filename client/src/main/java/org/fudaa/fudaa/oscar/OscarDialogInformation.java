/*
 * @file         OscarDialogInformation.java
 * @creation     2003-01-31
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;


/**
 * Boite de dialogue standard d'information avec titre,texte, bouton aide et bouton continuer.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public final class OscarDialogInformation extends BuDialog {
  BuCommonInterface app_;
  String helpfile_;

  public OscarDialogInformation(final BuCommonInterface _a, final BuInformationsSoftware _is, final String _t,
      final String _h) {
    super(_a, _is, BuResource.BU.getString("Information"), _t);
    helpfile_ = _h;
    app_ = _a;
    final BuPanel pnb = new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    // new FlowLayout(FlowLayout.RIGHT));
    final BuButton btAide_ = new BuButton(BuResource.BU.loadToolCommandIcon("AIDE"), BuResource.BU.getString("Aide"));
    btAide_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        aide();
      }
    });
    pnb.add(btAide_);
    final BuButton btContinuer_ = new BuButton(BuResource.BU.loadToolCommandIcon("CONTINUER"), BuResource.BU
        .getString("Continuer"));
    btContinuer_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        setResponse(JOptionPane.YES_OPTION);
        setVisible(false);
      }
    });
    getRootPane().setDefaultButton(btContinuer_);
    pnb.add(btContinuer_);
    getContentPane().add(pnb, BuBorderLayout.SOUTH);
  }

  protected void setResponse(final int _i) {
    super.reponse_ = _i;
  }

  public void aide() {
    // setVisible(false);
    app_.getImplementation().displayURL(helpfile_);
  }

  public void actionPerformed(final ActionEvent _e) {}

  public JComponent getComponent() {
    return null;
  }
}
