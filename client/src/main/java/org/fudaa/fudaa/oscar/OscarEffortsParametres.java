/*
 * @file         OscarEffortsParametres.java
 * @creation     2000-10-15
 * @modification $Date: 2007-01-19 11:13:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;

import org.fudaa.dodico.corba.oscar.SEffort;



/**
 * Description de l'onglet des parametres du efforts.
 * 
 * @version $Revision: 1.7 $ $Date: 2007-01-19 11:13:21 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public class OscarEffortsParametres extends OscarAbstractOnglet {
  /**
   *
   */
  OscarEffortsParametresContraintesVerticales cv_;
  /**
   *
   */
  OscarEffortsParametresContraintesHorizontales ch_;
  /**
   *
   */
  OscarEffortsParametresEffortsEnTeteDeRideau etr_;
  /**
   *
   */
  OscarEffortsParametresFichiersDeDiagrammes fd_;
  /**
   * Set d'onglet de l'onglet Efforts
   */
  JTabbedPane tpMain;
  /**
   *
   */
  JPanel pn1;

  /**
   *
   */
  public OscarEffortsParametres(final OscarFilleParametres _fp, final String _helpfile) {
    super(_fp.getApplication(), _fp, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    // Bloc Info-Aide
    tpMain = new JTabbedPane();
    pn1 = OscarLib.InfoAidePanel(OscarMsg.LAB005, this);
    // Set d'onglets 'Type d'Efforts'
    cv_ = new OscarEffortsParametresContraintesVerticales(this, OscarMsg.URL027);
    ch_ = new OscarEffortsParametresContraintesHorizontales(this, OscarMsg.URL026);
    etr_ = new OscarEffortsParametresEffortsEnTeteDeRideau(this, OscarMsg.URL025);
    fd_ = new OscarEffortsParametresFichiersDeDiagrammes(this, OscarMsg.URL024);
    final String msgTT2Lignes; //un string html qui permet d'avoir un ToolTip sur 2 lignes
    msgTT2Lignes ="<html>"+OscarMsg.LAB054+"<br>"+OscarMsg.LAB054_2+"</br>"+"</html>";
    tpMain.addTab(OscarMsg.TABLAB020, null, etr_, OscarMsg.LAB053);
    tpMain.addTab(OscarMsg.TABLAB018, null, cv_, OscarMsg.LAB051);
    tpMain.addTab(OscarMsg.TABLAB019, null, ch_, OscarMsg.LAB052);
    tpMain.addTab(OscarMsg.TABLAB021, null, fd_, msgTT2Lignes);
    //tpMain.setEnabledAt(3,false);
    tpMain.setTabPlacement(SwingConstants.LEFT);
    setCurrentTab(0);
    tpMain.addChangeListener(this);
    _p.add(pn1);
    _p.add(tpMain);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {}

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT009);
    _mv.addMessages(validation().getMessages());
    ((OscarAbstractOnglet) tpMain.getComponentAt(getCurrentTab())).stateChanged(_evt);
    setCurrentTab(tpMain.getSelectedIndex());
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans l'onglet efforts
   */
  public SEffort getParametresEfforts() {
    final SEffort _p = (SEffort) OscarLib.createIDLObject(SEffort.class);
    _p.contraintesVerticales = cv_.getContraintes();
    _p.contraintesHorizontales = ch_.getContraintes();
    _p.fichiersDeDiagramme = fd_.getFichiers();
    _p.effortEnTeteDeRideau = etr_.getEffort();
    _p.nombreContraintesVerticales = _p.contraintesVerticales.length;
    _p.nombreContraintesHorizontales = _p.contraintesHorizontales.length;
    return _p;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans les champs de l'onglet efforts
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) � importer
   */
  public synchronized void setParametresEfforts(SEffort _p) {
    if (_p == null) {
      _p = (SEffort) OscarLib.createIDLObject(SEffort.class);
    }
    cv_.setContraintes(_p.contraintesVerticales);
    ch_.setContraintes(_p.contraintesHorizontales);
    fd_.setFichiers(_p.fichiersDeDiagramme);
    etr_.setEffort(_p.effortEnTeteDeRideau);
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    _vm.add(cv_.validation());
    _vm.add(ch_.validation());
    _vm.add(fd_.validation());
    _vm.add(etr_.validation());
    return _vm;
  }
}
