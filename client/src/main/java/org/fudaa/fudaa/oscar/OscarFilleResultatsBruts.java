/*
 * @file         OscarFilleResultatsBruts.java
 * @creation     1998-10-05
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.JScrollPane;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;

import org.fudaa.dodico.corba.oscar.SResultatsDeformees;
import org.fudaa.dodico.corba.oscar.SResultatsEffortsTranchants;
import org.fudaa.dodico.corba.oscar.SResultatsMomentsFlechissants;
import org.fudaa.dodico.corba.oscar.SResultatsOscar;
import org.fudaa.dodico.corba.oscar.SResultatsPredimensionnement;
import org.fudaa.dodico.corba.oscar.SResultatsPressions;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;
import org.fudaa.fudaa.commun.projet.FudaaProjet;


/**
 * affiche une fenetre contenant les r�sultats bruts
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarFilleResultatsBruts extends OscarInternalFrameAdapter {
  /**
   * bouton d'action de sauvegarde de l'int�gralit� des r�sultats bruts
   */
  protected BuButton bt_sauvegardertout_;
  /**
   * bouton d'action d'impression de l'int�gralit� des r�sultats bruts
   */
  protected BuButton bt_imprimertout_;
  /**
   * bouton d'action de fermeture de la fen�tre
   */
  protected BuButton bt_fermer_;
  /**
   * onglet 'Pressions'
   */
  protected OscarDiagrammePressionsResultatsBruts pressions_;
  /**
   * onglet 'Pr�dimensionnement'
   */
  protected OscarPredimensionnementResultatsBruts predimensionnement_;
  /**
   * onglet 'D�form�e'
   */
  protected OscarDeformeeResultatsBruts deformee_;
  /**
   * onglet 'Autres Efforts'
   */
  protected OscarEffortsTranchantsResultatsBruts efforts_;
  /**
   * onglet 'Moments'
   */
  protected OscarMomentsFlechissantsResultatsBruts moments_;
  /**
   * classeur d'onglets principaux de la fen�tre
   */
  protected BuTabbedPane tpm_;
  /**
   * Bloc 'Info'
   */
  protected BuPanel b1_;
  /**
   * Bloc 'Boutons'
   */
  protected BuPanel b2_;
  protected int PRESSIONS_ONGLET;
  protected int PREDIMENSIONNEMENT_ONGLET;
  protected int DEFORMEE_ONGLET;
  protected int EFFORTS_ONGLET;
  protected int MOMENTS_ONGLET;

  /**
   * constructeur de la fenetre. construit une fen�tre d'affichage des r�sultats avec plusieurs onglets et rattach�e �
   * un projet ouvert.
   * 
   * @param _appli application � laquelle cette fen�tre sera rattach�e
   * @param _project projet en cours auquel cette fen�tre sera rattach�e pour la sauvegarde des donn�es sur disque
   */
  public OscarFilleResultatsBruts(final BuCommonInterface _appli, final FudaaProjet _project) {
    super(OscarMsg.TITLELAB013, OscarMsg.ICO003, OscarMsg.URL029, _appli, _project);
    /**
     * Initilisation du set d'onglet (ajout de tous les onglets du set)
     */
    pressions_ = new OscarDiagrammePressionsResultatsBruts(this, OscarMsg.URL012);
    predimensionnement_ = new OscarPredimensionnementResultatsBruts(this, OscarMsg.URL014);
    deformee_ = new OscarDeformeeResultatsBruts(this, OscarMsg.URL011);
    efforts_ = new OscarEffortsTranchantsResultatsBruts(this, OscarMsg.URL015);
    moments_ = new OscarMomentsFlechissantsResultatsBruts(this, OscarMsg.URL016);
    tpm_ = new BuTabbedPane();
    /** num�ro de l'onglet = tr�s important � respecter ! * */
    PRESSIONS_ONGLET = 0;
    PREDIMENSIONNEMENT_ONGLET = 1;
    DEFORMEE_ONGLET = 2;
    EFFORTS_ONGLET = 3;
    MOMENTS_ONGLET = 4;
    tpm_.addTab(OscarMsg.TABLAB003, null, pressions_, OscarMsg.TIPTEXT001);
    tpm_.addTab(OscarMsg.TABLAB004, null, predimensionnement_, OscarMsg.TIPTEXT002);
    tpm_.addTab(OscarMsg.TABLAB005, null, deformee_, OscarMsg.TIPTEXT003);
    tpm_.addTab(OscarMsg.TABLAB006, null, efforts_, OscarMsg.TIPTEXT004);
    tpm_.addTab(OscarMsg.TABLAB007, null, moments_, OscarMsg.TIPTEXT005);
    tpm_.addChangeListener(this);
    setCurrentTab(0);
    b1_ = OscarLib.TitledPanel(OscarMsg.TITLELAB_INFO);
    final BuLabelMultiLine _lb_info = OscarLib.LabelMultiLine(OscarMsg.LAB017);
    b1_.add(_lb_info);
    // BuLabelMultiLine _lb_info2 = OscarLib.LabelMultiLine("> R�sultats pr�sent�s pour un rideau non ancr�");
    // b1_.add(_lb_info2);
    b2_ = OscarLib.FlowPanel();
    bt_sauvegardertout_ = OscarLib.Button("Exporter", "ENREGISTRER", "SAUVEGARDER");
    bt_fermer_ = OscarLib.Button("Fermer", "ANNULER", "FERMER");
    final JLabel _lb = OscarLib.Label("<- l'int�gralit� des r�sultats bruts ->");
    bt_imprimertout_ = OscarLib.Button("Imprimer", "IMPRIMER", "IMPRIMER");
    bt_sauvegardertout_.addActionListener(this);
    bt_imprimertout_.addActionListener(this);
    bt_fermer_.addActionListener(this);
    b2_.add(bt_imprimertout_);
    b2_.add(_lb);
    b2_.add(bt_sauvegardertout_);
    b2_.add(bt_fermer_);
    final JPanel _p = OscarLib.EmptyPanel();
    _p.add(b1_);
    _p.add(tpm_);
    _p.add(b2_);
    final JScrollPane _sp1 = new JScrollPane(_p);
    getContent().add(_sp1);
    updatePanels();
    pack();
  }

  /**
   * execution de l'action �cout�e survenue.
   * 
   * @param _e �v�nement ayant engendr� l'action
   */
  protected void action(final String _action) {
    if (_action.equals("IMPRIMER")) {
      imprimer();
    }
    if (_action.equals("SAUVEGARDER")) {
      sauvegarder();
    }
    if (_action.equals("FERMER")) {
      fermer();
    }
  }

  /**
   *
   */
  public OscarAbstractOnglet getOnglet(final int _onglet) {
    OscarAbstractOnglet _o = null;
    switch (_onglet) {
    case OscarLib.ONGLET_PREDIMENSIONNEMENT: {
      _o = predimensionnement_;
      break;
    }
    case OscarLib.ONGLET_DEFORMEE: {
      _o = deformee_;
      break;
    }
    case OscarLib.ONGLET_PRESSION: {
      _o = pressions_;
      break;
    }
    case OscarLib.ONGLET_MOMENT: {
      _o = moments_;
      break;
    }
    case OscarLib.ONGLET_EFFORT: {
      _o = efforts_;
      break;
    }
    }
    return _o;
  }

  /**
   * ferme la fen�tre.
   */
  protected void fermer() {
    try {
      setVisible(false);
      setClosed(true);
    } catch (final Throwable _e) {}
  }

  /**
   * imprime la fen�tre.
   */
  protected void imprimer() {
    getImplementation().actionPerformed(new ActionEvent(this, 15, "IMPRIMER"));
  }

  /**
   * sauvegarde tous les r�sultats bruts dans un fichier HTML.
   */
  protected void sauvegarder() {
    final String _f = OscarLib.getFileChoosenByUser(this, "Exporter les r�sultats bruts", "Enregistrer le rapport HTML",
        OscarLib.USER_HOME + "resultatsbruts.html");
    String _txt = "";
    String _imgfile = "";
    String _htmlimgfile = "";
    if (_f == null) {
      return;
    }
    try {
      (new File(_f + "files" + OscarLib.FILE_SEPARATOR)).mkdirs();
    } catch (final Exception _e1) {}
    _txt += "<html>\n<head>\n<title>R&eacute;sultats bruts</title>\n</head>\n";
    _txt += "<body>\n";
    _txt += "<h1>R&eacute;sultats bruts</h1>\n";
    _txt += "<ol>\n";
    if (ongletIsEnabled(PREDIMENSIONNEMENT_ONGLET)) {
      _txt += "<li><a href=\"#predimensionnement\">Pr&eacute;dimensionnement du rideau</a>\n";
    }
    if (ongletIsEnabled(PRESSIONS_ONGLET)) {
      _txt += "<li><a href=\"#pressions\">Diagramme des pressions</a>\n";
    }
    if (ongletIsEnabled(EFFORTS_ONGLET)) {
      _txt += "<li><a href=\"#efforts\">Efforts tranchants</a>\n";
    }
    if (ongletIsEnabled(MOMENTS_ONGLET)) {
      _txt += "<li><a href=\"#moments\">Moments fl&eacute;chissants</a>\n";
    }
    if (ongletIsEnabled(DEFORMEE_ONGLET)) {
      _txt += "<li><a href=\"#deformee\">D&eacute;form&eacute;e</a>\n";
    }
    _txt += "</ol>\n";
    if (ongletIsEnabled(PREDIMENSIONNEMENT_ONGLET)) {
      final SResultatsPredimensionnement _predim = predimensionnement_.getResultatsPredimensionnement();
      _txt += "<h2><a name=predimensionnement>Pr&eacute;dimensionnement du rideau</a></h2>\n";
      _txt += "<table border=1 cellpadding=2 cellspacing=0>\n";
      _txt += "<tr><td><b>Cote de pression nulle</b></td><td>" + _predim.cotePressionNull + "</td><td>m</td></tr>\n";
      _txt += "<tr><td><b>Point de contre-but&eacute;e</b></td><td>" + _predim.pointButeeSimple + "</td><td>m</td></tr>\n";
      _txt += "<tr><td><b>Pied (20% de contre-but&eacute;e)</b></td><td>" + _predim.pied20Pourcent + "</td><td>m</td></tr>\n";
      _txt += "<tr><td><b>Pied (contre-but&eacute;e calcul&eacute;e)</b></td><td>" + _predim.piedContreButee
          + "</td><td>m</td></tr>\n";
      _txt += "<tr><td><b>Valeur du moment maximum</b></td><td>" + _predim.momentMaximum.valeur
          + "</td><td>kN.m/ml</td></tr>\n";
      _txt += "<tr><td><b>Cote du moment maximum</b></td><td>" + _predim.momentMaximum.cote + "</td><td>m</td></tr>\n";
      _txt += "<tr><td><b>Valeur du moment minimum</b></td><td>" + _predim.momentMinimum.valeur
          + "</td><td>kN.m/ml</td></tr>\n";
      _txt += "<tr><td><b>Cote du moment minimum</b></td><td>" + _predim.momentMinimum.cote + "</td><td>m</td></tr>\n";
      if (_predim.encastrement != VALEUR_NULLE.value) {
        _txt += "<tr><td><b>Encastrement demand&eacute;</b></td><td>" + _predim.encastrement + "</td><td>%</td></tr>\n";
        _txt += "<tr><td><b>R&eacute;action d'ancrage</b></td><td>" + _predim.reactionAncrage + "</td><td>kN</td></tr>\n";
        _txt += "<tr><td><b>Niveau d'ancrage</b></td><td>" + _predim.niveauAncrage + "</td><td>m</td></tr>\n";
      }
      _txt += "</table>\n";
    }
    if (ongletIsEnabled(PRESSIONS_ONGLET)) {
      final SResultatsPressions _pressions = pressions_.getResultatsPressions();
      _txt += "<h2><a name=pressions>Diagramme des pressions</a></h2>\n";
      _imgfile = _f + "files" + OscarLib.FILE_SEPARATOR + "img-pression.gif";
      _htmlimgfile = new File(_f).getName() + "files" + OscarLib.FILE_SEPARATOR + "img-pression.gif";
      OscarLib.exportGraphScriptToGif(pressions_.getDiagrammePressionsGrapheScript(), _imgfile);
      _txt += "<center><a href=\"" + _htmlimgfile + "\" target=\"_blank\"><img src=\"" + _htmlimgfile
          + "\" border=\"1\"></a></center>" + OscarLib.LINE_SEPARATOR;
      _txt += "<table border=1 cellpadding=2 cellspacing=0>\n";
      _txt += "<tr><td><b>Cote</b> <i>(m)</i></td><td><b>Valeur de la pression</b> <i>(kN.m/ml)</i></td></tr>\n";
      for (int i = 0; i < _pressions.pointsCotePression.length; i++) {
        _txt += "<tr><td>" + _pressions.pointsCotePression[i].cote + "</td><td>"
            + _pressions.pointsCotePression[i].valeur + "</td></tr>\n";
      }
      _txt += "</table>\n";
    }
    if (ongletIsEnabled(EFFORTS_ONGLET)) {
      final SResultatsEffortsTranchants _efforts = efforts_.getResultatsEffortsTranchants();
      _txt += "<h2><a name=efforts>Efforts tranchants</a></h2>\n";
      _imgfile = _f + "files" + OscarLib.FILE_SEPARATOR + "img-effort.gif";
      _htmlimgfile = new File(_f).getName() + "files" + OscarLib.FILE_SEPARATOR + "img-effort.gif";
      OscarLib.exportGraphScriptToGif(efforts_.getEffortsTranchantsGrapheScript(), _imgfile);
      _txt += "<center><a href=\"" + _htmlimgfile + "\" target=\"_blank\"><img src=\"" + _htmlimgfile
          + "\" border=\"1\"></a></center>" + OscarLib.LINE_SEPARATOR;
      _txt += "<table border=1 cellpadding=2 cellspacing=0>\n";
      _txt += "<tr><td><b>Cote</b> <i>(m)</i></td><td><b>Valeur de l'effort</b> <i>kN/ml</i></td></tr>\n";
      for (int i = 0; i < _efforts.pointsCoteEffort.length; i++) {
        _txt += "<tr><td>" + _efforts.pointsCoteEffort[i].cote + "</td><td>" + _efforts.pointsCoteEffort[i].valeur
            + "</td></tr>\n";
      }
      _txt += "</table>\n";
    }
    if (ongletIsEnabled(MOMENTS_ONGLET)) {
      final SResultatsMomentsFlechissants _moments = moments_.getResultatsMomentsFlechissants();
      _txt += "<h2><a name=moments>Moments fl&eacute;chissants</a></h2>\n";
      _imgfile = _f + "files" + OscarLib.FILE_SEPARATOR + "img-moment.gif";
      _htmlimgfile = new File(_f).getName() + "files" + OscarLib.FILE_SEPARATOR + "img-moment.gif";
      OscarLib.exportGraphScriptToGif(moments_.getMomentsFlechissantsGrapheScript(), _imgfile);
      _txt += "<center><a href=\"" + _htmlimgfile + "\" target=\"_blank\"><img src=\"" + _htmlimgfile
          + "\" border=\"1\"></a></center>" + OscarLib.LINE_SEPARATOR;
      _txt += "<table border=1 cellpadding=2 cellspacing=0>\n";
      _txt += "<tr><td><b>Cote</b> <i>(m)</i></td><td><b>Valeur du moment</b> <i>(kN.m/ml)</i></td></tr>\n";
      for (int i = 0; i < _moments.pointsCoteMoment.length; i++) {
        _txt += "<tr><td>" + _moments.pointsCoteMoment[i].cote + "</td><td>" + _moments.pointsCoteMoment[i].valeur
            + "</td></tr>\n";
      }
      _txt += "</table>\n";
    }
    if (ongletIsEnabled(DEFORMEE_ONGLET)) {
      final SResultatsDeformees _deformees = deformee_.getResultatsDeformees();
      _txt += "<h2><a name=deformee>D&eacute;form&eacute;e</a></h2>\n";
      _imgfile = _f + "files" + OscarLib.FILE_SEPARATOR + "img-deformee.gif";
      _htmlimgfile = new File(_f).getName() + "files" + OscarLib.FILE_SEPARATOR + "img-deformee.gif";
      OscarLib.exportGraphScriptToGif(deformee_.getDeformeesGrapheScript(), _imgfile);
      _txt += "<center><a href=\"" + _htmlimgfile + "\" target=\"_blank\"><img src=\"" + _htmlimgfile
          + "\" border=\"1\"></a></center>" + OscarLib.LINE_SEPARATOR;
      _txt += "<table border=1 cellpadding=2 cellspacing=0>\n";
      _txt += "<tr><td><b>Cote</b> <i>(m)</i></td><td><b>D&eacute;placement</b> <i>(m)</i></td></tr>\n";
      for (int i = 0; i < _deformees.pointsCoteDeformee.length; i++) {
        _txt += "<tr><td>" + _deformees.pointsCoteDeformee[i].cote + "</td><td>"
            + _deformees.pointsCoteDeformee[i].valeur + "</td></tr>\n";
      }
      _txt += "</table>\n";
    }
    _txt += "<br><br><i>g&eacute;n&eacute;r&eacute;e par le logiciel <b>" + OscarImplementation.informationsSoftware().name + "</b>.</i>\n";
    _txt += "</body>\n</html>\n";
    try {
      final FileWriter _fw = new FileWriter(_f);
      _fw.write(_txt, 0, _txt.length());
      _fw.flush();
      _fw.close();
      final File _file = new File(_f);
      getApplication().getImplementation().displayURL(_file.getAbsolutePath());
    } catch (final IOException _e1) {
      WSpy.Error("Impossible d'�crire le fichier sur le disque.");
    }
  }

  /**
   * renvoi une structure contenant les r�sultats actuellement affich�s dans les onglets de la fen�tre.
   * 
   * @return une structure de donn�es contenant toutes les infos de la fen�tre qui doivent �tre sauvegard�es dans le
   *         fichier Projet.
   */
  public SResultatsOscar getResultats() {
    final SResultatsOscar _r = new SResultatsOscar();
    try {
      _r.pressions = pressions_.getResultatsPressions();
    } catch (final Throwable _e) {}
    try {
      _r.predimensionnement = predimensionnement_.getResultatsPredimensionnement();
    } catch (final Throwable _e) {}
    try {
      _r.deformees = deformee_.getResultatsDeformees();
    } catch (final Throwable _e) {}
    try {
      _r.efforts = efforts_.getResultatsEffortsTranchants();
    } catch (final Throwable _e) {}
    try {
      _r.moments = moments_.getResultatsMomentsFlechissants();
    } catch (final Throwable _e) {}
    return _r;
  }

  /**
   *
   */
  protected boolean enableOnglet(final int _onglet) {
    try {
      tpm_.setEnabledAt(_onglet, true);
      return true;
    } catch (final Throwable _e) {
      return false;
    }
  }

  /**
   *
   */
  protected boolean disableOnglet(final int _onglet) {
    try {
      tpm_.setEnabledAt(_onglet, false);
      return true;
    } catch (final Throwable _e) {
      return false;
    }
  }

  /**
   *
   */
  public boolean ongletIsEnabled(final int _onglet) {
    try {
      return tpm_.isEnabledAt(_onglet);
    } catch (final Throwable _e) {
      return false;
    }
  }

  /**
   * mise � jour du contenu des onglets de la fen�tre.
   * 
   * @param _r r�sultats � charger dans les diff�rents onglets de la fen�tres. si null, alors on charge des param�tres
   *          vierge.
   */
  public synchronized void setResultats(SResultatsOscar _r) {
    if (_r == null) {
      _r = new SResultatsOscar();
    }
    if (_r.pressions != null) {
      pressions_.setResultatsPressions(_r.pressions);
      enableOnglet(PRESSIONS_ONGLET);
    } else {
      disableOnglet(PRESSIONS_ONGLET);
    }
    if (_r.predimensionnement != null) {
      predimensionnement_.setResultatsPredimensionnement(_r.predimensionnement);
      enableOnglet(PREDIMENSIONNEMENT_ONGLET);
    } else {
      disableOnglet(PREDIMENSIONNEMENT_ONGLET);
    }
    if (_r.deformees != null) {
      deformee_.setResultatsDeformees(_r.deformees);
      enableOnglet(DEFORMEE_ONGLET);
    } else {
      disableOnglet(DEFORMEE_ONGLET);
    }
    if (_r.efforts != null) {
      efforts_.setResultatsEffortsTranchants(_r.efforts);
      enableOnglet(EFFORTS_ONGLET);
    } else {
      disableOnglet(EFFORTS_ONGLET);
    }
    if (_r.moments != null) {
      moments_.setResultatsMomentsFlechissants(_r.moments);
      enableOnglet(MOMENTS_ONGLET);
    } else {
      disableOnglet(MOMENTS_ONGLET);
    }
  }

  /**
   * mise � jour du contenu des onglets de la fen�tre. cette fonction est appell�e automatiquement lorsqu'une
   * modification de l'�tat ou du contenu du projet est notifi�e.
   */
  protected void updatePanels() {
    setResultats((SResultatsOscar) getProjet().getResult(OscarResource.RESULTATS));
  }
}
