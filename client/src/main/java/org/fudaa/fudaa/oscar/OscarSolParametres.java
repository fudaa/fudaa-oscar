/*
 * @file         OscarSolParametres.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.beans.PropertyChangeEvent;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.oscar.SCoucheSol;
import org.fudaa.dodico.corba.oscar.SParametresGeneraux;
import org.fudaa.dodico.corba.oscar.SSol;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;


/**
 * Description de l'onglet des parametres du sol.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarSolParametres extends OscarAbstractOnglet {
  /** sous-onglet Sol en pouss�e */
  private OscarSolParametresSolEnPoussee poussee_;
  /** sous-onglet Sol en but�e */
  private OscarSolParametresSolEnButee butee_;
  /** case d'option de calcul en contraintes effectives */
  private BuRadioButton rb_cont_eff_;
  /** case d'option de choix de calcul en contraintes totales */
  private BuRadioButton rb_cont_tot_;
  /** classeur des sous-onglets (pouss�e et but�e) de l'onglet sol */
  private JTabbedPane tpm_;
  /** bloc 1 : informations et aide */
  private BuPanel b1_;
  /** bloc 2 : type de calcul */
  private BuPanel b2_;
  /** bloc 3 : informations */
  private BuPanel b3_;
  /** type de calcul actuellement choisi */
  private int typecalc_;

  /**
   * construit l'onglet de saisie des donn�es de sol (pouss�e et but�e)
   * 
   * @param _fp la fen�tre parente de l'onglet sol
   */
  public OscarSolParametres(final OscarFilleParametres _fp, final String _helpfile) {
    super(_fp.getApplication(), _fp, _helpfile);
    toggleTypeCalcul(OscarLib.SOL_CALCUL_EFFECTIF);
  }

  /**
   * initialise le conteneur swing principal du composant
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    final BuVerticalLayout _lo = OscarLib.VerticalLayout();
    _p.setLayout(_lo);
    _p.setBorder(OscarLib.EmptyBorder());
    rb_cont_eff_ = new BuRadioButton(OscarMsg.RBLAB003);
    rb_cont_tot_ = new BuRadioButton(OscarMsg.RBLAB004);
    tpm_ = new JTabbedPane();
    b1_ = OscarLib.InfoAideAndButtonPanel(OscarMsg.LAB002, "Dessin", "DESSIN", this, this, "COUCHES_SOL_DISPONIBLE");
    b2_ = OscarLib.TitledPanel(OscarMsg.TITLELAB010);
    b3_ = OscarLib.InfoPanel(OscarMsg.LAB027);
    rb_cont_eff_.setActionCommand("SOL_TYPE_CALCUL_CONTRAINTES_EFFECTIVES");
    rb_cont_tot_.setActionCommand("SOL_TYPE_CALCUL_CONTRAINTES_TOTALES");
    rb_cont_eff_.addActionListener(this);
    rb_cont_tot_.addActionListener(this);
    final ButtonGroup _gb_typ_calc = new ButtonGroup();
    _gb_typ_calc.add(rb_cont_eff_);
    _gb_typ_calc.add(rb_cont_tot_);
    b2_.add(rb_cont_eff_);
    b2_.add(rb_cont_tot_);
    // classeur d'onglets pouss�e / but�e
    poussee_ = new OscarSolParametresSolEnPoussee(this, OscarMsg.URL017);
    butee_ = new OscarSolParametresSolEnButee(this, OscarMsg.URL018);
    tpm_.addTab(OscarMsg.TABLAB001, null, poussee_, OscarMsg.LAB028);
    tpm_.addTab(OscarMsg.TABLAB002, null, butee_, OscarMsg.LAB029);
    setCurrentTab(0);
    tpm_.addChangeListener(this);
    addPropertyChangeListener("SOL_TYPE_CALCUL", poussee_);
    addPropertyChangeListener("SOL_TYPE_CALCUL", butee_);
    poussee_.addPropertyChangeListener("COUCHES_POUSSEE_DISPONIBLE", this);
    butee_.addPropertyChangeListener("COUCHES_BUTEE_DISPONIBLE", this);
    poussee_.addPropertyChangeListener("COTE_TERRE_PLEIN_POUSSEE", this);
    butee_.addPropertyChangeListener("COTE_TERRE_PLEIN_BUTEE", this);
    // Ajout des composants au conteneur principal
    _p.add(b1_);
    _p.add(b2_);
    _p.add(b3_);
    _p.add(tpm_);
    return _p;
  }

  /**
   * execution de l'action
   */
  protected void action(final String _action) {
    if (_action.equals("AIDE")) {
      aide();
    } else if (_action.startsWith("SOL_TYPE_CALCUL_CONTRAINTES_")
        && (((typecalc_ == OscarLib.SOL_CALCUL_TOTAL) && (_action.endsWith("EFFECTIVES"))) || ((typecalc_ == OscarLib.SOL_CALCUL_EFFECTIF) && (_action
            .endsWith("TOTALES"))))) {
      int i;
      if ((poussee_.getNbCouches() + butee_.getNbCouches()) <= 0) {
        i = JOptionPane.YES_OPTION;
      } else {
        i = OscarLib.DialogAdvancedConfirmation(getApplication(), OscarLib.getImplementation(getApplication())
            .getInformationsSoftware(), OscarMsg.DLG010, OscarMsg.DLG008);
      }
      if (i == JOptionPane.YES_OPTION) {
        toggleTypeCalculData();
      } else if (i == JOptionPane.NO_OPTION) {
        eraseTypeCalculData();
      } else {
        toggleTypeCalcul((getTypeCalcul() == OscarLib.SOL_CALCUL_EFFECTIF) ? OscarLib.SOL_CALCUL_TOTAL
            : OscarLib.SOL_CALCUL_EFFECTIF);
      }
    } else if (_action.equals("DESSIN")) {
      dessin();
    }
  }

  /**
   * m�thode appell�e lors d'un changement de propri�t� espionn�e par ce composant.
   * 
   * @param evt �v�nement qui d�crit le changement de propri�t�
   */
  public void propertyChange(final PropertyChangeEvent evt) {
    if ((evt.getPropertyName().equals("COUCHES_POUSSEE_DISPONIBLE")
        || (evt.getPropertyName().startsWith("COTE_TERRE_PLEIN_")) || evt.getPropertyName().equals(
        "COUCHES_BUTEE_DISPONIBLE"))) {
      if ((poussee_.getNbCouches() > 0) && (butee_.getNbCouches() > 0) && (poussee_.getCoteTerrePleinPoussee() != null)
          && (butee_.getCoteTerrePleinButee() != null)) {
        firePropertyChange("COUCHES_SOL_DISPONIBLE", false, true);
      } else {
        firePropertyChange("COUCHES_SOL_DISPONIBLE", true, false);
      }
    }
  }

  /**
   *
   */
  protected void imprimer() {}

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT001);
    _mv.addMessages(validation().getMessages());
    ((OscarAbstractOnglet) tpm_.getComponentAt(getCurrentTab())).stateChanged(_evt);
    setCurrentTab(tpm_.getSelectedIndex());
  }

  /**
   * retourne un entier repr�sentant le type de calcul en cours (actuellement choisi par l'utilisateur). voir les
   * constantes SOL_CALCUL_EFFECTIF et SOL_CALCUL_TOTAL de la classe OscarLib.
   */
  public int getTypeCalcul() {
    int _r = 0;
    try {
      if (rb_cont_eff_.isSelected()) {
        _r = OscarLib.SOL_CALCUL_EFFECTIF;
      } else if (rb_cont_tot_.isSelected()) {
        _r = OscarLib.SOL_CALCUL_TOTAL;
      } else {
        throw new Exception();
      }
    } catch (final Exception _e1) {
      WSpy.Error(OscarMsg.ERR009);
    }
    return _r;
  }

  /**
   * retourne la fen�tre parente de l'onglet sol
   */
  public OscarFilleParametres getFilleParametres() {
    return (OscarFilleParametres) getParentComponent();
  }

  /**
   * change le type de calcul vers le type de calcul sp�cifi�e (aucune action engendr� simplement changement du choix
   * des cases d'options).
   * 
   * @param _t type de calcul � activer. peut �tre SOL_CALCUL_EFFECTIF ou SOL_CALCUL_TOTAL de OscarLib
   */
  public void toggleTypeCalcul(final int _t) {
    if (_t == OscarLib.SOL_CALCUL_TOTAL) {
      rb_cont_tot_.setSelected(true);
      typecalc_ = OscarLib.SOL_CALCUL_TOTAL;
    } else if (_t == OscarLib.SOL_CALCUL_EFFECTIF) {
      rb_cont_eff_.setSelected(true);
      typecalc_ = OscarLib.SOL_CALCUL_EFFECTIF;
    }
  }

  /**
   * change l'apparence du formulaire pour le conformer au type de calcul qui vient d'�tre choisi en r�utilisant les
   * donn�es d�j� saisies (quand cela est possible). la s�lection du type de calcul n'est pas modifi�e. cette fonction
   * doit �tre appell�e juste apr�s que l'utilisateur ai chang� de type de calcul (case d'option).
   */
  public void toggleTypeCalculData() {
    final int _t = getTypeCalcul();
    int _old;
    int _new;
    if (_t == OscarLib.SOL_CALCUL_TOTAL) {
      _old = OscarLib.SOL_CALCUL_EFFECTIF;
      _new = _t;
      typecalc_ = OscarLib.SOL_CALCUL_TOTAL;
      firePropertyChange("SOL_TYPE_CALCUL", _old, _new);
    } else if (_t == OscarLib.SOL_CALCUL_EFFECTIF) {
      _old = OscarLib.SOL_CALCUL_TOTAL;
      _new = _t;
      typecalc_ = OscarLib.SOL_CALCUL_EFFECTIF;
      firePropertyChange("SOL_TYPE_CALCUL", _old, _new);
    } else {
      WSpy.Error(OscarMsg.ERR010);
    }
  }

  /**
   * vide int�gralement les donn�e d�j� saisies dans le formulaire de sol et change l'apparence du formulaire pour le
   * conformer au type de calcul qui vient d'�tre choisi en ne r�utilisant donc pas les donn�es d�j� saisies
   * (irr�versible). la s�lection du type de calcul n'est pas modifi�e. cette fonction doit �tre appell�e juste apr�s
   * que l'utilisateur ai chang� de type de calcul (case d'option).
   */
  public void eraseTypeCalculData() {
    final int _t = getTypeCalcul();
    int _old;
    int _new;
    if (_t == OscarLib.SOL_CALCUL_TOTAL) {
      _old = OscarLib.SOL_CALCUL_EFFECTIF;
      _new = _t;
      setParametresSol(null);
      setParametresGeneraux(null);
      toggleTypeCalcul(_t);
      firePropertyChange("SOL_TYPE_CALCUL", _old, _new);
    } else if (_t == OscarLib.SOL_CALCUL_EFFECTIF) {
      _old = OscarLib.SOL_CALCUL_TOTAL;
      _new = _t;
      setParametresSol(null);
      setParametresGeneraux(null);
      toggleTypeCalcul(_t);
      firePropertyChange("SOL_TYPE_CALCUL", _old, _new);
    } else {
      WSpy.Error(OscarMsg.ERR010);
    }
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans l'onglet sol
   */
  public SParametresGeneraux getParametresGeneraux() {
    final SParametresGeneraux _p = (SParametresGeneraux) OscarLib.createIDLObject(SParametresGeneraux.class);
    _p.choixCalculSol = (getTypeCalcul() == OscarLib.SOL_CALCUL_EFFECTIF) ? "L" : "C";
    try {
      _p.coteTerrePleinPoussee = poussee_.getCoteTerrePleinPoussee().doubleValue();
    } catch (final NullPointerException _e1) {
      _p.coteTerrePleinPoussee = VALEUR_NULLE.value;
    }
    try {
      _p.coteTerrePleinButee = butee_.getCoteTerrePleinButee().doubleValue();
    } catch (final NullPointerException _e1) {
      _p.coteTerrePleinButee = VALEUR_NULLE.value;
    }
    _p.projet = (new File(getFilleParametres().getProjet().getFichier())).getName();
    return _p;
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans l'onglet sol
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) � importer
   */
  public synchronized void setParametresGeneraux(SParametresGeneraux _p) {
    if (_p == null) {
      _p = (SParametresGeneraux) OscarLib.createIDLObject(SParametresGeneraux.class);
    }
    if (_p.coteTerrePleinPoussee != VALEUR_NULLE.value) {
      poussee_.setCoteTerrePleinPoussee(new Double(_p.coteTerrePleinPoussee));
    }
    if (_p.coteTerrePleinButee != VALEUR_NULLE.value) {
      butee_.setCoteTerrePleinButee(new Double(_p.coteTerrePleinButee));
    }
    if ((_p.choixCalculSol != null) && _p.choixCalculSol.equals(OscarLib.IDL_SOL_CHOIXCALCUL_TOTAL)) {
      toggleTypeCalcul(OscarLib.SOL_CALCUL_TOTAL);
      toggleTypeCalculData();
    } else {
      toggleTypeCalcul(OscarLib.SOL_CALCUL_EFFECTIF);
      toggleTypeCalculData();
    }
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans l'onglet sol
   */
  public SSol getParametresSol() {
    final SSol _p = (SSol) OscarLib.createIDLObject(SSol.class);
    _p.nombreCouchesPoussee = poussee_.getNbCouches();
    _p.couchesPoussee = poussee_.getCouches();
    _p.nombreCouchesButee = butee_.getNbCouches();
    _p.couchesButee = butee_.getCouches();
    return _p;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans les diff�rents sous onglets de l'onglet
   * sol
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) � importer
   */
  public synchronized void setParametresSol(SSol _p) {
    if (_p == null) {
      _p = (SSol) OscarLib.createIDLObject(SSol.class);
    }
    poussee_.setCouches(_p.couchesPoussee);
    butee_.setCouches(_p.couchesButee);
  }

  /**
   *
   */
  public Double getCoteBasDuSol() {
    Double _o = null;
    Double _o1 = null;
    Double _o2 = null;
    final Double _v1 = butee_.getCoteTerrePleinButee();
    final Double _v2 = butee_.getEpaisseurTotale();
    final Double _v3 = poussee_.getCoteTerrePleinPoussee();
    final Double _v4 = poussee_.getEpaisseurTotale();
    if ((_v1 != null) && (_v2 != null)) {
      _o1 = new Double(_v1.doubleValue() - _v2.doubleValue());
    }
    if ((_v3 != null) && (_v4 != null)) {
      _o2 = new Double(_v3.doubleValue() - _v4.doubleValue());
    }
    if ((_o1 != null) && (_o2 != null)) {
      _o = new Double((_o1.doubleValue() < _o2.doubleValue()) ? _o1.doubleValue() : _o2.doubleValue());
    } else if (_o1 != null) {
      _o = new Double(_o1.doubleValue());
    } else if (_o2 != null) {
      _o = new Double(_o2.doubleValue());
    }
    return _o;
  }

  /**
   *
   */
  public Double getCoteBasDuSolCotePoussee() {
    Double _o = null;
    Double _o2 = null;
    final Double _v3 = poussee_.getCoteTerrePleinPoussee();
    final Double _v4 = poussee_.getEpaisseurTotale();
    if ((_v3 != null) && (_v4 != null)) {
      _o2 = new Double(_v3.doubleValue() - _v4.doubleValue());
    }
    if (_o2 != null) {
      _o = new Double(_o2.doubleValue());
    }
    return _o;
  }

  /**
   *
   */
  public Double getCoteHautDuSol() {
    Double _o = null;
    final Double _v1 = poussee_.getCoteTerrePleinPoussee();
    final Double _v2 = butee_.getCoteTerrePleinButee();
    if ((_v1 != null) && (_v2 != null)) {
      _o = new Double((_v1.doubleValue() < _v2.doubleValue()) ? _v2.doubleValue() : _v1.doubleValue());
    } else if (_v1 != null) {
      _o = new Double(_v1.doubleValue());
    } else if (_v2 != null) {
      _o = new Double(_v2.doubleValue());
    }
    return _o;
  }

  /**
   *
   */
  public Double getCoteFondDeSouille() {
    Double _o = null;
    final Double _v1 = butee_.getCoteTerrePleinButee();
    if (_v1 != null) {
      _o = new Double(_v1.doubleValue());
    }
    return _o;
  }

  /**
   *
   */
  protected void dessin() {
    changeOnglet((ChangeEvent) null);
    OscarLib.showDialogGraphViewer(getApplication(), "Repr�sentation des couches de sol", getCouchesGrapheScript());
  }

  /**
   *
   */
  public String getCouchesGrapheScript() {
    double _min;
    double _max;
    final Double _dmin = getCoteBasDuSol();
    final Double _dmax = getCoteHautDuSol();
    if (_dmin == null) {
      _min = -1.0;
    } else {
      _min = _dmin.doubleValue();
    }
    if (_dmax == null) {
      _max = 1.0;
    } else {
      _max = _dmax.doubleValue() + 1.0;
    }
    return getCouchesGrapheScript(0.0, 1.0, _min, _max);
  }

  /**
   *
   */
  public String getCouchesGrapheScript(final double _xmin, final double _xmax, final double _min, final double _max) {
    final StringBuffer _s = new StringBuffer(4096);
    _s.append("graphe\n");
    _s.append("{\n");
    _s.append("  titre \"Couches de sol\"\n");
    _s.append("  sous-titre \"C�t� but�e et c�t� pouss�e\"\n");
    _s.append("  legende oui\n");
    _s.append("  animation non\n");
    _s.append("  marges\n");
    _s.append("  {\n");
    _s.append("    gauche 80\n");
    _s.append("    droite 120\n");
    _s.append("    haut 50\n");
    _s.append("    bas 30\n");
    _s.append("  }\n");
    _s.append("  axe\n");
    _s.append("  {\n");
    _s.append("    orientation horizontal\n");
    _s.append("    graduations non\n");
    _s.append("    minimum " + _xmin + "\n");
    _s.append("    maximum " + _xmax + "\n");
    _s.append("  }\n");
    _s.append("  axe\n");
    _s.append("  {\n");
    _s.append("    titre \"cote\"\n");
    _s.append("    unite \"m\"\n");
    _s.append("    orientation vertical\n");
    _s.append("    graduations oui\n");
    _s.append("    minimum " + _min + "\n");
    _s.append("    maximum " + _max + "\n");
    _s.append("  }\n");
    _s.append(getCouchesCourbeGrapheScript(_xmin, _xmax, _min, _max));
    _s.append("}");
    return _s.toString();
  }

  /**
   *
   */
  public String getCouchesCourbeGrapheScript(final double _xmin, final double _xmax, final double _ymin,
      final double _ymax) {
    final StringBuffer _s = new StringBuffer(4096);
    double _niveau = 0D;
    double _npou;
    double _nbut;
    SCoucheSol _couche;
    int i;
    Double _dniveau;
    boolean _poussee = false;
    boolean _butee = false;
    _dniveau = getCoteHautDuSol();
    if (_dniveau != null) {
      _poussee = true;
      _niveau = _dniveau.doubleValue();
      for (i = 0; i <= poussee_.getNbCouches(); i++) { // la couche 0 n'existe pas mais sert � la cote du toit du sol
        _s.append("  courbe\n");
        _s.append("  {\n");
        _s.append("    marqueurs non\n");
        _s.append("    type aire\n");
        _s.append("    valeurs\n");
        _s.append("    {\n");
        _couche = poussee_.getCouche(i);
        if (_couche == null) {} else {
          _niveau -= _couche.epaisseur;
          _s.append("      " + ((_xmin + _xmax) / 2) + " " + _niveau + "\n");
          _s.append("      " + _xmax + " " + _niveau + "\n");
        }
        _s.append("    }\n");
        _s.append("    aspect\n");
        _s.append("    {\n");
        _s.append("      surface.couleur F7CF9E\n");
        _s.append("      contour.couleur B26507\n");
        _s.append("      contour.largeur 20\n");
        _s.append("    }\n");
        _s.append("  }\n");
        // _s.append(" contrainte\n{\n");
        // _s.append(" titre \"Couche "+(i+1)+"\"\n");
        // _s.append(" couleur 000000\n");
        // _s.append(" orientation horizontal\n");
        // _s.append(" type max\n");
        // _s.append(" valeur "+(_niveau+(_couche.epaisseur/2))+"\n");
        // _s.append(" position 0.5\n");
        // _s.append(" }\n");
      }
    }
    _npou = _niveau;
    _dniveau = getCoteFondDeSouille();
    if (_dniveau != null) {
      _butee = true;
      _niveau = _dniveau.doubleValue();
      for (i = 0; i <= butee_.getNbCouches(); i++) { // la couche 0 n'existe pas mais sert � la cote du toit du sol
        _s.append("  courbe\n");
        _s.append("  {\n");
        _s.append("    marqueurs non\n");
        _s.append("    type aire\n");
        _s.append("    valeurs\n");
        _s.append("    {\n");
        _couche = butee_.getCouche(i);
        if (_couche == null) {} else {
          _niveau -= _couche.epaisseur;
          _s.append("      " + _xmin + "    " + _niveau + "\n");
          _s.append("      " + ((_xmin + _xmax) / 2) + "    " + _niveau + "\n");
        }
        _s.append("    }\n");
        _s.append("    aspect\n");
        _s.append("    {\n");
        _s.append("      surface.couleur F7CF9E\n");
        _s.append("      contour.couleur B26507\n");
        _s.append("      contour.largeur 20\n");
        _s.append("    }\n");
        _s.append("  }\n");
        // _s.append(" contrainte\n{\n");
        // _s.append(" titre \"Couche "+(i+1)+"\"\n");
        // _s.append(" couleur 000000\n");
        // _s.append(" orientation horizontal\n");
        // _s.append(" type max\n");
        // _s.append(" valeur "+(_niveau+(_couche.epaisseur/2))+"\n");
        // _s.append(" position 0.0\n");
        // _s.append(" }\n");
      }
    }
    _nbut = _niveau;
    if (_npou != _nbut) {
      // double _nmin;
      double _nx1;
      double _nx2;
      double _nminsup;
      // _nmin= (_npou < _nbut) ? _npou : _nbut;
      _nx1 = (_npou < _nbut) ? _xmin : ((_xmin + _xmax) / 2);
      _nx2 = (_npou < _nbut) ? ((_xmin + _xmax) / 2) : _xmax;
      _nminsup = (_npou < _nbut) ? _nbut : _npou;
      _s.append("  courbe\n");
      _s.append("  {\n");
      _s.append("    type aire\n");
      _s.append("    marqueurs non\n");
      _s.append("    valeurs\n");
      _s.append("    {\n");
      _s.append("      " + (_nx1) + " " + (_nminsup) + "\n");
      _s.append("      " + (_nx2) + " " + (_nminsup) + "\n");
      _s.append("    }\n");
      _s.append("    aspect\n");
      _s.append("    {\n");
      _s.append("      surface.couleur FFFFFF\n");
      _s.append("      contour.couleur FFFFFF\n");
      _s.append("    }\n");
      _s.append("  }\n");
    }
    if (_poussee && _butee) {
      _s.append("  courbe\n");
      _s.append("  {\n");
      _s.append("    marqueurs non\n");
      _s.append("    valeurs\n");
      _s.append("    {\n");
      _s.append("      " + ((_xmin + _xmax) / 2) + "    " + getCoteBasDuSol() + "\n");
      _s.append("      " + ((_xmin + _xmax) / 2) + "    " + getCoteHautDuSol() + "\n");
      _s.append("    }\n");
      _s.append("    aspect\n");
      _s.append("    {\n");
      _s.append("      surface.couleur F7CF9E\n");
      _s.append("      contour.couleur B26507\n");
      _s.append("      contour.largeur 20\n");
      _s.append("    }\n");
      _s.append("  }\n");
    }
    return _s.toString();
  }

  /**
   *
   */
  public synchronized ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    boolean _notsamecote = false;
    if ((getTypeCalcul() != OscarLib.SOL_CALCUL_TOTAL) && (getTypeCalcul() != OscarLib.SOL_CALCUL_EFFECTIF)) {
      _vm.add(OscarMsg.VMSG003);
    }
    final Double _d1 = poussee_.getCoteTerrePleinPoussee();
    final Double _d2 = poussee_.getEpaisseurTotale();
    final Double _d3 = butee_.getCoteTerrePleinButee();
    final Double _d4 = butee_.getEpaisseurTotale();
    if (((_d1 == null) || (_d2 == null) || (_d3 == null) || (_d4 == null))
        || (Math.abs(((_d1.doubleValue() - _d2.doubleValue()) - (_d3.doubleValue() - _d4.doubleValue()))) > OscarLib.DOUBLE_EPSILON)) {
      _notsamecote = true;
    }
    if (_notsamecote) {
      _vm.add(OscarMsg.VMSG031);
    }
    _vm.add(poussee_.validation());
    _vm.add(butee_.validation());
    return _vm;
  }
}
