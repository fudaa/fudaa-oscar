/*
 * @file         OscarEauParametres.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.oscar.SEau;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;


/**
 * Description de l'onglet des parametres de l'eau.
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarEauParametres extends OscarAbstractOnglet {
  /**
   * Case option "Calcul Hydrostatique"
   */
  private BuRadioButton rb_hyd_stat;
  /**
   * Case option "Calcul Hydrodynamique"
   */
  private BuRadioButton rb_hyd_dyn;
  /**
   * Groupe de choix d'options pour le type de calcul (static,dynamic)
   */
  private ButtonGroup gb_typ_calc;
  /**
   * Case � cocher pour activer la saisie de la cote de nappe c�t� pouss�e
   */
  private BuCheckBox cb_cot_pou;
  /**
   * Case � cocher pour activer la saisie de la cote de nappe c�t� but�e
   */
  private BuCheckBox cb_cot_but;
  /**
   * Champs cote de nappe c�t� pouss�e
   */
  private BuTextField tf_cot_pou;
  /**
   * Champs cote de nappe c�t� but�e
   */
  private BuTextField tf_cot_but;
  /**
   *
   */
  private BuLabel lb_cot_pou_unit;
  /**
   *
   */
  private BuLabel lb_cot_but_unit;
  /**
   * Bloc Info-aide
   */
  private BuPanel b1;
  /**
   * Bloc Type de calcul (static,dynamic)
   */
  private BuPanel b2;
  /**
   * Bloc action static
   */
  private BuPanel b3;
  /**
   * Bloc action dynamique
   */
  private BuPanel b4;
  /**
   * Bloc Info
   */
  private BuPanel b5;

  /**
   * Construit l'onglet Eau.
   */
  public OscarEauParametres(final OscarFilleParametres _fp, final String _helpfile) {
    super(_fp.getApplication(), _fp, _helpfile);
    // Affichage / Masquage des blocs
    afficherCalculStatic();
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    rb_hyd_stat = new BuRadioButton(OscarMsg.RBLAB001);
    rb_hyd_dyn = new BuRadioButton(OscarMsg.RBLAB002);
    gb_typ_calc = new ButtonGroup();
    cb_cot_pou = new BuCheckBox(OscarMsg.LAB007);
    cb_cot_but = new BuCheckBox(OscarMsg.LAB008);
    tf_cot_pou = OscarLib.DoubleField(OscarMsg.FORMAT011);
    tf_cot_but = OscarLib.DoubleField(OscarMsg.FORMAT012);
    lb_cot_pou_unit = OscarLib.Label(OscarMsg.UNITLAB_METRE);
    lb_cot_but_unit = OscarLib.Label(OscarMsg.UNITLAB_METRE);
    // Cr�e le bloc [Info-Aide]
    final OscarSolParametres _sol = (OscarSolParametres) ((OscarFilleParametres) getParentComponent())
        .getOnglet(OscarLib.ONGLET_SOL);
    b1 = OscarLib.InfoAideAndButtonPanel(OscarMsg.LAB003, "Dessin", "DESSIN", this, _sol, "COUCHES_SOL_DISPONIBLE");
    // Cr�e le bloc [Type de Calcul]
    b2 = new BuPanel();
    b2.setLayout(OscarLib.VerticalLayout());
    b2.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB001));
    gb_typ_calc.add(rb_hyd_stat);
    gb_typ_calc.add(rb_hyd_dyn);
    rb_hyd_dyn.setEnabled(false);
    rb_hyd_dyn.setSelected(false);
    rb_hyd_stat.setSelected(true);
    b2.add(rb_hyd_stat);
    b2.add(rb_hyd_dyn);
    rb_hyd_stat.setActionCommand("SELECTIONNER_CALCUL_STATIC");
    rb_hyd_stat.addActionListener(this);
    // rb_hyd_dyn.setActionCommand("SELECTIONNER_CALCUL_DYNAMIC");
    // rb_hyd_dyn.addActionListener(this);
    // Cr�e le bloc [Action statique de l'eau]
    b3 = new BuPanel();
    b3.setLayout(OscarLib.GridLayout(3));
    b3.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB002));
    b3.add(cb_cot_pou);
    b3.add(tf_cot_pou);
    b3.add(lb_cot_pou_unit);
    b3.add(cb_cot_but);
    b3.add(tf_cot_but);
    b3.add(lb_cot_but_unit);
    cb_cot_pou.setActionCommand("SELECTION_COTE_POUSSEE");
    cb_cot_but.setActionCommand("SELECTION_COTE_BUTEE");
    cb_cot_pou.addActionListener(this);
    cb_cot_but.addActionListener(this);
    toggleCotePoussee(false);
    toggleCoteButee(false);
    // Cr�e le bloc [Action dynamique de l'eau]
    b4 = new BuPanel();
    b4.setLayout(OscarLib.GridLayout(4));
    b4.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB003));
    // Cr�e le bloc [Informations]
    b5 = new BuPanel();
    b5.setLayout(OscarLib.VerticalLayout());
    b5.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB_INFO));
    // BuLabel lb_aide_puce1 = new BuLabel(OscarMsg.PUCE1);
    final BuLabelMultiLine lb_aide_txt1 = OscarLib.LabelMultiLine(OscarMsg.LAB006);
    // b5.add(lb_aide_puce1);
    b5.add(lb_aide_txt1);
    // BuLabel lb_aide_puce2 = new BuLabel(OscarMsg.PUCE1);
    final BuLabelMultiLine lb_aide_txt2 = OscarLib.LabelMultiLine(OscarMsg.LAB009);
    // b5.add(lb_aide_puce2);
    b5.add(lb_aide_txt2);
    _p.add(b1);
    _p.add(b2);
    _p.add(b3);
    _p.add(b4);
    _p.add(b5);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {
    if (_action.equals("AIDE")) {
      aide();
    } else if (_action.equals("DESSIN")) {
      dessin();
    } else if (_action.equals("SELECTION_COTE_POUSSEE")) {
      toggleCotePoussee(cb_cot_pou.isSelected());
    } else if (_action.equals("SELECTION_COTE_BUTEE")) {
      toggleCoteButee(cb_cot_but.isSelected());
    } else if (_action.equals("SELECTIONNER_CALCUL_STATIC")) {
      afficherCalculStatic();
    } else if (_action.equals("SELECTIONNER_CALCUL_DYNAMIC")) {
      afficherCalculDynamic();
    }
  }

  /**
   *
   */
  public Double getCoteNappeEauCotePoussee() {
    Double _v;
    _v = (Double) tf_cot_pou.getValue();
    if (!cb_cot_pou.isSelected()) {
      _v = null;
    }
    return _v;
  }

  /**
   *
   */
  public Double getCoteNappeEauCoteButee() {
    Double _v;
    try {
      _v = (Double) tf_cot_but.getValue();
    } catch (final Exception _e1) {
      _v = null;
    }
    if (!cb_cot_but.isSelected()) {
      _v = null;
    }
    return _v;
  }

  /**
   *
   */
  protected void dessin() {
    changeOnglet((ChangeEvent) null);
    OscarLib.showDialogGraphViewer(getApplication(), "Repr�sentation du terrain", getNappeEauGrapheScript());
  }

  /**
   *
   */
  public String getNappeEauGrapheScript() {
    double _min;
    double _max;
    final Double _dmin = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteBasDuSol();
    final Double _dmax = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteHautDuSol();
    if (_dmin == null) {
      _min = -1.0;
    } else {
      _min = _dmin.doubleValue();
    }
    if (_dmax == null) {
      _max = 1.0;
    } else {
      _max = _dmax.doubleValue() + 1.0;
    }
    return getNappeEauGrapheScript(0.0, 1.0, _min, _max);
  }

  /**
   *
   */
  public String getNappeEauGrapheScript(final double _xmin, final double _xmax, final double _min, final double _max) {
    final StringBuffer _s = new StringBuffer(4096);
    _s.append("graphe \n{\n");
    _s.append("  titre \"Terrain\"\n");
    _s.append("  sous-titre \"Couches de sol et nappes d'eau\"\n");
    _s.append("  legende oui\n");
    _s.append("  animation non\n");
    _s.append("  marges\n{\n");
    _s.append("    gauche 80\n");
    _s.append("    droite 120\n");
    _s.append("    haut 50\n");
    _s.append("    bas 30\n");
    _s.append("  }\n");
    _s.append("  \n");
    _s.append("  axe\n{\n");
    _s.append("    orientation horizontal\n");
    _s.append("    graduations non\n");
    _s.append("    minimum " + _xmin + "\n");
    _s.append("    maximum " + _xmax + "\n");
    _s.append("  }\n");
    _s.append("  axe\n{\n");
    _s.append("    titre \"cote\"\n");
    _s.append("    unite \"m\"\n");
    _s.append("    orientation vertical\n");
    _s.append("    graduations oui\n");
    _s.append("    minimum " + _min + "\n");
    _s.append("    maximum " + _max + "\n");
    _s.append("  }\n");
    _s.append(((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCouchesCourbeGrapheScript(
        _xmin, _xmax, _min, _max));
    _s.append(getNappeEauCotePousseeGrapheScript((_xmin + _xmax) / 2, _xmax, _min, _max));
    _s.append(getNappeEauCoteButeeGrapheScript(_xmin, (_xmin + _xmax) / 2, _min, _max));
    _s.append("}\n");
    return _s.toString();
  }

  /**
   *
   */
  public String getNappeEauCotePousseeGrapheScript(final double _xmin, final double _xmax, final double _ymin,
      final double _ymax) {
    final StringBuffer _s = new StringBuffer(4096);
    double _niveau;
    Double _dniveau;
    _dniveau = getCoteNappeEauCotePoussee();
    if (_dniveau != null) {
      _niveau = _dniveau.doubleValue();
      _s.append("  courbe\n{\n");
      // _s.append(" type aire\n");
      _s.append("    marqueurs non\n");
      _s.append("    valeurs\n{\n");
      _s.append("      " + _xmin + " " + _niveau + "\n");
      _s.append("      " + _xmax + " " + _niveau + "\n");
      _s.append("    }\n");
      _s.append("    aspect\n{\n");
      _s.append("      surface.couleur 0000FF\n");
      _s.append("      contour.couleur 0000FF\n");
      _s.append("      contour.largeur 10\n");
      _s.append("    }\n");
      _s.append("  }\n");
      _s.append("  contrainte\n{\n");
      _s.append("    titre \"nappe d'eau c�t� pouss�e\"\n");
      _s.append("    couleur 0000FF\n");
      _s.append("    orientation vertical\n");
      _s.append("    type max\n");
      _s.append("    valeur   " + _niveau + "\n");
      _s.append("    position " + (_xmin + (_xmax - _xmin) / 4) + "\n");
      _s.append("  }\n");
    }
    return _s.toString();
  }

  /**
   *
   */
  public String getNappeEauCoteButeeGrapheScript(final double _xmin, final double _xmax, final double _ymin,
      final double _ymax) {
    final StringBuffer _s = new StringBuffer(4096);
    double _niveau;
    Double _dniveau;
    _dniveau = getCoteNappeEauCoteButee();
    if (_dniveau != null) {
      _niveau = _dniveau.doubleValue();
      _s.append("  courbe\n{\n");
      // _s.append(" type aire\n");
      _s.append("    marqueurs non\n");
      _s.append("    valeurs\n{\n");
      _s.append("      " + _xmin + " " + _niveau + "\n");
      _s.append("      " + _xmax + " " + _niveau + "\n");
      _s.append("    }\n");
      _s.append("    aspect\n{\n");
      _s.append("      surface.couleur 0000FF\n");
      _s.append("      contour.couleur 0000FF\n");
      _s.append("      contour.largeur 10\n");
      _s.append("    }\n");
      _s.append("  }\n");
      _s.append("  contrainte\n{\n");
      _s.append("    titre \"nappe d'eau c�t� but�e\"\n");
      _s.append("    couleur 0000FF\n");
      _s.append("    orientation vertical\n");
      _s.append("    type max\n");
      _s.append("    valeur   " + _niveau + "\n");
      _s.append("    position " + (_xmin + ((_xmax - _xmin) / 4)) + "\n");
      _s.append("  }\n");
    }
    return _s.toString();
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT004);
    _mv.addMessages(validation().getMessages());
  }

  /**
   *
   */
  private void toggleCotePoussee(boolean _b) {
    if (!_b) {
      lb_cot_pou_unit.setEnabled(false);
      tf_cot_pou.setEnabled(false);
    } else {
      lb_cot_pou_unit.setEnabled(true);
      tf_cot_pou.setEnabled(true);
      tf_cot_pou.requestFocus();
    }
  }

  /**
   *
   */
  private void toggleCoteButee(boolean _b) {
    if (!_b) {
      lb_cot_but_unit.setEnabled(false);
      tf_cot_but.setEnabled(false);
    } else {
      lb_cot_but_unit.setEnabled(true);
      tf_cot_but.setEnabled(true);
      tf_cot_but.requestFocus();
    }
  }

  /**
   *
   */
  private void afficherCalculStatic() {
    b4.setVisible(false);
    b3.setVisible(true);
    b5.setVisible(true);
  }

  /**
   *
   */
  private void afficherCalculDynamic() {
    b4.setVisible(true);
    b3.setVisible(false);
    b5.setVisible(false);
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans l'onglet eau
   */
  public SEau getParametresEau() {
    final SEau _p = (SEau) OscarLib.createIDLObject(SEau.class);
    _p.typeCalculEau = (getTypeCalcul() == OscarLib.EAU_CALCUL_STATIC) ? OscarLib.IDL_EAU_CHOIXCALCUL_STATIC
        : OscarLib.IDL_EAU_CHOIXCALCUL_DYNAMIC;
    _p.presenceNappeEauCotePoussee = cb_cot_pou.isSelected();
    _p.presenceNappeEauCoteButee = cb_cot_but.isSelected();
    if (_p.typeCalculEau == OscarLib.IDL_EAU_CHOIXCALCUL_STATIC) {
      if (cb_cot_but.isSelected()) {
        try {
          _p.coteNappeEauCoteButee = ((Double) tf_cot_but.getValue()).doubleValue();
        } catch (final NullPointerException _e1) {
          _p.coteNappeEauCoteButee = VALEUR_NULLE.value;
        }
      } else {
        _p.coteNappeEauCoteButee = VALEUR_NULLE.value;
      }
      if (cb_cot_pou.isSelected()) {
        try {
          _p.coteNappeEauCotePoussee = ((Double) tf_cot_pou.getValue()).doubleValue();
        } catch (final NullPointerException _e1) {
          _p.coteNappeEauCotePoussee = VALEUR_NULLE.value;
        }
      } else {
        _p.coteNappeEauCotePoussee = VALEUR_NULLE.value;
      }
    } else {
      _p.coteNappeEauCotePoussee = VALEUR_NULLE.value;
      _p.coteNappeEauCoteButee = VALEUR_NULLE.value;
    }
    return _p;
  }

  /**
   * retourne un entier repr�sentant le type de calcul en cours (actuellement choisi par l'utilisateur). voir les
   * constantes EAU_CALCUL_DYNAMIC et EAU_CALCUL_STATIC de la classe OscarLib.
   */
  public int getTypeCalcul() {
    int _r = 0;
    try {
      if (rb_hyd_dyn.isSelected()) {
        _r = OscarLib.EAU_CALCUL_DYNAMIC;
      } else if (rb_hyd_stat.isSelected()) {
        _r = OscarLib.EAU_CALCUL_STATIC;
      } else {
        throw new Exception();
      }
    } catch (final Exception _e1) {
      WSpy.Error(OscarMsg.ERR012);
    }
    return _r;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans les champs de l'onglet eau
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) � importer
   */
  public synchronized void setParametresEau(SEau _p) {
    if (_p == null) {
      _p = (SEau) OscarLib.createIDLObject(SEau.class);
    }
    Double _v;
    toggleCotePoussee(_p.presenceNappeEauCotePoussee);
    if (_p.presenceNappeEauCotePoussee) {
      _v = new Double(_p.coteNappeEauCotePoussee);
      if (_v.doubleValue() != VALEUR_NULLE.value) {
        tf_cot_pou.setValue(_v);
      }
      cb_cot_pou.setSelected(true);
    } else {
      cb_cot_pou.setSelected(false);
    }
    toggleCoteButee(_p.presenceNappeEauCoteButee);
    if (_p.presenceNappeEauCoteButee) {
      _v = new Double(_p.coteNappeEauCoteButee);
      if (_v.doubleValue() != VALEUR_NULLE.value) {
        tf_cot_but.setValue(_v);
      }
      cb_cot_but.setSelected(true);
    } else {
      cb_cot_but.setSelected(false);
    }
    if (_p.typeCalculEau == OscarLib.IDL_EAU_CHOIXCALCUL_DYNAMIC) {
      afficherCalculDynamic();
      rb_hyd_dyn.setSelected(true);
    } else {
      afficherCalculStatic();
      rb_hyd_stat.setSelected(true);
    }
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    /* SParametresOscar _p= */
    ((OscarFilleParametres) getParentComponent()).getParametres();
    double _min;
    double _max;
    boolean _emptycotpou = false;
    boolean _emptycotbut = false;
    boolean _horslimitescotpou = false;
    boolean _horslimitescotbut = false;
    final Double _dmin = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL))
        .getCoteFondDeSouille();
    final Double _dmax = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteHautDuSol();
    if (_dmin == null) {
      _min = Double.MAX_VALUE;
    } else {
      _min = _dmin.doubleValue();
    }
    if (_dmax == null) {
      _max = Double.MIN_VALUE;
    } else {
      _max = _dmax.doubleValue();
    }
    final int _r = getTypeCalcul();
    if ((_r != OscarLib.EAU_CALCUL_DYNAMIC) && (_r != OscarLib.EAU_CALCUL_STATIC)) {
      _vm.add(OscarMsg.VMSG004);
    }
    if ((tf_cot_pou.getValue() == null) && cb_cot_pou.isSelected()) {
      _emptycotpou = true;
    }
    if ((tf_cot_but.getValue() == null) && cb_cot_but.isSelected()) {
      _emptycotbut = true;
    }
    if (cb_cot_pou.isSelected()
        && (tf_cot_pou.getValue() != null)
        && ((((Double) tf_cot_pou.getValue()).doubleValue() < _min) || (((Double) tf_cot_pou.getValue()).doubleValue() > _max))) {
      _horslimitescotpou = true;
    }
    if (cb_cot_but.isSelected()
        && (tf_cot_but.getValue() != null)
        && ((((Double) tf_cot_but.getValue()).doubleValue() < _min) || (((Double) tf_cot_but.getValue()).doubleValue() > _max))) {
      _horslimitescotbut = true;
    }
    if (_emptycotpou) {
      _vm.add(OscarMsg.VMSG005);
    }
    if (_emptycotbut) {
      _vm.add(OscarMsg.VMSG006);
    }
    if (_horslimitescotpou) {
      _vm.add(OscarMsg.VMSG007);
    }
    if (_horslimitescotbut) {
      _vm.add(OscarMsg.VMSG008);
    }
    return _vm;
  }
}
