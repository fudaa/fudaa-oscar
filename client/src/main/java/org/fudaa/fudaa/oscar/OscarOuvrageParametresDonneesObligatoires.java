/*
 * @file         OscarOuvrageParametresDonneesObligatoires.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;


/**
 * Description du sous-onglet 'Donn�es Obligatoires' de l'onglet des parametres des ouvrage.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:53 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarOuvrageParametresDonneesObligatoires extends OscarAbstractOnglet {
  /**
   * Case � cocher pr�sence de nappe de tirants ou non.
   */
  BuCheckBox cb_napp;
  /**
   * Champs cote de la nappe de tirants.
   */
  BuTextField tf_cot_tir;
  /**
   * Champs Pourcentage d'encastrement de la nappe de tirants.
   */
  BuTextField tf_pour_enc;
  BuLabel lb_cot_tir;
  BuLabel lb_cot_tir_unit;
  /**
   *
   */
  BuLabel lb_pour_enc;
  /**
   *
   */
  BuLabel lb_pour_enc_unit;
  /**
   *
   */
  BuPanel pn1;
  /**
   *
   */
  BuPanel pn2;

  /**
   *
   */
  public OscarOuvrageParametresDonneesObligatoires(final OscarOuvrageParametres _ouvrage, final String _helpfile) {
    super(_ouvrage.getApplication(), _ouvrage, _helpfile);
    toggleNappeTirants(false);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    cb_napp = new BuCheckBox(OscarMsg.LAB062);
    tf_cot_tir = OscarLib.DoubleField("#0.00");
    tf_pour_enc = OscarLib.DoubleField("#0.00");
    lb_cot_tir = OscarLib.Label(OscarMsg.LAB063);
    lb_pour_enc = OscarLib.Label(OscarMsg.LAB064);
    lb_cot_tir_unit = OscarLib.Label(OscarMsg.UNITLAB_METRE);
    lb_pour_enc_unit = OscarLib.Label(OscarMsg.UNITLAB_POURCENT);
    pn1 = OscarLib.InfoPanel(OscarMsg.LAB061);
    pn2 = new BuPanel();
    pn2.setLayout(OscarLib.VerticalLayout());
    pn2.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB024));
    cb_napp.setActionCommand("SELECTIONNER_NAPPE_TIRANTS");
    cb_napp.addActionListener(this);
    // Bloc 'Nappe de Tirants'
    final BuPanel pn3 = new BuPanel();
    final BuGridLayout lo4 = OscarLib.GridLayout(3);
    pn3.setLayout(lo4);
    pn3.setBorder(OscarLib.EmptyBorder());
    pn3.add(lb_cot_tir);
    pn3.add(tf_cot_tir);
    pn3.add(lb_cot_tir_unit);
    pn3.add(lb_pour_enc);
    pn3.add(tf_pour_enc);
    pn3.add(lb_pour_enc_unit);
    pn2.add(cb_napp);
    pn2.add(pn3);
    _p.add(pn1);
    _p.add(pn2);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {
    if (_action.equals("SELECTIONNER_NAPPE_TIRANTS")) {
      toggleNappeTirants(cb_napp.isSelected());
      firePropertyChange("NAPPE_TIRANTS_SELECTIONNEE", !cb_napp.isSelected(), cb_napp.isSelected());
    }
  }

  /**
   *
   */
  public void toggleNappeTirants(boolean _selected) {
    if (!_selected) {
      if (cb_napp.isSelected()) {
        cb_napp.setSelected(false);
      }
      lb_cot_tir.setEnabled(false);
      lb_cot_tir_unit.setEnabled(false);
      tf_cot_tir.setEnabled(false);
      lb_pour_enc.setEnabled(false);
      lb_pour_enc_unit.setEnabled(false);
      tf_pour_enc.setEnabled(false);
    } else {
      if (!cb_napp.isSelected()) {
        cb_napp.setSelected(true);
      }
      lb_cot_tir.setEnabled(true);
      lb_cot_tir_unit.setEnabled(true);
      tf_cot_tir.setEnabled(true);
      lb_pour_enc.setEnabled(true);
      lb_pour_enc_unit.setEnabled(true);
      tf_pour_enc.setEnabled(true);
      tf_cot_tir.requestFocus();
    }
  }

  /**
   *
   */
  public boolean getPresenceNappeTirants() {
    return cb_napp.isSelected();
  }

  /**
   *
   */
  public Double getCoteNappeTirants() {
    try {
      return (Double) tf_cot_tir.getValue();
    } catch (final Exception _e1) {
      return null;
    }
  }

  /**
   *
   */
  public synchronized void setCoteNappeTirants(final double _p) {
    tf_cot_tir.setValue(new Double(_p));
  }

  /**
   *
   */
  public Double getPourcentageEncastrementNappe() {
    try {
      return (Double) tf_pour_enc.getValue();
    } catch (final Exception _e1) {
      return null;
    }
  }

  /**
   *
   */
  public synchronized void setPourcentageEncastrementNappe(final double _p) {
    tf_pour_enc.setValue(new Double(_p));
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _e) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT015);
    _mv.addMessages(validation().getMessages());
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    final boolean _pnt = getPresenceNappeTirants();
    final Double _cnt = getCoteNappeTirants();
    final Double _pen = getPourcentageEncastrementNappe();
    final Double _cntmin = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteBasDuSol();
    final Double _cntmax = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL))
        .getCoteHautDuSol();
    if (_pnt && (_cnt == null)) {
      _vm.add(OscarMsg.VMSG042);
    }
    if (_pnt && (_pen == null)) {
      _vm.add(OscarMsg.VMSG043);
    }
    if ((_pen != null) && ((_pen.doubleValue() < 0.0) || (_pen.doubleValue() > 100.0))) {
      _vm.add(OscarMsg.VMSG044);
    }
    if ((_cnt != null)
        && ((_cntmax == null) || (_cntmin == null) || ((_cnt.doubleValue() < _cntmin.doubleValue()) || (_cnt
            .doubleValue() > _cntmax.doubleValue())))) {
      _vm.add(OscarMsg.VMSG045);
    }
    return _vm;
  }
}
