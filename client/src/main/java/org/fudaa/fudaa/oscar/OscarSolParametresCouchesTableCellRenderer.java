/*
 * @file         OscarSolParametresCouchesTableCellRenderer.java
 * @creation     2000-11-08
 * @modification $Date: 2007-01-19 11:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTableCellRenderer;

/**
 * Gestionnaire de rendu de cellule pour le tableau des couches de sol (pouss�e et but�e)
 * 
 * @version $Revision: 1.7 $ $Date: 2007-01-19 11:13:24 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public class OscarSolParametresCouchesTableCellRenderer extends BuTableCellRenderer {
  public OscarSolParametresCouchesTableCellRenderer() {
    super();
  }

  public OscarSolParametresCouchesTableCellRenderer(final int _digit) {
    super();
    setNumberFormat(OscarLib.NumberFormat(_digit));
  }

  /**
   * renvoi un objet "rendu" du contenu de la cellule sp�cifi� par rapport � la donn�e fournie.
   * 
   * @param table r�f�rence vers le tableau contenant les donn�es
   * @param value valeur de la cellule � afficher/traiter
   * @param isSelected sp�cifie si la cellule est actuellement selectionn�e
   * @param hasFocus sp�cifie si la cellule a actuellement le focus
   * @param row sp�cifi� le num�ro de ligne de la cellule
   * @param column sp�cifie le num�ro de colonne de la cellule
   */
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
      final boolean hasFocus, final int row, final int column) {
    Component _r = null;
    BuLabel _lb = null;
    if (row < 0) { // entetes de colonnes
      _lb = new BuLabel();
      _lb.setIcon(OscarResource.OSCAR.getIcon((String) value + ".gif"));
      _lb.setHorizontalAlignment(SwingConstants.CENTER);
      /* utilisez ceci pour afficher l'unit� dans l'ent�te de colonne */
      /*
       * try{ _lb.setVerticalTextPosition(SwingConstants.BOTTOM); _lb.setHorizontalTextPosition(SwingConstants.CENTER);
       * _lb.setText(OscarLib.getUnitForSymbol(((String)value))); }catch(IllegalArgumentException _e2){}
       */
      _lb.setOpaque(false);
      _lb.setPreferredSize(new Dimension(40, 40));
      final BuPanel _p = new BuPanel();
      _p.setLayout(new BorderLayout());
      _p.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
      _p.setBackground(UIManager.getColor("TableHeader.background"));
      _p.setOpaque(true);
      _p.add(_lb, BorderLayout.CENTER);
      _r = _p;
    } else {
      _lb = (BuLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      if (value instanceof Double) {} else if (value instanceof Integer) {
	  _lb.setHorizontalAlignment(SwingConstants.CENTER);
	  _lb.setText(((Integer) value).toString());
	  _lb.setForeground(Color.blue);
      } else if (value instanceof String) {
	  _lb.setHorizontalAlignment(SwingConstants.CENTER);
	  _lb.setText((String) value);
	  _lb.setForeground(Color.red);
      }
      _r = _lb;
    }
    return _r;
  }
}
