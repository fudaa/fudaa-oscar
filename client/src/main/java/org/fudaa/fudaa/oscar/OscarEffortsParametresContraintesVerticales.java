/*
 * @file         OscarEffortsParametresContraintesVerticales.java
 * @creation     2000-10-15
 * @modification $Date: 2007-01-19 11:13:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;

import org.fudaa.dodico.corba.oscar.SContrainte;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;


/**
 * Description du sous-onglet 'Contraintes Verticales' de l'onglet des parametres des efforts.
 * 
 * @version $Revision: 1.8 $ $Date: 2007-01-19 11:13:21 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public class OscarEffortsParametresContraintesVerticales extends OscarAbstractOnglet {
  /**
   * tableau contenant les informations sur les contraintes verticales suppl�mentaires
   */
  private BuTable tb_sup;
  /**
   * mod�le de donn�es du tableau contenant les informations sur les contraintes verticales suppl�mentaires
   */
  private DefaultTableModel dm_tb_sup;
  /**
   * bouton d'ajout d'une contrainte verticale
   */
  BuButton bt_add;
  /**
   * bouton de suppression d'une contrainte verticale
   */
  BuButton bt_del;
  /**
   *
   */
  BuPanel pn0;
  /**
   *
   */
  BuPanel pn1;

  /**
   *
   */
  public OscarEffortsParametresContraintesVerticales(final OscarEffortsParametres _efforts, final String _helpfile) {
    super(_efforts.getApplication(), _efforts, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    bt_add = OscarLib.Button("Ajouter", "OUI", "AJOUTER");
    bt_del = OscarLib.Button("Supprimer", "NON", "SUPPRIMER");
    pn0 = OscarLib.InfoPanel(OscarMsg.LAB055);
    pn1 = new BuPanel();
    pn1.setLayout(OscarLib.VerticalLayout());
    pn1.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB020));
    dm_tb_sup = new OscarEffortsParametresContraintesTableModel(OscarLib.EFFORTS_CVS);
    tb_sup = OscarLib.Table(dm_tb_sup);
    // tb_sup.getColumnModel().getColumn(0).setCellEditor(new OscarDoubleValueCellEditor( "#0.00" , Double.MIN_VALUE ,
    // Double.MAX_VALUE ));
    // tb_sup.getColumnModel().getColumn(1).setCellEditor(new OscarDoubleValueCellEditor( "#0.000", Double.MIN_VALUE ,
    // Double.MAX_VALUE ));
    // tb_sup.getColumnModel().getColumn(0).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(2));
    // tb_sup.getColumnModel().getColumn(1).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(3));
    bt_add.addActionListener(this);
    bt_del.addActionListener(this);
    tb_sup.addMouseListener(new MouseAdapter() {
      public void mouseReleased(final MouseEvent _evt) {
        clickedOnTable();
      }
    });
    // Bloc 'Contraintes'
    final JScrollPane sp1 = new JScrollPane(tb_sup);
    final BuPanel pn2 = new BuPanel();
    pn2.setBorder(OscarLib.EmptyBorder());
    pn2.setLayout(OscarLib.GridLayout(3));
    pn2.add(bt_add);
    pn2.add(bt_del);
    pn2.add(OscarLib.EmptyPanel());
    pn1.add(sp1);
    pn1.add(pn2);
    _p.add(pn0);
    _p.add(pn1);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {
    if (_action.equals("AJOUTER")) {
      ajouterContrainte();
    } else if (_action.equals("SUPPRIMER")) {
      supprimerContrainte();
    }
  }

  /**
   *
   */
  private void ajouterContrainte() {
    if (tb_sup.isEditing()) {
      tb_sup.getCellEditor().stopCellEditing();
    }
    dm_tb_sup.addRow(OscarLib.EmptyRow(dm_tb_sup.getColumnCount()));
    if (dm_tb_sup.getRowCount() > 0) {
      bt_del.setEnabled(true);
      tb_sup.setRowSelectionInterval(dm_tb_sup.getRowCount() - 1, dm_tb_sup.getRowCount() - 1);
      // tb_sup.editCellAt(dm_tb_sup.getRowCount()-1,0);
    }
  }

  /**
   *
   */
  private void supprimerContrainte() {
    if (tb_sup.getSelectedRow() <= -1) {
      return;
    }
    if (tb_sup.isEditing()) {
      tb_sup.getCellEditor().stopCellEditing();
    }
    dm_tb_sup.removeRow(tb_sup.getSelectedRow());
    if ((dm_tb_sup.getRowCount() == 0) || (tb_sup.getSelectedRowCount() <= 0)) {
      bt_del.setEnabled(false);
    }
  }

  /**
   * execut�e lorsque l'utilisateur clique sur une ligne du tableau des couches de sol en pouss�e.
   */
  protected void clickedOnTable() {
    if (tb_sup.getSelectedRowCount() > 0) {
      bt_del.setEnabled(true);
    } else {
      bt_del.setEnabled(false);
    }
  }

  /**
   * retourne le nombre de contraintes contenues dans le tableau
   */
  public int getNbContraintes() {
    return tb_sup.getRowCount();
  }

  /**
   * retourne une structure de donn�es contenant les informations de la contrainte _n du tableau. retourne null si la
   * contrainte n'existe pas dans le tableau.
   * 
   * @param _n num�ro de la contrainte � retourner (commence � 1)
   */
  public SContrainte getContrainte(final int _n) {
      SContrainte _c = null;
      try {
	  _c = (SContrainte) OscarLib.createIDLObject(SContrainte.class);
	  try {
	      _c.cote = Double.parseDouble(dm_tb_sup.getValueAt(_n - 1, 0).toString());
	      // _c.cote = ((Double) dm_tb_sup.getValueAt(_n - 1, 0)).doubleValue();
	  } catch (final Exception _e2) {
	      _c.cote = VALEUR_NULLE.value;
	  }
	  try {
	      _c.valeur = Double.parseDouble(dm_tb_sup.getValueAt(_n - 1, 1).toString());
	      // _c.valeur = ((Double) dm_tb_sup.getValueAt(_n - 1, 1)).doubleValue();
	  } catch (final Exception _e2) {
	      _c.valeur = VALEUR_NULLE.value;
	  }
      } catch (final ArrayIndexOutOfBoundsException _e1) {
	  WSpy.Error(OscarMsg.ERR013);
      }
      return _c;
  }

  /**
   * retourne une structure de donn�es contenant les informations de toutes les contraintes.
   */
  public SContrainte[] getContraintes() {
    if (tb_sup.isEditing()) {
      tb_sup.getCellEditor().stopCellEditing();
    }
    SContrainte[] _c = null;
    final int _i1 = 1;
    final int _i2 = getNbContraintes();
    try {
      _c = new SContrainte[_i2 - _i1 + 1];
    } catch (final NegativeArraySizeException _e2) {
      WSpy.Error(OscarMsg.ERR014);
    }
    for (int i = (_i1 - 1); i < _i2; i++) {
      try {
        _c[i - (_i1 - 1)] = getContrainte(i + 1);
      } catch (final NullPointerException _e1) {
        WSpy.Error(OscarMsg.ERR015);
      } catch (final ArrayIndexOutOfBoundsException _e2) {
        WSpy.Error(OscarMsg.ERR016);
        return null;
      }
    }
    return _c;
  }

  /**
   * charge les contraintes sp�cifi�es dans le tableau des contraintes
   * 
   * @param _c tableau contenant les contraintes � importer
   */
  public synchronized void setContraintes(final SContrainte[] _c) {
    if (tb_sup.isEditing()) {
      tb_sup.getCellEditor().stopCellEditing();
    }
    viderTableau();
    int _n = 0;
    try {
      _n = _c.length;
    } catch (final NullPointerException _e1) {
      WSpy.Error(OscarMsg.ERR017);
    }
    for (int i = 0; i < _n; i++) {
      ajouterContrainte();
      if (_c[i].cote != VALEUR_NULLE.value) {
        tb_sup.setValueAt(new Double(_c[i].cote), dm_tb_sup.getRowCount() - 1, 0);
      }
      if (_c[i].valeur != VALEUR_NULLE.value) {
        tb_sup.setValueAt(new Double(_c[i].valeur), dm_tb_sup.getRowCount() - 1, 1);
      }
    }
  }

  /**
   * supprime toutes les couches du tableau des couches
   */
  private void viderTableau() {
    if (tb_sup.getRowCount() <= -1) {
      return;
    }
    if (tb_sup.isEditing()) {
      tb_sup.getCellEditor().stopCellEditing();
    }
    final int _max = tb_sup.getRowCount();
    for (int i = 0; i < _max; i++) {
      dm_tb_sup.removeRow(0);
    }
    if ((dm_tb_sup.getRowCount() == 0) || (tb_sup.getSelectedRowCount() <= 0)) {
      bt_del.setEnabled(false);
    }
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT010);
    _mv.addMessages(validation().getMessages());
  }

  /**
   *
   */
  public ValidationMessages validation() {
    if (tb_sup.isEditing()) {
      tb_sup.getCellEditor().stopCellEditing();
    }
    final ValidationMessages _vm = new ValidationMessages();
    Double _v;
    double _vmin;
    double _vmax;
    double _lastval;
    int i;
    boolean _emptycell = false;
    boolean _horslimites = false;
    boolean _unordered = false;
    boolean _lessthan2 = false;
    final OscarSolParametres _sol = (OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL);
    final Double _dmin = _sol.getCoteBasDuSol();
    final Double _dmax = _sol.getCoteHautDuSol();
    if (_dmin == null) {
      _vmin = Double.MAX_VALUE;
    } else {
      _vmin = _dmin.doubleValue();
    }
    if (_dmax == null) {
      _vmax = Double.MIN_VALUE;
    } else {
      _vmax = _dmax.doubleValue();
    }
    _lastval = Double.MAX_VALUE;
    final int _max = tb_sup.getRowCount();
    for (i = 0; i < _max; i++) {
      try {
        _v = (Double) dm_tb_sup.getValueAt(i, 0);
      } catch (final Exception _e1) {
        final Object _o = dm_tb_sup.getValueAt(i, 0);
        if ((_o instanceof String) && (_o != null) && (!_o.equals(""))) {
          _v = Double.valueOf((String) _o);
        } else {
          _v = null;
        }
      }
      if (_v == null) {
        _emptycell = true;
      } else {
        if ((_v.doubleValue() < _vmin) || (_v.doubleValue() > _vmax)) {
          _horslimites = true;
        }
        if (_v.doubleValue() > _lastval) {
          _unordered = true;
        }
        _lastval = _v.doubleValue();
      }
      try {
        _v = (Double) dm_tb_sup.getValueAt(i, 1);
      } catch (final Exception _e1) {
        final Object _o = dm_tb_sup.getValueAt(i, 1);
        if ((_o instanceof String) && (_o != null) && (!_o.equals(""))) {
          _v = Double.valueOf((String) _o);
        } else {
          _v = null;
        }
      }
      if (_v == null) {
        _emptycell = true;
      }
    }
    if (_max == 1) {
      _lessthan2 = true;
    }
    if (_horslimites) {
      _vm.add(OscarMsg.VMSG023);
    }
    if (_unordered) {
      _vm.add(OscarMsg.VMSG024);
    }
    if (_emptycell) {
      _vm.add(OscarMsg.VMSG025);
    }
    if (_lessthan2) {
      _vm.add(OscarMsg.VMSG026);
    }
    return _vm;
  }
}
