package org.fudaa.fudaa.oscar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicArrowButton;


public class WTreeItem extends JPanel {
  protected boolean expanded_ = true;
  protected boolean enabled_ = true;
  public final static Dimension expandedSize_ = new Dimension(Integer.MAX_VALUE, 100);
  public final static Dimension collapsedSize_ = new Dimension(Integer.MAX_VALUE, 30);
  public final static Color borderColor_ = new Color(215, 215, 215);
  public final static Color borderColorSelected_ = new Color(239, 239, 239);
  protected WTree.TreeItemGUI content_ = null;
  protected JPanel barre_ = null;
  protected BasicArrowButton _b1 = null;
  protected JCheckBox _c1 = null;
  protected OscarFilleNoteDeCalculs.Settings _s = null;

  public WTreeItem(final String _txt, final WTree.TreeItemGUI _content) {
    content_ = _content;
    barre_ = new JPanel();
    barre_.setOpaque(false);
    setLayout(new BorderLayout());
    barre_.setLayout(new BorderLayout());
    _b1 = new BasicArrowButton(SwingConstants.SOUTH);
    _b1.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        toggleItemView();
      }
    });
    _b1.setPreferredSize(new Dimension(30, 30));
    final JPanel _b1panel = new JPanel();
    _b1panel.setOpaque(false);
    _b1panel.add(_b1);
    _b1panel.setBorder(new EmptyBorder(0, 0, 0, 5));
    barre_.add(_b1panel, BorderLayout.WEST);
    _c1 = new JCheckBox("inclure");
    _c1.setHorizontalTextPosition(SwingConstants.LEFT);
    _c1.setOpaque(false);
    _c1.setSelected(true);
    _c1.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        toggleItemEnable();
      }
    });
    barre_.add(_c1, BorderLayout.EAST);
    final JLabel _l1 = new JLabel(_txt);
    _l1.setVerticalAlignment(SwingConstants.CENTER);
    barre_.add(_l1, BorderLayout.CENTER);
    add(barre_, BorderLayout.NORTH);
    add(content_, BorderLayout.CENTER);
    toggleItemView();
    toggleItemEnable();
  }

  protected void toggleItemView(boolean _v) {
    if (!_v) {
      // setPreferredSize(collapsedSize_);
      setMinimumSize(null);
      setMaximumSize(collapsedSize_);
      content_.setVisible(false);
      expanded_ = false;
    } else {
      // setPreferredSize(expandedSize_);
      setMaximumSize(null);
      setMinimumSize(expandedSize_);
      content_.setVisible(true);
      expanded_ = true;
    }
  }

  public void toggleExpand(final boolean _v) {
    toggleItemView(_v);
    toggleItemEnable(_v);
    _c1.setSelected(_v);
  }

  protected void toggleItemView() {
    toggleItemView(!expanded_);
  }

  public void toggleItemEnable(boolean _v) {
    if (!_v) {
      enabled_ = false;
      _b1.setEnabled(false);
      setBorder(new CompoundBorder(new LineBorder(borderColor_, 2, false), new EmptyBorder(2, 2, 2, 2)));
      setBackground(borderColor_);
      toggleItemView(false);
    } else {
      enabled_ = true;
      _b1.setEnabled(true);
      setBorder(new CompoundBorder(new LineBorder(borderColorSelected_, 2, false), new EmptyBorder(2, 2, 2, 2)));
      setBackground(borderColorSelected_);
    }
  }

  protected void toggleItemEnable() {
    toggleItemEnable(!enabled_);
  }

  public OscarFilleNoteDeCalculs.Settings getConfig() {
    _s = content_.getConfig();
    _s.stocke(content_.getBase(), Boolean.valueOf(_c1.isSelected()));
    return _s;
  }
}
