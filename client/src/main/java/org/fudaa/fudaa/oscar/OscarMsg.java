/*
 * @file         OscarMsg.java
 * @creation     2000-10-15
 * @modification $Date: 2007-01-19 11:13:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

/**
 * Librairie Oscar des Messages
 * 
 * @version $Revision: 1.9 $ $Date: 2007-01-19 11:13:21 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public final class OscarMsg {
  // Messages affich�s par l'assistant
  public final static String ASS000 = "Bienvenue !";
  public final static String ASS001 = "Vous pouvez cr�er un\nnouveau projet ou\nen ouvrir un";
  public final static String ASS002 = "";
  public final static String ASS003 = "";
  public final static String ASS004 = "";
  public final static String ASS005 = "";
  public final static String ASS006 = "";
  public final static String ASS007 = "";
  public final static String ASS008 = "";
  public final static String ASS009 = "";
  public final static String ASS010 = "";
  // Messages affich�s dans des boites de dialogues Message ou Confirmation
  public final static String DLG001 = "Vous avez modifi� un param�tre.\nLes r�sultats ne sont plus valides:\nils vont �tre effac�s.\n";
  public final static String DLG002 = "Les param�tres sont charg�s";
  public final static String DLG003 = "Les param�tres sont enregistr�s";
  public final static String DLG004 = "Vous �tes sur le point de fermer le projet <NOMFICHIER>.\nSouhaitez-vous l'enregistrer auparavant ?\n";
  public final static String DLG005 = "Les param�tres sont charg�s";
  public final static String DLG006 = "Les calculs sont termin�s.\nCliquez sur \"Continuer\" pour voir les r�sultats.";
  public final static String DLG007 = "Les calculs sont termin�s.\n \nCertains calculs n'ont pas pu �tre effectu�s \nen raison de la configuration de vos donn�es. \nVous pouvez cependant visualiser les r�sultats \ndisponibles en cliquant sur continuer.";
  public final static String DLG008 = "Vous �tes sur le point de changer de type de calcul.\n \nVoulez-vous utiliser les donn�es relatives au sol\nd�j� saisies ?";
  public final static String DLG009 = "Les calculs n'ont pas pu �tre effectu�s.\n \n<ERREURS>\nCliquez sur continuer pour revenir � la saisie des donn�es.";
  public final static String DLG010 = "Changement de type de calcul";
  public final static String DLG011 = "La validation est termin�e.\n\nVos donn�es ne comportent a priori aucune erreur.\nCliquez sur 'Continuer' pour lancer les calculs.";
  public final static String DLG012 = "La validation est termin�e.\n\nVos donn�es sont incompl�tes pour effectuer l'ensemble des calculs (toutes les donn�es facultatives n'ont pas �t� renseign�es). Vous pouvez cependant lancer les calculs en cliquant sur 'Oui'."
      + " Vous pouvez visualiser le rapport d'avertissements pour plus de d�tails en cliquant sur 'Non'.\n\nVoulez-vous lancer les calculs ?";
  public final static String DLG013 = "Des erreurs bloquantes ont �t� rencontr�es, la validation n'a pas pu �tre effectu�e.\n\nIl est possible que vos donn�es comportent des erreurs ou que vous ayez omis de saisir des donn�es obligatoires.\nVeuillez consultez le rapport d'erreurs ci-dessous pour en connaitre les d�tails.";
  public final static String DLG014 = "Cette fonctionnalit� n'est pas disponible dans la pr�sente version du logiciel.";
  public final static String DLG015 = "Voulez-vous vraiment supprimer le fichier de diagramme suppl�mentaire <FILE> ?";
  public final static String DLG016 = "Exportation termin�e avec succ�s. Le note de calculs vient d'�tre enregistr�e au format HTML dans le fichier <FILE>, les images sont situ�es dans le r�pertoire <IMGDIR>.";
  public final static String DLG017 = "Erreur lors de l'exportation. La note de calculs n'a pas pu �tre export�e. Veuillez r�essayer.";
  public final static String DLG018 = "Impossible d'�crire un fichier temporaire sur le disque. La note de calculs ne peut pas �tre visualis�e. Veuillez r�essayer.";
  public final static String DLG019 = "Vous �tes sur le point de modifier les donn�es. Les r�sultats ne sont plus disponibles. Pour les visualiser � nouveau, vous devrez cliquer sur 'Valider' dans l'onglet 'Validation' apr�s vos �ventuelles modifications.";
  public final static String DLG020 = "Pour visualiser � nouveau les r�sultats et la note de calculs, vous devez cliquer sur 'Valider' dans l'onglet 'Validation'.";
  public final static String DLG021 = "Les calculs sont termin�s.\n\n<ERREURS>\nCliquez sur 'Continuer' pour voir les r�sultats disponibles.";
  // Messages affich�s dans des boites de dialogues Erreur
  public final static String ERR001 = "<001> Vous n'�tes pas connect� � un serveur Oscar !";
  public final static String ERR002 = "<002> Pour que les donn�es relatives aux tirants\n(espacement, section, limite �lastique) soient prises\nen compte, il faut saisir une valeur pour chacun des\nparam�tres. Une saisie partielle ne sera pas\nexploitable.";
  public final static String ERR003 = "<003> mode de calculatrice invalide";
  public final static String ERR004 = "<004> Num�ro de couche introuvable dans le tableau";
  public final static String ERR005 = "<005> Aucune couche dans la plage sp�cifi�e par les indices";
  public final static String ERR006 = "<006> Impossible de r�cup�rer les informations de la couche";
  public final static String ERR007 = "<007> Le nombre de couches dans le tableau a chang�, impossible de r�cup�rer la liste des couches";
  public final static String ERR008 = "<008> Impossible de charger une structure de couches nulle";
  public final static String ERR009 = "<009> Veuillez s�lectionner un type de calcul";
  public final static String ERR010 = "<010> Aucun type de calcul choisi ou type de calcul non valide";
  public final static String ERR011 = "<011> Impossible de redimensionner la fen�tre";
  public final static String ERR012 = "<012> Veuillez s�lectionner un type de calcul";
  public final static String ERR013 = "<013> Num�ro de contrainte introuvable dans le tableau";
  public final static String ERR014 = "<014> Aucune contrainte dans la plage sp�cifi�e par les indices";
  public final static String ERR015 = "<015> Impossible de r�cup�rer les informations de la contrainte";
  public final static String ERR016 = "<016> Le nombre de contraintes dans le tableau a chang�, impossible de r�cup�rer la liste des contraintes";
  public final static String ERR017 = "<017> Impossible de charger une structure de contraintes nulle";
  public final static String ERR018 = "<018> Vous ne pouvez pas ajouter plus de 15 fichiers dans cette version du logiciel";
  public final static String ERR019 = "<019> Num�ro de fichier introuvable dans le tableau";
  public final static String ERR020 = "<020> Aucun fichier dans la plage sp�cifi�e par les indices";
  public final static String ERR021 = "<021> Impossible de r�cup�rer les informations du fichier";
  public final static String ERR022 = "<022> Le nombre de fichier dans le tableau a chang�, impossible de r�cup�rer la liste des fichiers";
  public final static String ERR023 = "<023> Impossible de charger une structure de fichiers nulle";
  public final static String ERR024 = "<024> Connexion non valide, execution du code de calcul impossible";
  public final static String ERR025 = "<025> Aucun param�tre d'entr�e pour le code de calcul";
  public final static String ERR026 = "<026> Aucun r�sultat renvoy� par le code de calcul";
  public final static String ERR027 = "<027> Erreur lors de l'execution du code de calcul";
  public final static String ERR028 = "<028> Impossible de charger un fichier sans nom";
  public final static String ERR029 = "<029> Erreur rencontr�e lors de l'execution du code de calcul";
  public final static String ERR030 = "<030> Num�ro de couple (cote, pression) introuvable dans le tableau";
  public final static String ERR031 = "<031> Aucun couple (cote, pression) dans la plage sp�cifi�e par les indices";
  public final static String ERR032 = "<032> Impossible de r�cup�rer les informations du couple (cote, pression)";
  public final static String ERR033 = "<033> Le nombre de couples (cote, pression) dans le tableau a chang�, impossible de r�cup�rer la liste des couples";
  public final static String ERR034 = "<034> Impossible de charger une structure de couple (cote, pression) nulle";
  public final static String ERR035 = "<035> Num�ro de couple (cote, d�form�e) introuvable dans le tableau";
  public final static String ERR036 = "<036> Aucun couple (cote, d�form�e) dans la plage sp�cifi�e par les indices";
  public final static String ERR037 = "<037> Impossible de r�cup�rer les informations du couple (cote, d�form�e)";
  public final static String ERR038 = "<038> Le nombre de couples (cote, d�form�e) dans le tableau a chang�, impossible de r�cup�rer la liste des couples";
  public final static String ERR039 = "<039> Impossible de charger une structure de couple (cote, d�form�e) nulle";
  public final static String ERR040 = "<040> ";
  // Labels
  public final static String LAB001 = "Veuillez renseigner ces informations compl�mentaires. Vous devez imp�rativement renseigner les donn�es obligatoires.";
  public final static String LAB002 = "Choisissez le mode de calcul. G�n�ralement le calcul est effectu� avec les contraintes effectives, consultez l'aide pour plus de d�tail. Cliquez sur le bouton Dessin pour obtenir une repr�sentation sch�matique des couches de sol.";
  public final static String LAB003 = "La pr�sente version du logiciel Oscar ne permet de prendre en compte que l'action statique de l'eau. Cliquez sur le bouton Dessin pour obtenir une repr�sentation sch�matique du terrain.";
  public final static String LAB004 = "La version actuelle du logiciel Oscar ne permet que la prise en compte automatique d'une surcharge uniforme semi-infinie appliqu�e � partir de la t�te du rideau.";
  public final static String LAB005 = "Veuillez renseigner ces informations compl�mentaires si vous voulez prendre en compte d'autres efforts lors du calcul.";
  public final static String LAB006 = "L'eau doit �tre comprise entre le haut du sol et le bas du sol (reportez-vous aux donn�es saisies dans l'onglet 'Sol').";
  public final static String LAB007 = "Cote de la nappe c�t� pouss�e";
  public final static String LAB008 = "Cote de la nappe c�t� but�e";
  public final static String LAB009 = "Le calcul est diff�rent selon que l'on �tudie le comportement de l'ouvrage en contraintes effectives ou en contraintes totales.";
  public final static String LAB010 = "Pour lancer les calculs, vous devez valider la pr�sente fen�tre d'entr�e des donn�es.";
  public final static String LAB011 = "Pour valider les donn�es que vous avez d�j� saisies, cliquez sur le bouton 'Valider'. Si aucune erreur n'est d�tect�e, les calculs seront automatiquement lanc�s et vous pourrez alors visualiser les r�sultats en allant dans le menu 'R�sultats'. Si des erreurs sont d�tect�es, une liste vous en sera donn�e et vous devrez les corriger avant de lancer � nouveau les calculs.\nPour sauvegarder ces donn�es uniquement, sans les valider, et fermer le fichier projet, cliquez sur 'Fermer'.";
  public final static String LAB012 = "Vous trouverez ici les r�sultats concernant le diagramme de pression calcul� par rapport � vos donn�es. Vous pouvez imprimer, exporter ou visualiser la courbe de ce tableau.";
  public final static String LAB013 = "Vous trouverez ici les r�sultats concernant le pr�dimensionnement. Vous pouvez imprimer ou exporter ce tableau.";
  public final static String LAB014 = "Vous trouverez ici les r�sultats concernant la d�form�e. Vous pouvez imprimer, exporter ou visualiser la courbe de ce tableau.";
  public final static String LAB015 = "Vous trouverez ici les r�sultats concernant les efforts tranchants. Vous pouvez imprimer, exporter ou visualiser la courbe de ce tableau.";
  public final static String LAB016 = "Vous trouverez ici les r�sultats concernant les moments fl�chissants. Vous pouvez imprimer, exporter ou visualiser la courbe de ce tableau.";
  public final static String LAB017 = "Voici les r�sultats bruts calcul�s � partir de vos donn�es.\nPour imprimer l'int�gralit� des r�sultats bruts, cliquez sur le bouton 'Imprimer'.\nPour les sauvegarder au format HTML, cliquez sur le bouton 'Exporter'.\nVous pouvez �galement g�n�rer une note de calculs compl�te (donn�es et r�sultats) en cliquant sur le bouton 'Note' ci-dessus.";
  public final static String LAB018 = "Cet outil permet de calculer rapidement le coefficient de ";
  public final static String LAB019 = " projet� sur la normale � l'�cran.\nLes limites de cette calculatrice sont indiqu�es dans l'aide.";
  public final static String LAB020 = "Valeur du coefficient ";
  public final static String LAB021 = "Angle de frottement interne";
  public final static String LAB022 = "Obliquit� de la contrainte";
  public final static String LAB023 = "Inclinaison du terre-plein";
  public final static String LAB024 = "Inclinaison du parement";
  public final static String LAB025 = "non calculable";
  public final static String LAB026 = "Cote du toit du sol c�t� pouss�e";
  public final static String LAB027 = "Apr�s avoir choisi l'option de calcul ci-dessus, vous devez entrer les propri�t�s g�otechniques de chaque c�t� du rideau (en pouss�e et en but�e).";
  public final static String LAB028 = "Informations concernant le sol en pouss�e";
  public final static String LAB029 = "Informations concernant le sol en but�e";
  public final static String LAB030 = "Cote du toit du sol c�t� but�e";
  public final static String LAB031 = "Ces donn�es sont facultatives. Elles permettront de fournir une note de calculs plus d�taill�e. Cliquez sur l'aide pour plus d'informations.";
  public final static String LAB032 = "Si l'�cran de sout�nement est un rideau de palplanches, entrez leurs caract�ristiques.";
  public final static String LAB033 = "Module d'Young de l'acier";
  public final static String LAB034 = "Limite �lastique de l'acier";
  public final static String LAB035 = "Inertie des palplanches";
  public final static String LAB036 = "Demi-hauteur";
  public final static String LAB037 = "Vous pouvez entrer les caract�ristiques suivantes de la nappe d'ancrage.";
  public final static String LAB038 = "Espacement entre deux tirants";
  public final static String LAB039 = "Section d'un tirant";
  public final static String LAB040 = "Limite �lastique de l'acier";
  public final static String LAB041 = "pour E * I =";
  public final static String LAB042 = "S�lectionnez et faites varier vos crit�res d'incertitudes pour obtenir les r�sultats associ�s.";
  public final static String LAB043 = "cote de la nappe d'eau c�t� pouss�e";
  public final static String LAB044 = "cote du toit du sol c�t� but�e";
  public final static String LAB045 = "coefficient sur les propri�t�s g�otechniques";
  public final static String LAB046 = "surcharge uniforme semi-infinie";
  public final static String LAB047 = "Informations concernant la surcharge uniforme semi-infinie";
  public final static String LAB048 = "Informations concernant la surcharge uniforme en bande";
  public final static String LAB049 = "Informations concernant la surcharge lin�ique";
  public final static String LAB050 = "Valeur de la surcharge";
  public final static String LAB051 = "Informations concernant les contraintes verticales suppl�mentaires";
  public final static String LAB052 = "Informations concernant les contraintes horizontales suppl�mentaires";
  public final static String LAB053 = "Informations concernant le efforts en t�te de rideau";
  public final static String LAB054 = "Ajout de fichiers de diagrammes suppl�mentaires.";
  public final static String LAB054_2 ="Disponible dans une version ult�rieure de <b>Fudaa-Oscar </b>";
  public final static String LAB055 = "Ajouter ici les informations concernant les contraintes verticales suppl�mentaires. Attention : vous devez saisir les couples de valeurs dans un ordre d�croissant par rapport aux cotes. De plus, il convient de renseigner au moins deux lignes (voir l'aide pour plus d'explications).";
  public final static String LAB056 = "Ajouter ici les informations concernant les contraintes horizontales suppl�mentaires. Attention : vous devez saisir les couples de valeurs dans un ordre d�croissant par rapport aux cotes. De plus, il convient de renseigner au moins deux lignes (voir l'aide pour plus d'explications).";
  public final static String LAB057 = "Afin de prendre en compte d'�ventuels efforts d'amarrage, vous pouvez entrer des efforts en t�te de rideau.";
  public final static String LAB058 = "Force en t�te";
  public final static String LAB059 = "Moment en t�te";
  public final static String LAB060 = "Cliquez sur Ajouter puis entrez le nom du fichier contenant le diagramme pertinent ou cliquez sur Parcourir pour rechercher le fichier.";
  public final static String LAB061 = "Pr�cisez ici si l'ouvrage dispose d'une nappe de tirants. Par d�faut, le rideau n'est pas ancr�. Pour renseigner plus d'informations relatives aux palplanches et aux �ventuels tirants, rendez-vous dans le sous-onglet 'Donn�es Facultatives'.";
  public final static String LAB062 = "Pr�sence d'une nappe de tirants";
  public final static String LAB063 = "Cote des tirants :";
  public final static String LAB064 = "Pourcentage d'encastrement :";
  public final static String LAB065 = "Veuillez choisir les options de personnalisation de votre note de calculs. Ajoutez ou supprimez une partie en cochant ou d�cochant 'inclure' ; vous pouvez acc�der aux options de personnalisation de chacune des parties en cliquant sur la petite fl�che-bouton � gauche du titre de la partie. Ce bouton vous permettra de d�plier/replier les options de personnalisation concernant la partie en question. Lorsque vous aurez termin� de personnaliser votre note de calculs, cliquez sur l'un des boutons 'G�n�rer' ou 'Exporter'. Le bouton 'Exporter' vous permettra de sauvegarder la note de calculs dans un fichier HTML.";
  public final static String LAB066 = "Catalogue de palplanches";
  public final static String LAB067 = "Cette calculatrice permet de calculer la section d'un tirant en fonction de son diam�tre.";
  public final static String LAB068 = "Diam�tre";
  public final static String LAB069 = "d'o� la section";
  public final static String LAB070 = "non calculable";
  public final static String LAB071 = "Le coefficient Ka est calcul� par la formule de Poncelet-Prandtl-Rankine-M�ller-Breslau. Comme expliqu� dans l'aide, il est pr�f�rable d'utiliser les valeurs issues des tables de Caquot, K�risel et Absi. Cette calculatrice donne toutefois un ordre de grandeur du coefficient Ka projet� sur la normale � l'�cran (valeur proche du coefficient issu des tables, une fois projet�) dans certains cas particuliers.";
  public final static String LAB072 = "Le coefficient Kp est calcul� par la formule de Poncelet-Prandtl-Rankine-M�ller-Breslau. Comme expliqu� dans l'aide, il est pr�f�rable d'utiliser les valeurs issues des tables de Caquot, K�risel et Absi. Cette calculatrice donne toutefois un ordre de grandeur du coefficient Kp projet� sur la normale � l'�cran (valeur proche du coefficient issu des tables, une fois projet�) dans certains cas particuliers.";
  // Tool tip text
  public final static String TIPTEXT001 = "R�sultats concernant le diagramme des pressions";
  public final static String TIPTEXT002 = "R�sultats de pr�dimensionnement";
  public final static String TIPTEXT003 = "R�sultats concernant la d�form�e";
  public final static String TIPTEXT004 = "R�sultats concernant les efforts tranchants";
  public final static String TIPTEXT005 = "R�sultats concernant les moments fl�chissants";
  public final static String TIPTEXT006 = "Informations concernant le sol";
  public final static String TIPTEXT007 = "Informations concernant l'eau";
  public final static String TIPTEXT008 = "Informations concernant les surcharges";
  public final static String TIPTEXT009 = "Informations concernant les autres efforts";
  public final static String TIPTEXT010 = "Informations concernant l'ouvrage";
  public final static String TIPTEXT011 = "Validation des donn�es saisies";
  public final static String TIPTEXT012 = "<html>L'onglet Incertitudes sera activ� <br>dans une prochaine version de <b>Fudaa-Oscar</b></br></html>";
  public final static String TIPTEXT013 = "Informations concernant les donn�es obligatoires";
  public final static String TIPTEXT014 = "Informations concernant les donn�es facultatives";
  public final static String TIPTEXT015 = "";
  public final static String TIPTEXT016 = "";
  public final static String TIPTEXT017 = "";
  public final static String TIPTEXT018 = "";
  public final static String TIPTEXT019 = "";
  public final static String TIPTEXT020 = "";
  // Labels d'onglets
  public final static String TABLAB001 = "Pouss�e";
  public final static String TABLAB002 = "But�e";
  public final static String TABLAB003 = "Diagramme des pressions";
  public final static String TABLAB004 = "Pr�dimensionnement";
  public final static String TABLAB005 = "D�form�e";
  public final static String TABLAB006 = "Efforts tranchants";
  public final static String TABLAB007 = "Moments fl�chissants";
  public final static String TABLAB008 = "Sol";
  public final static String TABLAB009 = "Eau";
  public final static String TABLAB010 = "Surcharge";
  public final static String TABLAB011 = "Autres efforts";
  public final static String TABLAB012 = "Ouvrage";
  public final static String TABLAB013 = "Validation";
  public final static String TABLAB014 = "Incertitudes";
  public final static String TABLAB015 = "Uniforme Semi-Infinie";
  public final static String TABLAB016 = "Uniforme en Bande";
  public final static String TABLAB017 = "Lin�ique";
  public final static String TABLAB018 = "Contraintes Verticales";
  public final static String TABLAB019 = "Contraintes Horizontales";
  public final static String TABLAB020 = "Efforts en t�te de rideau";
  public final static String TABLAB021 = "Fichiers de diagrammes";
  public final static String TABLAB022 = "Donn�es Obligatoires";
  public final static String TABLAB023 = "Donn�es Facultatives";
  public final static String TABLAB024 = "";
  public final static String TABLAB025 = "";
  public final static String TABLAB026 = "";
  public final static String TABLAB027 = "";
  public final static String TABLAB028 = "";
  public final static String TABLAB029 = "";
  public final static String TABLAB030 = "";
  // Radio Button Labels
  public final static String RBLAB001 = "Hydrostatique";
  public final static String RBLAB002 = "Hydrodynamique";
  public final static String RBLAB003 = "Contraintes effectives";
  public final static String RBLAB004 = "Contraintes totales";
  public final static String RBLAB005 = "";
  public final static String RBLAB006 = "";
  public final static String RBLAB007 = "";
  public final static String RBLAB008 = "";
  public final static String RBLAB009 = "";
  public final static String RBLAB010 = "";
  // Labels d'unit�s de mesures
  public final static String UNITLAB_POURCENT = "%";
  public final static String UNITLAB_METRE = "m";
  public final static String UNITLAB_MILLIMETRE = "mm";
  public final static String UNITLAB_CENTIMETRE = "cm";
  public final static String UNITLAB_DEGRE = "degr�";
  public final static String UNITLAB_CENTIMETRE_CARRE = "cm�";
  public final static String UNITLAB_METRE_CARRE = "m�";
  public final static String UNITLAB_MEGA_PASCAL = "MPa";
  public final static String UNITLAB_CENTIMETRE_4_PAR_METRE_LINEAIRE = "cm4/ml";
  public final static String UNITLAB_KILO_NEWTON_METRE_CARRE = "kN.m�";
  public final static String UNITLAB_KILO_NEWTON_PAR_METRE_CARRE = "kN/m�";
  public final static String UNITLAB_KILO_NEWTON_PAR_METRE_LINEAIRE = "kN/ml";
  public final static String UNITLAB_KILO_NEWTON_METRE_PAR_METRE_LINEAIRE = "kN.m/ml";
  // Labels de symboles math�matiques
  public final static String MATHLAB_EQUAL = " = ";
  // Labels des titres de blocs et de fen�tre
  public final static String TITLELAB_INFO = "Information";
  public final static String TITLELAB001 = "Type de calcul";
  public final static String TITLELAB002 = "Action statique de l'eau";
  public final static String TITLELAB003 = "Action dynamique de l'eau";
  public final static String TITLELAB004 = "Calculatrice";
  public final static String TITLELAB005 = "Propri�t�s";
  public final static String TITLELAB006 = "Valeur du Ka";
  public final static String TITLELAB007 = "Valeur du Kp";
  public final static String TITLELAB008 = "Cote du sol";
  public final static String TITLELAB009 = "Couches";
  public final static String TITLELAB010 = "Type de calcul";
  public final static String TITLELAB011 = "Palplanches";
  public final static String TITLELAB012 = "Tirants";
  public final static String TITLELAB013 = "R�sultats bruts";
  public final static String TITLELAB014 = "Saisie des donn�es";
  public final static String TITLELAB015 = "Diagramme des pressions";
  public final static String TITLELAB016 = "D�form�e";
  public final static String TITLELAB017 = "Note de calculs";
  public final static String TITLELAB018 = "Incertitudes";
  public final static String TITLELAB019 = "Surcharge";
  public final static String TITLELAB020 = "Contraintes verticales suppl�mentaires";
  public final static String TITLELAB021 = "Contraintes horizontales suppl�mentaires";
  public final static String TITLELAB022 = "Efforts en t�te de rideau";
  public final static String TITLELAB023 = "Fichiers de diagrammes suppl�mentaires";
  public final static String TITLELAB024 = "Nappe de Tirants";
  public final static String TITLELAB025 = "Validation";
  public final static String TITLELAB026 = "Moments fl�chissants";
  public final static String TITLELAB027 = "Efforts tranchants";
  public final static String TITLELAB028 = "Pr�dimensionnement";
  public final static String TITLELAB029 = "Diam�tre du tirant";
  public final static String TITLELAB030 = "Section du tirant";
  public final static String TITLELAB031 = "Calculatrice Section d'un tirant";
  // Divers
  public final static String PUCE1 = " -> ";
  public final static String PUCE2 = "";
  public final static String PUCE3 = "";
  public final static String PUCE4 = "";
  public final static String PUCE5 = "";
  public final static String PUCE6 = "";
  public final static String PUCE7 = "";
  public final static String PUCE8 = "";
  public final static String PUCE9 = "";
  // Icons keywords
  public final static String ICO001 = "notedecalculs";
  public final static String ICO002 = "parametres";
  public final static String ICO003 = "resultats";
  // URL
  public final static String URL001 = "d-calculette-k";
  public final static String URL032 = "d-calculatrice-section";
  public final static String URL002 = "p-parametres-sol";
  public final static String URL017 = "p-parametres-sol-poussee";
  public final static String URL018 = "p-parametres-sol-butee";
  public final static String URL003 = "annexe-legende-tableau-couches#poussee";
  public final static String URL004 = "annexe-legende-tableau-couches#butee";
  public final static String URL013 = "p-parametres-sol-calcul";
  public final static String URL006 = "p-parametres-eau";
  public final static String URL007 = "p-parametres-surcharges";
  public final static String URL019 = "p-parametres-surcharges-l";
  public final static String URL020 = "p-parametres-surcharges-ub";
  public final static String URL021 = "p-parametres-surcharges-usi";
  public final static String URL008 = "p-parametres-efforts";
  public final static String URL024 = "p-parametres-efforts-fd";
  public final static String URL025 = "p-parametres-efforts-etr";
  public final static String URL026 = "p-parametres-efforts-chs";
  public final static String URL027 = "p-parametres-efforts-cvs";
  public final static String URL005 = "p-parametres-ouvrage";
  public final static String URL022 = "p-parametres-ouvrage-do";
  public final static String URL023 = "p-parametres-ouvrage-df";
  public final static String URL009 = "p-parametres-validation";
  public final static String URL010 = "p-parametres-incertitudes";
  public final static String URL011 = "p-resultatsbruts-deformee";
  public final static String URL012 = "p-resultatsbruts-pressions";
  public final static String URL014 = "p-resultatsbruts-predim";
  public final static String URL015 = "p-resultatsbruts-efforts";
  public final static String URL016 = "p-resultatsbruts-moments";
  public final static String URL028 = "annexe-catalogue-palplanches";
  public final static String URL034 = "annexe-messages-erreurs";
  public final static String URL029 = "p-resultatsbruts";
  public final static String URL030 = "p-parametres";
  public final static String URL031 = "p-notedecalculs";
  public final static String URL033 = "bienvenue";
  // Format de champs num�rique
  /** OscarOuvrageParametresDonneesFacultatives : module d'Young du rideau de palplanches */
  public final static String FORMAT001 = "#0.00";
  /** OscarOuvrageParametresDonneesFacultatives : limite �lastique de l'acier rideau de palplanches */
  public final static String FORMAT002 = "#0.00";
  /** OscarOuvrageParametresDonneesFacultatives : inertie des palplanches */
  public final static String FORMAT003 = "#0.00";
  /** OscarOuvrageParametresDonneesFacultatives : demie-hauteur des palplanches */
  public final static String FORMAT004 = "#0.00";
  /** OscarOuvrageParametresDonneesFacultatives : espace entre deux tirants */
  public final static String FORMAT005 = "#0.00";
  /** OscarOuvrageParametresDonneesFacultatives : section d'un tirant */
  public final static String FORMAT006 = "#0.00";
  /** OscarOuvrageParametresDonneesFacultatives : limite �lastique de l'acier des tirants */
  public final static String FORMAT007 = "#0.00";
  /** OscarDeformeeResultatsBruts : e * i */
  public final static String FORMAT008 = "#0.00";
  /** OscarSolParametresSolEnPoussee : cote du toit du sol c�t� pouss�e */
  public final static String FORMAT009 = "#0.00";
  /** OscarSolParametresSolEnButee : cote du toit du sol c�t� but�e */
  public final static String FORMAT010 = "#0.00";
  /** OscarEauParametres : cote de la nappe d'eau c�t� pouss�e */
  public final static String FORMAT011 = "#0.00";
  /** OscarEauParametres : cote de la nappe d'eau c�t� but�e */
  public final static String FORMAT012 = "#0.00";
  // Valeur initiale de champs num�rique
  /** OscarOuvrageParametresDonneesFacultatives : module d'Young du rideau de palplanches */
  public final static double INITIALVALUE001 = 210000D;
  /** OscarDeformeeResultatsBruts : e * i */
  public final static double INITIALVALUE002 = 1000D;
  /** OscarEauParametres : cote de la nappe c�t� pouss�e */
  public final static double INITIALVALUE003 = 0D;
  /** OscarEauParametres : cote de la nappe c�t� but�e */
  public final static double INITIALVALUE004 = 0D;
  /** OscarSurchargesParametresSurchargeUniformeSemiInfinie : valeur de la surcharge */
  public final static double INITIALVALUE005 = 0D;
  /** OscarEffortsParametresEffortEnTeteDeRideau : force en t�te */
  public final static double INITIALVALUE006 = 0D;
  /** OscarEffortsParametresEffortEnTeteDeRideau : moment en t�te */
  public final static double INITIALVALUE007 = 0D;
  /** OscarOuvrageParametresDonneesObligatoires : cote en pouss�e */
  public final static double INITIALVALUE008 = 0D;
  /** OscarOuvrageParametresDonneesObligatoires : pourcentage d'encastrement */
  public final static double INITIALVALUE009 = 100D;
  // Messages affich�s dans l'afficheur de messages
  // public final static String MSG001 = OscarLib.makeMessageString("PARAMETRES","Sol","choisir un type\nde calcul");
  // public final static String MSG003 = OscarLib.makeMessageString("PARAMETRES","Sol","ajouter des couches\nen but�e");
  // public final static String MSG010 = "";
  // Cat�gories de messages pour la validation (correspond aux diff�rents onglets possible)
  public final static String VMSGCAT001 = "Sol";
  public final static String VMSGCAT002 = VMSGCAT001 + " (pouss�e)";
  public final static String VMSGCAT003 = VMSGCAT001 + " (but�e)";
  public final static String VMSGCAT004 = "Eau";
  public final static String VMSGCAT005 = "Surcharges";
  public final static String VMSGCAT006 = VMSGCAT005 + " (unif. semi-inf.)";
  public final static String VMSGCAT007 = VMSGCAT005 + " (unif. en bande)";
  public final static String VMSGCAT008 = VMSGCAT005 + " (lin�ique)";
  public final static String VMSGCAT009 = "Efforts";
  public final static String VMSGCAT010 = VMSGCAT009 + " (C.V.S)";
  public final static String VMSGCAT011 = VMSGCAT009 + " (C.H.S)";
  public final static String VMSGCAT012 = VMSGCAT009 + " (t. de rid.)";
  public final static String VMSGCAT013 = VMSGCAT009 + " (f. de diag.)";
  public final static String VMSGCAT014 = "Ouvrage";
  public final static String VMSGCAT015 = VMSGCAT014 + " (donn�es obl.)";
  public final static String VMSGCAT016 = VMSGCAT014 + " (donn�es fac.)";
  public final static String VMSGCAT017 = "Incertitudes";
  // Messages pour la validation des onglets
  // public final static VMessage VMSG001 = new VMessage(1,VMSGCAT009,"Contraintes Verticales","doivent �tre
  // d�croissantes","les contraintes verticales doivent �tre ordonn�es de fa�on d�croissante par rapport aux
  // cotes.",VMessage.FATAL_ERROR);
  // public final static VMessage VMSG002 = new VMessage(2,VMSGCAT004,"trucmuche","Manquant","La saisie de ces donn�es
  // n'est pas obligatoires",VMessage.WARNING);
  public final static VMessage VMSG003 = new VMessage(3, VMSGCAT001, "Type de calcul", "� choisir",
      "vous devez choisir un type de calcul", VMessage.FATAL_ERROR);
  public final static VMessage VMSG004 = new VMessage(4, VMSGCAT004, "Type de calcul", "� choisir",
      "voud devez choisir un type de calcul", VMessage.FATAL_ERROR);
  public final static VMessage VMSG005 = new VMessage(5, VMSGCAT004, "Cote nappe d'eau c�t� pouss�e", "manquante",
      "vous devez saisir la cote de la nappe d'eau c�t� pouss�e", VMessage.FATAL_ERROR);
  public final static VMessage VMSG006 = new VMessage(6, VMSGCAT004, "Cote nappe d'eau c�t� but�e", "manquante",
      "vous devez saisir la cote de la nappe d'eau c�t� but�e", VMessage.FATAL_ERROR);
  public final static VMessage VMSG007 = new VMessage(7, VMSGCAT004, "Cote nappe d'eau c�t� pouss�e", "non valide",
      "la cote de la nappe d'eau c�t� pouss�e que vous avez saisie est hors limite", VMessage.FATAL_ERROR);
  public final static VMessage VMSG008 = new VMessage(8, VMSGCAT004, "Cote nappe d'eau c�t� but�e", "non valide",
      "la cote de la nappe d'eau c�t� but�e que vous avez saisie est hors limite", VMessage.FATAL_ERROR);
  public final static VMessage VMSG009 = new VMessage(9, VMSGCAT009, "Contraintes verticales", "non valide",
      "les cotes des contraintes verticales doivent �tre d�croissantes", VMessage.FATAL_ERROR);
  public final static VMessage VMSG010 = new VMessage(10, VMSGCAT002, "Couches en pouss�e", "manquantes",
      "vous devez ajouter des couches en pouss�e", VMessage.FATAL_ERROR);
  public final static VMessage VMSG011 = new VMessage(11, VMSGCAT003, "Couches en but�e", "manquantes",
      "vous devez ajouter des couches en but�e", VMessage.FATAL_ERROR);
  public final static VMessage VMSG013 = new VMessage(13, VMSGCAT002, "Cote toit du sol", "manquante",
      "vous devez saisir une valeur pour la cote du toit du sol c�t� pouss�e.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG014 = new VMessage(14, VMSGCAT003, "Cote toit du sol", "manquante",
      "vous devez saisir une valeur pour la cote du toit du sol c�t� but�e.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG015 = new VMessage(15, VMSGCAT002, "Epaisseur de couche", "doit �tre > 0",
      "les �paisseurs des couches de sol en pouss�e doivent �tre sup�rieure strictement � 0 m�tre.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG016 = new VMessage(
      16,
      VMSGCAT002,
      "Propri�t�s des couches",
      "manquantes",
      "vous devez saisir l'int�gralit� des propri�t�s des couches de sol. Autrement dit, toutes les cellules du tableau des couches doivent �tre remplie.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG017 = new VMessage(17, VMSGCAT002, "Propri�t�s des couches", "doivent �tre >= 0",
      "les propri�t�s des couches de sol doivent toutes avoir des valeurs positives ou nulles.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG018 = new VMessage(18, VMSGCAT003, "Epaisseur de couche", "doit �tre > 0",
      "les �paisseurs des couches de sol en pouss�e doivent �tre sup�rieure strictement � 0 m�tre.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG019 = new VMessage(
      19,
      VMSGCAT003,
      "Propri�t�s des couches",
      "manquante(s)",
      "vous devez saisir l'int�gralit� des propri�t�s des couches de sol. Autrement dit, toutes les cellules du tableau des couches doivent �tre remplie.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG020 = new VMessage(20, VMSGCAT003, "Propri�t�s des couches", "doivent �tre >= 0",
      "les propri�t�s des couches de sol doivent toutes avoir des valeurs positives ou nulles.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG021 = new VMessage(
      21,
      VMSGCAT013,
      "Fichier de diagramme",
      "introuvable",
      "veuillez v�rifier l'existence des fichiers de diagramme suppl�mentaires que vous avez ajout�. Certains fichiers semblent ne pas exister � l'endroit sp�cifi�.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG022 = new VMessage(22, VMSGCAT013, "Fichier de diagramme", "non sp�cifi�",
      "veuillez supprimer toutes les cellules vides dans le tableau des fichiers de diagramme suppl�mentaires.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG023 = new VMessage(
      23,
      VMSGCAT010,
      "Cote de la contrainte",
      "en dehors de l'intervalle [cote bas du sol;cote haut du sol]",
      "veuillez v�rifier vos donn�es concernant les contraintes verticales suppl�mentaires. Certaines cotes de ces valeurs sont situ�es en dehors des limites de l'intervalle [cote du bas du sol;cote du haut du sol].",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG024 = new VMessage(
      24,
      VMSGCAT010,
      "Cote de la contrainte",
      "non d�croissante",
      "les cotes des contraintes doivent �tre saisies dans l'ordre d�croissant dans le tableau des contraintes verticales suppl�mentaires.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG025 = new VMessage(25, VMSGCAT010, "Cote ou valeur de la contrainte",
      "manquante(s)", "vous devez remplir toutes les cases du tableau des contraintes verticales suppl�mentaires.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG026 = new VMessage(
      26,
      VMSGCAT010,
      "Nombre de contraintes",
      "doit �tre >= 2",
      "veuillez saisir un minimum de 2 couples (cote;contraintes) dans le tableau des contraintes verticales suppl�mentaires, ou alors ne saisissez aucune contraintes pour ne pas prendre en compte de contraintes verticales suppl�mentaires.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG027 = new VMessage(
      27,
      VMSGCAT011,
      "Cote de la contrainte",
      "en dehors de l'intervalle [cote bas du sol;cote haut du sol]",
      "veuillez v�rifier vos donn�es concernant les contraintes horizontales suppl�mentaires. Certaines cotes de ces valeurs sont situ�es en dehors des limites de l'intervalle [cote du bas du sol;cote du haut du sol].",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG028 = new VMessage(
      28,
      VMSGCAT011,
      "Cote de la contrainte",
      "non d�croissante",
      "les cotes des contraintes doivent �tre saisies dans l'ordre d�croissant dans le tableau des contraintes horizontales suppl�mentaires.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG029 = new VMessage(29, VMSGCAT011, "Cote ou valeur de la contrainte",
      "manquante(s)", "vous devez remplir toutes les cases du tableau des contraintes horizontales suppl�mentaires.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG030 = new VMessage(
      30,
      VMSGCAT011,
      "Nombre de contraintes",
      "doit �tre >= 2",
      "veuillez saisir un minimum de 2 couples (cote;contraintes) dans le tableau des contraintes horizontales suppl�mentaires, ou alors ne saisissez aucune contraintes pour ne pas prendre en compte de contraintes horizontales suppl�mentaires.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG031 = new VMessage(
      31,
      VMSGCAT001,
      "Bas du sol c�t� pouss�e et c�t� but�e",
      "doit correspondre",
      "veuillez v�rifier vos couches en pouss�e et en but�e ainsi que les cotes des terre-pleins c�t� pouss�e et c�t� but�e, car la cote du bas du sol c�t� pouss�e (cote T.P pouss�e - �paisseur des couches pouss�e) doit �tre �gale � la cote du bas du sol c�t� but�e (cote T.P but�e - �paisseur des couches but�e).",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG032 = new VMessage(
      32,
      VMSGCAT013,
      "nombre de points dans fichier",
      "doit �tre >= 2",
      "veuillez v�rifier que vos fichiers de diagrammes suppl�mentaires contiennent bien chacun au moins deux points (cote ; pressions).",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG033 = new VMessage(
      33,
      VMSGCAT013,
      "cote dans fichier",
      "doivent �tre d�croissantes",
      "veuillez v�rifier que vos fichiers de diagrammes suppl�mentaires contiennent bien un ensemble de points (cote ; pression) dont les valeurs des cotes sont fournies dans un ordre d�croissant.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG034 = new VMessage(
      34,
      VMSGCAT016,
      "donn�es relatives aux palplanches",
      "sont insuffisantes pour �tre prises en compte",
      "Pour que les donn�es relatives aux palplanches (module d'Young, limite �lastique, inertie, demi-hauteur) soient prises en compte, il faut saisir une valeur pour chacun de ces quatres param�tres. Une saisie partielle ne sera pas exploitable.",
      VMessage.WARNING);
  public final static VMessage VMSG035 = new VMessage(
      35,
      VMSGCAT016,
      "donn�es relatives aux tirants",
      "doivent �tre toutes renseign�es pour �tre prises en compte",
      "Pour que les donn�es relatives aux tirants (espacement, section, limite �lastique) soient prises en compte, il faut saisir une valeur pour chacun de ces trois param�tres. Une saisie partielle ne sera pas exploitable.",
      VMessage.WARNING);
  public final static VMessage VMSG036 = new VMessage(36, VMSGCAT006, "valeur de la surcharge",
      "doit �tre saisie pour �tre prise en compte",
      "pour que la surcharge uniforme semi-infinie soit pris en compte vous devez saisir une valeur de surcharge.",
      VMessage.WARNING);
  public final static VMessage VMSG037 = new VMessage(37, VMSGCAT006, "valeur de la surcharge", "doit �tre >= 0",
      "veuillez saisir une valeur de surcharge uniforme semi-infinie sup�rieure ou �gale � 0.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG038 = new VMessage(38, VMSGCAT012, "force en t�te",
      "doit �tre saisie pour �tre prise en compte",
      "pour que la force en t�te soit prise en compte vous devez saisir une valeur.", VMessage.WARNING);
  public final static VMessage VMSG039 = new VMessage(39, VMSGCAT012, "Force en t�te", "doit �tre >= 0",
      "veuillez saisir une valeur de force en t�te sup�rieure ou �gale � 0.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG040 = new VMessage(40, VMSGCAT012, "Moment en t�te",
      "doit �tre saisie pour �tre pris en compte",
      "pour que le moment en t�te soit pris en compte vous devez saisir une valeur.", VMessage.WARNING);
  public final static VMessage VMSG041 = new VMessage(41, VMSGCAT012, "force en t�te", "doit �tre >= 0",
      "veuillez saisir une valeur de moment en t�te sup�rieure ou �gale � 0.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG042 = new VMessage(
      42,
      VMSGCAT015,
      "cote des tirants",
      "manquante",
      "Pour que les donn�es relatives aux tirants (cote, encastrement) soient prises en compte, il faut saisir une valeur pour la cote des tirants.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG043 = new VMessage(
      43,
      VMSGCAT015,
      "pourcentage d'encastrement des tirants",
      "manquant",
      "Pour que les donn�es relatives aux tirants (cote, encastrement) soient prises en compte, il faut saisir une valeur pour l'encastrement de la nappe de tirants.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG044 = new VMessage(44, VMSGCAT015, "pourcentage d'encastrement",
      "doit �tre compris entre 0 et 100 %",
      "Vous devez saisir un pourcentage d'encastrement de la nappe de tirants compris entre 0 et 100 % (inclus).",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG045 = new VMessage(45, VMSGCAT015, "cote de la nappe de tirants", "hors limite",
      "Vous devez saisir une cote de la nappe de tirants situ�e entre la cote du bas du sol et la cote du haut du sol",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG047 = new VMessage(47, VMSGCAT017, "cote de la nappe d'eau c�t� pouss�e",
      "manquante", "Vous devez saisir une cote de nappe d'eau c�t� pouss�e pour prendre en compte l'incertitude",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG048 = new VMessage(48, VMSGCAT017, "cote du toit du sol c�t� but�e", "manquante",
      "Vous devez saisir une cote du toit du sol c�t� but�e pour prendre en compte l'incertitude", VMessage.FATAL_ERROR);
  public final static VMessage VMSG049 = new VMessage(49, VMSGCAT017, "coefficient sur les propri�t�s g�otechniques",
      "manquant",
      "Vous devez saisir un coefficient sur les propri�t�s g�otechniques pour prendre en compte l'incertitude",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG050 = new VMessage(50, VMSGCAT017, "surcharge uniforme semi-infinie", "manquante",
      "Vous devez saisir une valeur pour la surcharge uniforme semi-infinie pour prendre en compte l'incertitude",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG051 = new VMessage(
      51,
      VMSGCAT013,
      "fichier de diagramme suppl�mentaire",
      "doit contenir deux colonnes s�par�es par un point virgule",
      "Vous devez v�rifier que le(s) fichier(s) de diagramme suppl�mentaire(s) contiennent bien deux colonnes de donn�es s�par�es par un point virgule.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG052 = new VMessage(52, VMSGCAT017, "cote de la nappe d'eau c�t� pouss�e",
      "en dehors de l'intervalle [cote bas du sol;cote haut du sol]",
      "veuillez v�rifier vos donn�es concernant l'incertitude sur cette valeur.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG053 = new VMessage(53, VMSGCAT017, "cote du toit du sol c�t� but�e",
      "en dehors de l'intervalle [cote bas du sol c�t� pouss�e;cote haut du sol c�t� pouss�e]",
      "veuillez v�rifier vos donn�es concernant l'incertitude sur cette valeur.", VMessage.FATAL_ERROR);
  public final static VMessage VMSG054 = new VMessage(54, VMSGCAT017, "coefficient sur les propri�t�s g�otechniques",
      "doit �tre sup�rieur ou �gal � 1", "veuillez v�rifier vos donn�es concernant l'incertitude sur cette valeur.",
      VMessage.FATAL_ERROR);
  public final static VMessage VMSG055 = new VMessage(55, VMSGCAT017, "surcharge uniforme semi-infinie",
      "doit �tre sup�rieure ou �gale � 0 kN/m�",
      "veuillez v�rifier vos donn�es concernant l'incertitude sur cette valeur.", VMessage.FATAL_ERROR);
}
