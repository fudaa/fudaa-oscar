/*
 * @file         OscarDialogConfirmation.java
 * @creation     2002-10-23
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;

/**
 * une bo�te de dialogue de confirmation : oui, non, annuler.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarDialogConfirmation extends BuDialog {
  /**
   *
   */
  BuCommonInterface parent_;
  /**
   * bouton 'Aide'.
   */
  protected BuButton btAide_;
  /**
   * bouton 'Oui'.
   */
  protected BuButton btOui_;
  /**
   * bouton 'Non'.
   */
  protected BuButton btNon_;
  /**
   * bouton 'Annuler'.
   */
  protected BuButton btAnnuler_;

  /**
   * constructeur de la boite de dialogue.
   */
  public OscarDialogConfirmation(final BuCommonInterface _parent, final BuInformationsSoftware _isoft,
      final Object _title, final Object _message) {
    super(_parent, _isoft, BuResource.BU.getString((String) _title), _message);
    parent_ = _parent;
    final BuPanel pnb = new BuPanel();
    pnb.setLayout(new FlowLayout(FlowLayout.RIGHT));
    // Initialisation des Boutons
    btAide_ = new BuButton(BuResource.BU.loadToolCommandIcon("AIDE"), BuResource.BU.getString("Aide"));
    btOui_ = new BuButton(BuResource.BU.loadToolCommandIcon("OUI"), BuResource.BU.getString("Oui"));
    btNon_ = new BuButton(BuResource.BU.loadToolCommandIcon("NON"), BuResource.BU.getString("Non"));
    btAnnuler_ = new BuButton(BuResource.BU.loadToolCommandIcon("ANNULER"), BuResource.BU.getString("Annuler"));
    btAide_.setActionCommand("AIDE");
    btAide_.addActionListener(this);
    pnb.add(btAide_);
    btOui_.setActionCommand("OUI");
    btOui_.addActionListener(this);
    pnb.add(btOui_);
    btNon_.setActionCommand("NON");
    btNon_.addActionListener(this);
    pnb.add(btNon_);
    btAnnuler_.setActionCommand("ANNULER");
    btAnnuler_.addActionListener(this);
    pnb.add(btAnnuler_);
    getRootPane().setDefaultButton(btAnnuler_);
    content_.add(pnb, BuBorderLayout.SOUTH);
    setSize(500, 300);
  }

  public void setHelpEnabled(final boolean _v) {
    btAide_.setVisible(_v);
  }

  /**
   * retourne le composant.
   */
  public JComponent getComponent() {
    return null;
  }

  /**
   * lorsque l'utilisateur clic sur un des boutons.
   */
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    if (action.equals("AIDE")) {
      aide();
    } else if (action.equals("OUI")) {
      reponse_ = JOptionPane.YES_OPTION;
      setVisible(false);
    } else if (action.equals("NON")) {
      reponse_ = JOptionPane.NO_OPTION;
      setVisible(false);
    } else if (action.equals("ANNULER")) {
      reponse_ = JOptionPane.CANCEL_OPTION;
      setVisible(false);
    }
  }

  /**
   * affiche l'aide correspondant � la boite de dialogue de confirmation.
   */
  private void aide() {
    parent_.getImplementation().displayURL(OscarLib.helpUrl(OscarMsg.URL013));
  }
}
