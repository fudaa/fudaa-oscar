/*
 * @file         OscarImplementation.java
 * @creation     2002-10-07
 * @modification $Date: 2007-11-15 15:11:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.oscar.ICalculOscar;
import org.fudaa.dodico.corba.oscar.ICalculOscarHelper;
import org.fudaa.dodico.corba.oscar.IParametresOscar;
import org.fudaa.dodico.corba.oscar.IParametresOscarHelper;
import org.fudaa.dodico.corba.oscar.IResultatsOscar;
import org.fudaa.dodico.corba.oscar.IResultatsOscarHelper;
import org.fudaa.dodico.corba.oscar.SParametresOscar;
import org.fudaa.dodico.corba.oscar.SResultatsOscar;

import org.fudaa.dodico.oscar.DCalculOscar;

import org.fudaa.ebli.impression.EbliFillePrevisualisation;
import org.fudaa.ebli.impression.EbliMiseEnPageDialog;
import org.fudaa.ebli.impression.EbliPageable;

import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.FudaaParamChangeLog;
import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetInformationsFrame;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * L'implementation du client Oscar.
 * 
 * @version $Revision: 1.25 $ $Date: 2007-11-15 15:11:49 $ by $Author: hadouxad $
 * @author Olivier Hoareau
 */
public class OscarImplementation extends FudaaImplementation implements FudaaParamListener, FudaaProjetListener,
    PropertyChangeListener {
  /** Flag de niveau de messages d'erreurs / debugging � afficher. Voir la classe WSpy pour plus d'info */
  public final static int DEBUG = WSpy.ALL; // ALPHA_SOFTWARE;
  /** r�pertoire pour les mises � jour locales du logiciel */
  // public final static String LOCAL_UPDATE= ".";
  /** le logiciel est une application. */
  public final static boolean IS_APPLICATION = true;
  /** titre du logiciel. information qui sera affich�e dans la barre de titre de la fen�tre */
  public final static String SOFTWARE_TITLE = "Oscar";
  /** fen�tre principale de l'application. */
  public static java.awt.Frame FRAME;
  /** code de calcul Serveur Oscar. */
  public static ICalculOscar SERVEUR_OSCAR;
  /** informations sur la connexion courante au serveur Oscar. */
  public static IConnexion CONNEXION_OSCAR;
  /** nombre de fichiers r�cents � afficher dans le menu Fichier > R�ouvrir. */
  protected final static int RECENT_COUNT = 10;
  /** informations sur le logiciel. */
  protected static BuInformationsSoftware isOscar_ = new BuInformationsSoftware();
  /** informations sur le document. */
  protected static BuInformationsDocument idOscar_ = new BuInformationsDocument();
  /** donn�es param�tres � transmettre au serveur de calcul Oscar (1). */
  private IParametresOscar oscarParams;
  /** donn�es r�sultats � r�cup�rer du serveur de calcul Oscar (1). */
  private IResultatsOscar oscarResults;
  /** fen�tre de saisie des donn�es param�tres. */
  protected OscarFilleParametres fp_;
  /** fen�tre d'affichage des donn�es r�sultats bruts. */
  protected OscarFilleResultatsBruts frb_;
  /** projet en cours (ouvert). */
  protected FudaaProjet projet_;
  /** fen�tre d'affichage de la note de calculs. */
  protected OscarFilleNoteDeCalculs fRapport_;
  /** fen�tre permettant d'afficher un contenu Html (� utiliser avec showHtml()). */
  protected BuBrowserFrame fHtml_;
  /** assistant Fudaa. */
  protected BuAssistant assistant_;
  /** panneau des t�ches en cours. */
  protected BuTaskView taches_;
  /** panneau des messages. */
  protected OscarParamEventView msgView_;
  /** panneau des unit�s / conversions. */
  protected OscarUnitView unitView_;
  /** fen�tre de modification des propri�t�s du projet. */
  protected FudaaProjetInformationsFrame fProprietes_;
  /** fermer flag. */
  protected boolean fermerflag_;
  /** informations sur la version courante du logiciel. */
  static {
    /** titre du logiciel */
    isOscar_.name = "Oscar";
    /** version courante du logiciel */
    isOscar_.version = "1.4 beta 3";
    /** date de la version courante du logiciel */
    isOscar_.date = "9 janvier 2009";
    /** informations sur le droits r�serv�s */
    isOscar_.rights = "CETMEF (c) 2003";
    /** email du responsable du logiciel */
    isOscar_.contact= "Claire.Guiziou@developpement-durable.gouv.fr";
    /** informations sur la licence de d�veloppement */
    isOscar_.license = "GPL2";
    /** langage support� */
    isOscar_.languages = "fr";
    /** logo du logiciel */
    isOscar_.logo = OscarResource.OSCAR.getIcon("oscar-logo");
    /** banni�re du logiciel */
    isOscar_.banner = OscarResource.OSCAR.getIcon("oscar-banner");
    /** adresse internet du site du logiciel */
    isOscar_.http = "http://www.utc.fr/fudaa/oscar/";
    /** adresse internet du site de mise � jour du logiciel */
    isOscar_.update = "http://www.fudaa.org/distrib/deltas/oscar/";
    /** adresse internet/intranet de l'aide en ligne du logiciel */
    isOscar_.man = FudaaLib.LOCAL_MAN;
    /** auteurs-d�veloppeurs du logiciel */
    isOscar_.authors = new String[] { "Olivier Hoareau" };
    /** contributeurs au d�veloppement du logiciel */
    isOscar_.contributors = new String[] { "Equipes Dodico, Ebli, Fudaa et autres" };
    /** r�dacteurs de la documentation du logiciel */
    isOscar_.documentors = new String[] { "Manuel Le Moine" };
    /** testeurs du logiciel */
    isOscar_.testers = new String[] { "Laurent Beltran (DDE 58)", "Marc Citeau (LR Autun)", "Laurence Fayet (SNS)",
        "Yannick Laisis (SNS)", "Manuel Le Moine (CETMEF)", "St�phane Leblanc (DDE 22)", "Nathalie Neyret (CETMEF)",
        "Bertrand Thidet (SETRA)" };
    BuPrinter.INFO_LOG = isOscar_;
    BuPrinter.INFO_DOC = idOscar_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isOscar_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isOscar_;
  }

  public BuInformationsDocument getInformationsDocument() {
    //les catch de NullPointException sont plus que d�conseill�s
/*    try {
      return projet_.getInformationsDocument();
    } catch (final NullPointerException _e1) {
      return idOscar_;
    }*/
    //fait la meme chose  
    return projet_==null?idOscar_:projet_.getInformationsDocument();
  }

  /**
   * initialisation du MainPanel : logo, colonnes gauches et droites avec leur panneaux respectifs tels que : Assistant,
   * Visualiseur de T�ches en cours, Rapporteur de Messages...
   */
  public void init() {
    super.init();
    FRAME = getFrame();
    // Curseur de souris = SABLIER pendant le chargement
    final BuGlassPaneStop gps = new BuGlassPaneStop();
    gps.setVisible(true);
    gps.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    ((JFrame) FRAME).setGlassPane(gps);
    fp_ = null;
    projet_ = null;
    fRapport_ = null;
    aide_ = null;
    fHtml_ = null;
    if (BuPreferences.BU.getIntegerProperty("browser.type", 2) != 2) {
      BuPreferences.BU.putIntegerProperty("browser.type", 2);
    }
    try {
      // Initialisation du Titre de la fen�tre
      setTitle(SOFTWARE_TITLE + " " + isOscar_.version);
      // Ajout des menus sp�cifiques � l'application Oscar
      final BuMenuBar mb = getMainMenuBar();
      mb.addMenu(buildDonneesMenu(IS_APPLICATION));
      mb.addMenu(buildCalculMenu(IS_APPLICATION));
      mb.addMenu(buildResultatsMenu(IS_APPLICATION));
      // ...
      // Ajout des boutons de barre d'outils sp�cifiques � l'application Oscar
      final BuToolBar tb = getMainToolBar();
      // tb.addSeparator();
      // tb.addToolButton(BuResource.BU.getString("Donn�es"), "ENTRER_DONNEES", true);
      // tb.addToolButton(BuResource.BU.getString("Calculs"), "LANCER_CALCULS", true);
      tb.addToolButton(BuResource.BU.getString("Note"), "NOTE_DE_CALCULS", BuResource.BU.getToolIcon("index"), false);
      // Initialisation du Menu des fichiers R�cents
      final BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      try {
        mr.setPreferences(OscarPreferences.OSCAR);
        mr.setResource(OscarResource.OSCAR);
        mr.setEnabled(true);
      } catch (final NullPointerException _e1) {}
      // Initialisation des conteneurs principales de la fen�tres
      final BuMainPanel mp = getApp().getMainPanel();
      mp.setMessage("Veuillez patienter pendant le chargement du logiciel...");
      final BuColumn rc = mp.getRightColumn();
      assistant_ = new OscarAssistant();
      taches_ = new BuTaskView();
      msgView_ = new OscarParamEventView();
      final BuScrollPane sp2 = OscarLib.ColumnScrollPane(msgView_);
      unitView_ = new OscarUnitView();
      final BuScrollPane sp3 = OscarLib.ColumnScrollPane(unitView_);
      unitView_.addConversion("1 kN/m�", "1 kPa");
      unitView_.addConversion("1 MPa", "10 bar");
      unitView_.addConversion("1 bar", "10 t/m�");
      unitView_.addConversion("1 t/m�", "10 kPa");
      
      // Ajout des Panneaux dans les Colonnes Gauches et Droites
      // rc.addToggledComponent(BuResource.BU.getString("Assistant"),"ASSISTANT",assistant_,this);
      // rc.addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp, this);
      rc.addToggledComponent(BuResource.BU.getString("Unit�s"), "UNITES", sp3, this);
      rc.addToggledComponent(BuResource.BU.getString("Messages"), "MESSAGE", sp2, this);
      // Initialisation du MainPanel
      mp.setLogo(isOscar_.logo);
      mp.setTaskView(taches_);
      // D�finit l'afficheur de message
      setParamEventView(msgView_);
      // FudaaParamChangeLog.CHANGE_LOG.setApplication((BuApplication)OscarImplementation.FRAME, msgView_);
      getMainPanel().getDesktop().addPropertyChangeListener(this);
    } catch (final Throwable t) {
      WSpy.Error(t);
      t.printStackTrace();
    }
    setEnabledForAction("CREER", false);
    setEnabledForAction("OUVRIR", false);
    setEnabledForAction("REOUVRIR", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("QUITTER", false);
    setEnabledForAction("APERCU", false);
    setEnabledForAction("IMPRIMER", false);
    setEnabledForAction("CALCULS", false);
    setEnabledForAction("LANCER_CALCULS", false);
    setEnabledForAction("DONNEES", false);
    setEnabledForAction("ENTRER_DONNEES", false);
    setEnabledForAction("RESULTATS", false);
    setEnabledForAction("RESULTATS_BRUTS", false);
    setEnabledForAction("NOTE_DE_CALCULS", false);
    setEnabledForAction("COURBES", false);
    BuActionRemover.removeAction(getMainMenuBar(), "POINTEURAIDE");
    BuActionRemover.removeAction(getMainMenuBar(), "WWW_ACCUEIL");
    BuActionRemover.removeAction(getMainToolBar(), "COPIER");
    BuActionRemover.removeAction(getMainToolBar(), "COUPER");
    BuActionRemover.removeAction(getMainToolBar(), "COLLER");
    BuActionRemover.removeAction(getMainToolBar(), "DEFAIRE");
    BuActionRemover.removeAction(getMainToolBar(), "REFAIRE");
    BuActionRemover.removeAction(getMainToolBar(), "TOUTSELECTIONNER");
    BuActionRemover.removeAction(getMainToolBar(), "RANGERICONES");
    BuActionRemover.removeAction(getMainToolBar(), "RANGERPALETTES");
    BuActionRemover.removeAction(getMainToolBar(), "POINTEURAIDE");
    BuActionRemover.removeAction(getMainToolBar(), "RECHERCHER");
    BuActionRemover.removeAction(getMainToolBar(), "REMPLACER");
    BuActionRemover.removeAction(getMainToolBar(), "CONNECTER");
    BuActionRemover.removeAction(getMainToolBar(), "IMPRIMER");
    gps.setVisible(true);
  }

  /**
   * Initialisation des Activations/D�sactivations des diff�rents Menus.
   */
  protected void actionInit() {
    setEnabledForAction("CREER", true);
    setEnabledForAction("OUVRIR", true);
    setEnabledForAction("REOUVRIR", true);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("QUITTER", true);
    setEnabledForAction("APERCU", false);
    setEnabledForAction("IMPRIMER", true); //
    setEnabledForAction("CALCULS", false);
    setEnabledForAction("LANCER_CALCULS", false);
    setEnabledForAction("DONNEES", false);
    setEnabledForAction("ENTRER_DONNEES", false);
    setEnabledForAction("RESULTATS", false);
    setEnabledForAction("RESULTATS_BRUTS", false);
    setEnabledForAction("NOTE_DE_CALCULS", false);
    setEnabledForAction("COURBES", false);
  }

  /**
   * d�marrage du logiciel : affichage de l'�cran d'accueil, r�veil de l'assistant, connexion au serveur Oscar,
   * initialisation du projet, des pr�f�rences et des propri�t�s...
   */
  public void start() {
    super.start();
    final BuGlassPaneStop _gps = (BuGlassPaneStop) ((JFrame) FRAME).getGlassPane();
    _gps.setVisible(true);
    // L'assistant dit "Bienvenue !"
    // assistant_.changeAttitude(BuAssistant.PAROLE,OscarMsg.ASS000);
    // Cr�ation du nouveau Projet
    projet_ = new FudaaProjet(getApp(), OscarLib.FiltreFichier());
    projet_.addFudaaProjetListener(this);
    //
    final BuMainPanel mp = getApp().getMainPanel();
    mp.doLayout();
    mp.validate();
    assistant_.addEmitters((Container) getApp());
    // L'assistant dit "Vous pouvez cr�er un nouveau projet"
    // assistant_.changeAttitude(BuAssistant.ATTENTE,OscarMsg.ASS001);
    // Initialisation des pr�f�rences
    // ajoutePreferences(new VolumePreferencesPanel(this));
    _gps.setVisible(true);
    // Initialisation des propri�t�s du Projet
    fProprietes_ = new FudaaProjetInformationsFrame(this);
    fProprietes_.setProjet(projet_);
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    _gps.setVisible(true);
    actionInit();
    _gps.setVisible(true);
    showHtml(OscarLib.helpUrl(OscarMsg.URL033));
    mp.setMessage("");
    _gps.setVisible(false);
    // ((JFrame)FRAME).getGlassPane().setVisible(false);
  }

  /**
   *
   */
  public FudaaAstucesAbstract getAstuces() {
    return OscarAstuces.OSCAR;
  }

  /**
   *
   */
  public BuPreferences getApplicationPreferences() {
    return OscarPreferences.OSCAR;
  }

  /**
   * stocke le message en tant que message courant du panneau des messages.
   * 
   * @param _v message (�v�nement vue)
   */
  public void setParamEventView(final OscarParamEventView _v) {
    msgView_ = _v;
  }

  /**
   * retourne le message courant du panneau des messages.
   */
  public OscarParamEventView getParamEventView() {
    return msgView_;
  }

  /**
   * cr�ation du menu "Calculs" ainsi que de ses �l�ments.
   * 
   * @param _app bool�en qui sp�cifie si le parent est une application ou non pour le mode d'affichage du menu
   */
  protected BuMenu buildCalculMenu(final boolean _app) {
    // Cr�ation du menu
    final BuMenu r = new BuMenu(BuResource.BU.getString("Calculs"), "CALCULS");
    // Ajout des �l�ments du menu
    r.addMenuItem(BuResource.BU.getString("Lancer les calculs"), "LANCER_CALCULS", false);
    return r;
  }

  /**
   * cr�ation du menu "Donnees" ainsi que de ses �l�ments.
   * 
   * @param _app bool�en qui sp�cifie si le parent est une application ou non pour le mode d'affichage du menu
   */
  protected BuMenu buildDonneesMenu(final boolean _app) {
    // Cr�ation du menu
    final BuMenu r = new BuMenu(BuResource.BU.getString("Donn�es"), "DONNEES");
    // Ajout des �l�ments du menu
    r.addMenuItem(BuResource.BU.getString("Entr�e des donn�es"), "ENTRER_DONNEES", false);
    return r;
  }

  /**
   * cr�ation du menu "Resultats" ainsi que de ses �l�ments.
   * 
   * @param _app bool�en qui sp�cifie si le parent est une application ou non pour le mode d'affichage du menu
   */
  protected BuMenu buildResultatsMenu(final boolean _app) {
    // Cr�ation du menu
    final BuMenu r = new BuMenu(BuResource.BU.getString("R�sultats"), "RESULTATS");
    // Ajout des �l�ments du menu
    r.addMenuItem(BuResource.BU.getString("R�sultats bruts"), "RESULTATS_BRUTS", false);
    r.addMenuItem(BuResource.BU.getString("Note de calculs"), "NOTE_DE_CALCULS", false);
    // r.addMenuItem(BuResource.BU.getString("Courbes"), "COURBES", false);
    return r;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action.
   * 
   * @param _evt �v�nement ayant d�clench� l'action
   */
  public void actionPerformed(final ActionEvent _evt) {
    // R�cup�ration de la commande de l'action en cours et affichage sur la sortie erreur
    String action = _evt.getActionCommand();
    // R�cup�ration de la commande et de ses arguments
    final String arg = OscarLib.ActionArgumentStringOnly(action);
    action = OscarLib.ActionStringWithoutArgument(action);
    // Execution de la commande
    if (action.equals("CREER")) {
      creer();
    } else if (action.equals("OUVRIR")) {
      ouvrir(null);
    } else if (action.equals("REOUVRIR")) {
      ouvrir(arg);
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();
    } else if (action.equals("FERMER")) {
      fermer();
    } else if (action.startsWith("IMPORTOSCAR")) {
      importer(action.length() > 9 ? action.substring(9) : null);
    } else if (action.endsWith("IN") && (action.startsWith("ASC") || action.startsWith("XML"))) {
      importerProjet();
    } else if (action.equals("PROPRIETE")) {
      proprietes();
    } else if (action.equals("PARAMETRE")) {
      parametre();
    } else if (action.equals("RAPPORT")) {
      rapport();
    } else if (action.equals("PASIMPRESSION")) {
      pas_impression();
    } else if (action.equals("ENTRER_DONNEES")) {
      parametre();
    } else if (action.equals("LANCER_CALCULS")) {
      calculer();
    } else if (action.equals("RESULTATS_BRUTS")) {
      resultats_bruts();
    } else if (action.equals("NOTE_DE_CALCULS")) {
      rapport();
    } else if (action.equals("COURBES")) {
      courbes();
    } else if (action.equals("UNITES") || action.equals("ASSISTANT") || action.equals("TACHE")
        || action.equals("MESSAGE")) {
      enroule_deroule(action);
    } else if (action.equals("IMPRESSION") || action.equals("PREVISUALISER") || action.equals("MISEENPAGE")) {
      gestionnaireImpression(action);
    } else {
      super.actionPerformed(_evt);
    }
  }

  private void gestionnaireImpression(final String _commande) {
    final JInternalFrame frame = getCurrentInternalFrame();
    EbliPageable target = null;
    if (frame instanceof EbliPageable) {
      target = (EbliPageable) frame;
    } else if (frame instanceof EbliFillePrevisualisation) {
      target = ((EbliFillePrevisualisation) frame).getEbliPageable();
    } else {
      new BuDialogWarning(this, getInformationsSoftware(), FudaaResource.FUDAA
          .getString("Cette fen�tre n'est pas imprimable")).activate();
      return;
    }
    if ("IMPRIMER".equals(_commande)) {
      cmdImprimer(target);
    } else if ("MISEENPAGE".equals(_commande)) {
      cmdMiseEnPage(target);
    } else if ("PREVISUALISER".equals(_commande)) {
      cmdPrevisualisation(target);
    }
  }

  public void cmdImprimer(final EbliPageable _target) {
    final PrinterJob printJob = PrinterJob.getPrinterJob();
    printJob.setPageable(_target);
    if (printJob.printDialog()) {
      try {
        printJob.print();
      } catch (final Exception PrintException) {
        PrintException.printStackTrace();
      }
    }
  }

  public void cmdMiseEnPage(final EbliPageable _target) {
    new EbliMiseEnPageDialog(_target, getApp(), getInformationsSoftware()).activate();
  }

  public void cmdPrevisualisation(final EbliPageable _target) {
  // if( previsuFille_==null)
  // previsuFille_=new EbliFillePrevisualisation(getApp(),_target);
  // previsuFille_.setEbliPageable(_target);
  // addInternalFrame(previsuFille_);
  // try{
  // previsuFille_.setMaximum(true);
  // }catch( java.beans.PropertyVetoException _e){
  // previsuFille_.setSize(100,100);
  // }
  }

  /**
   * enroulement / d�roulement d'un panneau visible dans l'une des colonnes du bureau.
   * 
   * @param _panneau cha�ne de caract�res repr�sentant le panneau � enrouler ou d�rouler
   */
  private void enroule_deroule(final String _panneau) {
    final BuColumn rc = getMainPanel().getRightColumn();
    if (_panneau.equals("ASSISTANT") || _panneau.equals("TACHE") || _panneau.equals("MESSAGE")
        || _panneau.equals("UNITES")) {
      rc.toggleComponent(_panneau);
      setCheckedForAction(_panneau, rc.isToggleComponentVisible(_panneau));
    }
  }

  /**
   * affiche la fen�tre des propri�t�s du projet.
   */
  private void proprietes() {
    if (fProprietes_.getDesktopPane() != getMainPanel().getDesktop()) {
      addInternalFrame(fProprietes_);
    } else {
      activateInternalFrame(fProprietes_);
    }
  }

  public void showHtml(final String _title, final String _html) {
    if (fHtml_ == null) {
      fHtml_ = new BuBrowserFrame(this);
    }
    fHtml_.setHtmlSource(_html, _title);
    if (fHtml_.getDesktopPane() != getMainPanel().getDesktop()) {
      addInternalFrame(fHtml_);
    } else {
      activateInternalFrame(fHtml_);
    }
    try {
      fHtml_.setMaximum(true);
    } catch (final java.beans.PropertyVetoException _e) {} catch (final java.lang.Throwable _e1) {}
  }

  public void showHtml(final String _documentUrl) {
    if (fHtml_ == null) {
      fHtml_ = new BuBrowserFrame(this);
    }
    fHtml_.setDocumentUrl(_documentUrl);
    if (fHtml_.getDesktopPane() != getMainPanel().getDesktop()) {
      addInternalFrame(fHtml_);
    } else {
      activateInternalFrame(fHtml_);
    }
    try {
      fHtml_.setMaximum(true);
    } catch (final java.beans.PropertyVetoException _e) {} catch (final java.lang.Throwable _e1) {}
  }

  /**
   * appell�e automatiquement lorsqu'une structure de param�tres est cr��e.
   * 
   * @param _e �v�nement qui a engendr� le lancement de l'action
   */
  public void paramStructCreated(final FudaaParamEvent _e) {
    if (projet_.containsResults()) { // Il y a d�j� des r�sultats, alors...
      // Affiche une boite de dialogue "Param�tres chang�s"
      OscarLib.DialogMessage(getApp(), getInformationsSoftware(), OscarMsg.DLG001);
      // D�sactive le menu r�sultats et efface les r�sultats
      setEnabledForAction("RESULTAT", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(_e.getMessage());
    // Active le menu Enregistrer
    setEnabledForAction("ENREGISTRER", true);
  }

  /**
   * appell�e automatiquement lorsqu'une structure de param�tres est supprim�e.
   * 
   * @param _e �v�nement qui a engendr� le lancement de l'action
   */
  public void paramStructDeleted(final FudaaParamEvent _e) {
    if (projet_.containsResults()) { // Il y a d�j� des r�sultats, alors...
      // Affiche une boite de dialogue "Param�tres chang�s"
      OscarLib.DialogMessage(getApp(), getInformationsSoftware(), OscarMsg.DLG001);
      // D�sactive le menu r�sultats et efface les r�sultats
      setEnabledForAction("RESULTAT", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(_e.getMessage());
    // Active le menu Enregistrer
    setEnabledForAction("ENREGISTRER", true);
  }

  /**
   * appell�e automatiquement lorsqu'une structure de param�tres est modifi�e.
   * 
   * @param _e �v�nement qui a engendr� le lancement de l'action
   */
  public void paramStructModified(final FudaaParamEvent _e) {
    if (projet_.containsResults()) { // Il y a d�j� des r�sultats, alors...
      // Affiche une boite de dialogue "Param�tres chang�s"
      OscarLib.DialogMessage(getApp(), getInformationsSoftware(), OscarMsg.DLG001);
      // D�sactive les menus obsol�tes et efface les r�sultats
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(_e.getMessage());
    // Active le menu Enregistrer
    setEnabledForAction("ENREGISTRER", true);
  }

  /**
   * appell�e automatiquement lorsque les donn�es du projet changent.
   * 
   * @param _e �v�nement qui a engendr� le lancement de l'action
   */
  public void dataChanged(final FudaaProjetEvent _e) {
    switch (_e.getID()) {
    case FudaaProjetEvent.RESULT_ADDED:
    case FudaaProjetEvent.RESULTS_CLEARED:
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRER", true);
      break;
    }
  }

  /**
   * appell�e automatiquement lorsque le projet change d'�tat.
   * 
   * @param _e �v�nement qui a engendr� le lancement de l'action
   */
  public void statusChanged(final FudaaProjetEvent _e) {
    if (_e.getID() == FudaaProjetEvent.HEADER_CHANGED) {
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRER", true);
      if ((_e.getSource()) instanceof FudaaProjet) {
        majInformationsDocument(((FudaaProjet) _e.getSource()).getInformationsDocument());
      }
    }
  }

  /**
   * mise � jour des informations sur le document.
   * 
   * @param _id informations � mettre � jour sur le document
   */
  private void majInformationsDocument(final BuInformationsDocument _id) {
  // if( fRapport_!=null) fRapport_.setInformationsDocument(_id);
  }

  /**
   * lancement du calcul des r�sultats � partir des donn�es saisies par l'utilisateur.
   */
  public void oprCalculer() {
    // obligatoire pour r�cup�rer les data de la fen�tre de saisies des donn�es.
    projet_.addParam(OscarResource.PARAMETRES, fp_.getParametres());
    // boolean _calculOscar= false;
    String _messagesErreurs = null;
    SResultatsOscar _res = null;
    // V�rifie si l'utilisateur est connect� � un serveur
    if (!isConnected()) {
      OscarLib.DialogError(getApp(), isOscar_, OscarMsg.ERR001);
      return;
    }
    final BuMainPanel mp = getMainPanel();
    mp.setProgression(0);
    /** Transmission des param�tres * */
    mp.setMessage("Transmission des param�tres...");
    final SParametresOscar par = (SParametresOscar) projet_.getParam(OscarResource.PARAMETRES);
    oscarParams = IParametresOscarHelper.narrow(SERVEUR_OSCAR.parametres(CONNEXION_OSCAR));
    oscarParams.parametresOscar(par);
    projet_.clearResults();
    mp.setProgression(25);
    /** Lancement des calculs : XPSOL et ensuite XPLAN (indissociable) * */
    mp.setMessage("Calculs en cours...");
    try {
      SERVEUR_OSCAR.calcul(CONNEXION_OSCAR);
      oscarResults = IResultatsOscarHelper.narrow(SERVEUR_OSCAR.resultats(CONNEXION_OSCAR));
      // _calculOscar= true;
    } catch (final Throwable _e) {
      // _calculOscar= false;
    }
    mp.setProgression(75);
    /** R�cup�ration des r�sultats * */
    mp.setMessage("R�cup�ration des r�sultats...");
    _res = oscarResults.resultatsOscar();
    /** R�cup�ration des messages d'erreurs �ventuels * */
    if ((_res.erreurs != null) && (_res.erreurs.messages.length > 0)) {
      _messagesErreurs = "Veuillez prendre en compte les erreurs suivantes : \n \n";
      for (int i = 0; i < _res.erreurs.messages.length; i++) {
        _messagesErreurs += _res.erreurs.messages[i] + "\n";
      }
    }
    /** V�rification de la r�cup�ration de l'int�gralit� des r�sultats * */
    if (_res.pressions != null) {
      /** au moins Xpsol � r�ussi � tourner et renvoyer des r�sultats * */
      if (_messagesErreurs != null) {
        /** si il y a des erreurs, on n'affiche que le diagramme des pressions * */
        final SResultatsOscar _res2 = new SResultatsOscar();
        _res2.pressions = _res.pressions;
        _res2.erreurs = _res.erreurs;
        _res = _res2;
        mp.setMessage("Erreur(s) rencontr�e(s) !");
      } else {
        mp.setMessage("Calculs termin�s.");
      }
      projet_.addResult(OscarResource.RESULTATS, _res);
      mp.setProgression(100);
      setEnabledForAction("RESULTATS", true);
      setEnabledForAction("RESULTATS_BRUTS", true);
      setEnabledForAction("NOTE_DE_CALCULS", true);
      if (_messagesErreurs != null) {
        OscarLib.DialogMessageAide(getApp(), isOscar_, OscarLib.replaceKeyWordBy("erreurs", _messagesErreurs,
            OscarMsg.DLG021), OscarLib.helpUrl(OscarMsg.URL034));
      } else {
        OscarLib.DialogMessage(getApp(), isOscar_, OscarLib.replaceKeyWordBy("erreurs", "", OscarMsg.DLG021));
      }
      mp.setProgression(0);
      mp.setMessage("");
      resultats_bruts();
    } else {
      /** aucun code de calcul n'a r�ussi � tourner et renvoyer des r�sultats * */
      setEnabledForAction("RESULTATS", false);
      setEnabledForAction("RESULTATS_BRUTS", false);
      setEnabledForAction("NOTE_DE_CALCULS", false);
      mp.setProgression(100);
      mp.setMessage("Les calculs n'ont pas pu �tre effectu�s.");
      if (_messagesErreurs != null) {
        OscarLib.DialogMessageAide(getApp(), isOscar_, OscarLib.replaceKeyWordBy("erreurs", _messagesErreurs,
            OscarMsg.DLG009), OscarLib.helpUrl(OscarMsg.URL034));
      } else {
        OscarLib.DialogMessage(getApp(), isOscar_, OscarLib.replaceKeyWordBy("erreurs", "", OscarMsg.DLG009));
      }
      mp.setProgression(0);
      mp.setMessage("");
    }
  }

  /**
   * cr�ation d'un projet.
   */
  protected void creer() {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.creer();
    if (!projet_.estConfigure()) {
      projet_.fermer();
    } else {
      setEnabledForAction("DONNEES", true);
      setEnabledForAction("ENTRER_DONNEES", true);
      setEnabledForAction("CALCULS", false);
      setEnabledForAction("LANCER_CALCULS", false);
      setEnabledForAction("RESULTATS", false);
      setEnabledForAction("RESULTATS_BRUTS", false);
      setEnabledForAction("COURBES", false);
      setEnabledForAction("NOTE_DE_CALCULS", false);
      setEnabledForAction("IMPORTOSCAR", true);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("IMPRIMER", true);
      setEnabledForAction("EXPORTRES", false);
      setTitle(SOFTWARE_TITLE + " - " + projet_.getFichier());
      majInformationsDocument(getInformationsDocument());
      // affichage de la fen�tre de saisie des donn�es
      parametre();
    }
  }

  /**
   * ouverture d'un projet.
   */
  protected void ouvrir(final String arg) {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.ouvrir(arg);
    if (!projet_.estConfigure()) {
      projet_.fermer();
      setEnabledForAction("DONNEES", false);
      setEnabledForAction("ENTRER_DONNEES", false);
      setEnabledForAction("CALCULS", false);
      setEnabledForAction("LANCER_CALCULS", false);
      setEnabledForAction("RESULTATS", false);
      setEnabledForAction("RESULTATS_BRUTS", false);
      setEnabledForAction("COURBES", false);
      setEnabledForAction("NOTE_DE_CALCULS", false);
      setEnabledForAction("IMPORTOSCAR", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("EXPORTRES", false);
    } else {
      setEnabledForAction("DONNEES", true);
      setEnabledForAction("ENTRER_DONNEES", true);
      setEnabledForAction("CALCULS", false);
      setEnabledForAction("LANCER_CALCULS", false);
      setEnabledForAction("RESULTATS", false);
      setEnabledForAction("RESULTATS_BRUTS", false);
      setEnabledForAction("COURBES", false);
      setEnabledForAction("NOTE_DE_CALCULS", false);
      setEnabledForAction("IMPORTOSCAR", true);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("IMPRIMER", true);
      setEnabledForAction("EXPORTRES", false);
      setTitle(SOFTWARE_TITLE + " - " + projet_.getFichier());
      majInformationsDocument(getInformationsDocument());
      // affichage de la fen�tre de saisie des donn�es
      parametre();
      fp_.loadValidationMsg();
      if (arg == null) {
        final String r = projet_.getFichier();
        getMainMenuBar().addRecentFile(r, OscarLib.FILTRE_FICHIER);
        OscarPreferences.OSCAR.writeIniFile();
      }
    }
  }

  /**
   * enregistrement du projet en cours.
   */
  protected void enregistrer() {
    projet_.addParam(OscarResource.PARAMETRES, fp_.getParametres());
    projet_.enregistre();
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(OscarPreferences.OSCAR.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, OscarLib.FILTRE_FICHIER);
      OscarPreferences.OSCAR.writeIniFile();
    }
    setEnabledForAction("REOUVRIR", true);
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", true);
  }

  /**
   * enregistrement du projet sous un autre nom.
   */
  protected void enregistrerSous() {
    projet_.addParam(OscarResource.PARAMETRES, fp_.getParametres());
    projet_.enregistreSous();
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(OscarPreferences.OSCAR.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, OscarLib.FILTRE_FICHIER);
      OscarPreferences.OSCAR.writeIniFile();
    }
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", true);
    setTitle(SOFTWARE_TITLE + " - " + projet_.getFichier());
  }

  /**
   * fermeture du projet en cours.
   */
  protected boolean fermer() {
    fermerflag_ = true;
    // if( projet_.estConfigure()&&FudaaParamChangeLog.CHANGE_LOG.isDirty() ) { // � utiliser si gestion de modifs
    if (projet_.estConfigure()) {
      final int res = OscarLib.DialogAdvancedConfirmationWithoutHelp(getApp(), isOscar_, "Enregistrer", OscarLib
          .replaceKeyWordBy("nomfichier", projet_.getFichier(), OscarMsg.DLG004));
      if (res == JOptionPane.YES_OPTION) {
        enregistrer();
      } else if (res == JOptionPane.CANCEL_OPTION) {
        fermerflag_ = false;
        return false;
      }
    }
    if ((previsuFille_ != null) && previsuFille_.isVisible()) {
      previsuFille_.setVisible(false);
    }
    if ((fProprietes_ != null) && fProprietes_.isVisible()) {
      fProprietes_.setVisible(false);
    }
    final BuDesktop dk = getMainPanel().getDesktop();
    if (fRapport_ != null) {
      try {
        fRapport_.setClosed(true);
        fRapport_.setSelected(false);
      } catch (final PropertyVetoException e) {}
      dk.removeInternalFrame(fRapport_);
      fRapport_.removeInternalFrameListener(this);
      fRapport_ = null;
    }
    if (fp_ != null) {
      try {
        fp_.setClosed(true);
        fp_.setSelected(false);
      } catch (final PropertyVetoException e) {}
      dk.removeInternalFrame(fp_);
      fp_.removeInternalFrameListener(this);
      fp_ = null;
    }
    if (frb_ != null) {
      try {
        frb_.setClosed(true);
        frb_.setSelected(false);
      } catch (final PropertyVetoException e) {}
      dk.removeInternalFrame(frb_);
      frb_.removeInternalFrameListener(this);
      frb_ = null;
    }
    if (fHtml_ != null) {
      try {
        fHtml_.setClosed(true);
        fHtml_.setSelected(false);
      } catch (final PropertyVetoException e) {}
      dk.removeInternalFrame(fHtml_);
      fHtml_.removeInternalFrameListener(this);
      fHtml_ = null;
      fermerflag_ = false;
    }
    projet_.fermer();
    // on vide le panneau des messages :
    msgView_.removeAllMessages();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    dk.repaint();
    setEnabledForAction("DONNEES", false);
    setEnabledForAction("ENTRER_DONNEES", false);
    setEnabledForAction("CALCULS", false);
    setEnabledForAction("LANCER_CALCULS", false);
    setEnabledForAction("RESULTATS", false);
    setEnabledForAction("RESULTATS_BRUTS", false);
    setEnabledForAction("COURBES", false);
    setEnabledForAction("NOTE_DE_CALCULS", false);
    setEnabledForAction("IMPORTOSCAR", false);
    setEnabledForAction("EXPORTER", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("IMPRIMER", false);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("EXPORTRES", false);
    setTitle(SOFTWARE_TITLE);
    return true;
  }

  /**
   * importation d'un projet.
   */
  protected void importerProjet() {
    if (!projet_.estConfigure()) {
      projet_.fermer();
      setEnabledForAction("DONNEES", false);
      setEnabledForAction("ENTRER_DONNEES", false);
      setEnabledForAction("CALCULS", false);
      setEnabledForAction("LANCER_CALCULS", false);
      setEnabledForAction("RESULTATS", false);
      setEnabledForAction("RESULTATS_BRUTS", false);
      setEnabledForAction("COURBES", false);
      setEnabledForAction("NOTE_DE_CALCULS", false);
      setEnabledForAction("IMPORTOSCAR", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("EXPORTRES", false);
    } else {
      OscarLib.DialogMessage(getApp(), isOscar_, OscarMsg.DLG005);
      setEnabledForAction("DONNEES", true);
      setEnabledForAction("ENTRER_DONNEES", true);
      setEnabledForAction("CALCULS", false);
      setEnabledForAction("LANCER_CALCULS", false);
      setEnabledForAction("RESULTATS", false);
      setEnabledForAction("RESULTATS_BRUTS", false);
      setEnabledForAction("COURBES", false);
      setEnabledForAction("NOTE_DE_CALCULS", false);
      setEnabledForAction("IMPORTOSCAR", true);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("IMPRIMER", true);
      setEnabledForAction("EXPORTRES", false);
      setTitle(SOFTWARE_TITLE + " - " + projet_.getFichier());
      majInformationsDocument(getInformationsDocument());
    }
  }

  /**
   * importation.
   * 
   * @param _oscarId
   */
  protected void importer(final String _oscarId) {
    new BuDialogMessage(getApp(), getApp().getInformationsSoftware(), OscarMsg.DLG014).activate();
  }

  /**
   * fermeture de la fen�tre de saisie des param�tres (donn�es).
   */
  protected void closeFilleParams() {
    try {
      fp_.setClosed(true);
      fp_.setSelected(false);
      fp_ = null;
    } catch (final Throwable e) {}
  }

  /**
   * fermeture de la fen�tre de saisie des param�tres (donn�es).
   */
  protected void closeFilleResultatsBruts() {
    try {
      frb_.setClosed(true);
      frb_.setSelected(false);
      frb_ = null;
    } catch (final Throwable e) {}
  }

  /**
   * fermeture de la fen�tre de saisie des param�tres (donn�es).
   */
  protected void closeFilleNoteDeCalculs() {
    try {
      fRapport_.setClosed(true);
      fRapport_.setSelected(false);
      fRapport_ = null;
    } catch (final Throwable e) {}
  }

  /**
   * ouverture de la fen�tre de saise des param�tres (donn�es).
   */
  protected void parametre() {
    iconiserTout();
    if (fp_ == null) {
      fp_ = new OscarFilleParametres(getApp(), projet_);
      fp_.addPropertyChangeListener("CALCUL_DISPONIBLE", this);
      fp_.addPropertyChangeListener("RESULTATS_DISPONIBLE", this);
      fp_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_) {
            fermer();
          }
        }
      });
      addInternalFrame(fp_);
    } else {
      if (fp_.isClosed()) {
        addInternalFrame(fp_);
      } else {
        activateInternalFrame(fp_);
      }
    }
    try {
      fp_.setMaximum(true);
    } catch (final PropertyVetoException _exc) {}
  }

  protected void resetResultsFrame() {
    if (frb_ != null) {
      if (frb_.isVisible()) {
        OscarLib.DialogMessage((BuCommonInterface) null, getInformationsSoftware(), OscarMsg.DLG019);
      } else {
        OscarLib.DialogMessage((BuCommonInterface) null, getInformationsSoftware(), OscarMsg.DLG020);
      }
      closeFilleResultatsBruts();
    }
    if (fRapport_ != null) {
      if (fRapport_.isVisible()) {
        OscarLib.DialogMessage((BuCommonInterface) null, getInformationsSoftware(), OscarMsg.DLG019);
      } else {
        OscarLib.DialogMessage((BuCommonInterface) null, getInformationsSoftware(), OscarMsg.DLG020);
      }
      closeFilleNoteDeCalculs();
    }
  }

  public OscarFilleParametres getFilleParametres() {
    return fp_;
  }

  public OscarFilleResultatsBruts getFilleResultatsBruts() {
    return frb_;
  }

  public OscarFilleNoteDeCalculs getFilleNoteDeCalculs() {
    return fRapport_;
  }

  public void propertyChange(final PropertyChangeEvent _evt) {
    final String _p = _evt.getPropertyName();
    final Object _nv = _evt.getNewValue();
    // WSpy.Spy(_p+" = "+_nv+" (old value was "+_ov+")");
    if (_p.equals("CALCUL_DISPONIBLE")) {
      final boolean _v = ((Boolean) _nv).booleanValue();
      setEnabledForAction("CALCULS", _v);
      setEnabledForAction("LANCER_CALCULS", _v);
    } else if (_p.equals("RESULTATS_DISPONIBLE")) {
      final boolean _v = ((Boolean) _nv).booleanValue();
      setEnabledForAction("RESULTATS", _v);
      setEnabledForAction("RESULTATS_BRUTS", _v);
      setEnabledForAction("NOTE_DE_CALCULS", _v);
    }
  }

  /**
   * ouverture de la fen�tre de la note de calculs.
   */
  protected void rapport() {
    iconiserTout();
    if (fRapport_ == null) {
      fRapport_ = new OscarFilleNoteDeCalculs(getApp(), projet_);
      // FudaaFilleRapport(getApp());
      fRapport_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_) {
            closeFilleNoteDeCalculs();
          }
        }
      });
      fRapport_.setClosable(true);
      // fRapport_.insereEnteteEtude(getInformationsDocument());
      addInternalFrame(fRapport_);
      try {
        fRapport_.setMaximizable(true);
        fRapport_.setMaximum(true);
      } catch (final java.beans.PropertyVetoException _e1) {}
    } else {
      if (fRapport_.isClosed()) {
        addInternalFrame(fRapport_);
      } else {
        activateInternalFrame(fRapport_);
      }
    }
  }

  protected void pas_impression() {}

  /**
   * lancement de la t�che de calcul.
   */
  protected void calculer() {
    if (valideContraintesCalcul()) {
    	if (frb_ != null) closeFilleResultatsBruts();
   	    if (fRapport_ != null) closeFilleNoteDeCalculs();
   	    new BuTaskOperation(this, "Calcul", "oprCalculer").start();
    }
  }

  /**
   * iconise toutes les fen�tres du bureau.
   */
  protected void iconiserTout() {
    try {
      final Component[] _cs = getMainPanel().getDesktop().getComponents();
      for (int i = 0; i < _cs.length; i++) {
        final JInternalFrame _o = (JInternalFrame) _cs[i];
        _o.setIcon(true);
      }
    } catch (final Throwable _e) {}
  }

  /**
   * affichage de la fen�tre des r�sultats bruts.
   */
  protected void resultats_bruts() {
    // iconiserTout();
    if (frb_ == null) {
      frb_ = new OscarFilleResultatsBruts(getApp(), projet_);
      frb_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_) {
            closeFilleResultatsBruts();
          }
        }
      });
      addInternalFrame(frb_);
    } else {
      if (frb_.isClosed()) {
        addInternalFrame(frb_);
      } else {
        activateInternalFrame(frb_);
      }
    }
    frb_.pack();
   /* try {
      frb_.setMaximum(true);
    } catch (final PropertyVetoException _exc) {}
  */}

  /**
   * ouverture de la fen�tre courbes r�sultats.
   */
  protected void courbes() {}

  /**
   * mode plein �cran.
   */
  public void fullscreen() {
    super.fullscreen();
  }

  private boolean valideContraintesCalcul() {
    return true;
  }

  /**
   * fermeture du logiciel.
   */
  public void exit() {
    if (fermer()) {
      closeConnexions();
      super.exit();
    }
  }

  /**
   * appell�e � la fermeture du logiciel.
   */
  public void finalize() {
    closeConnexions();
  }

  /**
   * permet de confirmer ou non la fermeture du logiciel.
   */
  public boolean confirmExit() {
    return true;
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  protected void buildPreferences(final List _frAddTab) {
    super.buildPreferences(_frAddTab);
//    _frAddTab.add(new VolumePreferencesPanel(this));
  }

  protected void clearVariables() {
    CONNEXION_OSCAR = null;
    SERVEUR_OSCAR = null;
  }

  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_OSCAR, CONNEXION_OSCAR);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculOscar.class };
  }

  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculOscar.class);
    CONNEXION_OSCAR = c.getConnexion();
    SERVEUR_OSCAR = ICalculOscarHelper.narrow(c.getTache());
  }
}
