/*
 * @file         OscarFilleNoteDeCalculs.java
 * @creation     1998-10-05
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuCommonInterface;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;

import org.fudaa.dodico.corba.oscar.SResultatsOscar;

import org.fudaa.fudaa.commun.projet.FudaaProjet;


/**
 * affiche une fen�tre permettant de g�n�rer une note de calculs � partir des r�sultats pr�c�demment calcul�s.
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarFilleNoteDeCalculs extends OscarInternalFrameAdapter {
  /**
   *
   */
  public static final int NIVEAU_1 = 1;
  /**
   *
   */
  public static final int NIVEAU_2 = 2;
  /**
   *
   */
  public static final int NIVEAU_3 = 3;
  /**
   *
   */
  Settings settings_;
  WTree listconfig_;
  boolean close_;

  // OscarFilleNoteDeCalculs.Settings settings_;
  /**
   * constructeur de la fenetre. construit une fen�tre d'affichage de la note de calculs.
   * 
   * @param _appli application � laquelle cette fen�tre sera rattach�e.
   * @param _project projet en cours auquel cette fen�tre sera rattach�e.
   */
  public OscarFilleNoteDeCalculs(final BuCommonInterface _appli, final FudaaProjet _project) {
    super(OscarMsg.TITLELAB017, OscarMsg.ICO001, OscarMsg.URL031, _appli, _project);
    settings_ = createDefaultSettings();
    final JPanel _p = new JPanel();
    _p.setLayout(new BorderLayout());
    _p.setBorder(new EmptyBorder(5, 5, 5, 5));
    _p.add(OscarLib.InfoAidePanel(OscarMsg.LAB065, this), BorderLayout.NORTH);
    listconfig_ = new WTree(_project);
    // listconfig_.setPreferredSize(new Dimension(500,400));
    final JScrollPane _sp = new JScrollPane(listconfig_);
    _p.add(_sp, BorderLayout.CENTER);
    final JPanel _buttons = new JPanel();
    _buttons.setLayout(new FlowLayout());
    final JButton _bt_save = OscarLib.Button("Exporter", "ENREGISTRER", "ENREGISTRER");
    final JButton _bt_display = OscarLib.Button("G�n�rer", "INDEX", "AFFICHER");
    final JButton _bt_close = OscarLib.Button("Fermer", "ANNULER", "FERMER");
    _buttons.add(_bt_display);
    _buttons.add(_bt_save);
    _buttons.add(_bt_close);
    _buttons.setPreferredSize(new Dimension(500, 50));
    _p.add(_buttons, BorderLayout.SOUTH);
    _bt_close.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        fermer();
      }
    });
    _bt_save.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        final String _f = obtainFileFromUser();
        if (_f != null) {
          setFichier(_f);
          final Settings _config = listconfig_.getConfig();
          if (_config != null) {
            settings_.add(_config);
            if (generer("todisk")) {
              OscarLib.DialogMessage(getApplication(), OscarImplementation.informationsSoftware(), OscarLib
                  .replaceKeyWordBy("imgdir", _f + "files" + OscarLib.FILE_SEPARATOR, OscarLib.replaceKeyWordBy("file",
                      _f, OscarMsg.DLG016)));
            } else {
              OscarLib.DialogError(getApplication(), OscarImplementation.informationsSoftware(), OscarMsg.DLG017);
            }
          }
        }
      }
    });
    _bt_display.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
    	  final String _f = new File(OscarLib.NOTEDECALCULS_TEMP_HTMLFILE + "001.html").getAbsolutePath();
    	  setFichier(_f);
        final Settings _config = listconfig_.getConfig();
        if (_config != null) {
          settings_.add(_config);
          if (generer("toscreen")) {} else {
            OscarLib.DialogError(getApplication(), OscarImplementation.informationsSoftware(), OscarMsg.DLG018);
          }
        }
      }
    });
    final JScrollPane _sp1 = new JScrollPane(_p);
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(_sp1);
    // setSize(500,400);
    pack();
  }

  /**
   * ferme la fen�tre.
   */
  protected void fermer() {
    try {
      setVisible(false);
      setClosed(true);
    } catch (final Throwable _e) {}
  }

  /**
   * imprime la fen�tre.
   */
  protected void imprimer() {
    this.getImplementation().actionPerformed(new ActionEvent(this, 15, "IMPRIMER"));
  }

  /**
   *
   */
  protected String obtainFileFromUser() {
    return OscarLib.getFileChoosenByUser(this, "Exporter au format HTML", "Exporter le rapport HTML",
        OscarLib.USER_HOME + "note.html");
  }

  /**
   *
   */
  protected synchronized void setFichier(final String _f) {
    settings_.stocke("NOM_DU_FICHIER_HTML", _f);
  }

  /**
   * execution de l'action �cout�e survenue.
   * 
   * @param _e �v�nement ayant engendr� l'action
   */
  public void actionPerformed(final ActionEvent _e) {
  // String _a = _e.getActionCommand();
  }

  /**
   * renvoi une structure contenant toutes les pr�f�rences utilisateurs concernant l'apparence de la note de calculs �
   * g�n�rer. ces informations sont destin�es � �tre enregistr�es dans le fichier de sauvegarde du projet.
   * 
   * @return une structure de type <code>OscarFilleNoteDeCalculs.Settings</code>
   */
  public Settings getSettings() {
    final Settings _s = settings_;
    return _s;
  }

  /**
   * r�cup�ration des settings utilisateurs pour les pr�f�rences concernant la note de calculs � g�n�rer � partir d'une
   * structure.
   * 
   * @param _s structure de type <code>OscarFilleNoteDeCalculs.Settings</code> contenant les pr�f�rences utilisateurs
   *          � importer
   */
  private synchronized void setSettings(final Settings _s) {
    if (_s != null) {
      settings_ = _s;
    }
  }

  /**
   * mise � jour du contenu des onglets de la fen�tre. cette fonction est appell�e automatiquement lorsqu'une
   * modification de l'�tat ou du contenu du projet est notifi�e.
   */
  protected void updatePanels() {
    setSettings((Settings) project_.getResult(OscarResource.NOTEDECALCULS));
  }

  private boolean isChoosen(final String _setting) {
    final Boolean _b = (Boolean) getSettings().lit(_setting);
    if (_b == null) {
      return false;
    }
    return _b.booleanValue();
  }

  /**
   * g�n�re la note de calculs suivant les r�sultats et les pr�f�rences utilisateurs. la note de calculs est affich�e
   * dans la fen�tre dans un JEditorPane cette fonction est appell�e automatiquement lorsqu'une modification de l'�tat
   * ou du contenu du projet est notifi�e.
   */
  boolean generer(final String _mode) {
    String _html = "";
    String _tdm = "";
    String _content = "";
    final boolean _RPredim = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).predimensionnement == null) ? false
        : true;
    final boolean _RPressions = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).pressions == null) ? false
        : true;
    final boolean _RMoments = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).moments == null) ? false
        : true;
    final boolean _REfforts = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).efforts == null) ? false
        : true;
    final boolean _RDeformee = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).deformees == null) ? false
        : true;
    final Settings locSttings = getSettings();
    String _f = (String) locSttings.lit("NOM_DU_FICHIER_HTML");
    final HtmlDoc doc_ = new HtmlDoc();
    HtmlPartie _p = null;
    HtmlPartie _sp = null;
    HtmlPartie _ssp = null;
    if (_f != null) {
      CtuluLibFile.deleteDir(new File(_f + "files"));
      (new File(_f + "files")).mkdirs();
    } else {
      _f = "";
      (new File(OscarLib.NOTEDECALCULS_TEMP_HTMLFILE + "files")).mkdirs();
    }
    if (isChoosen("PRESENTATION")) {
      _p = new HtmlPartie("PRESENTATION", "Pr&eacute;sentation", NIVEAU_1, WHtmlContent.createContentFor("PRESENTATION", this,
          project_, locSttings));
      doc_.addPartie(_p);
    }
    if (isChoosen("RAPPELHYPOTHESES")) {
      _p = new HtmlPartie("RAPPELHYPOTHESES", "Rappel des hypoth&egrave;ses", NIVEAU_1, WHtmlContent.createContentFor(
          "RAPPELHYPOTHESES", this, project_, locSttings));
      if (isChoosen("RAPPELHYPOTHESES_SOL")) {
        _sp = new HtmlPartie("RAPPELHYPOTHESES_SOL", "Sol", NIVEAU_2, WHtmlContent.createContentFor(
            "RAPPELHYPOTHESES_SOL", this, project_, locSttings));
        _p.addSousPartie(_sp);
      }
      if (isChoosen("RAPPELHYPOTHESES_EAU")) {
        _sp = new HtmlPartie("RAPPELHYPOTHESES_EAU", "Eau", NIVEAU_2, WHtmlContent.createContentFor(
            "RAPPELHYPOTHESES_EAU", this, project_, locSttings));
        _p.addSousPartie(_sp);
      }
      if (isChoosen("RAPPELHYPOTHESES_SURCHARGES")) {
        _sp = new HtmlPartie("RAPPELHYPOTHESES_SURCHARGES", "Surcharges", NIVEAU_2, WHtmlContent.createContentFor(
            "RAPPELHYPOTHESES_SURCHARGES", this, project_, locSttings));
        _p.addSousPartie(_sp);
      }
      if (isChoosen("RAPPELHYPOTHESES_EFFORTS")) {
        _sp = new HtmlPartie("RAPPELHYPOTHESES_EFFORTS", "Autres efforts", NIVEAU_2, WHtmlContent.createContentFor(
            "RAPPELHYPOTHESES_EFFORTS", this, project_, locSttings));
        if (isChoosen("RAPPELHYPOTHESES_EFFORTS_TETEDERIDEAU")) {
          _ssp = new HtmlPartie("RAPPELHYPOTHESES_EFFORTS_TETEDERIDEAU", "Effort en t&ecirc;te de rideau", NIVEAU_3,
              WHtmlContent.createContentFor("RAPPELHYPOTHESES_EFFORTS_TETEDERIDEAU", this, project_, locSttings));
          _sp.addSousPartie(_ssp);
        }
        if (isChoosen("RAPPELHYPOTHESES_EFFORTS_CVS")) {
          _ssp = new HtmlPartie("RAPPELHYPOTHESES_EFFORTS_CVS", "Contraintes verticales suppl&eacute;mentaires", NIVEAU_3,
              WHtmlContent.createContentFor("RAPPELHYPOTHESES_EFFORTS_CVS", this, project_, locSttings));
          _sp.addSousPartie(_ssp);
        }
        if (isChoosen("RAPPELHYPOTHESES_EFFORTS_CHS")) {
          _ssp = new HtmlPartie("RAPPELHYPOTHESES_EFFORTS_CHS", "Contraintes horizontales suppl&eacute;mentaires", NIVEAU_3,
              WHtmlContent.createContentFor("RAPPELHYPOTHESES_EFFORTS_CHS", this, project_, locSttings));
          _sp.addSousPartie(_ssp);
        }
        if (isChoosen("RAPPELHYPOTHESES_EFFORTS_FDIAG")) {
          _ssp = new HtmlPartie("RAPPELHYPOTHESES_EFFORTS_FDIAG", "Fichiers de diagramme suppl&eacute;mentaires", NIVEAU_3,
              WHtmlContent.createContentFor("RAPPELHYPOTHESES_EFFORTS_FDIAG", this, project_, locSttings));
          _sp.addSousPartie(_ssp);
        }
        _p.addSousPartie(_sp);
      }
      if (isChoosen("RAPPELHYPOTHESES_OUVRAGE")) {
        _sp = new HtmlPartie("RAPPELHYPOTHESES_OUVRAGE", "Caract&eacute;ristiques de l'ouvrage", NIVEAU_2, WHtmlContent
            .createContentFor("RAPPELHYPOTHESES_OUVRAGE", this, project_, locSttings));
        if (isChoosen("RAPPELHYPOTHESES_OUVRAGE_ANCRAGE")) {
          _ssp = new HtmlPartie("RAPPELHYPOTHESES_OUVRAGE_ANCRAGE", "Ancrage", NIVEAU_3, WHtmlContent.createContentFor(
              "RAPPELHYPOTHESES_OUVRAGE_ANCRAGE", this, project_, locSttings));
          _sp.addSousPartie(_ssp);
        }
        if (isChoosen("RAPPELHYPOTHESES_OUVRAGE_PALPLANCHES")) {
          _ssp = new HtmlPartie("RAPPELHYPOTHESES_OUVRAGE_PALPLANCHES", "Palplanches", NIVEAU_3, WHtmlContent
              .createContentFor("RAPPELHYPOTHESES_OUVRAGE_PALPLANCHES", this, project_, locSttings));
          _sp.addSousPartie(_ssp);
        }
        _p.addSousPartie(_sp);
      }
      doc_.addPartie(_p);
    }
    if (isChoosen("CALCULS")) {
      _p = new HtmlPartie("CALCULS", "Calculs", NIVEAU_1, WHtmlContent.createContentFor("CALCULS", this, project_,
          locSttings));
      doc_.addPartie(_p);
    }
    if (isChoosen("RESULTATS")) {
      _p = new HtmlPartie("RESULTATS", "R&eacute;sultats", NIVEAU_1, WHtmlContent.createContentFor("RESULTATS", this,
          project_, locSttings));
      if (_RPressions && isChoosen("RESULTATS_DIAGRAMME")) {
        _sp = new HtmlPartie("RESULTATS_DIAGRAMME", "Diagramme des pressions", NIVEAU_2, WHtmlContent.createContentFor(
            "RESULTATS_DIAGRAMME", this, project_, locSttings));
        _p.addSousPartie(_sp);
      }
      if (_RPredim && isChoosen("RESULTATS_PREDIMENSIONNEMENT")) {
        _sp = new HtmlPartie("RESULTATS_PREDIMENSIONNEMENT", "Pr&eacute;dimensionnement du rideau", NIVEAU_2, WHtmlContent
            .createContentFor("RESULTATS_PREDIMENSIONNEMENT", this, project_, locSttings));
        _p.addSousPartie(_sp);
      }
      if (_RMoments && isChoosen("RESULTATS_MOMENT")) {
        _sp = new HtmlPartie("RESULTATS_MOMENT", "Moments fl&eacute;chissants du rideau", NIVEAU_2, WHtmlContent
            .createContentFor("RESULTATS_MOMENT", this, project_, locSttings));
        _p.addSousPartie(_sp);
      }
      if (_RDeformee && isChoosen("RESULTATS_DEFORMEE")) {
        _sp = new HtmlPartie("RESULTATS_DEFORMEE", "D&eacute;placement du rideau", NIVEAU_2, WHtmlContent.createContentFor(
            "RESULTATS_DEFORMEE", this, project_, locSttings));
        _p.addSousPartie(_sp);
      }
      if (_REfforts && isChoosen("RESULTATS_EFFORTS")) {
        _sp = new HtmlPartie("RESULTATS_EFFORTS", "Efforts dans les tirants", NIVEAU_2, WHtmlContent.createContentFor(
            "RESULTATS_EFFORTS", this, project_, locSttings));
        _p.addSousPartie(_sp);
      }
      doc_.addPartie(_p);
    }
    if (isChoosen("TABLEDESMATIERES")) {
      doc_.setTdmProfondeur(Integer.valueOf((String) locSttings.lit("TABLEDESMATIERES_PROFONDEUR")).intValue());
      _tdm = doc_.summarize();
    }
    _content = doc_.getContent();
    String _header = "<html>\n<head>\n<title>Note de calculs</title>\n</head>\n<body>\n";
    _header += "<table width=\"100%\" border=0>" + OscarLib.LINE_SEPARATOR;
    _header += "<tr>" + OscarLib.LINE_SEPARATOR;
    _header += "<td>";
    if (isChoosen("LOGOFUDAA")) {
      _header += "<img width=\"32\" height=\"32\" src=\"" + Oscar.class.getResource("fudaa-logo.gif") + "\" border=0>";
    } else {
      _header += "&nbsp;";
    }
    _header += "</td>" + OscarLib.LINE_SEPARATOR;
    _header += "<td width=\"100%\" valign=middle><h1>Note de calculs</h1></td>" + OscarLib.LINE_SEPARATOR;
    _header += "</tr>" + OscarLib.LINE_SEPARATOR;
    _header += "<tr>" + OscarLib.LINE_SEPARATOR;
    _header += "<td>";
    if (isChoosen("LOGOCETMEF")) {
      _header += "<img width=\"32\" height=\"32\" src=\"" + Oscar.class.getResource("cetmef-logo.gif") + "\" border=0>";
    } else {
      _header += "&nbsp;";
    }
    _header += "</td>" + OscarLib.LINE_SEPARATOR;
    _header += "<td width=\"100%\" valign=middle><h2>" + OscarImplementation.SOFTWARE_TITLE + "</h2></td>"
        + OscarLib.LINE_SEPARATOR;
    _header += "</tr>" + OscarLib.LINE_SEPARATOR;
    _header += "</table>" + OscarLib.LINE_SEPARATOR;
    final Calendar _cal = new GregorianCalendar();
    _cal.setTime(new Date(System.currentTimeMillis()));
    final int _dd = _cal.get(Calendar.DAY_OF_MONTH);
    final int _mm = _cal.get(Calendar.MONTH) + 1;
    final int _yyyy = _cal.get(Calendar.YEAR);
    final int _hh = _cal.get(Calendar.HOUR_OF_DAY);
    final int _min = _cal.get(Calendar.MINUTE);
    final int _ss = _cal.get(Calendar.SECOND);
    final String _date = _dd + "/" + _mm + "/" + _yyyy + " - " + _hh + ":" + _min + ":" + _ss;
    _header += _date;
    String _end = "</body>\n</html>";
    _html = _header + _tdm + _content + _end;
    if (_mode.equals("toscreen")) {
      File file = null;
      try {
        file = new File(OscarLib.NOTEDECALCULS_TEMP_HTMLFILE + "001.html");
        final FileWriter _fw = new FileWriter(file);
        _fw.write(_html, 0, _html.length());
        _fw.flush();
        _fw.close();
      } catch (final IOException _e1) {
        WSpy.Error("Impossible d'�crire le fichier sur le disque.");
        return false;
      }
      if (file != null) {
        //try {
          appli_.getImplementation().displayURL(file.getAbsolutePath());
        /*} catch (final MalformedURLException e) {
          if (CtuluLibMessage.DEBUG) {
            e.printStackTrace();
          }
          WSpy.Error("L'adresse du fichier " + file.getAbsolutePath() + " est invalide");
          return false;
        }*/
      }
      return true;
    } else if (_mode.equals("todisk")) {
      if (_f != null) {
        try {
          final FileWriter _fw = new FileWriter(_f);
          _fw.write(_html, 0, _html.length());
          _fw.flush();
          _fw.close();
        } catch (final IOException _e1) {
          WSpy.Error("Impossible d'�crire le fichier sur le disque.");
          return false;
        }
      } else {
        WSpy.Error("Impossible d'afficher la note de calculs");
        return false;
      }
    } else {
      WSpy.Error("mode de g�n�ration de la note de calcul incorrecte");
      return false;
    }
    return true;
  }

  // private int[] counters;
  /*
   * private int nb(int level) { if ((level > counters.length) || (level <= 0)) return -1; return ++counters[level - 1]; }
   */
  /*
   * private String nbt(int level, String _mode, String _sep) { String _txt= ""; int _n= nb(level); if (_n <= 0) return
   * ""; _txt= OscarLib.NumberPuce(_n, _mode); _txt += _sep; return _txt; }
   */
  /*
   * private void init_counters_after_level(int _n) { for (int i= 0; i < counters.length; i++) { if ((i + 1) > _n)
   * counters[i]= 0; } }
   */
  /**
   * classe permettant de stocker toutes les informations de pr�f�rences concernant la note de calculs.
   * 
   * @return une structure vide de type Settings.
   */
  public static Settings createSettings() {
    final Settings _s = new Settings();
    return _s;
  }

  /**
   * classe permettant de stocker toutes les informations de pr�f�rences concernant la note de calculs.
   * 
   * @return une structure vide de type Settings.
   */
  public static Settings createDefaultSettings() {
    final Settings _s = new Settings();
    return _s;
  }
  /**
   * classe permettant de stocker toutes les informations de pr�f�rences concernant la note de calculs.
   */
  public static class Settings {
    Hashtable settings_ = null;

    /**
     * construit un objet de stockage des pr�f�rences utilisateur pour la note de calculs.
     */
    public Settings() {
      settings_ = new Hashtable(4);
    }

    /*
     * private Collection values() { return settings_.values(); }
     */
    private Enumeration keys() {
      return settings_.keys();
    }

    public void stocke(final String _cle, final Object _valeur) {
      try {
        if (settings_.get(_cle) != null) {
          settings_.remove(_cle);
        }
        settings_.put(_cle, _valeur);
      } catch (final NullPointerException _e1) {}
    }

    public Object lit(final String _cle) {
      return settings_.get(_cle);
    }

    public void add(final Settings _toAdd) {
      try {
        for (final Enumeration _o = _toAdd.keys(); _o.hasMoreElements();) {
          final String _tmp = (String) _o.nextElement();
          stocke(_tmp, _toAdd.lit(_tmp));
        }
      } catch (final UnsupportedOperationException _e1) {
        WSpy.Error("unsupported operation exception");
      } catch (final ClassCastException _e2) {
        WSpy.Error("class cast exception");
      } catch (final IllegalArgumentException _e3) {
        WSpy.Error("illegal argument exception");
      } catch (final NullPointerException _e4) {
        WSpy.Error("null pointer exception");
      }
    }

    public String toString() {
      String _txt = "";
      String _tmp;
      Object _o;
      for (final Enumeration _ek = settings_.keys(); _ek.hasMoreElements();) {
        _o = _ek.nextElement();
        _tmp = _o + " = " + settings_.get(_o);
        // WSpy.Spy(_tmp);
        _txt += _tmp + "\n";
      }
      return _txt;
    }
  }
  class HtmlPartie {
    Settings htmlSettings_;
    String partie_;
    String titre_;
    Vector sousparties_;
    int niveau_;
    WHtmlContent contentgenerator_;

    public HtmlPartie(final String _partie, final String _titre, final int _niveau, final WHtmlContent _contentgenerator) {
      htmlSettings_ = getSettings();
      contentgenerator_ = _contentgenerator;
      partie_ = _partie;
      titre_ = _titre;
      niveau_ = _niveau;
      sousparties_ = new Vector();
    }

    public String getPartie() {
      return partie_;
    }

    public Vector getSousParties() {
      return sousparties_;
    }

    public String getTitre() {
      return titre_;
    }

    public String summarize() {
      String _txt = "";
      HtmlPartie _node = null;
      if (sousparties_.size() > 0) {
        if (niveau_ != 0) {
          _txt += "<ul>" + OscarLib.LINE_SEPARATOR;
        }
        for (int i = 0; i < sousparties_.size(); i++) {
          _node = (HtmlPartie) sousparties_.get(i);
          _txt += "<a class=\"partie" + niveau_ + "\" href=\"#" + _node.getPartie().toLowerCase() + "\">" + (i + 1)
              + ". " + _node.getTitre() + "</a><br>" + OscarLib.LINE_SEPARATOR;
          _txt += _node.summarize();
        }
        if (niveau_ != 0) {
          _txt += "</ul>" + OscarLib.LINE_SEPARATOR;
        }
      }
      return _txt;
    }

    public void addSousPartie(final HtmlPartie _souspartie) {
      sousparties_.add(_souspartie);
    }

    public String getContent() {
      String _txt = "";
      HtmlPartie _node = null;
      _txt += contentgenerator_.getContent();
      if (sousparties_.size() > 0) {
        for (int i = 0; i < sousparties_.size(); i++) {
          _node = (HtmlPartie) sousparties_.get(i);
          _txt += "<h" + (niveau_ + 2) + "><a class=\"partie" + niveau_ + "\" name=\""
              + _node.getPartie().toLowerCase() + "\">" + (i + 1) + ". " + _node.getTitre() + "</a></h" + (niveau_ + 2)
              + ">" + OscarLib.LINE_SEPARATOR;
          _txt += _node.getContent();
        }
      }
      return _txt;
    }
  }
  class HtmlDoc {
    Vector parties_ = null;
    int tdmprofondeur_ = 3;

    public HtmlDoc() {
      parties_ = new Vector();
    }

    public void setTdmProfondeur(final int _n) {
      if ((_n > 0) && (_n <= 3)) {
        tdmprofondeur_ = _n;
      }
    }

    public int getTdmProfondeur() {
      return tdmprofondeur_;
    }

    public void addPartie(final HtmlPartie _p) {
      parties_.add(_p);
    }

    public String summarize() {
      String _html = "";
      HtmlPartie _node = null;
      _html = "<h2>Sommaire</h2>" + OscarLib.LINE_SEPARATOR;
      if (parties_.size() > 0) {
        for (int i = 0; i < parties_.size(); i++) {
          _node = (HtmlPartie) parties_.get(i);
          _html += "<a class=\"partie1\" href=\"#" + _node.getPartie().toLowerCase() + "\">" + (i + 1) + ". "
              + _node.getTitre() + "</a><br>" + OscarLib.LINE_SEPARATOR;
          _html += _node.summarize();
        }
      }
      return _html;
    }

    public String getContent() {
      String _txt = "";
      HtmlPartie _node = null;
      if (parties_.size() > 0) {
        for (int i = 0; i < parties_.size(); i++) {
          _node = (HtmlPartie) parties_.get(i);
          _txt += "<h2><a class=\"partie1\" name=\"" + _node.getPartie().toLowerCase() + "\">" + (i + 1) + ". "
              + _node.getTitre() + "</a></h2>" + OscarLib.LINE_SEPARATOR;
          _txt += _node.getContent();
        }
      }
      return _txt;
    }
  }
}
