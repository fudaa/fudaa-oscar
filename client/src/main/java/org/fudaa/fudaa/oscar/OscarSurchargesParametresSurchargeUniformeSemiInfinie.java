/*
 * @file         OscarSurchargesParametresSurchargeUniformeSemiInfinie.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;
import com.memoire.bu.BuTextField;


/**
 * Description du sous-onglet 'Surcharge Uniforme Semi-Infinie' de l'onglet des parametres de Surcharges.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarSurchargesParametresSurchargeUniformeSemiInfinie extends OscarAbstractOnglet {
  /**
   * Champs valeur de la surcharge
   */
  private BuTextField tf_surchsi;

  /**
   *
   */
  public OscarSurchargesParametresSurchargeUniformeSemiInfinie(final OscarSurchargesParametres _surcharges,
      final String _helpfile) {
    super(_surcharges.getApplication(), _surcharges, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    // Bloc 'Surcharge'
    tf_surchsi = OscarLib.DoubleField("#0.00", OscarMsg.INITIALVALUE005);
    final BuPanel pn1 = new BuPanel();
    pn1.setLayout(OscarLib.GridLayout(2));
    pn1.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB019));
    final BuLabel lb_surchsi_val = OscarLib.Label(OscarMsg.LAB050);
    final BuLabel lb_surchsi_symb = OscarLib.Label("Q");
    final BuLabel lb_surchsi_equal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_surchsi_unit = OscarLib.Label(OscarMsg.UNITLAB_KILO_NEWTON_PAR_METRE_CARRE);
    final BuPicture _bp = new BuPicture(OscarLib.getImage("surcharge-uniforme.gif"));
    final BuPanel pn2 = new BuPanel();
    pn2.setLayout(OscarLib.GridLayout(5));
    pn2.add(lb_surchsi_val);
    pn2.add(lb_surchsi_symb);
    pn2.add(lb_surchsi_equal);
    pn2.add(tf_surchsi);
    pn2.add(lb_surchsi_unit);
    pn1.add(pn2);
    pn1.add(_bp);
    _p.add(pn1);
    return _p;
  }

  /**
   *
   */
  public Double getValeurSurcharge() {
    return ((Double) tf_surchsi.getValue());
  }

  /**
   *
   */
  public synchronized void setValeurSurcharge(final double _p) {
    tf_surchsi.setValue(new Double(_p));
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _e) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT006);
    _mv.addMessages(validation().getMessages());
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    final Double _vs = getValeurSurcharge();
    if (_vs == null) {
      _vm.add(OscarMsg.VMSG036);
    } else if (_vs.doubleValue() < 0.0) {
      _vm.add(OscarMsg.VMSG037);
    }
    return _vm;
  }
}
