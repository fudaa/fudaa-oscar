/*
 * @file         OscarIncertitudesParametres.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;


/**
 * Onglet incertitudes (paramètres)
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarIncertitudesParametres extends OscarAbstractOnglet {
  JCheckBox _c1;
  JCheckBox _c2;
  JCheckBox _c3;
  JCheckBox _c4;
  BuTextField _t1;
  BuTextField _t2;
  BuTextField _t3;
  BuTextField _t4;
  JLabel _l1;
  JLabel _l2;
  JLabel _l3;
  JLabel _l4;

  public OscarIncertitudesParametres(final OscarFilleParametres _fp, final String _helpfile) {
    super(_fp.getApplication(), _fp, _helpfile);
  }

  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    final BuVerticalLayout lo1 = OscarLib.VerticalLayout();
    _p.setLayout(lo1);
    _p.setBorder(OscarLib.EmptyBorder());
    // Bloc 'Informations'
    final JPanel _p0 = OscarLib.InfoAidePanel(OscarMsg.LAB042, this);
    final JPanel _p1 = OscarLib.TitledPanel(OscarMsg.TITLELAB018);
    _p1.setLayout(OscarLib.GridLayout(3));
    // critere 1
    _c1 = new JCheckBox(OscarMsg.LAB043);
    _t1 = OscarLib.DoubleField("#0.00");
    _l1 = new JLabel(OscarMsg.UNITLAB_METRE);
    _c1.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        toggleCriteria(1);
      }
    });
    _p1.add(_c1);
    _p1.add(_t1);
    _p1.add(_l1);
    // critere 2
    _c2 = new JCheckBox(OscarMsg.LAB044);
    _t2 = OscarLib.DoubleField("#0.00");
    _l2 = new JLabel(OscarMsg.UNITLAB_METRE);
    _c2.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        toggleCriteria(2);
      }
    });
    _p1.add(_c2);
    _p1.add(_t2);
    _p1.add(_l2);
    // critere 3
    _c3 = new JCheckBox(OscarMsg.LAB045);
    _t3 = OscarLib.DoubleField("#0.00");
    _l3 = new JLabel("");
    _c3.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        toggleCriteria(3);
      }
    });
    _p1.add(_c3);
    _p1.add(_t3);
    _p1.add(_l3);
    // critere 4
    _c4 = new JCheckBox(OscarMsg.LAB046);
    _t4 = OscarLib.DoubleField("#0.00");
    _l4 = new JLabel(OscarMsg.UNITLAB_KILO_NEWTON_PAR_METRE_CARRE);
    _c4.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        toggleCriteria(4);
      }
    });
    _p1.add(_c4);
    _p1.add(_t4);
    _p1.add(_l4);
    _p.add(_p0);
    _p.add(_p1);
    toggleCriteria(1, false);
    toggleCriteria(2, false);
    toggleCriteria(3, false);
    toggleCriteria(4, false);
    return _p;
  }

  void toggleCriteria(final int _n) {
    switch (_n) {
    case 1: {
      toggleCriteria(_n, _c1.isSelected());
      break;
    }
    case 2: {
      toggleCriteria(_n, _c2.isSelected());
      break;
    }
    case 3: {
      toggleCriteria(_n, _c3.isSelected());
      break;
    }
    case 4: {
      toggleCriteria(_n, _c4.isSelected());
      break;
    }
    }
  }

  private void toggleCriteria(final int _n, final boolean _v) {
    switch (_n) {
    case 1: {
      _l1.setEnabled(_v);
      _t1.setEnabled(_v);
      break;
    }
    case 2: {
      _l2.setEnabled(_v);
      _t2.setEnabled(_v);
      break;
    }
    case 3: {
      _l3.setEnabled(_v);
      _t3.setEnabled(_v);
      break;
    }
    case 4: {
      _l4.setEnabled(_v);
      _t4.setEnabled(_v);
      break;
    }
    }
  }

  /**
   *
   */
  public Double getCoteNappeEauCotePoussee() {
    return (Double) _t1.getValue();
  }

  /**
   *
   */
  public Double getCoteToitSolCoteButee() {
    return (Double) _t2.getValue();
  }

  /**
   *
   */
  public Double getCoefficientProprietesGeotechniques() {
    return (Double) _t3.getValue();
  }

  /**
   *
   */
  public Double getSurchargeUniformeSemiInfinie() {
    return (Double) _t4.getValue();
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _e) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT017);
    _mv.addMessages(validation().getMessages());
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    final boolean _cnecp = _c1.isSelected();
    final boolean _ctscb = _c2.isSelected();
    final boolean _cpg = _c3.isSelected();
    final boolean _susi = _c4.isSelected();
    double _min;
    double _max;
    final Double _cnecpV = getCoteNappeEauCotePoussee();
    final Double _ctscbV = getCoteToitSolCoteButee();
    final Double _cpgV = getCoefficientProprietesGeotechniques();
    final Double _susiV = getSurchargeUniformeSemiInfinie();
    // Double _cntmin = ((OscarSolParametres)getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteBasDuSol();
    // Double _cntmax = ((OscarSolParametres)getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteHautDuSol();
    Double _dmin = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteFondDeSouille();
    Double _dmax = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteHautDuSol();
    if (_dmin == null) {
      _min = Double.MAX_VALUE;
    } else {
      _min = _dmin.doubleValue();
    }
    if (_dmax == null) {
      _max = Double.MIN_VALUE;
    } else {
      _max = _dmax.doubleValue();
    }
    if (_cnecp && ((_cnecpV == null) || (_cnecpV.doubleValue() < _min) || (_cnecpV.doubleValue() > _max))) {
      _vm.add(OscarMsg.VMSG052);
    }
    _dmin = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteBasDuSolCotePoussee();
    _dmax = ((OscarSolParametres) getFilleParametres().getOnglet(OscarLib.ONGLET_SOL)).getCoteHautDuSol();
    if (_dmin == null) {
      _min = Double.MAX_VALUE;
    } else {
      _min = _dmin.doubleValue();
    }
    if (_dmax == null) {
      _max = Double.MIN_VALUE;
    } else {
      _max = _dmax.doubleValue();
    }
    if (_ctscb && ((_ctscbV == null) || (_ctscbV.doubleValue() < _min) || (_ctscbV.doubleValue() > _max))) {
      _vm.add(OscarMsg.VMSG053);
    }
    if (_cpg && ((_cpgV == null) || (_cpgV.doubleValue() < 1.0))) {
      _vm.add(OscarMsg.VMSG054);
    }
    if (_susi && ((_susiV == null) || (_susiV.doubleValue() < 0.0))) {
      _vm.add(OscarMsg.VMSG055);
    }
    return _vm;
  }
}
