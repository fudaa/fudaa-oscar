package org.fudaa.fudaa.oscar;

import java.io.File;

import javax.swing.JComponent;

import org.fudaa.dodico.corba.oscar.SParametresOscar;
import org.fudaa.dodico.corba.oscar.SResultatsOscar;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;

import org.fudaa.fudaa.commun.projet.FudaaProjet;


public abstract class WHtmlContent {
  FudaaProjet project_ = null;
  OscarFilleNoteDeCalculs.Settings settings_ = null;
  String html_ = null;
  SParametresOscar params_ = null;
  SResultatsOscar results_ = null;
  String fichier_;
  JComponent parent_;

  protected WHtmlContent() {
    this(null, null, null);
  }

  protected WHtmlContent(final JComponent _parent, final FudaaProjet _project,
      final OscarFilleNoteDeCalculs.Settings _settings) {
    project_ = _project;
    settings_ = _settings;
    html_ = "";
    parent_ = _parent;
    if (_project != null) {
      params_ = (SParametresOscar) project_.getParam(OscarResource.PARAMETRES);
      results_ = (SResultatsOscar) project_.getResult(OscarResource.RESULTATS);
    } else {
      params_ = null;
      results_ = null;
    }
    if (settings_ != null) {
      fichier_ = (String) settings_.lit("NOM_DU_FICHIER_HTML");
      if (fichier_ == null) {
        fichier_ = OscarLib.NOTEDECALCULS_TEMP_HTMLFILE;
      }
    } else {
      fichier_ = OscarLib.NOTEDECALCULS_TEMP_HTMLFILE;
    }
  }

  protected abstract void buildContent();

  protected void addPar(final String _txt) {
    html_ += "<p>" + _txt + "</p>" + OscarLib.LINE_SEPARATOR;
  }

  protected FudaaProjet getProject() {
    return project_;
  }

  protected OscarFilleNoteDeCalculs.Settings getSettings() {
    return settings_;
  }

  protected void addTable(final Object[] _colnames, final Object[][] _data) {
    html_ += "<table border=\"1\">" + OscarLib.LINE_SEPARATOR;
    html_ += "<tr>" + OscarLib.LINE_SEPARATOR;
    for (int i = 0; i < _colnames.length; i++) {
      html_ += "<td>" + _colnames[i] + "</td>";
    }
    html_ += "</tr>" + OscarLib.LINE_SEPARATOR;
    for (int j = 0; j < _data.length; j++) {
      html_ += "<tr>" + OscarLib.LINE_SEPARATOR;
      for (int k = 0; k < _data[j].length; k++) {
        html_ += "<td>" + _data[j][k] + "</td>";
      }
      html_ += "</tr>" + OscarLib.LINE_SEPARATOR;
    }
    html_ += "</table>" + OscarLib.LINE_SEPARATOR;
  }

  protected void addImgFromScript(final String _script, final String _imgname, final int _w, final int _h,
      final String _alt) {
    final String _img = fichier_ + "files" + OscarLib.FILE_SEPARATOR + _imgname;
    OscarLib.exportGraphScriptToGif(_script, _img);
    html_ += "<p><a href=\"" + fichier_.substring(fichier_.lastIndexOf(OscarLib.FILE_SEPARATOR)+1)+"files/"+_imgname + "\" target=\"_blank\"><img src=\"" + fichier_.substring(fichier_.lastIndexOf(OscarLib.FILE_SEPARATOR)+1)+"files/"+_imgname + "\" border=\"0\" "
        + ((_w == -1) ? "" : ("width=\"" + _w + "\"")) + " " + ((_h == -1) ? "" : ("height=\"" + _h + "\""))
        + " alt=\"" + _alt + "\"></a></p>" + OscarLib.LINE_SEPARATOR;
  }

  public String getContent() {
    buildContent();
    return html_;
  }

  public static WHtmlContent createContentFor(final String _type, final JComponent _parent, final FudaaProjet _project,
      final OscarFilleNoteDeCalculs.Settings _settings) {
    WHtmlContent _content = new Vide();
    if (_type.equals("PRESENTATION")) {
      _content = new Pr(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES")) {
      _content = new RH(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_SOL")) {
      _content = new RHSol(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_EAU")) {
      _content = new RHEau(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_SURCHARGES")) {
      _content = new RHSurcharges(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_EFFORTS")) {
      _content = new RHEfforts(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_EFFORTS_CVS")) {
      _content = new RHEffortsCvs(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_EFFORTS_CHS")) {
      _content = new RHEffortsChs(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_EFFORTS_TETEDERIDEAU")) {
      _content = new RHEffortsTdr(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_EFFORTS_FDIAG")) {
      _content = new RHEffortsFdiag(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_OUVRAGE")) {
      _content = new RHOuvrage(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_OUVRAGE_ANCRAGE")) {
      _content = new RHOuvrageAncrage(_parent, _project, _settings);
    } else if (_type.equals("RAPPELHYPOTHESES_OUVRAGE_PALPLANCHES")) {
      _content = new RHOuvragePalplanches(_parent, _project, _settings);
    } else if (_type.equals("CALCULS")) {
      _content = new Ca(_parent, _project, _settings);
    } else if (_type.equals("RESULTATS")) {
      _content = new Re(_parent, _project, _settings);
    } else if (_type.equals("RESULTATS_DIAGRAMME")) {
      _content = new ReDiag(_parent, _project, _settings);
    } else if (_type.equals("RESULTATS_PREDIMENSIONNEMENT")) {
      _content = new RePredim(_parent, _project, _settings);
    } else if (_type.equals("RESULTATS_MOMENT")) {
      _content = new ReMom(_parent, _project, _settings);
    } else if (_type.equals("RESULTATS_DEFORMEE")) {
      _content = new ReDef(_parent, _project, _settings);
    } else if (_type.equals("RESULTATS_EFFORTS")) {
      _content = new ReEff(_parent, _project, _settings);
    }
    return _content;
  }
  protected static class Vide extends WHtmlContent {
    public Vide() {}

    protected void buildContent() {}
  }
  protected static class Pr extends WHtmlContent {
    public Pr(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      final String _software = OscarImplementation.SOFTWARE_TITLE;
      final String _version = OscarImplementation.informationsSoftware().version;
      final String _vdate = OscarImplementation.informationsSoftware().date;
      final String _file = getProject().getFichier();
      final String _auteur = (String) getSettings().lit("PRESENTATION_AUTEUR");
      addPar("Cette note a pour objet de d&eacute;crire les calculs r&eacute;alis&eacute;s " + ((_auteur == null) ? "" : ("par " + _auteur))
          + " avec le logiciel <i>" + _software + "</i> (version " + _version + " du " + _vdate
          + ") du C.E.T.M.E.F dont les donn&eacute;es sont enregistr&eacute;es dans le fichier \"<b>" + _file + "</b>\".");
    }
  }
  protected static class RH extends WHtmlContent {
    public RH(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {}
  }
  protected static class RHSol extends WHtmlContent {
    public RHSol(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      final int _nbcouchespou = params_.sol.nombreCouchesPoussee;
      final int _nbcouchesbut = params_.sol.nombreCouchesButee;
      final boolean _ctrTot = params_.parametresGeneraux.choixCalculSol.equals(OscarLib.IDL_SOL_CHOIXCALCUL_TOTAL) ? true
          : false;
      addPar("Le comportement du sol est &eacute;tudi&eacute; en <b> contraintes " + ((_ctrTot) ? "totales" : "effectives") + "</b>.");
      addPar("La cote du toit du sol c&ocirc;t&eacute; pouss&eacute;e est de <b>" + params_.parametresGeneraux.coteTerrePleinPoussee
          + " m</b>.");
      addPar("La cote du toit du sol c&ocirc;t&eacute; but&eacute;e est de <b>" + params_.parametresGeneraux.coteTerrePleinButee
          + " m</b>.");
      addPar("La coupe du sol est la suivante :");
      try {
        final String _script = ((OscarSolParametres) ((OscarFilleParametres) ((OscarFilleNoteDeCalculs) parent_)
            .getInternalFrame(OscarLib.INTERNAL_FRAME_PARAMETRES)).getOnglet(OscarLib.ONGLET_SOL))
            .getCouchesGrapheScript();
        addImgFromScript(_script, "coupedusol.gif", -1, -1, "Couches de sol");
      } catch (final Throwable _e) {}
      addPar("Les couches de sol ont les caract&eacute;ristiques suivantes :");
      int i;
      Object[] _couches;
      Object[] _ep;
      Object[] _pvh;
      Object[] _pvd;
      Object[] _cp;
      Object[] _af;
      Object[] _tc;
      Object[] _c;
      // Pouss�e
      _couches = new Object[_nbcouchespou + 1];
      _ep = new Object[_nbcouchespou + 1];
      _pvh = new Object[_nbcouchespou + 1];
      _pvd = new Object[_nbcouchespou + 1];
      _cp = new Object[_nbcouchespou + 1];
      _af = new Object[_nbcouchespou + 1];
      _tc = new Object[_nbcouchespou + 1];
      _c = new Object[_nbcouchespou + 1];
      _couches[0] = "";
      _ep[0] = "Epaisseur (m)";
      _pvh[0] = "Poids volumique humide (kN/m<sup>3</sup>)";
      _pvd[0] = "Poids volumique " + ((_ctrTot) ? "satur&eacute;" : "d&eacute;jaug&eacute;") + " (kN/m<sup>3</sup>)";
      _cp[0] = "Coefficient de pouss&eacute;e";
      _c[0] = "Coh&eacute;sion " + ((_ctrTot) ? "non drain&eacute;e" : "drain&eacute;e") + " (kN/m<sup>2</sup>)";
      _af[0] = "Angle de frottement " + ((_ctrTot) ? "non drain&eacute;" : "drain&eacute;") + " (degr&eacute;s)";
      _tc[0] = "Terme de coh&eacute;sion (kN/m<sup>2</sup>)";
      for (i = 1; i < (_nbcouchespou + 1); i++) {
        _couches[i] = "Couche " + i;
        _ep[i] = "" + params_.sol.couchesPoussee[i - 1].epaisseur;
        _pvh[i] = "" + params_.sol.couchesPoussee[i - 1].poidsVolumiqueHumide;
        _pvd[i] = "" + params_.sol.couchesPoussee[i - 1].poidsVolumique;
        _cp[i] = "" + params_.sol.couchesPoussee[i - 1].coefficient;
        _af[i] = "" + params_.sol.couchesPoussee[i - 1].angleFrottement;
        _c[i] = "" + params_.sol.couchesPoussee[i - 1].cohesion;
        _tc[i] = ""
            + Math
                .round(OscarLib
                    .round(
                        ((params_.sol.couchesPoussee[i - 1].angleFrottement != 0.0) ? ((1 - params_.sol.couchesPoussee[i - 1].coefficient)
                            * params_.sol.couchesPoussee[i - 1].cohesion / Math
                            .tan(params_.sol.couchesPoussee[i - 1].angleFrottement))
                            : (2 * params_.sol.couchesPoussee[i - 1].cohesion)), 2) * 1000.0) / 1000.0;
      }
      addPar("<i>En pouss&eacute;e</i>");
      addTable(_couches, new Object[][] { _ep, _pvh, _pvd, _cp, _c, _af, _tc });
      // But�e
      _couches = new Object[_nbcouchesbut + 1];
      _ep = new Object[_nbcouchesbut + 1];
      _pvh = new Object[_nbcouchesbut + 1];
      _pvd = new Object[_nbcouchesbut + 1];
      _cp = new Object[_nbcouchesbut + 1];
      _af = new Object[_nbcouchesbut + 1];
      _tc = new Object[_nbcouchesbut + 1];
      _c = new Object[_nbcouchesbut + 1];
      _couches[0] = "";
      _ep[0] = "Epaisseur (m)";
      _pvh[0] = "Poids volumique humide (kN/m3)";
      _pvd[0] = "Poids volumique " + ((_ctrTot) ? "satur&eacute;" : "d&eacute;jaug&eacute;") + " (kN/m3)";
      _cp[0] = "Coefficient de pouss&eacute;e";
      _c[0] = "Coh&eacute;sion " + ((_ctrTot) ? "non drain&eacute;e" : "drain&eacute;e") + " (kN/m2)";
      _af[0] = "Angle de frottement " + ((_ctrTot) ? "non drain&eacute;" : "drain&eacute;") + " (degr&eacute;s)";
      _tc[0] = "Terme de coh&eacute;sion (kN/m2)";
      for (i = 1; i < (_nbcouchesbut + 1); i++) {
        _couches[i] = "Couche " + i;
        _ep[i] = "" + params_.sol.couchesButee[i - 1].epaisseur;
        _pvh[i] = "" + params_.sol.couchesButee[i - 1].poidsVolumiqueHumide;
        _pvd[i] = "" + params_.sol.couchesButee[i - 1].poidsVolumique;
        _cp[i] = "" + params_.sol.couchesButee[i - 1].coefficient;
        _af[i] = "" + params_.sol.couchesButee[i - 1].angleFrottement;
        _c[i] = "" + params_.sol.couchesButee[i - 1].cohesion;
        _tc[i] = ""
            + Math
                .round(OscarLib
                    .round(
                        ((params_.sol.couchesButee[i - 1].angleFrottement != 0.0) ? ((params_.sol.couchesButee[i - 1].coefficient - 1)
                            * params_.sol.couchesButee[i - 1].cohesion / Math
                            .tan(params_.sol.couchesButee[i - 1].angleFrottement))
                            : (2 * params_.sol.couchesButee[i - 1].cohesion)), 2) * 1000.0) / 1000.0;
      }
      addPar("<i>En but&eacute;e</i>");
      addTable(_couches, new Object[][] { _ep, _pvh, _pvd, _cp, _c, _af, _tc });
    }
  }
  protected static class RHEau extends WHtmlContent {
    public RHEau(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      addPar("On consid&egrave;re l'action <b>"
          + (params_.eau.typeCalculEau.equals(OscarLib.IDL_EAU_CHOIXCALCUL_STATIC) ? "statique" : "dynamique")
          + "</b> de l'eau sur le rideau de sout&egrave;nement.");
      if (params_.eau.presenceNappeEauCotePoussee) {
        addPar("L'eau c&ocirc;t&eacute; pouss&eacute;e est &agrave; la cote " + params_.eau.coteNappeEauCotePoussee + " m.");
      } else {
        addPar("Il n'y a pas d'eau c&ocirc;t&eacute; pouss&eacute;e.");
      }
      if (params_.eau.presenceNappeEauCoteButee) {
        addPar("L'eau c&ocirc;t&eacute; but&eacute;e est &agrave; la cote " + params_.eau.coteNappeEauCoteButee + " m.");
      } else {
        addPar("Il n'y a pas d'eau c&ocirc;t&eacute; but&eacute;e.");
      }
      if (params_.eau.presenceNappeEauCotePoussee || params_.eau.presenceNappeEauCoteButee) {
        addPar("Le terrain a la configuration suivante :");
        try {
          final String _script = ((OscarEauParametres) ((OscarFilleParametres) ((OscarFilleNoteDeCalculs) parent_)
              .getInternalFrame(OscarLib.INTERNAL_FRAME_PARAMETRES)).getOnglet(OscarLib.ONGLET_EAU))
              .getNappeEauGrapheScript();
          addImgFromScript(_script, "terrain.gif", -1, -1, "Couches de sol");
        } catch (final Throwable _e) {}
      }
    }
  }
  protected static class RHSurcharges extends WHtmlContent {
    public RHSurcharges(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      if (!params_.surcharge.typeSurcharge.equals("")) {
        if (params_.surcharge.typeSurcharge.indexOf(OscarLib.IDL_SURCHARGE_TYPE_UNIFORME_SI) > -1) {
          addPar("La surcharge uniforme semi-infinie sur le terre-plein (c&ocirc;t&eacute; pouss&eacute;e) est &eacute;gale &agrave; <b>Q = "
              + params_.surcharge.valeurSurchargeUniformeSemiInfinie + " kN/m<sup>2</sup></b>.");
        }
      } else {
        addPar("Aucune surcharge n'est prise en compte.");
      }
    }
  }
  protected static class RHEfforts extends WHtmlContent {
    public RHEfforts(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {}
  }
  protected static class RHEffortsCvs extends WHtmlContent {
    public RHEffortsCvs(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      final int _nb = params_.effort.nombreContraintesVerticales;
      if (_nb > 0) {
        addPar("On consid&egrave;re les contraintes verticales suppl&eacute;mentaires suivantes :");
        int i;
        Object[][] _lignes;
        _lignes = new Object[_nb][];
        for (i = 0; i < _nb; i++) {
          _lignes[i] = new Object[] { "" + params_.effort.contraintesVerticales[i].cote,
              "" + params_.effort.contraintesVerticales[i].valeur };
        }
        addTable(new Object[] { "Cote", "Contrainte verticale" }, _lignes);
      } else {
        addPar("Aucune contrainte verticale suppl&eacute;mentaire.");
      }
    }
  }
  protected static class RHEffortsChs extends WHtmlContent {
    public RHEffortsChs(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      final int _nb = params_.effort.nombreContraintesHorizontales;
      if (_nb > 0) {
        addPar("On consid&egrave;re les contraintes horizontales suppl&eacute;mentaires suivantes :");
        int i;
        Object[][] _lignes;
        _lignes = new Object[_nb][];
        for (i = 0; i < _nb; i++) {
          _lignes[i] = new Object[] { "" + params_.effort.contraintesHorizontales[i].cote,
              "" + params_.effort.contraintesHorizontales[i].valeur };
        }
        addTable(new Object[] { "Cote", "Contrainte horizontale" }, _lignes);
      } else {
        addPar("Aucune contrainte horizontale suppl&eacute;mentaire.");
      }
    }
  }
  protected static class RHEffortsTdr extends WHtmlContent {
    public RHEffortsTdr(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      addPar("La force horizontale en t&ecirc;te est de <b>F = " + params_.effort.effortEnTeteDeRideau.valeurForceEnTete
          + " kN/ml.</b>");
      addPar("Le moment en t&ecirc;te est de <b>M = " + params_.effort.effortEnTeteDeRideau.valeurMomentEnTete
          + " kN.m/ml.</b>");
    }
  }
  protected static class RHEffortsFdiag extends WHtmlContent {
    public RHEffortsFdiag(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      final int _nb = params_.effort.fichiersDeDiagramme.length;
      if (_nb > 0) {
        addPar("On consid&egrave;re les fichiers de diagramme suivants, en plus du diagramme de pression d&ucirc; &agrave; l'action du terrain et de l'eau d&eacute;finis dans les param&egrave;tres (ce diagramme est donn&eacute; plus loin) :");
        for (int i = 0; i < _nb; i++) { // Pour chaque fichier de diagramme suppl�mentaire...
          html_ += OscarLib.getCsvFileInHtml(params_.effort.fichiersDeDiagramme[i].fichier);
        }
      } else {
        addPar("Aucun fichier de diagramme suppl&eacute;mentaire.");
      }
    }
  }
  protected static class RHOuvrage extends WHtmlContent {
    public RHOuvrage(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {}
  }
  protected static class RHOuvrageAncrage extends WHtmlContent {
    public RHOuvrageAncrage(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      String _a, _b, _c;
      if (params_.ouvrage.presenceNappeTirants.equals("O")) {
        addPar("Le rideau est <b>ancr&eacute;</b> &agrave; la cote <b>" + params_.ouvrage.coteNappeTirants + " m</b>.");
        addPar("L'<b>encastrement</b> du rideau dans le sol est suppos&eacute; &eacute;gal &agrave; <b>"
            + params_.ouvrage.pourcentageEncastrementNappe + "%</b>.");
        _a = (params_.ouvrage.espaceEntreDeuxTirants != VALEUR_NULLE.value) ? ("<li>l'espacement entre deux tirants est de <b>"
            + params_.ouvrage.espaceEntreDeuxTirants + " m</b>,")
            : null;
        _b = (params_.ouvrage.sectionTirants != VALEUR_NULLE.value) ? ("<li>la section d'un tirant est de <b>"
            + OscarLib.round((params_.ouvrage.sectionTirants * 10000), 2) + " cm<sup>2</sup></b> (soit "
            + OscarLib.round(params_.ouvrage.sectionTirants, 6) + " m<sup>2</sup>),") : null;
        _c = (params_.ouvrage.limiteElastiqueAcierTirants != VALEUR_NULLE.value) ? ("<li>la limite &eacute;lastique de l'acier est de <b>"
            + params_.ouvrage.limiteElastiqueAcierTirants + " MPa</b>.")
            : null;
        if ((_a != null) && (_b != null) && (_c != null)) {
          addPar("Les caract&eacute;ristiques de la nappe d'ancrage sont les suivantes :");
          addPar("<ul>" + _a + _b + _c + "</ul>");
        } else {
          addPar("Certaines donn&eacute;es facultatives relatives aux tirants n'ont pas &eacute;t&eacute; renseign&eacute;es.");
        }
      } else {
        addPar("Le rideau n'est <b>pas ancr&eacute;</b>.");
      }
    }
  }
  protected static class RHOuvragePalplanches extends WHtmlContent {
    public RHOuvragePalplanches(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      String _a, _b, _c, _d;
      _a = (params_.ouvrage.moduleYoungAcier != VALEUR_NULLE.value) ? ("<li>le module d'Young de l'acier vaut <b>"
          + params_.ouvrage.moduleYoungAcier + " MPa</b>,") : null;
      _b = (params_.ouvrage.limiteElastiqueAcier != VALEUR_NULLE.value) ? ("<li>la limite &eacute;lastique de l'acier vaut <b>"
          + params_.ouvrage.limiteElastiqueAcier + " MPa</b>,")
          : null;
      _c = (params_.ouvrage.inertiePalplanches != VALEUR_NULLE.value) ? ("<li>l'inertie des palplanches vaut <b>"
          + params_.ouvrage.inertiePalplanches + " cm4/ml</b>,") : null;
      _d = (params_.ouvrage.demieHauteur != VALEUR_NULLE.value) ? ("<li>la demi-hauteur des palplanches vaut <b>"
          + (params_.ouvrage.demieHauteur) + " cm</b>.") : null;
      if ((_a != null) && (_b != null) && (_c != null) && (_d != null)) {
        addPar("Les palplanches ont les caract&eacute;ristiques suivantes :");
        addPar("<ul>" + _a + _b + _c + _d + "</ul>");
      } else {
        addPar("Certaines donn&eacute;es facultatives relatives aux palplanches n'ont pas &eacute;t&eacute; renseign&eacute;es.");
      }
    }
  }
  protected static class Ca extends WHtmlContent {
    public Ca(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      if (params_.ouvrage.presenceNappeTirants.equals("O")) {
        addPar("Le rideau &eacute;tant ancr&eacute;, on utilise la <b>m&eacute;thode de la ligne &eacute;lastique</b>.");
      } else {
        addPar("Le rideau n'&eacute;tant pas ancr&eacute;, on utilise la <b>m&eacute;thode du rideau encastr&eacute; en pied</b>.");
      }
      if ((results_.predimensionnement == null) || (results_.pressions == null) || (results_.moments == null)
          || (results_.deformees == null) || (results_.efforts == null)) {
        String _msg = "";
        try {
          for (int i = 0; i < results_.erreurs.messages.length; i++) {
            _msg += ((_msg.equals("")) ? "" : "<br>") + results_.erreurs.messages[i];
          }
        } catch (final Throwable _e) {
          _msg = "Aucun message d'erreur disponible.";
        }
        addPar("<font color=red><b><u>Attention</u> : le logiciel "
            + OscarImplementation.SOFTWARE_TITLE
            + " a rencontr&eacute; des difficult&eacute;s pour effectuer la totalit&eacute; des calculs. Le code de calcul a renvoy&eacute; le message d'erreur suivant:</b><br>"
            + _msg
            + "<br><b>Vous trouverez ci-dessous les r&eacute;sultats disponibles mais nous vous conseillons de modifier les donn&eacute;es afin de corriger l'erreur.</b></font>");
      }
    }
  }
  protected static class Re extends WHtmlContent {
    public Re(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {}
  }
  protected static class ReDiag extends WHtmlContent {
    public ReDiag(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      final int _nb = results_.pressions.pointsCotePression.length;
      if (_nb > 0) {
        String _diagfilenames = "";
        for (int i = 0; i < params_.effort.fichiersDeDiagramme.length; i++) {
          _diagfilenames += (_diagfilenames.equals("") ? "" : ", ")
              + (new File(params_.effort.fichiersDeDiagramme[i].fichier)).getName();
        }
        addPar("Le terrain (sol et eau), les surcharges, les &eacute;ventuelles contraintes horizontales et verticales suppl&eacute;mentaires agissent sur le rideau de sout&egrave;nement en pouss&eacute;e et en but&eacute;e selon le diagramme de pressions suivant :");
        int i;
        Object[][] _lignes;
        _lignes = new Object[_nb][];
        for (i = 0; i < _nb; i++) {
          _lignes[i] = new Object[] { OscarLib.formatterNombre(results_.pressions.pointsCotePression[i].cote),
              OscarLib.formatterNombre("pression", results_.pressions.pointsCotePression[i].valeur) };
        }
        addTable(new Object[] { "Cote (m)", "Pression (kN/m<sup>2</sup>)" }, _lignes);
        addPar("Ce diagramme de pressions peut &ecirc;tre sch&eacute;matis&eacute; par le dessin suivant :");
        try {
          final String _script = ((OscarDiagrammePressionsResultatsBruts) ((OscarFilleResultatsBruts) ((OscarFilleNoteDeCalculs) parent_)
              .getInternalFrame(OscarLib.INTERNAL_FRAME_RESULTATSBRUTS)).getOnglet(OscarLib.ONGLET_PRESSION))
              .getDiagrammePressionsGrapheScript();
          addImgFromScript(_script, "pressions.gif", -1, -1, "diagramme de pressions");
        } catch (final Throwable _e) {}
        if (!_diagfilenames.equals("")) {
          addPar("Ce diagramme de pressions est ajout&eacute; aux diagrammes de pressions suppl&eacute;mentaires (" + _diagfilenames
              + ") indiqu&eacute;s ci-avant, pour pr&eacute;dimensionner l'&eacute;cran de sout&egrave;nement.");
        } else {
          addPar("Ce diagramme de pressions va servir &agrave; pr&eacute;dimensionner l'&eacute;cran de sout&egrave;nement (aucun fichier de diagramme suppl&eacute;mentaire n'a &eacute;t&eacute; d&eacute;fini).");
        }
      } else {
        addPar("Aucun diagramme de pressions disponible.");
      }
    }
  }
  protected static class RePredim extends WHtmlContent {
    public RePredim(final JComponent _parent, final FudaaProjet _project,
        final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      Object[][] _data;
      String _p1;
      String _p2;
      final double momax = Math.abs(results_.predimensionnement.momentMaximum.valeur);
      final double momin = Math.abs(results_.predimensionnement.momentMinimum.valeur);
      if (params_.ouvrage.presenceNappeTirants.equals(OscarLib.IDL_OUVRAGE_CHOIXTIRANTS_OUI)) {
        _data = new Object[][] {
            { "Cote de pression nulle", OscarLib.formatterNombre(results_.predimensionnement.cotePressionNull) + " m" },
            { "Point de contre-but&eacute;e", OscarLib.formatterNombre(results_.predimensionnement.pointButeeSimple) + " m" },
            { "<b>Pied (20% contre-but&eacute;e)</b>", "<b>" + OscarLib.formatterNombre(results_.predimensionnement.pied20Pourcent) + " m</b>" },
            { "Pied (contre-but&eacute;e calcul&eacute;e)", OscarLib.formatterNombre(results_.predimensionnement.piedContreButee) + " m" },
            {
                ((momax > momin) ? "<b>" : "") + "Valeur du moment maximum" + ((momax > momin) ? "</b>" : ""),
                ((momax > momin) ? "<b>" : "") + OscarLib.formatterNombre("moment", results_.predimensionnement.momentMaximum.valeur) + " kN.m/ml"
                    + ((momax > momin) ? "</b>" : "") },
            { "Cote du moment maximum", OscarLib.formatterNombre(results_.predimensionnement.momentMaximum.cote) + " m" },
            {
                ((momin > momax) ? "<b>" : "") + "Valeur du moment minimum" + ((momin > momax) ? "</b>" : ""),
                ((momin > momax) ? "<b>" : "") + OscarLib.formatterNombre("moment", results_.predimensionnement.momentMinimum.valeur) + " kN.m/ml"
                    + ((momin > momax) ? "</b>" : "") },
            { "Cote du moment minimum", OscarLib.formatterNombre(results_.predimensionnement.momentMinimum.cote) + " m" },
            { "Encastrement demand&eacute;", OscarLib.formatterNombre(results_.predimensionnement.encastrement) + "%" },
            { "R&eacute;action d'ancrage", OscarLib.formatterNombre(results_.predimensionnement.reactionAncrage) + " kN/ml" },
            { "Niveau d'ancrage", OscarLib.formatterNombre(results_.predimensionnement.niveauAncrage) + " m" } };
        if (results_.predimensionnement.encastrement >= 100) {
          _p1 = "Le rideau est ancr&eacute; et l'encastrement est &eacute;gal &agrave; 100%. La fiche pertinente &agrave; retenir est celle obtenue par majoration forfaitaire de 20% de la hauteur entre le point de pression nulle et le point d'application de la contre-but&eacute;e. Le pied du rideau est donc, d'apr&egrave;s le tableau ci-dessus, &agrave; la cote "
              + results_.predimensionnement.pied20Pourcent + " m.";
          _p2 = "Par cons&eacute;quent, la longueur totale du rideau vaut "
              + params_.parametresGeneraux.coteTerrePleinPoussee
              + " - "
              + results_.predimensionnement.pied20Pourcent
              + " = "
              + OscarLib.round(
                  (params_.parametresGeneraux.coteTerrePleinPoussee - results_.predimensionnement.pied20Pourcent), 2)
              + " m.";
        } else {
          _p1 = "Le rideau est ancr&eacute; et l'encastrement est &eacute;gal &agrave; "
              + results_.predimensionnement.encastrement
              + "% (alors que le CETMEF pr&eacute;conise de prendre 100%). La fiche pertinente &agrave; retenir est celle pour laquelle la contre-but&eacute;e a &eacute;t&eacute; calcul&eacute;e effectivement (et non celle obtenue par majoration forfaitaire de 20%). Le pied du rideau doit donc &ecirc;tre, d'apr&egrave;s le tableau ci-dessus, &agrave; la cote "
              + results_.predimensionnement.piedContreButee + " m.";
          _p2 = "Par cons&eacute;quent, la longueur totale du rideau vaut "
              + params_.parametresGeneraux.coteTerrePleinPoussee
              + " - "
              + results_.predimensionnement.piedContreButee
              + " = "
              + OscarLib.round(
                  (params_.parametresGeneraux.coteTerrePleinPoussee - results_.predimensionnement.piedContreButee), 2)
              + " m.";
        }
      } else {
        _data = new Object[][] {
            { "Cote de pression nulle", results_.predimensionnement.cotePressionNull + " m" },
            { "Point de but&eacute;e simple", results_.predimensionnement.pointButeeSimple + " m" },
            { "Pied (20% contre-but&eacute;e)", results_.predimensionnement.pied20Pourcent + " m" },
            { "<b>Pied (contre-but&eacute;e calcul&eacute;e)</b>", "<b>" + results_.predimensionnement.piedContreButee + " m</b>" },
            {
                ((momax > momin) ? "<b>" : "") + "Valeur du moment maximum" + ((momax > momin) ? "</b>" : ""),
                ((momax > momin) ? "<b>" : "") + results_.predimensionnement.momentMaximum.valeur + " kN.m/ml"
                    + ((momax > momin) ? "</b>" : "") },
            { "Cote du moment maximum", results_.predimensionnement.momentMaximum.cote + " m" },
            {
                ((momin > momax) ? "<b>" : "") + "Valeur du moment minimum" + ((momin > momax) ? "</b>" : ""),
                ((momin > momax) ? "<b>" : "") + results_.predimensionnement.momentMinimum.valeur + " kN.m/ml"
                    + ((momin > momax) ? "</b>" : "") },
            { "Cote du moment minimum", results_.predimensionnement.momentMinimum.cote + " m" } };
        _p1 = "Le rideau n'est pas ancr&eacute;. Le CETMEF recommande de retenir la fiche obtenue &agrave; partir du calcul effectif de la contre-but&eacute;e (et non celle issue de la majoration forfaitaire de 20%). Le pied du rideau doit donc &ecirc;tre, d'apr&egrave;s le tableau ci-dessus, &agrave; la cote "
            + results_.predimensionnement.piedContreButee + " m.";
        _p2 = "Par cons&eacute;quent, la longueur totale du rideau vaut "
            + params_.parametresGeneraux.coteTerrePleinPoussee
            + " - "
            + results_.predimensionnement.piedContreButee
            + " = "
            + OscarLib.round(
                (params_.parametresGeneraux.coteTerrePleinPoussee - results_.predimensionnement.piedContreButee), 2)
            + " m.";
      }
      if (params_.effort.fichiersDeDiagramme.length > 0) {
        addPar("Pour reprendre ces efforts (ainsi que ceux des fichiers de diagrammes suppl&eacute;mentaires), le rideau doit avoir les caract&eacute;ristiques suivantes :");
      } else {
        addPar("Pour reprendre ces efforts, le rideau doit avoir les caract&eacute;ristiques suivantes :");
      }
      addTable(new Object[] { "Param&egrave;tres", "Valeurs" }, _data);
      addPar(_p1);
      addPar(_p2);
      if ((params_.ouvrage.moduleYoungAcier != VALEUR_NULLE.value)
          && (params_.ouvrage.limiteElastiqueAcier != VALEUR_NULLE.value)
          && (params_.ouvrage.inertiePalplanches != VALEUR_NULLE.value)
          && (params_.ouvrage.demieHauteur != VALEUR_NULLE.value)) {
        final double Mmax = (momax > momin) ? momax : momin;
        final double se = params_.ouvrage.limiteElastiqueAcier;
        final double sadm = 2 * se / 3;
        final double miinf = (Mmax / sadm) / 1000;
        final double isurv = (params_.ouvrage.inertiePalplanches / params_.ouvrage.demieHauteur);
        addPar("<b><i>R&eacute;sultats suppl&eacute;mentaires issus de la connaissance des donn&eacute;es facultatives</i></b>");
        addPar("En valeur absolue, le moment maximal &agrave; reprendre par les palplanches vaut <b>Mmax = "
            + OscarLib.round(Mmax, 2) + " kN.m/ml</b>.");
        addPar("La limite &eacute;lastique des palplanches vaut <b>sigma-e = " + OscarLib.round(se, 2) + " MPa.</b>");
        addPar("La contrainte admissible vaut alors <b>sigma-adm = 2/3 sigma-e = " + sadm + " MPa.</b>");
        addPar("Le module d'inertie des palplanches doit donc, d'apr&egrave;s les calculs, &ecirc;tre sup&eacute;rieur ou &eacute;gal &agrave; <b>I/v &gt;= Mmax/sigma-adm = "
            + OscarLib.round(miinf, 4) + " m<sup>3</sup>/ml</b>, soit <b>" + OscarLib.round((miinf * 1000000), 2) + " cm<sup>3</sup>/ml</b>.");
        addPar("Le module d'inertie renseign&eacute; dans les donn&eacute;es est &eacute;gal &agrave; <b>I/v = " + OscarLib.round(isurv, 4) + " cm<sup>3</sup>/ml</b>.");
        if (isurv >= (miinf * 1000000)) {
          addPar("Il est bien sup&eacute;rieur au module minimal calcul&eacute;, <b>" + OscarLib.round((miinf * 1000000), 2)
              + " cm<sup>3</sup>/ml</b>, <b><font color=blue>le rideau est donc correctement pr&eacute;dimensionn&eacute;</font></b>.");
        } else {
          addPar("Il est inf&eacute;rieur au module minimal calcul&eacute;, <b>" + OscarLib.round((miinf * 1000000), 2)
              + " cm<sup>3</sup>/ml</b>, <b><font color=red>le rideau n'est donc pas correctement pr&eacute;dimensionn&eacute;</font></b>.");
        }
      } else {
        addPar("Les donn&eacute;es facultatives n'ont pas &eacute;t&eacute; renseign&eacute;es, pour les palplanches, en totalit&eacute;. Certains r&eacute;sultats suppl&eacute;mentaires ne sont donc pas disponibles.");
      }
    }
  }
  protected static class ReDef extends WHtmlContent {
    public ReDef(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      try {
        double efoisi = (params_.ouvrage.moduleYoungAcier * params_.ouvrage.inertiePalplanches) / 100000;
        WSpy.Spy("young = " + params_.ouvrage.moduleYoungAcier + " | " + "inertie = "
            + params_.ouvrage.inertiePalplanches);
        double _vdmax = 0.0;
        if ((params_.ouvrage.inertiePalplanches == VALEUR_NULLE.value)
            || (params_.ouvrage.moduleYoungAcier == VALEUR_NULLE.value)) {
          efoisi = 1000.0;
        }
        final String _script = ((OscarDeformeeResultatsBruts) ((OscarFilleResultatsBruts) ((OscarFilleNoteDeCalculs) parent_)
            .getInternalFrame(OscarLib.INTERNAL_FRAME_RESULTATSBRUTS)).getOnglet(OscarLib.ONGLET_DEFORMEE))
            .getDeformeesGrapheScript();
        addImgFromScript(_script, "deformees.gif", -1, -1, "d&eacute;form&eacute;e du rideau");
        addPar("On donne &agrave; titre indicatif la d&eacute;form&eacute;e du rideau pour un produit d'inertie <b>E * I = "
            + OscarLib.round(efoisi, 2) + " kN.m<sup>2</sup></b>.");
        for (int i = 0; i < results_.deformees.pointsCoteDeformee.length; i++) {
          _vdmax = (Math.abs(results_.deformees.pointsCoteDeformee[i].valeur * OscarMsg.INITIALVALUE002 / efoisi) > _vdmax) ? Math
              .abs(results_.deformees.pointsCoteDeformee[i].valeur * OscarMsg.INITIALVALUE002 / efoisi)
              : _vdmax;
        }
        addPar("Le d&eacute;placement horizontal maximum vaut, pour ce produit d'inertie, <b>"
            + OscarLib.formatterNombre("deformee", _vdmax / 100) + " m</b>.");
      } catch (final Throwable _e) {}
    }
  }
  protected static class ReMom extends WHtmlContent {
    public ReMom(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      addPar("La repr&eacute;sentation graphique du moment fl&eacute;chissant en fonction de la cote est donn&eacute;e par la figure ci-dessous :");
      try {
        final String _script = ((OscarMomentsFlechissantsResultatsBruts) ((OscarFilleResultatsBruts) ((OscarFilleNoteDeCalculs) parent_)
            .getInternalFrame(OscarLib.INTERNAL_FRAME_RESULTATSBRUTS)).getOnglet(OscarLib.ONGLET_MOMENT))
            .getMomentsFlechissantsGrapheScript();
        addImgFromScript(_script, "moments.gif", -1, -1, "moments fl&eacute;chissants du rideau");
      } catch (final Throwable _e) {}
      addPar("La valeur du moment maximum est <b>" + OscarLib.formatterNombre("moment", results_.predimensionnement.momentMaximum.valeur)
          + " kN.m/ml</b> ; il est appliqu&eacute; &agrave; la cote <b>" + OscarLib.formatterNombre(results_.predimensionnement.momentMaximum.cote) + " m</b>.");
      addPar("La valeur du moment minimum est <b>" + OscarLib.formatterNombre("moment", results_.predimensionnement.momentMinimum.valeur)
          + " kN.m/ml</b> ; il est appliqu&eacute; &agrave; la cote <b>" + OscarLib.formatterNombre(results_.predimensionnement.momentMinimum.cote) + " m</b>.");
    }
  }
  protected static class ReEff extends WHtmlContent {
    public ReEff(final JComponent _parent, final FudaaProjet _project, final OscarFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      if (results_.predimensionnement.encastrement != VALEUR_NULLE.value) {
        if ((results_.predimensionnement.reactionAncrage != VALEUR_NULLE.value)
            && (params_.ouvrage.limiteElastiqueAcierTirants != VALEUR_NULLE.value)
            && (params_.ouvrage.sectionTirants != VALEUR_NULLE.value)
            && (params_.ouvrage.espaceEntreDeuxTirants != VALEUR_NULLE.value)) {
          addPar("<b><i>R&eacute;sultats suppl&eacute;mentaires issus de la connaissance des donn&eacute;es facultatives</i></b>");
          final double se = params_.ouvrage.limiteElastiqueAcierTirants;
          final double e = params_.ouvrage.espaceEntreDeuxTirants;
          final double sadm = 2 * se / 3;
          final double t = results_.predimensionnement.reactionAncrage * e;
          final double s = t / params_.ouvrage.sectionTirants;
          addPar("La traction dans un tirant vaut, d'apr&egrave;s les calculs, <b>T = traction par m&egrave;tre lin&eacute;aire * espacement = "
              + OscarLib.round(Math.abs(t), 2) + " kN</b>.");
          addPar("La contrainte dans un tirant vaut <b>Sigma = T / section = " + OscarLib.round(Math.abs(s), 1)
              + " kPa</b>.");
          addPar("La limite &eacute;lastique des tirants vaut <b>Sigma-e = " + OscarLib.round(se, 2) + " MPa.</b>");
          addPar("La contrainte admissible du tirant vaut <b>Sigma-adm = 2/3 * Sigma-e = " + OscarLib.round(sadm, 2)
              + " MPa</b>.");
          if (s <= sadm) {
            addPar("La contrainte dans les tirants (<b>" + OscarLib.round(Math.abs(s) / 1000, 2)
                + " MPa</b>) est bien inf&eacute;rieure &agrave; la contrainte admissible (<b>" + OscarLib.round(sadm, 2)
                + " MPa</b>). <b><font color=blue>Ils sont donc correctement pr&eacute;dimensionn&eacute;s.</font></b>");
          } else {
            addPar("<b>Attention</b> : la contrainte dans les tirants (<b>" + OscarLib.round(Math.abs(s) / 1000, 2)
                + " MPa</b>) est sup&eacute;rieure &agrave; la contrainte admissible (<b>" + OscarLib.round(sadm, 2)
                + " MPa</b>). <b><font color=red>Ils ne sont donc pas correctement pr&eacute;dimensionn&eacute;s.</font></b>");
          }
        } else {
          addPar("Les donn&eacute;es facultatives n'ont pas &eacute;t&eacute; renseign&eacute;es, pour les tirants, en totalit&eacute;. Certains r&eacute;sultats suppl&eacute;mentaires ne sont donc pas disponibles.");
        }
      } else {
        addPar("Le rideau n'est pas ancr&eacute;.");
      }
    }
  }
}
