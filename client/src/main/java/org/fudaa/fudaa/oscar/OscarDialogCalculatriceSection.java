/*
 * @file         OscarDialogCalculatriceSection.java
 * @creation     1998-10-05
 * @modification $Date: 2007-05-04 13:59:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * bo�te de dialogue permettant de calculer la section d'un tirant en fonction de son diam�tre
 * 
 * @version $Revision: 1.8 $ $Date: 2007-05-04 13:59:30 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarDialogCalculatriceSection extends JDialog implements ActionListener, KeyListener {
  /**
   * r�f�rence vers l'application principal
   */
  private BuCommonInterface app_;
  /**
   * titre de la bo�te de dialogue
   */
  private String title_ = null;
  /**
   * champs de saisie du diam�tre
   */
  private BuTextField tf_diametre_ = null;
  /**
   * champs de visualisation de la section
   */
  private BuTextField tf_section_ = null;
  /**
   * bouton d'aide
   */
  private BuButton bt_aide_ = null;
  /**
   * bouton de validation. Permet de r�cup�rer la valeur calcul�e
   */
  private BuButton bt_valider_ = null;
  /**
   * bouton d'annulation
   */
  private BuButton bt_annuler_ = null;
  /**
   * conteneur swing principal de la bo�te de dialogue
   */
  private JComponent content_ = null;
  /**
   * bloc 1 : information
   */
  private BuPanel b1_ = null;
  /**
   * bloc 2 : propri�t�s
   */
  private BuPanel b2_ = null;
  /**
   * bloc 3 : section
   */
  private BuPanel b3_ = null;
  /**
   * bloc 4 : boutons
   */
  private BuPanel b4_ = null;
  /**
   * flag permettant de savoir si l'utilisateur vient de cliquer ou non sur le bouton Valider
   */
  private boolean valider_ = false;

  /**
   * construit une bo�te de dialogue permettant de calculer la section d'un tirant en fonction de son diam�tre.
   */
  public OscarDialogCalculatriceSection(final BuCommonInterface _app) {
    super(_app.getImplementation().getFrame(), true);
    app_ = _app;
    content_ = (JComponent) getContentPane();
    title_ = OscarMsg.TITLELAB031;
    final BuVerticalLayout _lo = OscarLib.VerticalLayout();
    content_.setLayout(_lo);
    content_.setBorder(OscarLib.EmptyBorder());
    tf_diametre_ = OscarLib.DoubleField("#0.00");
    tf_section_ = OscarLib.DoubleField("#0.00");
    bt_aide_ = OscarLib.Button("Aide", "AIDE", "AIDE");
    bt_valider_ = OscarLib.Button("Valider", "VALIDER", "VALIDER");
    bt_annuler_ = OscarLib.Button("Annuler", "ANNULER", "ANNULER");
    b1_ = new BuPanel();
    b1_.setLayout(OscarLib.VerticalLayout());
    b1_.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB_INFO));
    b2_ = new BuPanel();
    b2_.setLayout(OscarLib.GridLayout(5));
    b2_.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB029));
    b3_ = new BuPanel();
    b3_.setLayout(new FlowLayout(FlowLayout.LEFT));
    b3_.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB030));
    b4_ = OscarLib.FlowPanel();
    bt_aide_.addActionListener(this);
    bt_valider_.addActionListener(this);
    bt_annuler_.addActionListener(this);
    tf_diametre_.addKeyListener(this);
    bt_valider_.setEnabled(false);
    tf_section_.setEnabled(false);
    // Bloc Informations
    final BuLabelMultiLine _lb_infos = OscarLib.LabelMultiLine(OscarMsg.LAB067);
    b1_.add(_lb_infos);
    // Bloc Propri�t�s
    final BuLabel _lb_diametre_text = OscarLib.Label(OscarMsg.LAB068);
    final BuLabel _lb_diametre_egal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel _lb_diametre_unit = OscarLib.Label(OscarMsg.UNITLAB_MILLIMETRE);
    b2_.add(_lb_diametre_text);
    b2_.add(_lb_diametre_egal);
    b2_.add(tf_diametre_);
    b2_.add(_lb_diametre_unit);
    // Bloc K
    final BuLabel _lb_section_text = OscarLib.Label(OscarMsg.LAB069);
    final BuLabel _lb_section_egal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel _lb_section_unit = OscarLib.Label(OscarMsg.UNITLAB_CENTIMETRE_CARRE);
    b3_.add(_lb_section_text);
    b3_.add(_lb_section_egal);
    b3_.add(tf_section_);
    b3_.add(_lb_section_unit);
    // Bloc Boutons
    b4_.add(bt_aide_);
    b4_.add(bt_annuler_);
    b4_.add(bt_valider_);
    // Ajout des blocs au conteneur principal
    content_.add(b1_);
    content_.add(b2_);
    content_.add(b3_);
    content_.add(b4_);
    // cr�ation de la bo�te de dialogue
    setLocation(40, 40);
    setTitle(title_);
    setSize(500, 400);
    setResizable(false);
    pack();
    tf_diametre_.requestFocus();
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   * 
   * @param _evt �v�nement qui a engendr� l'action
   */
  public void actionPerformed(final ActionEvent _evt) {
    // R�cup�ration de la commande de l'action en cours et affichage sur la sortie
    // erreur
    final String action = _evt.getActionCommand();
    if (action.equals("AIDE")) {
      aide();
    } else if (action.equals("VALIDER")) {
      valider();
    } else if (action.equals("ANNULER")) {
      annuler();
    }
  }

  /**
   * permet d'attendre que l'utilisateur clique sur un des boutons annuler ou valider, pour renvoyer soit la valeur
   * calcul�e (valider) soit null (annuler) et fermer la bo�te de dialogue. Renvoi un Double.
   */
  public Double activate() {
    Double _d = null;;
    show();
    // cette partie du code n'est execut�e que si la fen�tre dispara�t (par un
    // setVisible � false)
    if (hasBeenValidate()) {
      _d = (Double) tf_section_.getValue();
    }
    return _d;
  }

  /**
   * affiche l'aide concernant la bo�te de dialogue de calcul de section d'un tirant.
   */
  private void aide() {
    app_.getImplementation().displayURL(OscarLib.helpUrl(OscarMsg.URL032));
  }

  /**
   * permet de fermer la bo�te de dialogue calculatrice en renvoyant null au lieu d'une valeur.
   */
  private void annuler() {
    bt_valider_.setEnabled(false);
    setVisible(false);
  }

  /**
   * retourne true si l'utilisateur vient juste de cliquer sur le bouton Valider, false sinon. Cette fonction permet de
   * filtrer l'action de fermeture de fen�tre par la croix, et d'�viter que l'utilisateur valider par cette action, qui
   * doit �tre �quivalente � un clic sur Annuler.
   */
  private boolean hasBeenValidate() {
    final boolean _r = valider_;
    return _r;
  }

  /**
   * permet de calculer la valeur du coefficient K (Ka ou Kp) en fonction du mode et d'afficher le r�sultat dans le
   * champs de visualisation du coefficient.
   */
  private void calculer() {
    Double _k = null;
    try {
      final double _diam = ((Double) tf_diametre_.getValue()).doubleValue();
      _k = new Double(Math.PI * _diam * _diam / 400);
      if (_k.isNaN()) {
        throw new Exception();
      }
      tf_section_.setValue(_k);
      bt_valider_.setEnabled(true);
    } catch (final Exception _e1) {
      tf_section_.setText(OscarMsg.LAB070);
      bt_valider_.setEnabled(false);
    }
  }

  /**
   * permet de fermer la bo�te de dialogue et de renvoyer la valeur du coefficient K correspondant.
   */
  private void valider() {
    bt_valider_.setEnabled(true);
    valider_ = true;
    setVisible(false);
  }

  /**
   * appell�e automatiquement lorsqu'une touche est press�e
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyPressed(final KeyEvent _e) {}

  /**
   * appell�e automatiquement lorsqu'une touche est relach�e. A chaque fois que cette m�thode est appell�e, on v�rifie
   * si le coefficient K peut �tre recalcul�e (mis � jour par rapport � la saisie), si c'est le cas on le recalcule et
   * on l'affiche, sinon une vide son champs de visualisation.
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyReleased(final KeyEvent _e) {

    if (tf_diametre_.getValue() != null) {
      // tf_diametre_.getValue().equals(null);
      calculer();
    } else {
      bt_valider_.setEnabled(false);
      tf_section_.setText("");
    }

  }

  /**
   * appell�e automatiquement lorsque l'utilisateur tape sur une touche du clavier
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyTyped(final KeyEvent _e) {}
}
