package org.fudaa.fudaa.oscar;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuPreferences;

import org.fudaa.dodico.corba.oscar.SResultatsOscar;

import org.fudaa.fudaa.commun.projet.FudaaProjet;


public class WTree extends JPanel {
  WTreeItem presentation_ = null;
  WTreeItem pagedegarde_ = null;
  WTreeItem tabledesmatieres_ = null;
  WTreeItem rappelhypotheses_ = null;
  WTreeItem calculs_ = null;
  WTreeItem resultats_ = null;

  // WTreeItem index_ = null;
  // WTreeItem glossaire_ = null;
  // WTreeItem listefigures_ = null;
  public OscarFilleNoteDeCalculs.Settings getConfig() {
    final OscarFilleNoteDeCalculs.Settings _s = OscarFilleNoteDeCalculs.createSettings();
    _s.add(presentation_.getConfig());
    _s.add(pagedegarde_.getConfig());
    _s.add(tabledesmatieres_.getConfig());
    _s.add(rappelhypotheses_.getConfig());
    _s.add(calculs_.getConfig());
    _s.add(resultats_.getConfig());
    // _s.add(index_.getConfig());
    // _s.add(glossaire_.getConfig());
    // _s.add(listefigures_.getConfig());
    return _s;
  }

  public WTree(final FudaaProjet _project) {
    super();
    final JPanel _c = new JPanel();
    _c.setLayout(new BoxLayout(_c, BoxLayout.Y_AXIS));
    _c.setBorder(new EmptyBorder(10, 10, 10, 10));
    presentation_ = new WTreeItem("Pr�sentation", new Presentation(_project));
    // presentation_.toggleExpand(true);
    presentation_.toggleItemEnable(true);
    pagedegarde_ = new WTreeItem("Page de garde", new PageDeGarde(_project));
    pagedegarde_.toggleItemEnable(true);
    pagedegarde_.setVisible(false);
    tabledesmatieres_ = new WTreeItem("Table des mati�res", new TableDesMatieres(_project));
    tabledesmatieres_.toggleItemEnable(true);
    tabledesmatieres_.setVisible(false);
    rappelhypotheses_ = new WTreeItem("Rappel des hypoth�ses", new RappelHypotheses(_project));
    rappelhypotheses_.toggleItemEnable(true);
    calculs_ = new WTreeItem("Calculs", new Calculs(_project));
    calculs_.toggleItemEnable(true);
    resultats_ = new WTreeItem("R�sultats", new Resultats(_project));
    resultats_.toggleItemEnable(true);
    // index_ = new WTreeItem("Index",new Index(_project));
    // glossaire_ = new WTreeItem("Glossaire",new Glossaire(_project));
    // listefigures_ = new WTreeItem("Liste des figures",new ListeFigures(_project));
    _c.add(presentation_);
    _c.add(Box.createVerticalStrut(5));
    _c.add(pagedegarde_);
    _c.add(Box.createVerticalStrut(5));
    _c.add(tabledesmatieres_);
    _c.add(Box.createVerticalStrut(5));
    _c.add(rappelhypotheses_);
    _c.add(Box.createVerticalStrut(5));
    _c.add(calculs_);
    _c.add(Box.createVerticalStrut(5));
    _c.add(resultats_);
    _c.add(Box.createVerticalStrut(5));
    _c.add(Box.createVerticalGlue());
    _c.add(Box.createVerticalStrut(1));
    // JScrollBar _vs = new JScrollBar(JScrollBar.VERTICAL);
    // JScrollPane _js = new JScrollPane(_c);
    // _js.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    // _js.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    // _js.setVerticalScrollBar(_vs);
    setLayout(new BorderLayout());
    // JPanel _z = new JPanel();
    // _z.setLayout(new BorderLayout());
    // _z.setBorder(new CompoundBorder(new EmptyBorder(0,5,0,2),new MatteBorder(1,0,0,1,new Color(190,190,190))));
    // _z.add(_vs);
    // add(_z,BorderLayout.EAST);
    // add(_js,BorderLayout.CENTER);
    add(_c, BorderLayout.CENTER);
  }

  public static void main(final String[] args) {
    final JFrame _f = new JFrame("WTreeItem Sample");
    _f.setBounds(50, 50, 400, 400);
    final WTree _t = new WTree((FudaaProjet) null);
    _f.setContentPane(_t);
    _f.setVisible(true);
    _f.addWindowListener(new WindowAdapter() {
      public void windowClosing(final WindowEvent _e) {
        WSpy.Spy("Config : \n" + (_t.getConfig()).toString() + "\n");
        System.exit(0);
      }
    });
  }
  static abstract class TreeItemGUI extends JPanel {
    int i;
    GridBagLayout _gb;
    GridBagConstraints _gbc;
    protected String _base;
    protected OscarFilleNoteDeCalculs.Settings _s;
    protected FudaaProjet project_;

    public TreeItemGUI(final String base, final FudaaProjet _project) {
      super();
      project_ = _project;
      _gb = new GridBagLayout();
      _gbc = new GridBagConstraints();
      _base = base;
      setLayout(_gb);
      i = 0;
      _s = OscarFilleNoteDeCalculs.createSettings();
      setBorder(new EmptyBorder(3, 3, 3, 3));
    }

    public String getBase() {
      return _base;
    }

    protected abstract OscarFilleNoteDeCalculs.Settings getConfig();

    protected void add(final JComponent _object, final double _weightx, final double _weighty, final int _gridx,
        final int _gridy, final int _gridwidth, final int _gridheight, final int _anchor, final int _fill) {
      _gbc.weightx = _weightx;
      _gbc.weighty = _weighty;
      _gbc.gridx = _gridx;
      _gbc.gridy = _gridy;
      _gbc.gridwidth = _gridwidth;
      _gbc.gridheight = _gridheight;
      _gbc.anchor = _anchor;
      _gbc.fill = _fill;
      _gb.setConstraints(_object, _gbc);
      add(_object);
    }
  }
  static class PageDeGarde extends TreeItemGUI {
    JCheckBox _cb_logofudaa;
    JCheckBox _cb_logocetmef;
    JCheckBox _cb_image;
    JCheckBox _cb_titre;
    JCheckBox _cb_auteur;
    JCheckBox _cb_date;
    JTextField _tf_image;
    JTextField _tf_titre;
    JTextField _tf_date;
    JTextField _tf_auteur;
    JTextArea _ta_description;
    JButton _bt_image;

    public PageDeGarde(final FudaaProjet _project) {
      super("PAGEDEGARDE", _project);
      final JLabel _l_info = new JLabel("Informations");
      add(_l_info, 1.0, 1.0, 0, i++, 3, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // Logo Fudaa
      _cb_logofudaa = new JCheckBox("Logo de FUDAA - Oscar");
      add(_cb_logofudaa, 1.0, 1.0, 0, i++, 3, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      _cb_logofudaa.setSelected(true);
      // Logo CETMEF
      _cb_logocetmef = new JCheckBox("Logo du C.E.T.M.E.F");
      add(_cb_logocetmef, 1.0, 1.0, 0, i, 3, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      _cb_logocetmef.setSelected(true);
      // Fichier image
      _cb_image = new JCheckBox("Fichier image");
      add(_cb_image, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _tf_image = new JTextField("");
      _tf_image.setVisible(false);
      add(_tf_image, 1.0, 1.0, 1, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_image.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("image");
        }
      });
      _bt_image = new JButton("Parcourir");
      _bt_image.setVisible(false);
      add(_bt_image, 0.0, 1.0, 2, i++, 1, 1, GridBagConstraints.EAST, GridBagConstraints.VERTICAL);
      _bt_image.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          choisirImage();
        }
      });
      // Titre
      _cb_titre = new JCheckBox("Titre");
      add(_cb_titre, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_titre.setSelected(true);
      _tf_titre = new JTextField("Note de Calculs");
      _tf_titre.setVisible(false);
      add(_tf_titre, 1.0, 1.0, 1, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_titre.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("titre");
        }
      });
      // Auteur
      _cb_auteur = new JCheckBox("Auteur");
      add(_cb_auteur, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_auteur.setSelected(true);
      _tf_auteur = new JTextField("Olivier Hoareau");
      _tf_auteur.setVisible(false);
      add(_tf_auteur, 1.0, 1.0, 1, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_auteur.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("auteur");
        }
      });
      // Date
      _cb_date = new JCheckBox("Date");
      add(_cb_date, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_date.setSelected(true);
      _tf_date = new JTextField("06/12/2002");
      _tf_date.setVisible(false);
      add(_tf_date, 1.0, 1.0, 1, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_date.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("date");
        }
      });
      final JLabel _l_description = new JLabel("Description");
      add(_l_description, 1.0, 1.0, 0, i++, 3, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      _ta_description = new JTextArea("");
      add(_ta_description, 1.0, 1.0, 0, i++, 3, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      toggleInfoItem("auteur");
      toggleInfoItem("date");
    }

    void toggleInfoItem(final String _item) {
      if (_item.equals("auteur")) {
        _tf_auteur.setVisible(_cb_auteur.isSelected());
      } else if (_item.equals("image")) {
        _tf_image.setVisible(_cb_image.isSelected());
        _bt_image.setVisible(_cb_image.isSelected());
      } else if (_item.equals("date")) {
        _tf_date.setVisible(_cb_date.isSelected());
      } else if (_item.equals("titre")) {
        _tf_titre.setVisible(_cb_titre.isSelected());
      }
      setVisible(false);
      setVisible(true);
    }

    void choisirImage() {
      System.out.println("choisir image.");
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      _s.stocke(_base + "_" + "LOGOFUDAA", new Boolean(_cb_logofudaa.isSelected()));
      _s.stocke(_base + "_" + "LOGOCETMEF", new Boolean(_cb_logocetmef.isSelected()));
      _s.stocke(_base + "_" + "TITRE", (_cb_titre.isSelected()) ? _tf_titre.getText() : (Object) null);
      _s.stocke(_base + "_" + "AUTEUR", (_cb_auteur.isSelected()) ? _tf_auteur.getText() : (Object) null);
      _s.stocke(_base + "_" + "DATE", (_cb_date.isSelected()) ? _tf_date.getText() : (Object) null);
      _s.stocke(_base + "_" + "DESCRIPTION", ((_ta_description.getText() != null) && (!_ta_description.getText()
          .equals(""))) ? _ta_description.getText() : (Object) null);
      return _s;
    }
  }
  static class TableDesMatieres extends TreeItemGUI {
    JComboBox _c_prof;
    JRadioButton _rb_parties1;
    JRadioButton _rb_parties2;
    JRadioButton _rb_sousparties1;
    JRadioButton _rb_sousparties2;
    JRadioButton _rb_sousparties3;

    public TableDesMatieres(final FudaaProjet _project) {
      super("TABLEDESMATIERES", _project);
      final JLabel _l_prof = new JLabel("Profondeur :");
      add(_l_prof, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _c_prof = new JComboBox(new Object[] { "1", "2", "3", "4" });
      add(_c_prof, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _c_prof.setSelectedIndex(2);
      final JLabel _l_parties = new JLabel("Parties");
      add(_l_parties, 1.0, 1.0, 0, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      _rb_parties1 = new JRadioButton("chiffres romains : I, II, III, ...");
      add(_rb_parties1, 1.0, 1.0, 0, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      _rb_parties2 = new JRadioButton("chiffres arabes : 1, 2, 3, ...");
      add(_rb_parties2, 1.0, 1.0, 0, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      final ButtonGroup _bg1 = new ButtonGroup();
      _bg1.add(_rb_parties1);
      _bg1.add(_rb_parties2);
      _rb_parties1.setSelected(true);
      final JLabel _l_sousparties = new JLabel("Sous-Parties");
      add(_l_sousparties, 1.0, 1.0, 0, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      _rb_sousparties1 = new JRadioButton("chiffres arabes : 1, 2, 3, ...");
      add(_rb_sousparties1, 1.0, 1.0, 0, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      _rb_sousparties2 = new JRadioButton("lettres majuscules : A, B, C, ...");
      add(_rb_sousparties2, 1.0, 1.0, 0, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      _rb_sousparties3 = new JRadioButton("lettres minuscules : a, b, c, ...");
      add(_rb_sousparties3, 1.0, 1.0, 0, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      final ButtonGroup _bg2 = new ButtonGroup();
      _bg2.add(_rb_sousparties1);
      _bg2.add(_rb_sousparties2);
      _bg2.add(_rb_sousparties3);
      _rb_sousparties1.setSelected(true);
      this.setVisible(false); // � retirer dans la version finale
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      _s.stocke(_base + "_" + "PROFONDEUR", _c_prof.getSelectedItem());
      _s.stocke(_base + "_" + "PARTIES", (_rb_parties1.isSelected()) ? "I" : "1");
      _s.stocke(_base + "_" + "SOUSPARTIES", (_rb_sousparties1.isSelected()) ? "1"
          : (_rb_sousparties2.isSelected()) ? "A" : "a");
      return _s;
    }
  }
  static class Presentation extends TreeItemGUI {
    JCheckBox _cb_auteur;
    JCheckBox _cb_logiciel;
    JCheckBox _cb_date;
    JCheckBox _cb_titre;
    JCheckBox _cb_fichier;
    JTextField _tf_auteur;
    JTextField _tf_logiciel;
    JTextField _tf_date;
    JTextField _tf_titre;
    JTextField _tf_fichier;
    JTextArea _ta_commentaires;

    public Presentation(final FudaaProjet _project) {
      super("PRESENTATION", _project);
      final JLabel _l_info = new JLabel("Informations");
      add(_l_info, 1.0, 1.0, 0, i++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // Auteur
      _cb_auteur = new JCheckBox("Auteur");
      add(_cb_auteur, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_auteur.setSelected(true);
      _tf_auteur = new JTextField(BuPreferences.BU.getStringProperty("user.firstname") + " "
          + BuPreferences.BU.getStringProperty("user.lastname"));
      _tf_auteur.setVisible(false);
      add(_tf_auteur, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_auteur.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("auteur");
        }
      });
      // Logiciel
      _cb_logiciel = new JCheckBox("Logiciel");
      add(_cb_logiciel, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_logiciel.setSelected(true);
      _tf_logiciel = new JTextField(OscarImplementation.SOFTWARE_TITLE);
      _tf_logiciel.setVisible(false);
      add(_tf_logiciel, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_logiciel.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("logiciel");
        }
      });
      // Date
      _cb_date = new JCheckBox("Date");
      add(_cb_date, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_date.setSelected(true);
      final Calendar _cal = new GregorianCalendar();
      _cal.setTime(new Date(System.currentTimeMillis()));
      final int _dd = _cal.get(Calendar.DAY_OF_MONTH);
      final int _mm = _cal.get(Calendar.MONTH) + 1;
      final int _yyyy = _cal.get(Calendar.YEAR);
      _tf_date = new JTextField(_dd + "/" + _mm + "/" + _yyyy);
      _tf_date.setVisible(false);
      add(_tf_date, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_date.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("date");
        }
      });
      // Titre
      _cb_titre = new JCheckBox("Titre");
      add(_cb_titre, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_titre.setSelected(true);
      _tf_titre = new JTextField("Note de Calculs");
      _tf_titre.setVisible(false);
      add(_tf_titre, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_titre.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("titre");
        }
      });
      // Fichier
      _cb_fichier = new JCheckBox("Fichier");
      add(_cb_fichier, 0.0, 1.0, 0, i, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_fichier.setSelected(true);
      _tf_fichier = new JTextField(project_.getFichier());
      _tf_fichier.setVisible(false);
      add(_tf_fichier, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      _cb_fichier.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("fichier");
        }
      });
      // JLabel _l_commentaires = new JLabel("Commentaires");
      // add(_l_commentaires,1.0,1.0,0,i++,2,1,GridBagConstraints.WEST,GridBagConstraints.BOTH);
      _ta_commentaires = new JTextArea("");
      // add(_ta_commentaires,1.0,1.0,0,i++,2,1,GridBagConstraints.WEST,GridBagConstraints.BOTH);
      toggleInfoItem("auteur");
      toggleInfoItem("date");
      toggleInfoItem("titre");
      toggleInfoItem("logiciel");
      toggleInfoItem("fichier");
    }

    void toggleInfoItem(final String _item) {
      if (_item.equals("auteur")) {
        _tf_auteur.setVisible(_cb_auteur.isSelected());
      } else if (_item.equals("logiciel")) {
        _tf_logiciel.setVisible(_cb_logiciel.isSelected());
      } else if (_item.equals("date")) {
        _tf_date.setVisible(_cb_date.isSelected());
      } else if (_item.equals("titre")) {
        _tf_titre.setVisible(_cb_titre.isSelected());
      } else if (_item.equals("fichier")) {
        _tf_fichier.setVisible(_cb_fichier.isSelected());
      }
      setVisible(false);
      setVisible(true);
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      _s.stocke(_base + "_" + "AUTEUR", (_cb_auteur.isSelected()) ? _tf_auteur.getText() : (Object) null);
      _s.stocke(_base + "_" + "LOGICIEL", (_cb_logiciel.isSelected()) ? _tf_logiciel.getText() : (Object) null);
      _s.stocke(_base + "_" + "DATE", (_cb_date.isSelected()) ? _tf_date.getText() : (Object) null);
      _s.stocke(_base + "_" + "TITRE", (_cb_titre.isSelected()) ? _tf_titre.getText() : (Object) null);
      _s.stocke(_base + "_" + "FICHIER", (_cb_fichier.isSelected()) ? _tf_fichier.getText() : (Object) null);
      _s.stocke(_base + "_" + "COMMENTAIRES", ((_ta_commentaires.getText() != null) && (!_ta_commentaires.getText()
          .equals(""))) ? _ta_commentaires.getText() : (Object) null);
      return _s;
    }
  }
  static class RappelHypotheses extends TreeItemGUI {
    JCheckBox _cb_sol;
    JCheckBox _cb_typecontrainte;
    JCheckBox _cb_couchesbutee;
    JCheckBox _cb_couchespoussee;
    JCheckBox _cb_eau;
    JCheckBox _cb_typecalcul;
    JCheckBox _cb_cotecotepoussee;
    JCheckBox _cb_cotecotebutee;
    JCheckBox _cb_surcharges;
    JCheckBox _cb_typesurcharge;
    JCheckBox _cb_valeursurcharge;
    JCheckBox _cb_efforts;
    JCheckBox _cb_cvs;
    JCheckBox _cb_chs;
    JCheckBox _cb_efforttetederideau;
    JCheckBox _cb_fdiag;
    JCheckBox _cb_ouvrage;
    JCheckBox _cb_ancrage;
    JCheckBox _cb_palplanches;
    JTextArea _ta_commentaires;

    public RappelHypotheses(final FudaaProjet _project) {
      super("RAPPELHYPOTHESES", _project);
      // Sol
      _cb_sol = new JCheckBox("Sol");
      add(_cb_sol, 0.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_sol.setSelected(true);
      _cb_sol.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("sol");
        }
      });
      // // Types de contraintes
      _cb_typecontrainte = new JCheckBox("type de contraintes");
      _cb_typecontrainte.setEnabled(false);
      _cb_typecontrainte.setSelected(true);
      add(_cb_typecontrainte, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Couches but�e
      _cb_couchesbutee = new JCheckBox("Couches en but�e");
      _cb_couchesbutee.setEnabled(false);
      _cb_couchesbutee.setSelected(true);
      add(_cb_couchesbutee, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Couches pouss�e
      _cb_couchespoussee = new JCheckBox("Couches en pouss�e");
      _cb_couchespoussee.setEnabled(false);
      _cb_couchespoussee.setSelected(true);
      add(_cb_couchespoussee, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // Eau
      _cb_eau = new JCheckBox("Eau");
      add(_cb_eau, 0.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_eau.setSelected(true);
      _cb_eau.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("eau");
        }
      });
      // // Types de calcul
      _cb_typecalcul = new JCheckBox("type de calcul (statique / dynamique)");
      _cb_typecalcul.setEnabled(false);
      _cb_typecalcul.setSelected(true);
      add(_cb_typecalcul, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Cote c�t� pouss�e
      _cb_cotecotepoussee = new JCheckBox("cote c�t� pouss�e");
      _cb_cotecotepoussee.setEnabled(false);
      _cb_cotecotepoussee.setSelected(true);
      add(_cb_cotecotepoussee, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Cote c�t� but�e
      _cb_cotecotebutee = new JCheckBox("cote c�t� but�e");
      _cb_cotecotebutee.setEnabled(false);
      _cb_cotecotebutee.setSelected(true);
      add(_cb_cotecotebutee, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // Surcharges
      _cb_surcharges = new JCheckBox("Surcharges");
      _cb_surcharges.setSelected(true);
      add(_cb_surcharges, 0.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_surcharges.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("surcharges");
        }
      });
      // // Types de surcharges
      _cb_typesurcharge = new JCheckBox("type de surcharge");
      _cb_typesurcharge.setEnabled(false);
      _cb_typesurcharge.setSelected(true);
      add(_cb_typesurcharge, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Valeur de la surcharge
      _cb_valeursurcharge = new JCheckBox("valeur de la surcharge");
      _cb_valeursurcharge.setEnabled(false);
      _cb_valeursurcharge.setSelected(true);
      add(_cb_valeursurcharge, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // Autres Efforts
      _cb_efforts = new JCheckBox("Autres efforts");
      _cb_efforts.setSelected(true);
      add(_cb_efforts, 0.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_efforts.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("efforts");
        }
      });
      // // Contraintes Verticales Suppl�mentaires
      _cb_cvs = new JCheckBox("contraintes verticales suppl�mentaires");
      _cb_cvs.setEnabled(false);
      _cb_cvs.setSelected(true);
      add(_cb_cvs, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Contraintes Horizontales Suppl�mentaires
      _cb_chs = new JCheckBox("contraintes horizontales suppl�mentaires");
      _cb_chs.setEnabled(false);
      _cb_chs.setSelected(true);
      add(_cb_chs, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Effort en t�te de rideau
      _cb_efforttetederideau = new JCheckBox("effort en t�te de rideau");
      _cb_efforttetederideau.setEnabled(false);
      _cb_efforttetederideau.setSelected(true);
      add(_cb_efforttetederideau, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Fichiers de diagramme suppl�mentaires
      _cb_fdiag = new JCheckBox("fichiers de diagramme suppl�mentaires");
      _cb_fdiag.setEnabled(false);
      _cb_fdiag.setSelected(true);
      add(_cb_fdiag, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // Caract�ristiques de l'ouvrage
      _cb_ouvrage = new JCheckBox("Ouvrage");
      _cb_ouvrage.setSelected(true);
      add(_cb_ouvrage, 0.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      _cb_ouvrage.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("ouvrage");
        }
      });
      // // Ancrage
      _cb_ancrage = new JCheckBox("ancrage");
      _cb_ancrage.setEnabled(false);
      _cb_ancrage.setSelected(true);
      add(_cb_ancrage, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // // Palplanches
      _cb_palplanches = new JCheckBox("Palplanches");
      _cb_palplanches.setEnabled(false);
      _cb_palplanches.setSelected(true);
      add(_cb_palplanches, 1.0, 1.0, 1, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // add(_l_commentaires,1.0,1.0,0,i++,2,1,GridBagConstraints.WEST,GridBagConstraints.BOTH);
      _ta_commentaires = new JTextArea("");
      // add(_ta_commentaires,1.0,1.0,0,i++,3,1,GridBagConstraints.WEST,GridBagConstraints.BOTH);
      toggleInfoItem("sol");
      toggleInfoItem("eau");
      toggleInfoItem("surcharges");
      toggleInfoItem("efforts");
      toggleInfoItem("ouvrage");
    }

    void toggleInfoItem(final String _item) {
      if (_item.equals("sol")) {
        _cb_typecontrainte.setEnabled(_cb_sol.isSelected());
        _cb_couchesbutee.setEnabled(_cb_sol.isSelected());
        _cb_couchespoussee.setEnabled(_cb_sol.isSelected());
      } else if (_item.equals("eau")) {
        _cb_typecalcul.setEnabled(_cb_eau.isSelected());
        _cb_cotecotepoussee.setEnabled(_cb_eau.isSelected());
        _cb_cotecotebutee.setEnabled(_cb_eau.isSelected());
      } else if (_item.equals("surcharges")) {
        _cb_typesurcharge.setEnabled(_cb_surcharges.isSelected());
        _cb_valeursurcharge.setEnabled(_cb_surcharges.isSelected());
      } else if (_item.equals("efforts")) {
        _cb_cvs.setEnabled(_cb_efforts.isSelected());
        _cb_chs.setEnabled(_cb_efforts.isSelected());
        _cb_efforttetederideau.setEnabled(_cb_efforts.isSelected());
        _cb_fdiag.setEnabled(_cb_efforts.isSelected());
      } else if (_item.equals("ouvrage")) {
        _cb_ancrage.setEnabled(_cb_ouvrage.isSelected());
        _cb_palplanches.setEnabled(_cb_ouvrage.isSelected());
      }
      setVisible(false);
      setVisible(true);
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      _s.stocke(_base + "_" + "SOL", new Boolean(_cb_sol.isSelected()));
      _s.stocke(_base + "_" + "SOL_TYPECONTRAINTE", Boolean.valueOf(_cb_typecontrainte.isSelected()));
      _s.stocke(_base + "_" + "SOL_COUCHESBUTEE", Boolean.valueOf(_cb_couchesbutee.isSelected()));
      _s.stocke(_base + "_" + "SOL_COUCHESPOUSSEE", Boolean.valueOf(_cb_couchespoussee.isSelected()));
      _s.stocke(_base + "_" + "EAU", Boolean.valueOf(_cb_eau.isSelected()));
      _s.stocke(_base + "_" + "EAU_TYPECALCUL", Boolean.valueOf(_cb_typecalcul.isSelected()));
      _s.stocke(_base + "_" + "EAU_COTECOTEBUTEE", Boolean.valueOf(_cb_cotecotebutee.isSelected()));
      _s.stocke(_base + "_" + "EAU_COTECOTEPOUSSEE", Boolean.valueOf(_cb_cotecotepoussee.isSelected()));
      _s.stocke(_base + "_" + "SURCHARGES", Boolean.valueOf(_cb_surcharges.isSelected()));
      _s.stocke(_base + "_" + "SURCHARGES_TYPESURCHARGE", Boolean.valueOf(_cb_typesurcharge.isSelected()));
      _s.stocke(_base + "_" + "SURCHARGES_VALEURSURCHARGE", Boolean.valueOf(_cb_valeursurcharge.isSelected()));
      _s.stocke(_base + "_" + "EFFORTS", Boolean.valueOf(_cb_efforts.isSelected()));
      _s.stocke(_base + "_" + "EFFORTS_CVS", Boolean.valueOf(_cb_cvs.isSelected()));
      _s.stocke(_base + "_" + "EFFORTS_CHS", Boolean.valueOf(_cb_chs.isSelected()));
      _s.stocke(_base + "_" + "EFFORTS_FDIAG", Boolean.valueOf(_cb_fdiag.isSelected()));
      _s.stocke(_base + "_" + "EFFORTS_TETEDERIDEAU", Boolean.valueOf(_cb_efforttetederideau.isSelected()));
      _s.stocke(_base + "_" + "OUVRAGE", Boolean.valueOf(_cb_ouvrage.isSelected()));
      _s.stocke(_base + "_" + "OUVRAGE_ANCRAGE", Boolean.valueOf(_cb_ancrage.isSelected()));
      _s.stocke(_base + "_" + "OUVRAGE_PALPLANCHES", Boolean.valueOf(_cb_palplanches.isSelected()));
      _s.stocke(_base + "_" + "COMMENTAIRES", ((_ta_commentaires.getText() != null) && (!_ta_commentaires.getText()
          .equals(""))) ? _ta_commentaires.getText() : (Object) null);
      return _s;
    }
  }
  static class Calculs extends TreeItemGUI {
    JCheckBox _cb_methode;
    JCheckBox _cb_codesdecalculs;

    public Calculs(final FudaaProjet _project) {
      super("CALCULS", _project);
      // M�thode utilis�e
      _cb_methode = new JCheckBox("m�thode utilis�e");
      _cb_methode.setSelected(true);
      add(_cb_methode, 1.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // code(s) de calculs utilis�(s)
      _cb_codesdecalculs = new JCheckBox("code(s) de calculs utilis�(s)");
      _cb_codesdecalculs.setSelected(true);
      add(_cb_codesdecalculs, 1.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      _s.stocke(_base + "_" + "METHODE", Boolean.valueOf(_cb_methode.isSelected()));
      _s.stocke(_base + "_" + "CODESDECALCULS", Boolean.valueOf(_cb_codesdecalculs.isSelected()));
      return _s;
    }
  }
  static class Resultats extends TreeItemGUI {
    JCheckBox _cb_diag;
    JCheckBox _cb_predim;
    JCheckBox _cb_def;
    JCheckBox _cb_mom;
    JCheckBox _cb_eff;
    boolean _RPredim = false;
    boolean _RPressions = false;
    boolean _RMoments = false;
    boolean _REfforts = false;
    boolean _RDeformee = false;

    public Resultats(final FudaaProjet _project) {
      super("RESULTATS", _project);
      _RPredim = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).predimensionnement == null) ? false
          : true;
      _RPressions = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).pressions == null) ? false : true;
      _RMoments = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).moments == null) ? false : true;
      _REfforts = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).efforts == null) ? false : true;
      _RDeformee = (((SResultatsOscar) project_.getResult(OscarResource.RESULTATS)).deformees == null) ? false : true;
      if (_RPressions) {
        // Diagramme des pressions
        _cb_diag = new JCheckBox("diagramme des pressions");
        _cb_diag.setSelected(true);
        add(_cb_diag, 1.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      }
      if (_RPredim) {
        // Pr�dimensionnement du rideau
        _cb_predim = new JCheckBox("pr�dimensionnement du rideau");
        _cb_predim.setSelected(true);
        add(_cb_predim, 1.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      }
      if (_RDeformee) {
        // D�form�e
        _cb_def = new JCheckBox("d�form�e du rideau");
        _cb_def.setSelected(true);
        add(_cb_def, 1.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      }
      if (_RMoments) {
        // Moments Fl�chissants
        _cb_mom = new JCheckBox("moments fl�chissants du rideau");
        _cb_mom.setSelected(true);
        add(_cb_mom, 1.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      }
      if (_REfforts) {
        // Efforts dans les tirants
        _cb_eff = new JCheckBox("efforts dans les tirants");
        _cb_eff.setSelected(true);
        add(_cb_eff, 1.0, 1.0, 0, i++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      }
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      if (_RPressions) {
        _s.stocke(_base + "_" + "DIAGRAMME", Boolean.valueOf(_cb_diag.isSelected()));
      }
      if (_RPredim) {
        _s.stocke(_base + "_" + "PREDIMENSIONNEMENT", Boolean.valueOf(_cb_predim.isSelected()));
      }
      if (_RMoments) {
        _s.stocke(_base + "_" + "MOMENT", Boolean.valueOf(_cb_mom.isSelected()));
      }
      if (_RDeformee) {
        _s.stocke(_base + "_" + "DEFORMEE", Boolean.valueOf(_cb_def.isSelected()));
      }
      if (_REfforts) {
        _s.stocke(_base + "_" + "EFFORTS", Boolean.valueOf(_cb_eff.isSelected()));
      }
      return _s;
    }
  }
  static class Index extends TreeItemGUI {
    public Index(final FudaaProjet _project) {
      super("INDEX", _project);
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      return _s;
    }
  }
  static class Glossaire extends TreeItemGUI {
    public Glossaire(final FudaaProjet _project) {
      super("GLOSSAIRE", _project);
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      return _s;
    }
  }
  static class ListeFigures extends TreeItemGUI {
    public ListeFigures(final FudaaProjet _project) {
      super("LISTEFIGURES", _project);
    }

    protected OscarFilleNoteDeCalculs.Settings getConfig() {
      return _s;
    }
  }
}
