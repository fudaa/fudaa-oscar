package org.fudaa.fudaa.oscar;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;

import com.memoire.bu.BuNumericValueValidator;
import com.memoire.bu.BuTextField;



public class OscarDoubleValueCellEditor implements TableCellEditor {
  protected BuTextField tf;
  protected EventListenerList listenerList = new EventListenerList();
  protected ChangeEvent changeEvent = new ChangeEvent(this);

  public OscarDoubleValueCellEditor(final String _format, final double _min, final double _max) {
    tf = OscarLib.DoubleField(_format);
    tf.setValueValidator(new BuNumericValueValidator(false, _min, _max));
    tf.setDisplayFormat(new DecimalFormat(_format));
    tf.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent event) {
        fireEditingStopped();
      }
    });
    tf.setBorder(new EmptyBorder(1, 1, 1, 1));
  }

  public void addCellEditorListener(final CellEditorListener listener) {
    listenerList.add(CellEditorListener.class, listener);
  }

  public void removeCellEditorListener(final CellEditorListener listener) {
    listenerList.remove(CellEditorListener.class, listener);
  }

  protected void fireEditingStopped() {
    CellEditorListener listener;
    final Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i++) {
      if (listeners[i] == CellEditorListener.class) {
        listener = (CellEditorListener) listeners[i + 1];
        listener.editingStopped(changeEvent);
      }
    }
  }

  protected void fireEditingCanceled() {
    CellEditorListener listener;
    final Object[] listeners = listenerList.getListenerList();
    for (int i = 0; i < listeners.length; i++) {
      if (listeners[i] == CellEditorListener.class) {
        listener = (CellEditorListener) listeners[i + 1];
        listener.editingCanceled(changeEvent);
      }
    }
  }

  public void cancelCellEditing() {
    fireEditingCanceled();
  }

  public boolean stopCellEditing() {
    fireEditingStopped();
    return true;
  }

  public boolean isCellEditable(final EventObject event) {
    return true;
  }

  public boolean shouldSelectCell(final EventObject event) {
    return true;
  }

  public Object getCellEditorValue() {
    return tf.getValue();
  }

  public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected,
      final int row, final int column) {
    if ((value instanceof Double)) {
      tf.setValue(value);
    } else if (!(value instanceof String)) {
      tf.setValue(new Double(0.0));
    }
    return tf;
  }
}
