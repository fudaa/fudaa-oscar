/*
 * @file         OscarOuvrageParametresDonneesFacultatives.java
 * @creation     2000-10-15
 * @modification $Date: 2007-05-04 13:59:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;


/**
 * Sous-onglet 'Donn�es Facultatives' de l'onglet des parametres de l'ouvrage.
 * 
 * @version $Revision: 1.7 $ $Date: 2007-05-04 13:59:31 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarOuvrageParametresDonneesFacultatives extends OscarAbstractOnglet {
  /**
   * Groupe d'options de choix de l'unit� de la section d'un tirant
   */
  private ButtonGroup bg_nap_tir_sec_unit;
  /**
   * Case option "unit� cm2" unit� de la section d'un tirant
   */
  private BuRadioButton rb_nap_tir_sec_unit_cm2;
  /**
   * Case option "unit� m2" unit� de la section d'un tirant
   */
  private BuRadioButton rb_nap_tir_sec_unit_m2;
  /**
   * Champs Module d'Young de l'acier
   */
  private BuTextField tf_rid_pal_young;
  /**
   * Champs Limite �lastique de l'acier
   */
  private BuTextField tf_rid_pal_elast;
  /**
   * Champs Inertie des palplanches
   */
  private BuTextField tf_rid_pal_inert;
  /**
   * Champs Demi-hauteur des palplanches
   */
  private BuTextField tf_rid_pal_demih;
  /**
   * Champs Espace entre deux tirants
   */
  private BuTextField tf_nap_tir_esp;
  /**
   * Champs Section des tirants
   */
  private BuTextField tf_nap_tir_sec;
  /**
   * Champs Limite �lastique de l'acier
   */
  private BuTextField tf_nap_tir_elast;
  /**
   * bloc info
   */
  private BuPanel pn1;
  /**
   * bloc rideau de palplanches
   */
  private BuPanel pn2;
  /**
   * bloc nappe de tirants
   */
  private BuPanel pn4;
  /**
   * bouton de calculatrice de section de tirant
   */
  private BuButton bt_calc;

  /**
   * construit l'onglet rattach� � l'�l�ment parent _parent
   */
  public OscarOuvrageParametresDonneesFacultatives(final OscarOuvrageParametres _ouvrage, final String _helpfile) {
    super(_ouvrage.getApplication(), _ouvrage, _helpfile);
  }

  /**
   * construit l'interface GUI du formulaire
   */
  protected JComponent construireGUI() {
    bt_calc = OscarLib.Button("Calculatrice", "CALCULATRICE");
    bg_nap_tir_sec_unit = new ButtonGroup();
    rb_nap_tir_sec_unit_cm2 = new BuRadioButton(OscarMsg.UNITLAB_CENTIMETRE_CARRE);
    rb_nap_tir_sec_unit_m2 = new BuRadioButton(OscarMsg.UNITLAB_METRE_CARRE);
    tf_rid_pal_young = OscarLib.DoubleField(OscarMsg.FORMAT001);
    tf_rid_pal_elast = OscarLib.DoubleField(OscarMsg.FORMAT002);
    tf_rid_pal_inert = OscarLib.DoubleField(OscarMsg.FORMAT003);
    tf_rid_pal_demih = OscarLib.DoubleField(OscarMsg.FORMAT004);
    tf_nap_tir_esp = OscarLib.DoubleField(OscarMsg.FORMAT005);
    tf_nap_tir_sec = OscarLib.DoubleField(OscarMsg.FORMAT006);
    tf_nap_tir_elast = OscarLib.DoubleField(OscarMsg.FORMAT007);
    pn1 = OscarLib.TitledPanel(OscarMsg.TITLELAB_INFO);
    pn2 = OscarLib.TitledPanel(OscarMsg.TITLELAB011);
    pn4 = OscarLib.TitledPanel(OscarMsg.TITLELAB012);
    final BuPanel _p = OscarLib.EmptyPanel();
    bg_nap_tir_sec_unit.add(rb_nap_tir_sec_unit_cm2);
    bg_nap_tir_sec_unit.add(rb_nap_tir_sec_unit_m2);
    rb_nap_tir_sec_unit_cm2.setSelected(true);
    // Bloc 'Info'
    final BuLabelMultiLine lb_info = OscarLib.LabelMultiLine(OscarMsg.LAB031);
    pn1.add(lb_info);
    // Bloc 'Rideau de Palplanches'
    final BuLabelMultiLine lb_rid_pal_info = OscarLib.LabelMultiLine(OscarMsg.LAB032);
    final BuPanel pn3 = OscarLib.GridPanel(5);
    final BuLabel lb_rid_pal_young = OscarLib.Label(OscarMsg.LAB033);
    final BuLabel lb_rid_pal_young_symb = OscarLib.MathSymbol(OscarLib.MATH_E);
    final BuLabel lb_rid_pal_young_eq = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_rid_pal_young_unit = OscarLib.Label(" " + OscarMsg.UNITLAB_MEGA_PASCAL);
    final BuLabel lb_rid_pal_elast = OscarLib.Label(OscarMsg.LAB034);
    final BuLabel lb_rid_pal_elast_symb = OscarLib.MathSymbol(OscarLib.MATH_SIGMA_E);
    final BuLabel lb_rid_pal_elast_eq = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_rid_pal_elast_unit = OscarLib.Label(" " + OscarMsg.UNITLAB_MEGA_PASCAL);
    final BuLabel lb_rid_pal_inert = OscarLib.Label(OscarMsg.LAB035);
    final BuLabel lb_rid_pal_inert_symb = OscarLib.MathSymbol(OscarLib.MATH_I);
    final BuLabel lb_rid_pal_inert_eq = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_rid_pal_inert_unit = OscarLib.Label(" " + OscarMsg.UNITLAB_CENTIMETRE_4_PAR_METRE_LINEAIRE);
    final BuLabel lb_rid_pal_demih = OscarLib.Label(OscarMsg.LAB036);
    final BuLabel lb_rid_pal_demih_symb = OscarLib.MathSymbol(OscarLib.MATH_PETIT_V);
    final BuLabel lb_rid_pal_demih_eq = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_rid_pal_demih_unit = OscarLib.Label(" " + OscarMsg.UNITLAB_CENTIMETRE);
    pn3.add(lb_rid_pal_young);
    pn3.add(lb_rid_pal_young_symb);
    pn3.add(lb_rid_pal_young_eq);
    pn3.add(tf_rid_pal_young);
    pn3.add(lb_rid_pal_young_unit);
    pn3.add(lb_rid_pal_elast);
    pn3.add(lb_rid_pal_elast_symb);
    pn3.add(lb_rid_pal_elast_eq);
    pn3.add(tf_rid_pal_elast);
    pn3.add(lb_rid_pal_elast_unit);
    pn3.add(lb_rid_pal_inert);
    pn3.add(lb_rid_pal_inert_symb);
    pn3.add(lb_rid_pal_inert_eq);
    pn3.add(tf_rid_pal_inert);
    pn3.add(lb_rid_pal_inert_unit);
    pn3.add(lb_rid_pal_demih);
    pn3.add(lb_rid_pal_demih_symb);
    pn3.add(lb_rid_pal_demih_eq);
    pn3.add(tf_rid_pal_demih);
    pn3.add(lb_rid_pal_demih_unit);
    final BuButton _bt_lib = OscarLib.Button(OscarMsg.LAB066, "BIBLIOTHEQUE");
    _bt_lib.addActionListener(this);
    final JPanel _jp = new JPanel();
    _jp.setLayout(new BuGridLayout(1));
    _jp.add(_bt_lib);
    pn3.add(_jp);
    pn2.add(lb_rid_pal_info);
    pn2.add(pn3);
    // Bloc 'Nappe de Tirants'
    final BuLabelMultiLine lb_nap_tir_info = OscarLib.LabelMultiLine(OscarMsg.LAB037);
    final BuPanel pn5 = OscarLib.GridPanel(6);
    final BuLabel lb_nap_tir_esp = OscarLib.Label(OscarMsg.LAB038);
    final BuLabel lb_nap_tir_esp_eq = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_nap_tir_esp_unit = OscarLib.Label(" " + OscarMsg.UNITLAB_METRE);
    final BuLabel lb_nap_tir_sec = OscarLib.Label(OscarMsg.LAB039);
    final BuLabel lb_nap_tir_sec_eq = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    bt_calc.addActionListener(this);
    // BuPanel pn6 = new BuPanel();
    // pn6.setLayout(new FlowLayout(FlowLayout.LEFT));
    final BuLabel lb_nap_tir_elast = OscarLib.Label(OscarMsg.LAB040);
    final BuLabel lb_nap_tir_elast_eq = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_nap_tir_elast_unit = OscarLib.Label(" " + OscarMsg.UNITLAB_MEGA_PASCAL);
    pn5.add(lb_nap_tir_esp);
    pn5.add(lb_nap_tir_esp_eq);
    pn5.add(tf_nap_tir_esp);
    pn5.add(lb_nap_tir_esp_unit);
    pn5.add(new JPanel());
    pn5.add(new JPanel());
    pn5.add(lb_nap_tir_sec);
    pn5.add(lb_nap_tir_sec_eq);
    pn5.add(tf_nap_tir_sec);
    pn5.add(rb_nap_tir_sec_unit_cm2);
    pn5.add(rb_nap_tir_sec_unit_m2);
    pn5.add(bt_calc);
    pn5.add(lb_nap_tir_elast);
    pn5.add(lb_nap_tir_elast_eq);
    pn5.add(tf_nap_tir_elast);
    pn5.add(lb_nap_tir_elast_unit);
    pn5.add(new JPanel());
    pn5.add(new JPanel());
    pn4.add(lb_nap_tir_info);
    pn4.add(pn5);
    toggleNappeTirants(false);
    _p.add(pn1);
    _p.add(pn2);
    _p.add(pn4);
    return _p;
  }

  /**
   * appell�e automatiquement lors d'une action �cout�e par le composant
   * 
   * @param _a action � traiter
   */
  protected void action(final String _a) {
    if (_a.equals("BIBLIOTHEQUE")) {
      bibliotheque();
    } else if (_a.equals("CALCULATRICE")) {
      calculatriceSection();
    }
  }

  /**
   * affiche ou masque les propri�t�s de la nappe de tirants
   * 
   * @param _etat bool�en qui indique si une nappe de tirants doit �tre prise en compte
   */
  public void toggleNappeTirants(final boolean _etat) {
    pn4.setVisible(_etat);
  }

  /**
   *
   */
  public void bibliotheque() {
    getImplementation().displayURL(OscarLib.helpUrl(OscarMsg.URL028));
  }

  /**
   * affiche la calculatrice Section et stocke la valeur calcul�e par l'utilisateur dans la cellule Section.
   */
  private void calculatriceSection() {
    final Double _section = OscarLib.CalculatriceSection((OscarOuvrageParametres) getParentComponent());
    try {
      //fred hein ?
      //_section.equals(null);
      tf_nap_tir_sec.setValue(_section);
      rb_nap_tir_sec_unit_cm2.setSelected(true);
    } catch (final NullPointerException _e1) {}
  }

  /**
   *
   */
  public boolean getPresenceNappeTirants() {
    return pn4.isVisible();
  }

  /**
   *
   */
  public Double getModuleYoungAcier() {
    return (Double) tf_rid_pal_young.getValue();
  }

  /**
   *
   */
  public synchronized void setModuleYoungAcier(final double _p) {
    // tf_rid_pal_young.setValue(new Double(_p/1000));
    tf_rid_pal_young.setValue(new Double(_p));
  }

  /**
   *
   */
  public Double getLimiteElastiqueAcier() {
    return (Double) tf_rid_pal_elast.getValue();
  }

  /**
   *
   */
  public synchronized void setLimiteElastiqueAcier(final double _p) {
    tf_rid_pal_elast.setValue(new Double(_p));
  }

  /**
   *
   */
  public Double getInertiePalplanches() {
    final Double _v = (Double) tf_rid_pal_inert.getValue();
    if (_v != null) {
      return new Double(_v.doubleValue());
    }
    return null;
  }

  /**
   *
   */
  public synchronized void setInertiePalplanches(final double _p) {
    // tf_rid_pal_inert.setValue(new Double(_p*100000000));
    tf_rid_pal_inert.setValue(new Double(_p));
  }

  /**
   *
   */
  public Double getDemieHauteur() {
    final Double _v = (Double) tf_rid_pal_demih.getValue();
    if (_v != null) {
      return _v;
    }
    return null;
  }

  /**
   *
   */
  public synchronized void setDemieHauteur(final double _p) {
    tf_rid_pal_demih.setValue(new Double(_p));
  }

  /**
   *
   */
  public Double getEspaceEntreDeuxTirants() {
    return (Double) tf_nap_tir_esp.getValue();
  }

  /**
   *
   */
  public synchronized void setEspaceEntreDeuxTirants(final double _p) {
    tf_nap_tir_esp.setValue(new Double(_p));
  }

  /**
   *
   */
  public Double getSectionTirants() {
    Double _d = null;
    double _coeff = 1;
    if (rb_nap_tir_sec_unit_cm2.isSelected()) {
      _coeff = 10000D;
    }
    try {
      _d = new Double(((Double) tf_nap_tir_sec.getValue()).doubleValue() / _coeff);
      // on convertit en m�
    } catch (final NullPointerException _e1) {}
    return _d;
  }

  /**
   *
   */
  public synchronized void setSectionTirants(final double _p) {
    tf_nap_tir_sec.setValue(new Double(_p * 10000.0));
    rb_nap_tir_sec_unit_cm2.setSelected(true);
  }

  /**
   *
   */
  public Double getLimiteElastiqueAcierTirants() {
    return (Double) tf_nap_tir_elast.getValue();
  }

  /**
   *
   */
  public synchronized void setLimiteElastiqueAcierTirants(final double _p) {
    tf_nap_tir_elast.setValue(new Double(_p));
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _e) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT016);
    _mv.addMessages(validation().getMessages());
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    final Double _mya = getModuleYoungAcier();
    final Double _st = getSectionTirants();
    final Double _leat = getLimiteElastiqueAcierTirants();
    final Double _eedt = getEspaceEntreDeuxTirants();
    final Double _lea = getLimiteElastiqueAcier();
    final Double _ip = getInertiePalplanches();
    final Double _dh = getDemieHauteur();
    if ((_lea == null) && (_ip == null) && (_dh == null)) {
      if ((_mya != null) && (_mya.doubleValue() != OscarMsg.INITIALVALUE001)) {
        _vm.add(OscarMsg.VMSG034);
      }
    } else if ((_lea == null) || (_ip == null) || (_dh == null)) {
      _vm.add(OscarMsg.VMSG034);
    } else if (_mya == null) {
      _vm.add(OscarMsg.VMSG034);
    }
    if (!((_st == null) && (_leat == null) && (_eedt == null))
        && !((_st != null) && (_leat != null) && (_eedt != null))) {
      _vm.add(OscarMsg.VMSG035);
    }
    return _vm;
  }
}
