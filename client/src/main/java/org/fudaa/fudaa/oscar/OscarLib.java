/*
 * @file         OscarLib.java
 * @creation     2000-10-15
 * @modification $Date: 2007-11-15 15:11:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.IndexColorModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gif.AcmeGifEncoder;

import org.fudaa.dodico.corba.oscar.*;

import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Lecteur;

import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;

/**
 * Librairie de m�thodes et constantes utiles pour le d�veloppement de Oscar
 * 
 * @version $Revision: 1.15 $ $Date: 2007-11-15 15:11:49 $ by $Author: hadouxad $
 * @author Olivier Hoareau
 */
public final class OscarLib {
	// Constantes de personnalisation du logiciel
	public final static int TITLED_BORDER_LEFTMARGIN = 2;
	public final static int TITLED_BORDER_TOPMARGIN = 1;
	public final static int TITLED_BORDER_RIGHTMARGIN = 2;
	public final static int TITLED_BORDER_BOTTOMMARGIN = 1;
	public final static int EMPTY_BORDER_LEFTMARGIN = 2;
	public final static int EMPTY_BORDER_TOPMARGIN = 1;
	public final static int EMPTY_BORDER_RIGHTMARGIN = 2;
	public final static int EMPTY_BORDER_BOTTOMMARGIN = 1;
	public final static int VERTICAL_LAYOUT_VGAP = 1;
	public final static int GRID_LAYOUT_VGAP = 1;
	public final static int GRID_LAYOUT_HGAP = 5;
	public final static int TABLE_ROWHEIGHT = 17;
	public final static int TABLE_PREFERRED_WIDTH = 400;
	public final static int TABLE_PREFERRED_HEIGHT = 68;
	public final static int TEXT_FIELD_COLUMNS = 10;
	public final static int INTERNALFRAME_PREFERRED_WIDTH = 500;
	public final static int INTERNALFRAME_PREFERRED_HEIGHT = 700;
	public final static int COLUMN_SCROLLPANE_WIDTH = 150;
	public final static int COLUMN_SCROLLPANE_HEIGHT = 120;
	public final static int NUMBER_FORMAT_MAX_FRACTION_DIGITS = 3;
	public final static String FILTRE_FICHIER = "oscar";
	public final static int LABEL_VERTICAL_ALIGNMENT = SwingConstants.CENTER;
	public final static int EFFORTS_NOMBRE_FICHIERS_DIAGRAMMES_MAXI = 15;
	public final static String LINE_SEPARATOR = getLineSeparator();
	public final static String NOTEDECALCULS_TEMP_HTMLFILE = System.getProperty("java.io.tmpdir")
	+ System.getProperty("file.separator") + "tmp";
	// sans extension
	// Constantes de fonctionnement, � ne pas modifier !
	public final static String FILE_SEPARATOR = System.getProperty("file.separator");
	public final static String USER_HOME = System.getProperty("user.home") + FILE_SEPARATOR;
	public final static int POUSSEE = 1;
	public final static int BUTEE = 2;
	public final static int EFFORTS_CVS = 1;
	public final static int EFFORTS_CHS = 2;
	public final static double DOUBLE_EPSILON = 1.0 / 10000000000.0; // 10e-10
	public final static int ONGLET_SOL = 1;
	public final static int ONGLET_EAU = 2;
	public final static int ONGLET_SURCHARGES = 3;
	public final static int ONGLET_EFFORTS = 4;
	public final static int ONGLET_OUVRAGE = 5;
	public final static int ONGLET_VALIDATION = 6;
	public final static int ONGLET_INCERTITUDES = 7;
	public final static int ONGLET_PREDIMENSIONNEMENT = 8;
	public final static int ONGLET_PRESSION = 9;
	public final static int ONGLET_DEFORMEE = 10;
	public final static int ONGLET_MOMENT = 11;
	public final static int ONGLET_EFFORT = 12;
	public final static int INTERNAL_FRAME_PARAMETRES = 1;
	public final static int INTERNAL_FRAME_RESULTATSBRUTS = 2;
	public final static int INTERNAL_FRAME_NOTEDECALCULS = 3;
	public final static double DOUBLE_NULL = VALEUR_NULLE.value;
	public final static int VALIDATION_SUCCEED = 1;
	public final static int VALIDATION_WARNED = 2;
	public final static int VALIDATION_FAILED = 3;
	public final static int CALCULATRICEK_POUSSEE_EFFECTIVE = OscarDialogCalculatriceK.MODE_POUSSEE_EFFECTIF;
	public final static int CALCULATRICEK_POUSSEE_TOTALE = OscarDialogCalculatriceK.MODE_POUSSEE_TOTAL;
	public final static int CALCULATRICEK_BUTEE_EFFECTIVE = OscarDialogCalculatriceK.MODE_BUTEE_EFFECTIF;
	public final static int CALCULATRICEK_BUTEE_TOTALE = OscarDialogCalculatriceK.MODE_BUTEE_TOTAL;
	public static final int SOL_CALCUL_EFFECTIF = 1;
	public static final int SOL_CALCUL_TOTAL = 2;
	public static final int EAU_CALCUL_STATIC = 1;
	public static final int EAU_CALCUL_DYNAMIC = 2;
	// Symboles math�matiques
	public final static BuIcon MATH_PHI_PRIME = getIcon("phi-prime");
	public final static BuIcon MATH_PHI_MU = getIcon("phi-mu");
	public final static BuIcon MATH_BETA = getIcon("beta");
	public final static BuIcon MATH_LAMBDA = getIcon("lambda");
	public final static BuIcon MATH_DELTA = getIcon("delta");
	public final static BuIcon MATH_SIGMA_E = getIcon("sigma-e");
	public final static BuIcon MATH_E = getIcon("e");
	public final static BuIcon MATH_I = getIcon("i");
	public final static BuIcon MATH_PETIT_V = getIcon("petit-v");
	// Valeurs possible pour les donn�es IDL
	public final static String IDL_SOL_CHOIXCALCUL_TOTAL = "C";
	public final static String IDL_SOL_CHOIXCALCUL_EFFECTIF = "L";
	public final static String IDL_EAU_CHOIXCALCUL_STATIC = "S";
	public final static String IDL_EAU_CHOIXCALCUL_DYNAMIC = "D";
	public final static String IDL_SURCHARGE_TYPE_LINEIQUE = "L";
	public final static String IDL_SURCHARGE_TYPE_UNIFORME_B = "B";
	public final static String IDL_SURCHARGE_TYPE_UNIFORME_SI = "S";
	public final static String IDL_OUVRAGE_CHOIXTIRANTS_OUI = "O";
	public final static String IDL_OUVRAGE_CHOIXTIRANTS_NON = "N";
	public final static String IDL_EAU_PRESENCE_NAPPE_OUI = "O";
	public final static String IDL_EAU_PRESENCE_NAPPE_NON = "N";

	// M�thodes utiles
	public static boolean writeFile(final String _f, final String _txt) {
		try {
			final FileWriter _fw = new FileWriter(_f);
			_fw.write(_txt, 0, _txt.length());
			_fw.flush();
			_fw.close();
			return true;
		} catch (final Exception _e1) {
			return false;
		}
	}

	public static String getLineSeparator() {
		return CtuluLibString.LINE_SEP;
	}

	/**
	 * convertit le fichier fortran <code>_source</code> en un fichier de donn�es csv (s�prateur point-virgule)
	 * <code>_target</code>, en prenant en compte les largeurs de colonnes <code>_colsSize</code> dans le fichier
	 * source.
	 */
	public static boolean convertFortranFileToCsv(final String _source, final String _target, final int[][] _colsSize) {
		final int i = 0;
		int j;
		int[] _fmtI;
		int[] _line1;
		if (_colsSize.length > 1) {
			_line1 = _colsSize[0];
			_fmtI = _colsSize[1];
		} else {
			_line1 = null;
			_fmtI = _colsSize[0];
		}
		String _csv = "";
		String _txt = "";
		try {
			final FortranReader _fr = new FortranReader(new FileReader(_source));
			while (true) {
				if ((_line1 != null) && (i == 0)) {
					_fr.readFields(_line1);
				} else {
					_fr.readFields(_fmtI);
				}
				_txt = "";
				for (j = 0; j < _fr.getNumberOfFields(); j++) {
					_txt += ((_txt.length() > 0) ? ";" : "") + (_fr.stringField(j)).trim();
				}
				_csv += _txt + LINE_SEPARATOR;
			}
		} catch (final EOFException _e1) {} catch (final Exception _e1) {
			return false;
		}
		return writeFile(_target, _csv);
	}

	/**
	 * convertit le fichier csv <code>_source</code> en un fichier de donn�es Fortran (largeur de colonne fixe)
	 * <code>_target</code> dont les largeurs de colonnes sont sp�cifi�es dans <code>_colsSize</code>.
	 */
	public static boolean convertCsvFileToFortran(final String _source, final String _target, final int[][] _colsSize) {
		final Vector _data = readCsvFile(_source);
		if (_data == null) {
			return false;
		}
		final Object[] _lines = _data.toArray();
		int[] _fmtI;
		int[] _line1;
		if (_colsSize.length > 1) {
			_line1 = _colsSize[0];
			_fmtI = _colsSize[1];
		} else {
			_line1 = null;
			_fmtI = _colsSize[0];
		}
		try {
			final FortranWriter _fw = new FortranWriter(new FileWriter(_target));
			_fw.setLineSeparator(LINE_SEPARATOR);
			for (int i = 0; i < _lines.length; i++) {
				final Vector _v = (Vector) _lines[i];
				final Object[] _o = _v.toArray();
				for (int j = 0; j < _o.length; j++) {
					try {
						if ((_line1 != null) && (i == 0)) {
							final String _s = (String) _o[j];
							_fw.stringField(j, _s);
						} else {
							final Double _s = new Double((String) _o[j]);
							_fw.doubleField(j, _s.doubleValue());
						}
					} catch (final NullPointerException _e2) {
						_fw.stringField(j, "");
					}
				}
				if ((_line1 != null) && (i == 0)) {
					_fw.writeFields(_line1);
				} else {
					_fw.writeFields(_fmtI);
				}
			}
			_fw.flush();
			_fw.close();
			return true;
		} catch (final Exception _e1) {
			return false;
		}
	}

	// public static String makeMessageString(String _mode,String _cat,String _txt){
	// String _msg="";
	// if(_mode.equals("PARAMETRES")){
	// _msg = "<html><font size=\"1\"><b>"+_cat+"</b> : "+FuLib.replace(_txt,"\n","<br>")+"<br></font></html>";
	// }
	// return _msg;
	// }
	/**
	 *
	 */
	public static String replace(final String _what, final String _with, final String _in) {
		return FuLib.replace(_in, _what, _with);
	}

	/**
	 * renvoie une icone repr�sentant le fichier icone sp�cifi�
	 * 
	 * @param _i nom du fichier icone a charger (sans l'extension .gif)
	 */
	public static BuIcon getIcon(final String _i) {
		return OscarResource.OSCAR.getIcon(_i + ".gif");
	}

	/**
	 * renvoie l'image sp�cifi�e dans un objet BuIcon
	 * 
	 * @param _i nom du fichier image a charger
	 */
	public static BuIcon getImage(final String _i) {
		return new BuIcon(Oscar.class.getResource(_i));
	}

	/**
	 *
	 */
	public static String NumberPuce(final int _n, final String _mode) {
		String _txt = "";
		if (_mode.equals("I")) {
			switch (_n) {
			case 1: {
				_txt = "I";
				break;
			}
			case 2: {
				_txt = "II";
				break;
			}
			case 3: {
				_txt = "III";
				break;
			}
			case 4: {
				_txt = "IV";
				break;
			}
			case 5: {
				_txt = "V";
				break;
			}
			case 6: {
				_txt = "VI";
				break;
			}
			case 7: {
				_txt = "VII";
				break;
			}
			case 8: {
				_txt = "VIII";
				break;
			}
			case 9: {
				_txt = "IX";
				break;
			}
			case 10: {
				_txt = "X";
				break;
			}
			default: {
				_txt = "" + _n;
				break;
			}
			}
		} else if (_mode.equals("1")) {
			_txt = "" + _n;
		} else if (_mode.equals("A")) {
			switch (_n) {
			case 1: {
				_txt = "A";
				break;
			}
			case 2: {
				_txt = "B";
				break;
			}
			case 3: {
				_txt = "C";
				break;
			}
			case 4: {
				_txt = "D";
				break;
			}
			case 5: {
				_txt = "E";
				break;
			}
			case 6: {
				_txt = "F";
				break;
			}
			case 7: {
				_txt = "G";
				break;
			}
			case 8: {
				_txt = "H";
				break;
			}
			case 9: {
				_txt = "I";
				break;
			}
			case 10: {
				_txt = "J";
				break;
			}
			case 11: {
				_txt = "K";
				break;
			}
			case 12: {
				_txt = "L";
				break;
			}
			case 13: {
				_txt = "M";
				break;
			}
			case 14: {
				_txt = "N";
				break;
			}
			case 15: {
				_txt = "O";
				break;
			}
			case 16: {
				_txt = "P";
				break;
			}
			case 17: {
				_txt = "Q";
				break;
			}
			case 18: {
				_txt = "R";
				break;
			}
			case 19: {
				_txt = "S";
				break;
			}
			case 20: {
				_txt = "T";
				break;
			}
			case 21: {
				_txt = "U";
				break;
			}
			case 22: {
				_txt = "V";
				break;
			}
			case 23: {
				_txt = "W";
				break;
			}
			case 24: {
				_txt = "X";
				break;
			}
			case 25: {
				_txt = "Y";
				break;
			}
			case 26: {
				_txt = "Z";
				break;
			}
			default: {
				_txt = "" + _n;
				break;
			}
			}
		} else if (_mode.equals("a")) {
			switch (_n) {
			case 1: {
				_txt = "a";
				break;
			}
			case 2: {
				_txt = "b";
				break;
			}
			case 3: {
				_txt = "c";
				break;
			}
			case 4: {
				_txt = "d";
				break;
			}
			case 5: {
				_txt = "e";
				break;
			}
			case 6: {
				_txt = "f";
				break;
			}
			case 7: {
				_txt = "g";
				break;
			}
			case 8: {
				_txt = "h";
				break;
			}
			case 9: {
				_txt = "i";
				break;
			}
			case 10: {
				_txt = "j";
				break;
			}
			case 11: {
				_txt = "k";
				break;
			}
			case 12: {
				_txt = "l";
				break;
			}
			case 13: {
				_txt = "m";
				break;
			}
			case 14: {
				_txt = "n";
				break;
			}
			case 15: {
				_txt = "o";
				break;
			}
			case 16: {
				_txt = "p";
				break;
			}
			case 17: {
				_txt = "q";
				break;
			}
			case 18: {
				_txt = "r";
				break;
			}
			case 19: {
				_txt = "s";
				break;
			}
			case 20: {
				_txt = "t";
				break;
			}
			case 21: {
				_txt = "u";
				break;
			}
			case 22: {
				_txt = "v";
				break;
			}
			case 23: {
				_txt = "w";
				break;
			}
			case 24: {
				_txt = "x";
				break;
			}
			case 25: {
				_txt = "y";
				break;
			}
			case 26: {
				_txt = "z";
				break;
			}
			default: {
				_txt = "" + _n;
				break;
			}
			}
		}
		return _txt;
	}

	/**
	 * boite de dialogue pour enregistrement de fichier
	 */
	public static String getFileChoosenByUser(final Component parent, final String _title, final String _approvebutton,
			final String _currentpath) {
		final JFileChooser chooser = new JFileChooser();
		if (_approvebutton != null) {
			chooser.setApproveButtonText(_approvebutton);
		}
		if (_title != null) {
			chooser.setDialogTitle(_title);
		}
		if (_currentpath != null) {
			chooser.setCurrentDirectory(new File(_currentpath));
		}
		if (_currentpath != null) {
			chooser.setSelectedFile(new File(_currentpath));
		}
		final int returnVal = chooser.showOpenDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return chooser.getSelectedFile().getPath();
		}
		return null;
	}

	/**
	 * boite de dialogue pour enregistrement de fichier
	 */
	public static String getFileChoosenByUser(final Component parent) {
		return getFileChoosenByUser(parent, null, null, null);
	}

	/**
	 *
	 */
	public static String getUnitForSymbol(final String _symb) {
		String _unit = null;
		try {
			if (_symb.equals("diese")) {
				_unit = "123...";
			} else if (_symb.equals("sigma")) {
				_unit = "m�tre";
			} else if (_symb.equals("gamma-h")) {
				_unit = "kN/m3";
			} else if (_symb.equals("gamma-sat")) {
				_unit = "kN/m3";
			} else if (_symb.equals("Ka")) {
				_unit = "...";
			} else if (_symb.equals("c-prime")) {
				_unit = "kN/m2";
			} else if (_symb.equals("phi-prime")) {
				_unit = "degr�";
			} else {
				_unit = "";
			}
		} catch (final NullPointerException _e1) {
			_unit = "";
		}
		return _unit;
	}

	/**
	 * retourne le chemin complet du fichier html d'aide correspondante � la cl� sp�cifi�e.
	 * 
	 * @param _h chaine de caract�res repr�sentant l'aide � afficher. il s'agit du nom du fichier sans l'extension .html
	 */
	public static String helpUrl(final String _h) {
		String _url = _h;
		try {
			final int i1 = 0;
			final int i2 = _h.indexOf('#');
			final int i3 = _h.length();
			final String _fichier = _h.substring(i1, i2) + ".html";
			final String _signet = _h.substring(i2 + 1, i3);
			_url = _fichier + "#" + _signet;
		} catch (final ArrayIndexOutOfBoundsException _e1) {
			_url += ".html";
		} catch (final NullPointerException _e2) {
			_url += ".html";
		} catch (final StringIndexOutOfBoundsException _e3) {
			_url += ".html";
		}
		//-- test des methode de path
		
		System.out.println("OscarImplementation.informationsSoftware().baseManUrl(): "+OscarImplementation.informationsSoftware().man);
		System.out.println("OscarImplementation.informationsSoftware().name.toLowerCase(): "+OscarImplementation.informationsSoftware().name.toLowerCase());
		
		final String _u = //"file:"+
			//+ OscarImplementation.informationsSoftware().man
			cheminAide()
		//+ OscarImplementation.informationsSoftware().name.toLowerCase()+ "/" 
		+ _url;
		return _u;
	}


	//-- Ajout de la methode qui calcule le chemin serveur en fonction du type d'utilisation
	// AH
	protected static final String cheminAide() {
	    String path = System.getProperty("OSCAR_SERVEUR");
	    if ((path == null) || path.equals("")) {
	      path = OscarImplementation.informationsSoftware().man
	          + File.separator + "oscar";
	    }
	    if (!path.endsWith(File.separator)) {
	      path += File.separator;
	    }
	    return path;
	  }	
	
	/**
	 * M�thode qui renvoie un composant border form� de fusion entre un composant border avec titre _t (TITLED) contenant
	 * un composant border avec marges (EMPTY).
	 */
	public static Border TitledBorder(final String _t) {
		final TitledBorder tb = new TitledBorder(_t);
		final EmptyBorder eb = new EmptyBorder(TITLED_BORDER_TOPMARGIN, TITLED_BORDER_LEFTMARGIN,
				TITLED_BORDER_BOTTOMMARGIN, TITLED_BORDER_RIGHTMARGIN);
		final CompoundBorder cb = new CompoundBorder(tb, eb);
		return cb;
	}

	/**
	 * M�thode qui renvoie un composant border avec marges par d�faut (voir les constantes EMPTY_BORDER_*).
	 */
	public static Border EmptyBorder() {
		final EmptyBorder eb = new EmptyBorder(EMPTY_BORDER_TOPMARGIN, EMPTY_BORDER_LEFTMARGIN, EMPTY_BORDER_BOTTOMMARGIN,
				EMPTY_BORDER_RIGHTMARGIN);
		return eb;
	}

	/**
	 * M�thode qui renvoie un layout manager (vertical) avec les espacements de cellules par d�faut (voir les constantes
	 * VERTICAL_LAYOUT_*).
	 */
	public static BuVerticalLayout VerticalLayout() {
		final BuVerticalLayout vl = new BuVerticalLayout();
		vl.setVgap(VERTICAL_LAYOUT_VGAP);
		return vl;
	}

	/**
	 * M�thode qui renvoie un layout manager (flow) avec les propri�t�s par d�faut (voir les constantes FLOW_LAYOUT_*).
	 */
	public static FlowLayout FlowLayout() {
		final FlowLayout fl = new FlowLayout();
		return fl;
	}

	/**
	 * M�thode qui renvoie un panel avec un layout manager flow, et ses propri�t�s par d�faut (voir les constantes
	 * FLOW_PANEL_*).
	 */
	public static BuPanel FlowPanel() {
		final BuPanel _p = new BuPanel();
		final FlowLayout _fl = FlowLayout();
		_p.setLayout(_fl);
		_p.setBorder(EmptyBorder());
		return _p;
	}

	/**
	 * M�thode qui renvoie un panel avec un layout manager box horizontal
	 */
	public static BuPanel HFlowPanel() {
		final BuPanel _p = new BuPanel();
		final BoxLayout _bl = new BoxLayout(_p, BoxLayout.X_AXIS);
		_p.setLayout(_bl);
		_p.setBorder(EmptyBorder());
		return _p;
	}

	/**
	 * M�thode qui renvoie un layout manager (grid) avec les espacements de cellules par d�faut (voir les constantes
	 * VERTICAL_LAYOUT_*) et le nombre de colonnes pass� en param�tres.
	 */
	public static BuGridLayout GridLayout(final int _n) {
		final BuGridLayout gl = new BuGridLayout();
		gl.setColumns(_n);
		gl.setHgap(GRID_LAYOUT_HGAP);
		gl.setVgap(GRID_LAYOUT_VGAP);
		return gl;
	}

	/**
	 *
	 */
	public static OscarImplementation getImplementation(final BuCommonInterface _appli) {
		final OscarImplementation xi = ((OscarImplementation) _appli.getImplementation());
		return xi;
	}

	/**
	 * renvoie un tableau de tableau d'Object (string en fait) de _r lignes et de _c colonnes.
	 */
	public static Object[][] EmptyArray(final int _r, final int _c) {
		final Object[][] tab = new Object[_r][_c];
		for (int i = 0; i < _r; i++) {
			for (int j = 0; j < _c; j++) {
				tab[i][j] = CtuluLibString.EMPTY_STRING;
			}
		}
		return tab;
	}

	/**
	 * renvoie un tableau d'Object (string en fait) de de _c colonnes.
	 */
	public static Object[] EmptyRow(final int _c) {
		final Object[] tab = new Object[_c];
		for (int i = 0; i < _c; i++) {
			tab[i] = CtuluLibString.EMPTY_STRING;
		}
		return tab;
	}

	/**
	 *
	 */
	public static void exportGraphScriptToGif(final String _script, final String _file) {
		final Graphe _g = Graphe.parse(new Lecteur(new StringReader(_script)), (Applet) null);
		// Palette Web => 216 couleurs
		final byte[] _cr = new byte[256];
		final byte[] _cg = new byte[256];
		final byte[] _cb = new byte[256];
		int i = 0;
		int _rc;
		int _gc;
		int _bc;
		_rc = 0;
		while (_rc <= 255) {
			_gc = 0;
			while (_gc <= 255) {
				_bc = 0;
				while (_bc <= 255) {
					_cr[i] = (byte) _rc;
					_cg[i] = (byte) _gc;
					_cb[i] = (byte) _bc;
					_bc += 51;
					i++;
				}
				_gc += 51;
			}
			_rc += 51;
		}
		final BufferedImage _img = new BufferedImage(720, 400, BufferedImage.TYPE_BYTE_INDEXED, new IndexColorModel(8, 216,
				_cr, _cg, _cb));
		_g.dessine(_img.createGraphics(), 0, 0, 720, 400, 0, (ImageObserver) null);
		try {
			final AcmeGifEncoder _age = new AcmeGifEncoder(_img, new FileOutputStream(_file));
			_age.encode();
		} catch (final Exception _e1) {
			WSpy.Error("Erreur d'entr�e/sortie. Impossible d'�crire le fichier image sur le disque dur");
		}
	}

	/**
	 *
	 */
	public static void showDialogGraphViewer(final BuCommonInterface _app, final String _title, final String _script) {
		// Lecteur _lin = new Lecteur(new ByteArrayInputStream(_script.getBytes()));
		final Lecteur _lin = new Lecteur(new StringReader(_script));
		final Graphe _g = Graphe.parse(_lin, (Applet) null);
		// Graphe _g = Graphe.parse(new Lecteur(new InputStreamReader(new StringReader(_script))),(Applet)null);
		final BGraphe _bg = new BGraphe();
		_bg.setGraphe(_g);
		final OscarDialogPictureViewer _xdpv = new OscarDialogPictureViewer(_app, _title, _bg);
		_xdpv.setVisible(true);
	}

	/**
	 * renvoie un composant TableModel vide (sans aucune ligne) avec les colonnes contenues dans le tableau _c (tableau de
	 * string contenant la liste des labels de colonnes).
	 */
	public static DefaultTableModel TableModel(final Object[] _c) {
		final DefaultTableModel m_ = new DefaultTableModel(_c, 0);
		return m_;
	}

	/**
	 * renvoie un composant Table vide (sans aucune ligne) avec les colonnes contenues dans le tableau _c (tableau de
	 * string contenant la liste des labels de colonnes). Les propri�t�s de l'objet Table sont positionn�es � celles par
	 * d�faut (voir les constantes TABLE_*). Les tailles de colonnes sont contenues dans l'objet _s. Une taille marqu�e
	 * "0", correspond � une taille ind�finie.
	 */
	public static BuTable Table(final Object[] _c, final int[] _s) {
		final BuTable tab = new BuTable();
		tab.setRowHeight(TABLE_ROWHEIGHT);
		tab.setPreferredScrollableViewportSize(new Dimension(TABLE_PREFERRED_WIDTH, TABLE_PREFERRED_HEIGHT));
		int _tmpsize = 0;
		TableColumn _tmptcm = null;
		// DefaultTableModel _tm = new DefaultTableModel();
		for (int i = 0; i < _c.length; i++) {
			try {
				_tmpsize = (_s[i] == 0) ? 75 : _s[i];
				_tmptcm = new TableColumn(i + 1);
				_tmptcm.setPreferredWidth(_tmpsize);
				_tmptcm.setHeaderValue(_c[i]);
				tab.addColumn(_tmptcm);
			} catch (final ArrayIndexOutOfBoundsException _e1) {
				_tmpsize = 75;
				_tmptcm = new TableColumn(i + 1);
				_tmptcm.setPreferredWidth(_tmpsize);
				_tmptcm.setHeaderValue(_c[i]);
				tab.addColumn(_tmptcm);
			}
		}
		return tab;
	}

	/**
	 * renvoie un composant Table vide (sans aucune ligne) avec les colonnes contenues dans le tableau _c (tableau de
	 * string contenant la liste des labels de colonnes). Les propri�t�s de l'objet Table sont positionn�es � celles par
	 * d�faut (voir les constantes TABLE_*).
	 */
	public static BuTable Table(final Object[] _c) {
		final BuTable tab = new BuTable(new Object[0][0], _c);
		tab.setRowHeight(TABLE_ROWHEIGHT);
		tab.setPreferredScrollableViewportSize(new Dimension(TABLE_PREFERRED_WIDTH, TABLE_PREFERRED_HEIGHT));
		return tab;
	}

	/**
	 * renvoie un composant Table vide (sans aucune ligne) avec les colonnes contenues dans le tableau _c (tableau de
	 * string contenant la liste des labels de colonnes). Les propri�t�s de l'objet Table sont positionn�es � celles par
	 * d�faut (voir les constantes TABLE_*).
	 */
	public static BuTable Table(final DefaultTableModel _m) {
		final BuTable tab = new BuTable(_m);
		tab.setRowHeight(TABLE_ROWHEIGHT);
		tab.setPreferredScrollableViewportSize(new Dimension(TABLE_PREFERRED_WIDTH, TABLE_PREFERRED_HEIGHT));
		tab.setCellSelectionEnabled(true);
		tab.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		return tab;
	}

	/**
	 * renvoie un composant champs de saisie permettant de saisir des chiffres de type double initialis� avec la valeur
	 * _v.
	 * 
	 * @param _format mod�le de format d'affichage du nombre double
	 * @param _v valeur initiale affich�e dans le champs
	 */
	public static BuTextField DoubleField(final String _format, final double _v) {
		final BuTextField tf = DoubleField(_format);
		final Double _val = new Double(_v);
		tf.setValue(_val);
		tf.setColumns((TEXT_FIELD_COLUMNS > _val.toString().length()) ? TEXT_FIELD_COLUMNS : _val.toString().length());
		return tf;
	}

	/**
	 * renvoie un composant champs de saisie permettant de saisir des chiffres de type double.
	 * 
	 * @param _format mod�le de format d'affichage du nombre double
	 */
	public static BuTextField DoubleField(final String _format) {
		final BuTextField tf = BuTextField.createDoubleField();
		tf.setDisplayFormat(new DecimalFormat(_format));
		tf.setColumns((TEXT_FIELD_COLUMNS > _format.length()) ? TEXT_FIELD_COLUMNS : _format.length());
		tf.setRequestFocusEnabled(true);
		// tf.setText("");
		return tf;
	}

	/**
	 * renvoie un composant TextField (champs de saisie) avec ses propri�t�s positionn�es � celles par d�faut (voir les
	 * constantes TEXT_FIELD_*).
	 */
	public static BuTextField TextField() {
		final BuTextField tf = BuTextField.createDoubleField();
		tf.setColumns(TEXT_FIELD_COLUMNS);
		return tf;
	}

	/**
	 * renvoie un composant Button (bouton de commande) avec son identifiant de commande.
	 */
	public static BuButton Button(final String _label, final String _cmd) {
		final BuButton bt = new BuButton(_label);
		bt.setActionCommand(_cmd);
		return bt;
	}

	/**
	 * renvoie un composant Button (bouton de commande) avec son identifiant de commande et son ic�ne
	 * 
	 * @param _label titre du bouton
	 * @param _icone identifiant du bouton � utiliser pour retrouver son ic�ne
	 * @param _cmd identifiant de commande du bouton
	 */
	public static BuButton Button(final String _label, final String _icone, final String _cmd) {
		final BuButton _bt = new BuButton(BuResource.BU.loadToolCommandIcon(_icone), BuResource.BU.getString(_label));
		_bt.setActionCommand(_cmd);
		// _bt.setPreferredSize(new Dimension(120,30));
		return _bt;
	}

	/**
	 * renvoie un float (nombre) correspondant � la valeur d'un champs texte.
	 */
	public static float TextFieldFloatValue(final BuTextField _tf) {
		final float _f = Float.valueOf(_tf.getText()).floatValue();
		return _f;
	}

	/**
	 * renvoie un double (nombre) correspondant � la valeur d'un champs texte.
	 */
	public static double TextFieldDoubleValue(final BuTextField _tf) {
		final double _d = ((Double) _tf.getValue()).doubleValue();
		return _d;
	}

	/**
	 * renvoie un composant Label avec ses propri�t�s positionn�es � celles par d�faut (notamment l'alignement vertical du
	 * texte : voir LABEL_...
	 * 
	 * @param _text texte � afficher dans le label
	 */
	public static BuLabel Label(final String _text) {
		final BuLabel _lb = new BuLabel(_text);
		_lb.setVerticalAlignment(LABEL_VERTICAL_ALIGNMENT);
		return _lb;
	}

	/**
	 * renvoie un composant Label avec ses propri�t�s positionn�es � celles par d�faut (notamment l'alignement vertical du
	 * texte : voir LABEL_... Le label dispose d'une icone
	 * 
	 * @param _text texte � afficher dans le label
	 */
	public static BuLabel Label(final String _text, final String _iconFile) {
		final BuLabel _lb = new BuLabel(_text);
		_lb.setIcon(OscarResource.OSCAR.getIcon(_iconFile));
		_lb.setVerticalAlignment(LABEL_VERTICAL_ALIGNMENT);
		return _lb;
	}

	/**
	 * renvoie un composant Label contenant l'image d'un symbol math�matique sp�cifi�e avec ses propri�t�s positionn�es �
	 * celles par d�faut (notamment l'alignement vertical du texte : voir LABEL_... Le label dispose d'une icone
	 * 
	 * @param _symb fichier symbole � afficher
	 */
	public static BuLabel MathSymbol(final BuIcon _symb) {
		final BuLabel _lb = new BuLabel("");
		_lb.setIcon(_symb);
		_lb.setVerticalAlignment(LABEL_VERTICAL_ALIGNMENT);
		return _lb;
	}

	/**
	 * renvoie un composant cadre vide avec un titre
	 * 
	 * @param _titre texte � afficher en tant que titre du panel
	 */
	public static BuPanel TitledPanel(final String _titre) {
		final BuPanel _p = EmptyPanel();
		_p.setBorder(new CompoundBorder(TitledBorder(_titre), EmptyBorder()));
		return _p;
	}

	/**
	 * renvoie un composant cadre vide avec un layout manager grid de _n colonnes
	 * 
	 * @param _n nombre de colonnes du layout manager
	 */
	public static BuPanel GridPanel(final int _n) {
		final BuPanel _p = EmptyPanel();
		_p.setLayout(new BuGridLayout(_n));
		return _p;
	}

	/**
	 * renvoie un composant cadre d'info avec un texte d'info
	 * 
	 * @param _txt texte d'info � afficher
	 */
	public static BuPanel InfoPanel(final String _txt) {
		final BuLabelMultiLine _lb = new BuLabelMultiLine(_txt);
		_lb.setWrapMode(BuLabelMultiLine.WORD);
		_lb.setPreferredSize(_lb.computeHeight(INTERNALFRAME_PREFERRED_WIDTH));
		final BuPanel _p = new BuPanel();
		_p.setBorder(new CompoundBorder(TitledBorder(OscarMsg.TITLELAB_INFO), EmptyBorder()));
		_p.setLayout(new BorderLayout());
		_p.add(_lb);
		return _p;
	}

	/**
	 * renvoie un composant LabelMultiline avec ses propri�t�s positionn�es � celles par d�faut (notamment sa taille en
	 * largeur : voir INTERNALFRAME_PREFERRED_WIDTH).
	 */
	public static BuLabelMultiLine LabelMultiLine(final String txt) {
		final BuLabelMultiLine lb = new BuLabelMultiLine(txt);
		lb.setWrapMode(BuLabelMultiLine.WORD);
		lb.setPreferredSize(lb.computeHeight(INTERNALFRAME_PREFERRED_WIDTH));
		return lb;
	}

	/**
	 * renvoie un composant ScrollPane (conteneur permettant le d�filement par ascenseurs) aux dimensions standards pour
	 * �tre ins�r� dans une des colonnes (gauches ou droites). Ses propri�t�s sont positionn�es � celles par d�faut (voir
	 * les constantes COLUMN_SCROLLPANE_*).
	 */
	public static BuScrollPane ColumnScrollPane(final JComponent _c) {
		final BuScrollPane sp = new BuScrollPane(_c);
		sp.setPreferredSize(new Dimension(COLUMN_SCROLLPANE_WIDTH, COLUMN_SCROLLPANE_HEIGHT));
		return sp;
	}

	/**
	 *
	 */
	public static void ViderTableau(final JTable _tb, final DefaultTableModel _dm) {
		try {
			if (_tb.getRowCount() <= -1) {
				return;
			}
			if (_tb.isEditing()) {
				_tb.getCellEditor().stopCellEditing();
			}
			for (int i = 0; i < _tb.getRowCount(); i++) {
				_dm.removeRow(i);
			}
		} catch (final NullPointerException _e1) {}
	}

	/**
	 * renvoie un objet FudaaFiltreFichier en positionnant sa donn�es "Filtre" par d�faut avec l'extension des fichiers du
	 * logiciel (voir les constantes FILTRE_FICHIER).
	 */
	public static FudaaFiltreFichier FiltreFichier() {
		final FudaaFiltreFichier ff = new FudaaFiltreFichier(FILTRE_FICHIER);
		return ff;
	}

	/**
	 * renvoie une chaine de caract�re correspondant au chemin syst�me par d�faut pour les boites de dialogues ouvrir et
	 * nouveau.
	 */
	public static String DefaultDirectory() {
		final String s = System.getProperty("user.dir") + File.separator + "exemples" + File.separator + "oscar";
		return s;
	}

	/**
	 * renvoie une chaine de caract�res correspondant � la liste des arguments de la commande pass�e en param�tres mais
	 * sans le mot cl� de l'action de cette commande, ne renvoie null si la commande ne contenait aucun argument.
	 */
	public static String ActionArgumentStringOnly(final String _a) {
		final int i = _a.indexOf('(');
		String arg = null;
		if (i >= 0) {
			arg = _a.substring(i + 1, _a.length() - 1);
		}
		return arg;
	}

	/**
	 * renvoie une chaine de caract�re correspondant au mot cl� de l'action pass�e en param�tres sans sa liste �ventuelle
	 * de param�tres, si aucun param�tres n'�tant pr�sent dans la commande initiale, renvoie le chaine tel quel.
	 */
	public static String ActionStringWithoutArgument(final String _a) {
		final int i = _a.indexOf('(');
		final String s = (i >= 0) ? _a.substring(0, i) : _a;
		return s;
	}

	/**
	 * affiche un rapport d'erreur de validation des donn�es dans une fen�tre HTML
	 */
	public static void ShowValidationMessagesWindow(final BuCommonInterface _a, final ValidationMessages _vm) {
		String _html = "";
		int i = 0;
		_html += "<html>" + LINE_SEPARATOR + "";
		_html += "<head><title>Rapport d'erreurs de validation</title></head>" + LINE_SEPARATOR + "";
		_html += "<body>" + LINE_SEPARATOR + "";
		_html += "<h1>Rapport d'erreurs de validation</h1>" + LINE_SEPARATOR + "";
		final VMessage[] _fatalMsg = _vm.getFatalErrorMessages();
		final VMessage[] _warningMsg = _vm.getWarningMessages();
		if (_fatalMsg.length > 0) {
			_html += "<h2>Erreurs bloquantes</h2>" + LINE_SEPARATOR + "";
			_html += "<table width=\"100%\" border=1>" + LINE_SEPARATOR + "";
			_html += "<tr><td><b>Onglet</b></td><td><b>Donn�es</b></td><td><b>Etat</b></td><td><b>Message</b></td></tr>"
				+ LINE_SEPARATOR + "";
			for (i = 0; i < _fatalMsg.length; i++) {
				_html += "<tr bgcolor=\"#" + (((i % 2) == 1) ? "DDDDDD" : "FFFFFF") + "\">" + LINE_SEPARATOR + "";
				_html += "<td><b>" + _fatalMsg[i].getOnglet() + "</b></td>";
				_html += "<td>" + _fatalMsg[i].getDonnees() + "</td>";
				_html += "<td><i>" + _fatalMsg[i].getEtat() + "</i></td>";
				_html += "<td><font color=red>" + _fatalMsg[i].getMessage() + "</font></td>" + LINE_SEPARATOR + "";
				_html += "</tr>" + LINE_SEPARATOR + "";
			}
			_html += "</table>" + LINE_SEPARATOR + "";
		}
		if (_warningMsg.length > 0) {
			_html += "<h2>Avertissements</h2>" + LINE_SEPARATOR + "";
			_html += "<table width=\"100%\" border=1>" + LINE_SEPARATOR + "";
			_html += "<tr><td><b>Onglet</b></td><td><b>Donn�es</b></td><td><b>Etat</b></td><td><b>Message</b></td></tr>"
				+ LINE_SEPARATOR + "";
			for (i = 0; i < _warningMsg.length; i++) {
				_html += "<tr bgcolor=\"#" + (((i % 2) == 1) ? "DDDDDD" : "FFFFFF") + "\">" + LINE_SEPARATOR + "";
				_html += "<td><b>" + _warningMsg[i].getOnglet() + "</b></td>";
				_html += "<td>" + _warningMsg[i].getDonnees() + "</td>";
				_html += "<td><i>" + _warningMsg[i].getEtat() + "</i></td>";
				_html += "<td><font color=blue>" + _warningMsg[i].getMessage() + "</font></td>" + LINE_SEPARATOR + "";
				_html += "</tr>" + LINE_SEPARATOR + "";
			}
			_html += "</table>" + LINE_SEPARATOR + "";
		}
		_html += "<br><br><b>Remarques :</b><br>Les erreurs bloquantes vous emp�chent de lancer les calculs, vous devez donc les corriger pour pouvoir continuer. Les avertissements ne vous emp�chent pas de lancer les calculs mais vous signalent que certains calculs ne seront peut �tre pas effectu�s.";
		_html += "</body>" + LINE_SEPARATOR + "";
		_html += "</html>" + LINE_SEPARATOR + "";
		final OscarImplementation _oscar = (OscarImplementation) _a.getImplementation();
		_oscar.showHtml("Rapport d'erreurs", _html);
	}

	/**
	 *
	 */
	public static Vector readCsvFile(final String _f) {
		Vector _v;
		Vector _v0;
		LineNumberReader _lnr;
		try {
			_lnr = new LineNumberReader(new FileReader(_f));
		} catch (final FileNotFoundException _e1) {
			WSpy.Error("impossible d'afficher le contenu du fichier, fichier introuvable.");
			return null;
		}
		String _s;
		StringTokenizer st;
		try {
			_v0 = new Vector();
			while ((_s = _lnr.readLine()) != null) {
				st = new StringTokenizer(_s, ";");
				_v = new Vector();
				while (st.hasMoreTokens()) {
					_v.addElement(st.nextToken());
				}
				_v0.addElement(_v);
			}
			return _v0;
		} catch (final IOException _e1) {
			WSpy
			.Error("erreur d'entr�e/sortie survenue lors de la lecture du fichier. impossible d'afficher le contenu du fichier.");
			return null;
		}
	}

	/**
	 *
	 */
	public static boolean verifierCotesDecroissantesDansFichierCsv(final String _f) {
		final Vector _data = readCsvFile(_f);
		Double _dv;
		double _cur;
		if (_data == null) {
			return false;
		}
		_cur = Double.MAX_VALUE;
		final Object[] _lines = _data.toArray();
		for (int i = 0; i < _lines.length; i++) {
			final Vector _v = (Vector) _lines[i];
			final Object[] _o = _v.toArray();
			for (int j = 0; j < 1 /* _o.length */; j++) {
				final String _s = (String) _o[j];
				try {
					if (i > 0) {
						_dv = new Double(_s);
						if (_cur < _dv.doubleValue()) {
							return false;
						}
						_cur = _dv.doubleValue();
					}
				} catch (final Exception _e) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 *
	 */
	public static boolean verifierDeuxCotesMinimumDansFichierCsv(final String _f) {
		final Vector _data = readCsvFile(_f);
		// Double _dv;
		// double _cur;
		if (_data == null) {
			return false;
		}
		// _cur= Double.MAX_VALUE;
		final Object[] _lines = _data.toArray();
		for (int i = 0; i < _lines.length; i++) {
			final Vector _v = (Vector) _lines[i];
			final Object[] _o = _v.toArray();
			for (int j = 0; j < _o.length; j++) {
				final String _s = (String) _o[j];
				try {
					if (i > 0) {
						/* _dv= */new Double(_s);
					}
				} catch (final Exception _e) {
					return false;
				}
			}
			if (i > 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 */
	public static boolean verifierDeuxColonnesDansFichierCsv(final String _f) {
		final Vector _data = readCsvFile(_f);
		// double _cur;
		if (_data == null) {
			return false;
		}
		// _cur= Double.MAX_VALUE;
		final Object[] _lines = _data.toArray();
		int _min = 100;
		for (int i = 1; i < _lines.length; i++) {
			final Vector _v = (Vector) _lines[i];
			_min = (_min < _v.size()) ? _min : _v.size();
		}
		if (_min < 2) {
			return false;
		}
		return true;
	}

	/**
	 *
	 */
	public static String getCsvFileInHtml(final String _f) {
		String _html;
		final Vector _data = readCsvFile(_f);
		if (_data == null) {
			return null;
		}
		_html = "<b>Fichier :</b> " + _f + "<br><br>" + LINE_SEPARATOR + "";
		_html += "<table border=1>" + LINE_SEPARATOR + "";
		final Object[] _lines = _data.toArray();
		for (int i = 0; i < _lines.length; i++) {
			_html += "<tr>";
			final Vector _v = (Vector) _lines[i];
			final Object[] _o = _v.toArray();
			for (int j = 0; j < _o.length; j++) {
				final String _s = (String) _o[j];
				_html += "<td>" + ((i == 0) ? "<b>" : "") + _s + ((i == 0) ? "</b>" : "") + "</td>";
			}
			_html += "</tr>" + LINE_SEPARATOR + "";
		}
		_html += "</table>" + LINE_SEPARATOR + "";
		return _html;
	}

	/**
	 * affiche un rapport rapide du contenu du fichier csv sp�cifi�
	 */
	public static boolean ShowCsvFileWindow(final BuCommonInterface _a, final String _f, final String _title) {
		String _html = "";
		String _content;
		_html += "<html>" + LINE_SEPARATOR + "";
		_html += "<head><title>" + _title + "</title></head>" + LINE_SEPARATOR + "";
		_html += "<body>" + LINE_SEPARATOR + "";
		_html += "<h1>" + _title + "</h1>" + LINE_SEPARATOR + "";
		_content = getCsvFileInHtml(_f);
		if (_content == null) {
			return false;
		}
		_html += _content;
		_html += "</body>" + LINE_SEPARATOR + "";
		_html += "</html>" + LINE_SEPARATOR + "";
		final OscarImplementation _oscar = (OscarImplementation) _a.getImplementation();
		_oscar.showHtml(_title, _html);
		return true;
	}

	/**
	 * Affiche une boite de dialogue de message � l'�cran avec le texte _t et renvoie le code du bouton cliqu� par
	 * l'utilisateur (entier).
	 */
	public static int DialogMessage(final BuCommonInterface _a, final BuInformationsSoftware _is, final String _t) {
		final BuDialogMessage dm = new BuDialogMessage(_a, _is, _t);
		dm.setSize(500, 300);
		final int i = dm.activate();
		return i;
	}

	/**
	 * Affiche une boite de dialogue de message � l'�cran avec le texte _t et renvoie le code du bouton cliqu� par
	 * l'utilisateur (entier). Affiche en plus un bouton d'aide qui pointera vers le fichier d'aide <code>_h</code>.
	 */
	public static int DialogMessageAide(final BuCommonInterface _a, final BuInformationsSoftware _is, final String _t,
			final String _h) {
		final OscarDialogInformation d = new OscarDialogInformation(_a, _is, _t, _h);
		d.setSize(500, 300);
		final int i = d.activate();
		return i;
	}

	/**
	 * Affiche une boite de dialogue d'erreur � l'�cran avec le texte _t et renvoie le code du bouton cliqu� par
	 * l'utilisateur (entier).
	 */
	public static int DialogError(final BuCommonInterface _a, final BuInformationsSoftware _is, final String _t) {
		final BuDialogError de = new BuDialogError(_a, _is, _t);
		de.setSize(500, 300);
		final int i = de.activate();
		return i;
	}

	/**
	 * Affiche une boite de dialogue de confirmation � l'�cran avec le texte _t et renvoie le code du bouton cliqu� par
	 * l'utilisateur (entier).
	 */
	public static int DialogConfirmation(final BuCommonInterface _a, final BuInformationsSoftware _is, final String _t) {
		final BuDialogConfirmation dc = new BuDialogConfirmation(_a, _is, _t);
		dc.setSize(500, 300);
		final int i = dc.activate();
		return i;
	}

	/**
	 *
	 */
	public static double round(final double _v, final int _precision) {
		try {
			return (Math.round(_v * Math.pow(10D, _precision)) / Math.pow(10D, _precision));
		} catch (final Exception _e1) {
			return _v;
		}
	}

	/**
	 * remplace toutes les occurences de<code>'&lt;_pattern&gt;'</code> dans <code>_source</code> par
	 * <code>_to</code>. retourne <code>_source</code>, si aucune occurence n'a �t� trouv�e.
	 */
	public static String replaceKeyWordBy(final String _pattern, final String _to, final String _source) {
		int _i1;
		String _target;
		String _avant;
		String _apres;
		_target = _source;
		do {
			_i1 = _target.indexOf("<" + _pattern.toUpperCase() + ">");
			if (_i1 >= 0) {
				_avant = _source.substring(0, _i1);
				_apres = _source.substring(_i1 + 2 + _pattern.length());
				_target = _avant + ((_to != null) ? _to : "") + _apres;
			}
		} while (_i1 >= 0);
		return _target;
	}

	/**
	 * Affiche une boite de dialogue de confirmation � l'�cran avec le texte _t et renvoie le code du bouton cliqu� par
	 * l'utilisateur (entier).
	 */
	public static int DialogAdvancedConfirmation(final BuCommonInterface _a, final BuInformationsSoftware _is,
			final String _title, final String _t) {
		final OscarDialogConfirmation dc = new OscarDialogConfirmation(_a, _is, _title, _t);
		dc.setSize(500, 300);
		final int i = dc.activate();
		return i;
	}

	/**
	 * Affiche une boite de dialogue de confirmation � l'�cran avec le texte _t et renvoie le code du bouton cliqu� par
	 * l'utilisateur (entier).
	 */
	public static int DialogAdvancedConfirmationWithoutHelp(final BuCommonInterface _a, final BuInformationsSoftware _is,
			final String _title, final String _t) {
		final OscarDialogConfirmation dc = new OscarDialogConfirmation(_a, _is, _title, _t);
		dc.setHelpEnabled(false);
		dc.setSize(500, 300);
		final int i = dc.activate();
		return i;
	}

	/**
	 * renvoie un objet NumberFormat avec les propri�t�s par d�faut (voir les constantes NUMBER_FORMAT_*).
	 */
	public static NumberFormat NumberFormat() {
		final NumberFormat _nf = NumberFormat.getInstance(Locale.US);
		_nf.setMaximumFractionDigits(NUMBER_FORMAT_MAX_FRACTION_DIGITS);
		_nf.setGroupingUsed(false);
		return _nf;
	}

	/**
	 * renvoie un objet NumberFormat avec les propri�t�s par d�faut (voir les constantes NUMBER_FORMAT_*).
	 */
	public static NumberFormat NumberFormat(final int _digit) {
		final NumberFormat _nf = NumberFormat.getInstance(Locale.FRENCH);
		_nf.setMaximumFractionDigits(_digit);
		_nf.setGroupingUsed(false);
		return _nf;
	}

	/**
	 * affiche la calculatrice de coefficient Ka ou Kp � l'�cran (en modal) et renvoie la valeur calculer par la
	 * calculette (Double). renvoie null si l'utilisateur a cliquer sur Annuler.
	 * 
	 * @param _sol r�f�rence vers l'onglet sol
	 * @param _mode mode de calcul pour la calculatrice. Voir OscarLib.CALCULATRICEK_... pour les diff�rents modes
	 */
	public static Double CalculatriceK(final OscarSolParametres _sol, final int _mode) {
		final Double _k = (new OscarDialogCalculatriceK(_sol.getFilleParametres().getApplication(), _mode)).activate();
		return OscarLib.round(_k, 3);
	}

	/**
	 * affiche la calculatrice de section de tirant � l'�cran (en modal) et renvoie la valeur calcul�e par la calculette
	 * (Double). renvoie null si l'utilisateur a cliqu� sur Annuler.
	 * 
	 * @param _ouvrage r�f�rence vers l'onglet ouvrage
	 */
	public static Double CalculatriceSection(final OscarOuvrageParametres _ouvrage) {
		final Double _section = (new OscarDialogCalculatriceSection(_ouvrage.getFilleParametres().getApplication()))
		.activate();
		return _section;
	}

	/**
	 *
	 */
	public static BuPanel EmptyPanel() {
		final BuPanel _ep = new BuPanel();
		final BuVerticalLayout lo1 = VerticalLayout();
		_ep.setLayout(lo1);
		_ep.setBorder(EmptyBorder());
		return _ep;
	}

	/**
	 *
	 */
	public static BuPanel InfoAidePanel(final String _texte, final ActionListener _p) {
		// Bloc info-aide
		final BuPanel pn1 = new BuPanel();
		final BorderLayout lo1 = new BorderLayout();
		pn1.setLayout(lo1);
		pn1.setBorder(new EmptyBorder(0, 0, 0, 0));
		// // Bloc 'Informations'
		final BuPanel pn2 = new BuPanel();
		final BuVerticalLayout lo2 = VerticalLayout();
		pn2.setLayout(lo2);
		pn2.setBorder(OscarLib.TitledBorder("Informations"));
		int n2 = 0;
		final BuLabelMultiLine lb_info = LabelMultiLine(_texte);
		pn2.add(lb_info, n2++);
		// // Bloc Boutons
		final BuPanel pn3 = new BuPanel();
		// BoxLayout lo3=new BoxLayout(pn3,BoxLayout.X_AXIS);
		final BorderLayout lo3 = new BorderLayout();
		pn3.setLayout(lo3);
		// pn3.setBorder(TitledBorder("Aide"));
		pn3.setBorder(new EmptyBorder(8, 0, 1, 1));
		final BuButton bt_aide = new BuButton(BuResource.BU.loadCommandIcon("AIDE"));
		pn3.add(bt_aide);
		bt_aide.addActionListener(_p);
		bt_aide.setActionCommand("AIDE");
		pn1.add(pn2, BorderLayout.CENTER);
		pn1.add(pn3, BorderLayout.EAST);
		return pn1;
	}

	/**
	 *
	 */
	public static BuPanel InfoAideAndButtonPanel(final String _texte, final String _secondbuttonTitle,
			final String _secondbuttonActionCommand, final JComponent _p) {
		// Bloc info-aide
		final BuPanel pn1 = new BuPanel();
		final BorderLayout lo1 = new BorderLayout();
		pn1.setLayout(lo1);
		pn1.setBorder(new EmptyBorder(0, 0, 0, 0));
		// // Bloc 'Informations'
		final BuPanel pn2 = new BuPanel();
		final BuVerticalLayout lo2 = VerticalLayout();
		pn2.setLayout(lo2);
		pn2.setBorder(OscarLib.TitledBorder("Informations"));
		int n2 = 0;
		final BuLabelMultiLine lb_info = LabelMultiLine(_texte);
		pn2.add(lb_info, n2++);
		// // Bloc Boutons
		final BuPanel pn3 = new BuPanel();
		final GridLayout lo3 = new GridLayout(1, 2);
		pn3.setLayout(lo3);
		// pn3.setBorder(TitledBorder("Aide"));
		pn3.setBorder(new EmptyBorder(8, 0, 1, 1));
		final BuButton bt_second = new BuButton(_secondbuttonTitle);
		pn3.add(bt_second);
		bt_second.addActionListener((ActionListener) _p);
		bt_second.setActionCommand(_secondbuttonActionCommand);
		final BuButton bt_aide = Button("", "AIDE", "AIDE");
		// new BuButton(new BuIcon(OscarResource.OSCAR.getImage("oscar-help")));
		pn3.add(bt_aide);
		bt_aide.addActionListener((ActionListener) _p);
		// bt_aide.setActionCommand("AIDE");
		pn1.add(pn2, BorderLayout.CENTER);
		pn1.add(pn3, BorderLayout.EAST);
		return pn1;
	}

	/**
	 *
	 */
	public static BuPanel InfoAideAndButtonPanel(final String _texte, final String _secondbuttonTitle,
			final String _secondbuttonActionCommand, final JComponent _p, final JComponent _pc, final String _propertyChange) {
		// Bloc info-aide
		final BuPanel pn1 = new BuPanel();
		final BorderLayout lo1 = new BorderLayout();
		pn1.setLayout(lo1);
		pn1.setBorder(new EmptyBorder(0, 0, 0, 0));
		// // Bloc 'Informations'
		final BuPanel pn2 = new BuPanel();
		final BuVerticalLayout lo2 = VerticalLayout();
		pn2.setLayout(lo2);
		pn2.setBorder(OscarLib.TitledBorder("Informations"));
		int n2 = 0;
		final BuLabelMultiLine lb_info = LabelMultiLine(_texte);
		pn2.add(lb_info, n2++);
		// // Bloc Boutons
		final BuPanel pn3 = new BuPanel();
		final GridLayout lo3 = new GridLayout(1, 2);
		pn3.setLayout(lo3);
		// pn3.setBorder(TitledBorder("Aide"));
		pn3.setBorder(new EmptyBorder(8, 0, 1, 1));
		final BuButton bt_second = new BuButton(_secondbuttonTitle);
		pn3.add(bt_second);
		bt_second.addActionListener((ActionListener) _p);
		bt_second.setActionCommand(_secondbuttonActionCommand);
		bt_second.setEnabled(false);
		_pc.addPropertyChangeListener(_propertyChange, new PropertyChangeListener() {
			public void propertyChange(final PropertyChangeEvent evt) {
				if (((Boolean) evt.getNewValue()).booleanValue()) {
					bt_second.setEnabled(true);
				} else {
					bt_second.setEnabled(false);
				}
			}
		});
		final BuButton bt_aide = Button("", "AIDE", "AIDE");
		// new BuButton(new BuIcon(OscarResource.OSCAR.getImage("oscar-help")));
		pn3.add(bt_aide);
		bt_aide.addActionListener((ActionListener) _p);
		// bt_aide.setActionCommand("AIDE");
		pn1.add(pn2, BorderLayout.CENTER);
		pn1.add(pn3, BorderLayout.EAST);
		return pn1;
	}

	/**
	 *
	 */
	public static BuPanel InfoAideAndTwoButtonPanel(final String _texte, final String _secondbuttonTitle,
			final String _secondbuttonActionCommand, final String _thirdbuttonTitle, final String _thirdbuttonActionCommand,
			final JComponent _p) {
		// Bloc info-aide
		final BuPanel pn1 = new BuPanel();
		final BorderLayout lo1 = new BorderLayout();
		pn1.setLayout(lo1);
		pn1.setBorder(new EmptyBorder(0, 0, 0, 0));
		// // Bloc 'Informations'
		final BuPanel pn2 = new BuPanel();
		final BuVerticalLayout lo2 = VerticalLayout();
		pn2.setLayout(lo2);
		pn2.setBorder(OscarLib.TitledBorder("Informations"));
		int n2 = 0;
		final BuLabelMultiLine lb_info = LabelMultiLine(_texte);
		pn2.add(lb_info, n2++);
		// // Bloc Boutons
		final BuPanel pn3 = new BuPanel();
		final GridLayout lo3 = new GridLayout(1, 2);
		pn3.setLayout(lo3);
		pn3.setBorder(TitledBorder("Aide"));
		final BuPanel pn4 = new BuPanel();
		final GridLayout lo4 = new GridLayout(2, 1);
		pn4.setLayout(lo4);
		final BuButton bt_second = new BuButton(_secondbuttonTitle);
		pn4.add(bt_second);
		bt_second.addActionListener((ActionListener) _p);
		bt_second.setActionCommand(_secondbuttonActionCommand);
		final BuButton bt_third = new BuButton(_thirdbuttonTitle);
		pn4.add(bt_third);
		bt_third.addActionListener((ActionListener) _p);
		bt_third.setActionCommand(_thirdbuttonActionCommand);
		pn3.add(pn4);
		final BuButton bt_aide = new BuButton(new BuIcon(OscarResource.OSCAR.getImage("oscar-help")));
		pn3.add(bt_aide);
		bt_aide.addActionListener((ActionListener) _p);
		bt_aide.setActionCommand("AIDE");
		pn1.add(pn2, BorderLayout.CENTER);
		pn1.add(pn3, BorderLayout.EAST);
		return pn1;
	}

	/**
	 * ajoute une ligne dans le tableau.
	 */
	public static void ajouterLigne(final DefaultTableModel _dm, final BuTable _tb, final String _c1, final String _c2) {
		_dm.addRow(EmptyRow(_dm.getColumnCount()));
		_tb.setValueAt((_c1), _dm.getRowCount() - 1, 0);
		_tb.setValueAt((_c2), _dm.getRowCount() - 1, 1);
	}

	/**
	 * retourne un objet IDL instance de la classe de type sp�cifi�e.
	 */
	public static Object createIDLObject(final Class _class) {
		Object o = null;
		if (_class.equals(SParametresOscar.class)) {
			final SParametresOscar _idl = new SParametresOscar();
			o = _idl;
		} else if (_class.equals(SCommentairesOscar.class)) {
			final SCommentairesOscar _idl = new SCommentairesOscar();
			_idl.titre = "tapez votre commentaire ici.";
			o = _idl;
		} else if (_class.equals(SParametresGeneraux.class)) {
			final SParametresGeneraux _idl = new SParametresGeneraux();
			_idl.coteTerrePleinPoussee = VALEUR_NULLE.value;
			_idl.coteTerrePleinButee = VALEUR_NULLE.value;
			o = _idl;
		} else if (_class.equals(SSol.class)) {
			final SSol _idl = new SSol();
			o = _idl;
		} else if (_class.equals(SCoucheSol.class)) {
			final SCoucheSol _idl = new SCoucheSol();
			_idl.epaisseur = VALEUR_NULLE.value;
			_idl.poidsVolumiqueHumide = VALEUR_NULLE.value;
			_idl.poidsVolumique = VALEUR_NULLE.value;
			_idl.coefficient = VALEUR_NULLE.value;
			_idl.cohesion = VALEUR_NULLE.value;
			_idl.angleFrottement = VALEUR_NULLE.value;
			o = _idl;
		} else if (_class.equals(SEau.class)) {
			final SEau _idl = new SEau();
			_idl.typeCalculEau = IDL_EAU_CHOIXCALCUL_STATIC;
			_idl.presenceNappeEauCotePoussee = false;
			_idl.presenceNappeEauCoteButee = false;
			_idl.coteNappeEauCotePoussee = VALEUR_NULLE.value;
			_idl.coteNappeEauCoteButee = VALEUR_NULLE.value;
			o = _idl;
		} else if (_class.equals(SSurcharge.class)) {
			final SSurcharge _idl = new SSurcharge();
			_idl.valeurSurchargeLineique = VALEUR_NULLE.value;
			_idl.valeurSurchargeUniformeEnBande = VALEUR_NULLE.value;
			_idl.valeurSurchargeUniformeSemiInfinie = VALEUR_NULLE.value;
			_idl.typeSurcharge = "";
			o = _idl;
		} else if (_class.equals(SEffort.class)) {
			final SEffort _idl = new SEffort();
			o = _idl;
		} else if (_class.equals(SContrainte.class)) {
			final SContrainte _idl = new SContrainte();
			_idl.cote = VALEUR_NULLE.value;
			_idl.valeur = VALEUR_NULLE.value;
			o = _idl;
		} else if (_class.equals(SEffortEnTeteDeRideau.class)) {
			final SEffortEnTeteDeRideau _idl = new SEffortEnTeteDeRideau();
			_idl.valeurForceEnTete = 0.0;
			_idl.valeurMomentEnTete = 0.0;
			o = _idl;
		} else if (_class.equals(SFichierDiagramme.class)) {
			final SFichierDiagramme _idl = new SFichierDiagramme();
			_idl.fichier = "";
			// _idl.pointsCotePression = new SPointCoteValeur[];
			o = _idl;
		} else if (_class.equals(SOuvrage.class)) {
			final SOuvrage _idl = new SOuvrage();
			_idl.presenceNappeTirants = IDL_OUVRAGE_CHOIXTIRANTS_NON;
			_idl.coteNappeTirants = VALEUR_NULLE.value;
			_idl.pourcentageEncastrementNappe = OscarMsg.INITIALVALUE009;
			_idl.espaceEntreDeuxTirants = VALEUR_NULLE.value;
			_idl.sectionTirants = VALEUR_NULLE.value;
			_idl.limiteElastiqueAcierTirants = VALEUR_NULLE.value;
			_idl.moduleYoungAcier = OscarMsg.INITIALVALUE001;
			_idl.limiteElastiqueAcier = VALEUR_NULLE.value;
			_idl.inertiePalplanches = VALEUR_NULLE.value;
			_idl.demieHauteur = VALEUR_NULLE.value;
			o = _idl;
		} else if (_class.equals(SResultatsPressions.class)) {
			final SResultatsPressions _idl = new SResultatsPressions();
			o = _idl;
		} else if (_class.equals(SResultatsMomentsFlechissants.class)) {
			final SResultatsMomentsFlechissants _idl = new SResultatsMomentsFlechissants();
			o = _idl;
		} else if (_class.equals(SResultatsEffortsTranchants.class)) {
			final SResultatsEffortsTranchants _idl = new SResultatsEffortsTranchants();
			o = _idl;
		} else if (_class.equals(SResultatsDeformees.class)) {
			final SResultatsDeformees _idl = new SResultatsDeformees();
			o = _idl;
		} else if (_class.equals(SResultatsPredimensionnement.class)) {
			final SResultatsPredimensionnement _idl = new SResultatsPredimensionnement();
			o = _idl;
		} else {}
		return o;
	}
	
	/**
	 * Methode qui formatte le nombre en specifiant le nombre de chiffres apres la virgule
	 * @param nombre
	 */
public static String formatterNombre(double nombre)	
{
	String val=Double.toString(nombre);
	if(val.indexOf(".")==-1)
		return val;//-- cas 1: pas de chiffres apres la virgule
	
	if(val.length()-val.indexOf(".")<=3)
		return val;//-- cas 2: au plus 2 chiffres arp�s la virgule: on renvoie tel quel
	
	
	//--cas 3: plus de 2 chiffres apres la virgule: on formatte
	String formatC="0.000";
	 
	String valeur = new DecimalFormat(formatC).format(nombre);
	return valeur.replace(",", ".");
	//return Double.toString(Math.round(nombre*1000)/1000.);
}

/**
 * Methode qui formatte la valeur selon le nombre de chiffres significatifs souhait�s
 * @param nombre
 */
public static String formatterNombre(String type, double nombre)	
{
	String formatC;
	if(type == "deformee") formatC="0.000";
	else if (type == "pression") formatC="0.0";
	else if (type == "effort") formatC="0.0";
	else if (type == "moment") formatC="0.0";
	else formatC="0.000";
	String valeur =  new DecimalFormat(formatC).format(nombre);
	return valeur.replace(",", ".");
}

}
