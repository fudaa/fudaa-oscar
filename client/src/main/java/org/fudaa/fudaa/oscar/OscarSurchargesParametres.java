/*
 * @file         OscarSurchargesParametres.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;

import org.fudaa.dodico.corba.oscar.SSurcharge;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;


/**
 * Description de l'onglet des parametres de surcharges.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarSurchargesParametres extends OscarAbstractOnglet {
  /**
   *
   */
  OscarSurchargesParametresSurchargeLineique sl_;
  /**
   *
   */
  OscarSurchargesParametresSurchargeUniformeEnBande sub_;
  /**
   *
   */
  OscarSurchargesParametresSurchargeUniformeSemiInfinie susi_;
  /**
   * Set d'onglet de l'onglet Surcharges
   */
  JTabbedPane tpMain;
  /**
   *
   */
  JPanel pn1;

  /**
   *
   */
  public OscarSurchargesParametres(final OscarFilleParametres _fp, final String _helpfile) {
    super(_fp.getApplication(), _fp, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    // Bloc Info-Aide
    tpMain = new JTabbedPane();
    pn1 = OscarLib.InfoAidePanel(OscarMsg.LAB004, this);
    // Set d'onglets 'Type de Sol'
    sl_ = new OscarSurchargesParametresSurchargeLineique(this, OscarMsg.URL019);
    sub_ = new OscarSurchargesParametresSurchargeUniformeEnBande(this, OscarMsg.URL020);
    susi_ = new OscarSurchargesParametresSurchargeUniformeSemiInfinie(this, OscarMsg.URL021);
    tpMain.addTab(OscarMsg.TABLAB015, null, susi_, OscarMsg.LAB047);
    tpMain.addTab(OscarMsg.TABLAB016, null, sub_, OscarMsg.LAB048);
    tpMain.addTab(OscarMsg.TABLAB017, null, sl_, OscarMsg.LAB049);
    tpMain.setEnabledAt(0, true);
    tpMain.setEnabledAt(1, false);
    tpMain.setEnabledAt(2, false);
    setCurrentTab(0);
    tpMain.addChangeListener(this);
    // --
    _p.add(pn1);
    _p.add(tpMain);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {}

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT005);
    _mv.addMessages(validation().getMessages());
    ((OscarAbstractOnglet) tpMain.getComponentAt(getCurrentTab())).stateChanged(_evt);
    setCurrentTab(tpMain.getSelectedIndex());
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans l'onglet surcharges
   */
  public SSurcharge getParametresSurcharges() {
    final SSurcharge _p = (SSurcharge) OscarLib.createIDLObject(SSurcharge.class);
    String _flag = "";
    final Double _sl = sl_.getValeurSurcharge();
    final Double _sub = sub_.getValeurSurcharge();
    final Double _susi = susi_.getValeurSurcharge();
    try {
      _p.valeurSurchargeLineique = _sl.doubleValue();
    } catch (final NullPointerException _e1) {
      _p.valeurSurchargeLineique = VALEUR_NULLE.value;
    }
    try {
      _p.valeurSurchargeUniformeEnBande = _sub.doubleValue();
    } catch (final NullPointerException _e1) {
      _p.valeurSurchargeUniformeEnBande = VALEUR_NULLE.value;
    }
    try {
      _p.valeurSurchargeUniformeSemiInfinie = _susi.doubleValue();
    } catch (final NullPointerException _e1) {
      _p.valeurSurchargeUniformeSemiInfinie = VALEUR_NULLE.value;
    }
    if ((_sl != null) && (_sl.doubleValue() != 0.0)) {
      _flag += OscarLib.IDL_SURCHARGE_TYPE_LINEIQUE;
    }
    if ((_sub != null) && (_sub.doubleValue() != 0.0)) {
      _flag += OscarLib.IDL_SURCHARGE_TYPE_UNIFORME_B;
    }
    if ((_susi != null) && (_susi.doubleValue() != 0.0)) {
      _flag += OscarLib.IDL_SURCHARGE_TYPE_UNIFORME_SI;
    }
    _p.typeSurcharge = _flag;
    return _p;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans les champs de l'onglet surcharges
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) � importer
   */
  public void setParametresSurcharges(SSurcharge _p) {
    if (_p == null) {
      _p = (SSurcharge) OscarLib.createIDLObject(SSurcharge.class);
    }
    if (_p.valeurSurchargeLineique != VALEUR_NULLE.value) {
      sl_.setValeurSurcharge(_p.valeurSurchargeLineique);
    }
    if (_p.valeurSurchargeUniformeEnBande != VALEUR_NULLE.value) {
      sub_.setValeurSurcharge(_p.valeurSurchargeUniformeEnBande);
    }
    if (_p.valeurSurchargeUniformeSemiInfinie != VALEUR_NULLE.value) {
      susi_.setValeurSurcharge(_p.valeurSurchargeUniformeSemiInfinie);
    }
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    _vm.add(sl_.validation());
    _vm.add(sub_.validation());
    _vm.add(susi_.validation());
    return _vm;
  }
}
