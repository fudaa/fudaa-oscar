/*
 * @file         OscarAstuces.java
 * @creation     2002-10-07
 * @modification $Date: 2007-01-19 13:14:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import com.memoire.bu.BuPreferences;

import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;

/**
 * Classe pour les astuces du logiciel Fudaa-Oscar (Oscar).
 * 
 * @version $Revision: 1.9 $ $Date: 2007-01-19 13:14:29 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarAstuces extends FudaaAstucesAbstract {
  /**
   * renvoie un nouvel objet OscarAstuces.
   */
  public static OscarAstuces OSCAR = new OscarAstuces();

  /**
   * renvoie un nouvel objet FudaaAstuces (parent de OscarAstuces).
   * 
   * @return une instance d'objet <code>FudaaAstuces</code>
   */
  protected FudaaAstucesAbstract getParent() {
    final FudaaAstuces _fa = FudaaAstuces.FUDAA;
    return _fa;
  }

  /**
   * renvoie un nouvel objet OscarPreferences.
   * 
   * @return une instance d'objet <code>OscarPreferences</code>
   */
  protected BuPreferences getPrefs() {
    final BuPreferences bp = OscarPreferences.OSCAR;
    return bp;
  }
}
