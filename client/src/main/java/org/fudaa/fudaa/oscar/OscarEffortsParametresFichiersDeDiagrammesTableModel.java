/*
 * @file         OscarEffortsParametresFichiersDeDiagrammesTableModel.java
 * @creation     2000-11-08
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.table.DefaultTableModel;


/**
 * Mod�le de donn�es pour le tableau des fichiers de diagrammes
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarEffortsParametresFichiersDeDiagrammesTableModel extends DefaultTableModel {
  /**
   *
   */
  public OscarEffortsParametresFichiersDeDiagrammesTableModel() {
    this(new Object[] { "Fichier" });
  }

  /**
   * cr�e un mod�le de donn�es utilis� pour les tableaux de fichiers de diagrammes suppl�mentaires.
   * 
   * @param columnNames tableau d'objet contenant les noms des colonnes � cr�er
   */
  public OscarEffortsParametresFichiersDeDiagrammesTableModel(final Object[] columnNames) {
    super(columnNames, 0);
  }

  /**
   * d�finit si la cellule situ�e � l'emplacement sp�cifi�e est �ditable par l'utilisateur ou non.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public boolean isCellEditable(final int row, final int column) {
    return true;
  }

  /**
   * retourne l'objet valeur de la cellule sp�cifi�e.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public Object getValueAt(final int row, final int column) {
    return super.getValueAt(row, column);
  }

  /**
   * modifie la valeur de la cellule sp�cifi�e.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public void setValueAt(final Object _o, final int row, final int column) {
    super.setValueAt(_o, row, column);
  }
}
