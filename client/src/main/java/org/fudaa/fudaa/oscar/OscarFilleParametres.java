/*
 * @file         OscarFilleParametres.java
 * @creation     1998-10-05
 * @modification $Date: 2007-01-19 11:13:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuTabbedPane;

import org.fudaa.dodico.corba.oscar.SCommentairesOscar;
import org.fudaa.dodico.corba.oscar.SParametresOscar;

import org.fudaa.fudaa.commun.projet.FudaaProjet;




/**
 * affiche une fen�tre de saisie des param�tres (donn�es).
 * 
 * @version $Revision: 1.10 $ $Date: 2007-01-19 11:13:21 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public class OscarFilleParametres extends OscarInternalFrameAdapter {
  /** onglet 'Sol' */
  protected OscarSolParametres sol_ = null;
  /** onglet 'Eau' */
  protected OscarEauParametres eau_ = null;
  /** onglet 'Surcharges' */
  protected OscarSurchargesParametres surcharges_ = null;
  /** onglet 'Autres efforts' */
  protected OscarEffortsParametres efforts_ = null;
  /** onglet 'Ouvrage' */
  protected OscarOuvrageParametres ouvrage_ = null;
  /** onglet 'Validation' */
  protected OscarValidationParametres validation_ = null;
  /** onglet 'Incertitudes'. */
  protected OscarIncertitudesParametres incertitudes_ = null;
  /** set d'onglets principaux de la fen�tre */
  protected BuTabbedPane tpm_ = null;

  /**
   * constructeur de la fenetre. construit une fen�tre de saisie des donn�es avec plusieurs onglets et rattach�e � un
   * projet ouvert.
   */
  public OscarFilleParametres(final BuCommonInterface _appli, final FudaaProjet _project) {
    super(OscarMsg.TITLELAB014, OscarMsg.ICO002, OscarMsg.URL030, _appli, _project);
    sol_ = new OscarSolParametres(this, OscarMsg.URL002);
    eau_ = new OscarEauParametres(this, OscarMsg.URL006);
    surcharges_ = new OscarSurchargesParametres(this, OscarMsg.URL007);
    efforts_ = new OscarEffortsParametres(this, OscarMsg.URL008);
    ouvrage_ = new OscarOuvrageParametres(this, OscarMsg.URL005);
    validation_ = new OscarValidationParametres(this, OscarMsg.URL009);
    incertitudes_ = new OscarIncertitudesParametres(this, OscarMsg.URL010);
    tpm_ = new BuTabbedPane();
    tpm_.addTab(OscarMsg.TABLAB008, null, sol_, OscarMsg.TIPTEXT006);
    tpm_.addTab(OscarMsg.TABLAB009, null, eau_, OscarMsg.TIPTEXT007);
    tpm_.addTab(OscarMsg.TABLAB010, null, surcharges_, OscarMsg.TIPTEXT008);
    tpm_.addTab(OscarMsg.TABLAB011, null, efforts_, OscarMsg.TIPTEXT009);
    tpm_.addTab(OscarMsg.TABLAB012, null, ouvrage_, OscarMsg.TIPTEXT010);
    tpm_.addTab(OscarMsg.TABLAB013, null, validation_, OscarMsg.TIPTEXT011);
    tpm_.addTab(OscarMsg.TABLAB014, null, incertitudes_, OscarMsg.TIPTEXT012);
    tpm_.addChangeListener(this);
    tpm_.setEnabledAt(0, true); // onglet "Sol" actif
    tpm_.setEnabledAt(1, false); // onglet "Eau" inactif
    tpm_.setEnabledAt(2, false); // onglet "Surcharges" inactif
    tpm_.setEnabledAt(3, false); // onglet "Autres Efforts" inactif
    tpm_.setEnabledAt(4, false); // onglet "Ouvrage" inactif
    tpm_.setEnabledAt(5, true); // onglet "Validation" actif
    tpm_.setEnabledAt(6, true); // onglet "Incertitudes" inactif
    setCurrentTab(0); // sauvegarde de l'onglet courant
    sol_.addPropertyChangeListener("COUCHES_SOL_DISPONIBLE", this);
    final JScrollPane _sp1 = new JScrollPane(tpm_);
    getContent().add(_sp1);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    updatePanels();
    pack();
  }

  /**
   *
   */
  public OscarAbstractOnglet getOnglet(final int _onglet) {
    OscarAbstractOnglet _o = null;
    switch (_onglet) {
    case OscarLib.ONGLET_SOL: {
      _o = sol_;
      break;
    }
    case OscarLib.ONGLET_EAU: {
      _o = eau_;
      break;
    }
    case OscarLib.ONGLET_SURCHARGES: {
      _o = surcharges_;
      break;
    }
    case OscarLib.ONGLET_EFFORTS: {
      _o = efforts_;
      break;
    }
    case OscarLib.ONGLET_OUVRAGE: {
      _o = ouvrage_;
      break;
    }
    case OscarLib.ONGLET_VALIDATION: {
      _o = validation_;
      break;
    }
    case OscarLib.ONGLET_INCERTITUDES: {
      _o = incertitudes_;
      break;
    }
    }
    return _o;
  }

  /**
   * renvoi une structure contenant les param�tres actuellement affich�s dans les onglets de la fen�tre.
   */
  public SParametresOscar getParametres() {
    final SParametresOscar _p = (SParametresOscar) OscarLib.createIDLObject(SParametresOscar.class);
    _p.sol = sol_.getParametresSol();
    _p.eau = eau_.getParametresEau();
    _p.surcharge = surcharges_.getParametresSurcharges();
    _p.effort = efforts_.getParametresEfforts();
    _p.ouvrage = ouvrage_.getParametresOuvrage();
    _p.parametresGeneraux = sol_.getParametresGeneraux();
    _p.commentairesOscar = (SCommentairesOscar) OscarLib.createIDLObject(SCommentairesOscar.class);
    return _p;
  }

  /**
   * mise � jour du contenu des onglets de la fen�tre.
   */
  private synchronized void setParametres(SParametresOscar _p) {
    if (_p == null) {
      _p = (SParametresOscar) OscarLib.createIDLObject(SParametresOscar.class);
    }
    sol_.setParametresGeneraux(_p.parametresGeneraux);
    sol_.setParametresSol(_p.sol);
    eau_.setParametresEau(_p.eau);
    surcharges_.setParametresSurcharges(_p.surcharge);
    efforts_.setParametresEfforts(_p.effort);
    ouvrage_.setParametresOuvrage(_p.ouvrage);
  }

  /**
   * mise � jour du contenu des onglets de la fen�tre. cette fonction est appell�e automatiquement lorsqu'une
   * modification de l'�tat ou du contenu du projet est notifi�e.
   */
  protected void updatePanels() {
    setParametres((SParametresOscar) getProjet().getParam(OscarResource.PARAMETRES));
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    // OscarParamEventView _mv = ((OscarImplementation)getApplication().getImplementation()).getParamEventView();
    ((OscarAbstractOnglet) tpm_.getComponentAt(getCurrentTab())).stateChanged(_evt);
    setCurrentTab(tpm_.getSelectedIndex());
  }

  /**
   * m�thode appell�e lors d'un changement de propri�t� espionn�e par ce composant.
   */
  public void propertyChange(final PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("COUCHES_SOL_DISPONIBLE") && ((Boolean) evt.getNewValue()).booleanValue()) {
      tpm_.setEnabledAt(0, true); // onglet "Sol"
      tpm_.setEnabledAt(1, true); // onglet "Eau"
      tpm_.setEnabledAt(2, true); // onglet "Surcharges"
      tpm_.setEnabledAt(3, true); // onglet "Autres Efforts"
      tpm_.setEnabledAt(4, true); // onglet "Ouvrage"
      tpm_.setEnabledAt(5, true); // onglet "Validation"
     /* if (!("FALSE".equals(System.getProperty("INCERTITUDES")))) {
        tpm_.setEnabledAt(6, true); // onglet "Incertitudes"
      }*/
      tpm_.setEnabledAt(6, true); // onglet "Incertitudes"
    }
  }

  /**
   * 
   */
  public void loadValidationMsg() {
    final OscarParamEventView _mv = ((OscarImplementation) getApplication().getImplementation()).getParamEventView();
    _mv.removeAllMessages();
    final ValidationMessages _vm = new ValidationMessages();
    _vm.add(sol_.validation());
    _vm.add(eau_.validation());
    _vm.add(surcharges_.validation());
    _vm.add(efforts_.validation());
    _vm.add(ouvrage_.validation());
    _vm.add(incertitudes_.validation());
    _mv.addMessages(_vm.getMessages());
  }

  /**
   * lance la validation des donn�es saisies par l'utilisateur et affiche les messages d'erreurs �ventuels.
   */
  public void valider() {
    final ValidationMessages _vm = new ValidationMessages();
    _vm.add(sol_.validation());
    _vm.add(eau_.validation());
    _vm.add(surcharges_.validation());
    _vm.add(efforts_.validation());
    _vm.add(ouvrage_.validation());
    _vm.add(incertitudes_.validation());
    if (_vm.hasFatalError()) {
      /**
       * afficher une boite de dialogue pour dire que les donn�es contiennent des erreurs fatales et que les calculs ne
       * peuvent �tre lanc�s
       */
      OscarLib.DialogError(getApplication(), getApplication().getInformationsSoftware(), OscarMsg.DLG013);
      OscarLib.ShowValidationMessagesWindow(getApplication(), _vm);
      firePropertyChange("CALCUL_DISPONIBLE", true, false);
      firePropertyChange("RESULTATS_DISPONIBLE", true, false);
    } else if (_vm.hasWarning()) {
      /**
       * afficher une boite de dialogue pour dire que les donn�es contiennent des erreurs mais que le calculs peut �tre
       * lanc�
       */
      final int _i = OscarLib.DialogConfirmation(getApplication(), getApplication().getInformationsSoftware(),
          OscarMsg.DLG012);
      firePropertyChange("CALCUL_DISPONIBLE", false, true);
      firePropertyChange("RESULTATS_DISPONIBLE", true, false);
      if (_i == JOptionPane.YES_OPTION) {
        getProjet().addParam(OscarResource.PARAMETRES, getParametres());
        ((OscarImplementation) getApplication().getImplementation()).actionPerformed(new ActionEvent(this, 10,
            "LANCER_CALCULS"));
      } else {
        OscarLib.ShowValidationMessagesWindow(getApplication(), _vm);
      }
    } else {
      /**
       * afficher une boite de dialogue pour dire que les donn�es ne contiennent aucune erreur et que les calculs
       * peuvent �tre lanc�s
       */
      firePropertyChange("CALCUL_DISPONIBLE", false, true);
      firePropertyChange("RESULTATS_DISPONIBLE", true, false);
      OscarLib.DialogMessage(getApplication(), getApplication().getInformationsSoftware(), OscarMsg.DLG011);
      ((OscarImplementation) getApplication().getImplementation()).actionPerformed(new ActionEvent(this, 10,
          "LANCER_CALCULS"));
      getProjet().addParam(OscarResource.PARAMETRES, getParametres());
    }
  }
}
