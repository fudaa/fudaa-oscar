/*
 * @file         OscarEffortsParametresFichiersDeDiagrammesTableCellRenderer.java
 * @creation     2000-11-08
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.Component;

import javax.swing.JTable;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTableCellRenderer;


/**
 * Gestionnaire de rendu de cellule pour le tableau des fichiers de diagrammes suppl�mentaires
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarEffortsParametresFichiersDeDiagrammesTableCellRenderer extends BuTableCellRenderer {
  /**
   * renvoi un objet "rendu" du contenu de la cellule sp�cifi� par rapport � la donn�e fournie.
   * 
   * @param table r�f�rence vers le tableau contenant les donn�es
   * @param value valeur de la cellule � afficher/traiter
   * @param isSelected sp�cifie si la cellule est actuellement selectionn�e
   * @param hasFocus sp�cifie si la cellule a actuellement le focus
   * @param row sp�cifi� le num�ro de ligne de la cellule
   * @param column sp�cifie le num�ro de colonne de la cellule
   */
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
      final boolean hasFocus, final int row, final int column) {
    Component _r = null;
    BuLabel _lb = null;
    _lb = (BuLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    if (value instanceof Double) {}
    _r = _lb;
    return _r;
  }
}
