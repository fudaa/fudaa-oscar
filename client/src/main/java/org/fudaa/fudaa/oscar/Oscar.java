/*
 * @file         Oscar.java
 * @creation     2002-10-07
 * @modification $Date: 2007-01-19 13:14:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import org.fudaa.fudaa.commun.impl.Fudaa;

/**
 * Classe principale du logiciel Fudaa-Oscar (Oscar).
 * 
 * @version $Revision: 1.8 $ $Date: 2007-01-19 13:14:29 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class Oscar {
  /**
   * m�thode principale de la classe, app�l�e en premier au d�marrage de l'application.
   * 
   * @param args liste des arguments en ligne de commande
   */
  public static void main(final String[] args) {
    final Fudaa f = new Fudaa();
    f.launch(args, OscarImplementation.informationsSoftware(), false);
    f.startApp(new OscarImplementation());
  }
}
