/*
 * @file         OscarDialogCalculatriceK.java
 * @creation     1998-10-05
 * @modification $Date: 2007-05-04 13:59:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * bo�te de dialogue permettant de calculer le coefficient K (Ka ou Kp) et de retourner la valeur
 * 
 * @version $Revision: 1.8 $ $Date: 2007-05-04 13:59:31 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarDialogCalculatriceK extends JDialog implements ActionListener, KeyListener {
  /**
   * r�f�rence vers l'application principal
   */
  private BuCommonInterface app_;
  /**
   * flag � utiliser pour le calcul en contraintes totales pour le sol en pouss�e
   */
  public final static int MODE_POUSSEE_TOTAL = 1;
  /**
   * flag � utiliser pour le calcul en contraintes effectives pour le sol en pouss�e
   */
  public final static int MODE_POUSSEE_EFFECTIF = 2;
  /**
   * flag � utiliser pour le calcul en contraintes totales pour le sol en but�e
   */
  public final static int MODE_BUTEE_TOTAL = 3;
  /**
   * flag � utiliser pour le calcul en contraintes effectives pour le sol en but�e
   */
  public final static int MODE_BUTEE_EFFECTIF = 4;
  /**
   * label pour les modes en pouss�e
   */
  private final static String MODE_POUSSEE = "pouss�e";
  /**
   * coefficient pour les modes en pouss�e
   */
  private final static String COEF_POUSSEE = "Ka";
  /**
   * label pour les modes en but�e
   */
  private final static String MODE_BUTEE = "but�e";
  /**
   * coefficient pour les modes en but�e
   */
  private final static String COEF_BUTEE = "Kp";
  /**
   * label pour les modes en contraintes totales
   */
  /*
   * private final static String CT_TOT= "totales";
   */
  /*  *//**
         * label pour les modes en contraintes effectives
         */
  /*
   * private final static String CT_EFF= "effectives";
   */
  /**
   * label pour les termes drain�s (contraintes effectives)
   */
  private final static String DRAINE = "dra�n�";
  /**
   * label pour les termes non drain�s (contraintes totales)
   */
  private final static String NON_DRAINE = "non dra�n�";
  /*  *//**
         * indice pour les termes drain�s (contraintes effectives)
         */
  /*
   * private final static String INDICE_DRAINE= "'";
   */
  /**
   * indice pour les termes non drain�s (contraintes totales)
   */
  /*
   * private final static String INDICE_NON_DRAINE= "u";
   */
  /**
   * flag du mode de calculatrice. Peut prendre les valeurs : MODE_POUSSEE_TOTAL, MODE_POUSSEE_EFFECTIF,
   * MODE_BUTEE_TOTAL, MODE_BUTEE_EFFECTIF
   */
  private int mode_;
  /**
   * titre de la bo�te de dialogue
   */
  private String title_ = null;
  /**
   * mode pouss�e/but�e choisi. Peut prendre les valeurs : MODE_POUSSEE, MODE_BUTEE
   */
  private String pbMode_ = null;
  /**
   * coefficient pouss�e/but�e correspondant au mode choisi. Peut prendre les valeurs : COEF_POUSSEE, COEF_BUTEE
   */
  private String pbCoef_ = null;
  /**
   * type dra�n�/non-dra�n� correspondant au mode choisi. Peut prendre les valeurs : DRAINE, NON_DRAINE
   */
  private String dndMode_ = null;
  /**
   * indice dra�n�/non-dra�n� � rajouter devant certains symboles. Peut prendre les valeurs : INDICE_DRAINE,
   * INDICE_NON_DRAINE
   */
  /*
   * private String dndIndice_= null;
   */
  /*  *//**
         * type de calcul contraintes effectives/totales correspondant au mode choisi. Peut prendre les valeurs :
         * CT_TOT, CT_EFF
         */
  /*
   * private String ctMode_= null;
   */
  /**
   * champs de saisie de l'angle de frottement
   */
  private BuTextField tf_angfrott_ = null;
  /**
   * champs de saisie de l'obliquit� de la contrainte
   */
  private BuTextField tf_oblcontr_ = null;
  /**
   * champs de saisie de l'inclinaison du terre-plein
   */
  private BuTextField tf_incterpl_ = null;
  /**
   * champs de saisie de l'inclinaison du parement
   */
  private BuTextField tf_incparem_ = null;
  /**
   * champs de visualisation du coefficient K (Ka ou Kp)
   */
  private BuTextField tf_coeffk_ = null;
  /**
   * bouton d'aide
   */
  private BuButton bt_aide_ = null;
  /**
   * bouton de validation. Permet de r�cup�rer la valeur calcul�e
   */
  private BuButton bt_valider_ = null;
  /**
   * bouton d'annulation
   */
  private BuButton bt_annuler_ = null;
  /**
   * conteneur swing principal de la bo�te de dialogue
   */
  private JComponent content_ = null;
  /**
   * bloc 1 : information
   */
  private BuPanel b1_ = null;
  /**
   * bloc 2 : propri�t�s
   */
  private BuPanel b2_ = null;
  /**
   * bloc 3 : coefficient K
   */
  private BuPanel b3_ = null;
  /**
   * bloc 5 : information
   */
  private BuPanel b5_ = null;
  /**
   * bloc 4 : boutons
   */
  private BuPanel b4_ = null;
  /**
   * flag permettant de savoir si l'utilisateur vient de cliquer ou non sur le bouton Valider
   */
  private boolean valider_ = false;

  /**
   * construit une bo�te de dialogue permettant de calculer le coefficient K pour le mode sp�cifi�.
   * 
   * @param _app fen�tre m�re qui a appell� cette bo�te de dialogue, permet de rattacher le B.d.D � cette fen�tre pour
   *          un syst�me modal.
   * @param _mode type du coefficient � calculer. voir les constantes MODE_POUSSEE_... et MODE_BUTEE_...
   */
  public OscarDialogCalculatriceK(final BuCommonInterface _app, final int _mode) {
    super(_app.getImplementation().getFrame(), true);
    app_ = _app;
    initMode(_mode);
    content_ = (JComponent) getContentPane();
    final BuVerticalLayout _lo = OscarLib.VerticalLayout();
    content_.setLayout(_lo);
    content_.setBorder(OscarLib.EmptyBorder());
    tf_angfrott_ = OscarLib.DoubleField("#0.0");
    tf_oblcontr_ = OscarLib.DoubleField("#0.000");
    tf_incterpl_ = OscarLib.DoubleField("#0.000");
    tf_incparem_ = OscarLib.DoubleField("#0.000");
    tf_coeffk_ = OscarLib.DoubleField("#0.000");
    bt_aide_ = OscarLib.Button("Aide", "AIDE", "CALCULATRICE_K_AIDE");
    bt_valider_ = OscarLib.Button("Valider", "VALIDER", "CALCULATRICE_K_VALIDER");
    bt_annuler_ = OscarLib.Button("Annuler", "ANNULER", "CALCULATRICE_K_ANNULER");
    b1_ = new BuPanel();
    b1_.setLayout(OscarLib.VerticalLayout());
    b1_.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB_INFO));
    b2_ = new BuPanel();
    b2_.setLayout(OscarLib.GridLayout(5));
    b2_.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB005));
    b3_ = new BuPanel();
    b3_.setLayout(new FlowLayout(FlowLayout.LEFT));
    b3_.setBorder(OscarLib.TitledBorder((pbCoef_.equals(COEF_POUSSEE)) ? OscarMsg.TITLELAB006 : OscarMsg.TITLELAB007));
    b5_ = new BuPanel();
    b5_.setLayout(OscarLib.VerticalLayout());
    b5_.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB_INFO));
    b4_ = OscarLib.FlowPanel();
    bt_aide_.addActionListener(this);
    bt_valider_.addActionListener(this);
    bt_annuler_.addActionListener(this);
    tf_angfrott_.addKeyListener(this);
    tf_oblcontr_.addKeyListener(this);
    tf_incterpl_.addKeyListener(this);
    tf_incparem_.addKeyListener(this);
    bt_valider_.setEnabled(false);
    tf_coeffk_.setEnabled(false);
    // Bloc Informations
    final BuLabelMultiLine _lb_infos = OscarLib.LabelMultiLine(OscarMsg.LAB018 + pbMode_ + " " + pbCoef_
        + OscarMsg.LAB019);
    b1_.add(_lb_infos);
    // Bloc Propri�t�s
    final BuLabel _lb_angfrott_text = OscarLib.Label(OscarMsg.LAB021 + " " + dndMode_);
    final BuLabel _lb_angfrott_symb = OscarLib.MathSymbol((dndMode_.equals(DRAINE)) ? OscarLib.MATH_PHI_PRIME
        : OscarLib.MATH_PHI_MU);
    final BuLabel _lb_angfrott_egal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel _lb_angfrott_unit = OscarLib.Label(OscarMsg.UNITLAB_DEGRE);
    b2_.add(_lb_angfrott_text);
    b2_.add(_lb_angfrott_symb);
    b2_.add(_lb_angfrott_egal);
    b2_.add(tf_angfrott_);
    b2_.add(_lb_angfrott_unit);
    final BuLabel _lb_oblcontr_text = OscarLib.Label(OscarMsg.LAB022);
    final BuLabel _lb_oblcontr_symb = OscarLib.MathSymbol(OscarLib.MATH_DELTA);
    final BuLabel _lb_oblcontr_egal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel _lb_oblcontr_unit = OscarLib.Label(OscarMsg.UNITLAB_DEGRE);
    b2_.add(_lb_oblcontr_text);
    b2_.add(_lb_oblcontr_symb);
    b2_.add(_lb_oblcontr_egal);
    b2_.add(tf_oblcontr_);
    b2_.add(_lb_oblcontr_unit);
    final BuLabel _lb_incterpl_text = OscarLib.Label(OscarMsg.LAB023);
    final BuLabel _lb_incterpl_symb = OscarLib.MathSymbol(OscarLib.MATH_BETA);
    final BuLabel _lb_incterpl_egal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel _lb_incterpl_unit = OscarLib.Label(OscarMsg.UNITLAB_DEGRE);
    b2_.add(_lb_incterpl_text);
    b2_.add(_lb_incterpl_symb);
    b2_.add(_lb_incterpl_egal);
    b2_.add(tf_incterpl_);
    b2_.add(_lb_incterpl_unit);
    final BuLabel _lb_incparem_text = OscarLib.Label(OscarMsg.LAB024);
    final BuLabel _lb_incparem_symb = OscarLib.MathSymbol(OscarLib.MATH_LAMBDA);
    final BuLabel _lb_incparem_egal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel _lb_incparem_unit = OscarLib.Label(OscarMsg.UNITLAB_DEGRE);
    b2_.add(_lb_incparem_text);
    b2_.add(_lb_incparem_symb);
    b2_.add(_lb_incparem_egal);
    b2_.add(tf_incparem_);
    b2_.add(_lb_incparem_unit);
    // Bloc K
    final BuLabel _lb_coeffk_text = OscarLib.Label(OscarMsg.LAB020);
    final BuLabel _lb_coeffk_symb = OscarLib.Label(" " + pbCoef_ + " ");
    final BuLabel _lb_coeffk_egal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    b3_.add(_lb_coeffk_text);
    b3_.add(_lb_coeffk_symb);
    b3_.add(_lb_coeffk_egal);
    b3_.add(tf_coeffk_);
    // Bloc Informations
    final BuLabelMultiLine _lb_infos2 = OscarLib.LabelMultiLine((pbMode_ == MODE_POUSSEE) ? OscarMsg.LAB071
        : OscarMsg.LAB072);
    b5_.add(_lb_infos2);
    // Bloc Boutons
    b4_.add(bt_aide_);
    b4_.add(bt_annuler_);
    b4_.add(bt_valider_);
    // Ajout des blocs au conteneur principal
    content_.add(b1_);
    content_.add(b2_);
    content_.add(b3_);
    content_.add(b5_);
    content_.add(b4_);
    // cr�ation de la bo�te de dialogue
    setLocation(40, 40);
    setTitle(title_);
    setSize(500, 400);
    setResizable(false);
    pack();
    tf_angfrott_.requestFocus();
  }

  /**
   * initialise les variables de mode de calculatrice
   * 
   * @param _mode mode de la calculatrice
   */
  private void initMode(final int _mode) {
    mode_ = _mode;
    switch (mode_) {
    case MODE_POUSSEE_EFFECTIF: {
      pbMode_ = MODE_POUSSEE;
      pbCoef_ = COEF_POUSSEE;
      // ctMode_= CT_EFF;
      dndMode_ = DRAINE;
      // dndIndice_= INDICE_DRAINE;
      break;
    }
    case MODE_POUSSEE_TOTAL: {
      pbMode_ = MODE_POUSSEE;
      pbCoef_ = COEF_POUSSEE;
      // ctMode_= CT_TOT;
      dndMode_ = NON_DRAINE;
      // dndIndice_= INDICE_NON_DRAINE;
      break;
    }
    case MODE_BUTEE_EFFECTIF: {
      pbMode_ = MODE_BUTEE;
      pbCoef_ = COEF_BUTEE;
      // ctMode_= CT_EFF;
      dndMode_ = DRAINE;
      // dndIndice_= INDICE_DRAINE;
      break;
    }
    case MODE_BUTEE_TOTAL: {
      pbMode_ = MODE_BUTEE;
      pbCoef_ = COEF_BUTEE;
      // ctMode_= CT_TOT;
      dndMode_ = NON_DRAINE;
      // dndIndice_= INDICE_NON_DRAINE;
      break;
    }
    default: {
      WSpy.Error(OscarMsg.ERR003);
      setVisible(false);
      return;
    }
    }
    title_ = OscarMsg.TITLELAB004 + " " + pbCoef_;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   * 
   * @param _evt �v�nement qui a engendr� l'action
   */
  public void actionPerformed(final ActionEvent _evt) {
    // R�cup�ration de la commande de l'action en cours et affichage sur la sortie
    // erreur
    final String action = _evt.getActionCommand();
    if (action.equals("CALCULATRICE_K_AIDE")) {
      aide();
    } else if (action.equals("CALCULATRICE_K_VALIDER")) {
      valider();
    } else if (action.equals("CALCULATRICE_K_ANNULER")) {
      annuler();
    }
  }

  /**
   * permet d'attendre que l'utilisateur clique sur un des boutons annuler ou valider, pour renvoyer soit la valeur
   * calcul�e (valider) soit null (annuler) et fermer la bo�te de dialogue. Renvoi un Double.
   */
  public Double activate() {
    Double _d = null;;
    show();
    // cette partie du code n'est execut�e que si la fen�tre dispara�t (par un
    // setVisible � false)
    if (hasBeenValidate()) {
      _d = (Double) tf_coeffk_.getValue();
    }
    return _d;
  }

  /**
   * affiche l'aide concernant la bo�te de dialogue de calcul du coefficient K.
   */
  private void aide() {
    app_.getImplementation().displayURL(OscarLib.helpUrl(OscarMsg.URL001));
  }

  /**
   * permet de fermer la bo�te de dialogue calculatrice en renvoyant null au lieu d'une valeur.
   */
  private void annuler() {
    bt_valider_.setEnabled(false);
    setVisible(false);
  }

  /**
   * retourne true si l'utilisateur vient juste de cliquer sur le bouton Valider, false sinon. Cette fonction permet de
   * filtrer l'action de fermeture de fen�tre par la croix, et d'�viter que l'utilisateur valider par cette action, qui
   * doit �tre �quivalente � un clic sur Annuler.
   */
  private boolean hasBeenValidate() {
    final boolean _r = valider_;
    return _r;
  }

  /**
   * permet de calculer la valeur du coefficient K (Ka ou Kp) en fonction du mode et d'afficher le r�sultat dans le
   * champs de visualisation du coefficient.
   */
  private void calculer() {
    Double _k = null;
    double _numerateur;
    double _denominateur;
    double _denominateur1;
    double _denominateur2;
    try {
      if (pbMode_.equals(MODE_POUSSEE)) {
        final double phi = Math.toRadians(OscarLib.TextFieldDoubleValue(tf_angfrott_));
        final double delta = Math.toRadians(OscarLib.TextFieldDoubleValue(tf_oblcontr_));
        final double beta = Math.toRadians(OscarLib.TextFieldDoubleValue(tf_incterpl_));
        final double lambda = Math.toRadians(OscarLib.TextFieldDoubleValue(tf_incparem_));
        final double eta = (Math.PI / 2) + lambda;
        _numerateur = Math.pow(Math.sin(eta - phi), 2D);
        _denominateur1 = Math.pow(Math.sin(eta), 2D) * Math.sin(eta + delta);
        _denominateur2 = Math.pow((1 + Math.sqrt((Math.sin(phi + delta) * Math.sin(phi - beta))
            / (Math.sin(eta + delta) * Math.sin(eta - beta)))), 2D);
        _denominateur = _denominateur1 * _denominateur2;
        _k = new Double(_numerateur / _denominateur);
        if (_k.isNaN()) {
          throw new Exception();
        }
      } else if (pbMode_.equals(MODE_BUTEE)) {
        final double phi = Math.toRadians(OscarLib.TextFieldDoubleValue(tf_angfrott_));
        final double delta = Math.toRadians(OscarLib.TextFieldDoubleValue(tf_oblcontr_));
        final double beta = Math.toRadians(OscarLib.TextFieldDoubleValue(tf_incterpl_));
        final double lambda = Math.toRadians(OscarLib.TextFieldDoubleValue(tf_incparem_));
        final double eta = (Math.PI / 2) + lambda;
        _numerateur = Math.pow(Math.sin(eta + phi), 2D);
        _denominateur = Math.pow(Math.sin(eta), 2D) * Math.sin(eta + delta);
        _denominateur *= Math.pow((1 - Math.sqrt((Math.sin(phi - delta) * Math.sin(phi + beta))
            / (Math.sin(eta + delta) * Math.sin(eta - beta)))), 2D);
        _k = new Double(_numerateur / _denominateur);
        if (_k.isNaN()) {
          throw new Exception();
        }
      }
      tf_coeffk_.setValue(_k);
      bt_valider_.setEnabled(true);
    } catch (final Exception _e1) {
      tf_coeffk_.setText(OscarMsg.LAB025);
      bt_valider_.setEnabled(false);
    }
  }

  /**
   * permet de fermer la bo�te de dialogue et de renvoyer la valeur du coefficient K correspondant.
   */
  private void valider() {
    bt_valider_.setEnabled(true);
    valider_ = true;
    setVisible(false);
  }

  /**
   * appell�e automatiquement lorsqu'une touche est press�e
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyPressed(final KeyEvent _e) {}

  /**
   * appell�e automatiquement lorsqu'une touche est relach�e. A chaque fois que cette m�thode est appell�e, on v�rifie
   * si le coefficient K peut �tre recalcul�e (mis � jour par rapport � la saisie), si c'est le cas on le recalcule et
   * on l'affiche, sinon une vide son champs de visualisation.
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyReleased(final KeyEvent _e) {
    if (tf_angfrott_.getValue() != null && tf_oblcontr_.getValue() != null && tf_incterpl_.getValue() != null
        && tf_incparem_.getValue() != null) {
      calculer();
    } else {
      bt_valider_.setEnabled(false);
      tf_coeffk_.setText("");
    }
    // /fred hein equals null a remplacer par ==null
    /*
     * try { tf_angfrott_.getValue().equals(null); tf_oblcontr_.getValue().equals(null);
     * tf_incterpl_.getValue().equals(null); tf_incparem_.getValue().equals(null); calculer(); } catch (final
     * NullPointerException _e1) { bt_valider_.setEnabled(false); tf_coeffk_.setText(""); }
     */
  }

  /**
   * appell�e automatiquement lorsque l'utilisateur tape sur une touche du clavier
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyTyped(final KeyEvent _e) {}
}
