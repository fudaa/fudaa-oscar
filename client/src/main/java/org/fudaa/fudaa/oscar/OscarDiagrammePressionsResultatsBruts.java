/*
 * @file         OscarDiagrammePressionsResultatsBruts.java
 * @creation     2000-10-15
 * @modification $Date: 2007-11-15 15:11:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableCellRenderer;

import org.fudaa.dodico.corba.oscar.SPointCoteValeur;
import org.fudaa.dodico.corba.oscar.SResultatsPressions;


/**
 * Classe de l'onglet 'Pressions' de la fen�tre 'r�sultats bruts'
 * 
 * @version $Revision: 1.7 $ $Date: 2007-11-15 15:11:49 $ by $Author: hadouxad $
 * @author Olivier Hoareau
 */
public class OscarDiagrammePressionsResultatsBruts extends OscarAbstractOnglet {
  /**
   * tableau des couples cote-pression.
   */
  private BuTable tb_pressions_;
  /**
   * mod�le de donn�es pour le tableau des couples cote-pression.
   */
  private DefaultTableModel dm_tb_pressions_;
  /**
   * bouton imprimer le tableau
   */
  private BuButton bt_imprimer_;
  /**
   * bouton sauvegarder le tableau
   */
  private BuButton bt_sauvegarder_;
  /**
   * bouton afficher la courbe repr�sentant les valeurs du tableau
   */
  private BuButton bt_courbe_;
  /**
   * bloc 'Info'
   */
  private BuPanel b1_;
  /**
   * bloc 'Pressions'
   */
  private BuPanel b2_;

  /**
   * construit un onglet 'Pressions' rattach� � la fen�tre <code>_resultats</code>.
   * 
   * @param _resultats fen�tre d'affichages des r�sultats bruts
   */
  public OscarDiagrammePressionsResultatsBruts(final OscarFilleResultatsBruts _resultats, final String _helpfile) {
    super(_resultats.getApplication(), _resultats, _helpfile);
  }

  /**
   * m�thode appell�e lors de la construction de l'objet pour construire l'interface GUI de l'onglet.
   * 
   * @return doit retourner un composant contenant l'interface
   */
  protected JComponent construireGUI() {
    final BuPanel _p = new BuPanel();
    _p.setLayout(new BorderLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    b1_ = OscarLib.InfoAidePanel(OscarMsg.LAB012, this);
    b2_ = OscarLib.TitledPanel(OscarMsg.TITLELAB015);
    b2_.setLayout(new BorderLayout());
    bt_imprimer_ = OscarLib.Button("Imprimer", "IMPRIMER", "IMPRIMER");
    bt_sauvegarder_ = OscarLib.Button("Exporter", "ENREGISTRER", "SAUVEGARDER");
    bt_courbe_ = OscarLib.Button("Courbe", "VOIR", "COURBE");
    dm_tb_pressions_ = new OscarDiagrammesPressionsResultatsBrutsTableModel(
        new Object[] { "Cote (m)", "Pression (kPa)" });
    tb_pressions_ = OscarLib.Table(dm_tb_pressions_);
    // tb_pressions_.setPreferredScrollableViewportSize(new Dimension(OscarLib.TABLE_PREFERRED_WIDTH,187));
    final BuTableCellRenderer _cellrenderer = new OscarDiagrammesPressionsResultatsBrutsTableCellRenderer();
    for (int i = 0; i < tb_pressions_.getColumnModel().getColumnCount(); i++) {
      tb_pressions_.getColumnModel().getColumn(i).setCellRenderer(_cellrenderer);
      tb_pressions_.getColumnModel().getColumn(i).setPreferredWidth((i == 0) ? 50 : 100);
    }
    final JScrollPane _sp = new JScrollPane(tb_pressions_);
    final BuPanel _b2b = OscarLib.FlowPanel();
    bt_imprimer_.addActionListener(this);
    bt_sauvegarder_.addActionListener(this);
    bt_courbe_.addActionListener(this);
    _b2b.add(bt_imprimer_);
    _b2b.add(bt_sauvegarder_);
    _b2b.add(bt_courbe_);
    b2_.add(_b2b, BorderLayout.NORTH);
    b2_.add(_sp, BorderLayout.CENTER);
    _p.add(b1_, BorderLayout.NORTH);
    _p.add(b2_, BorderLayout.CENTER);
    return _p;
  }

  /**
   * m�thode appell�e lors d'une action utilisateur �cout�e.
   * 
   * @param _a action � traiter
   */
  protected void action(final String _action) {
    if (_action.equals("IMPRIMER")) {
      imprimer();
    } else if (_action.equals("SAUVEGARDER")) {
      sauvegarder();
    } else if (_action.equals("COURBE")) {
      dessin();
    }
  }

  /**
   *
   */
  protected void sauvegarder() {
    final String _f = OscarLib.getFileChoosenByUser(this, "Exporter les donn�es brutes", "Exporter", OscarLib.USER_HOME
        + "pressions.csv");
    String _txt = "";
    if (_f == null) {
      return;
    }
    final SResultatsPressions _sr = getResultatsPressions();
    _txt += "cote;pression" + OscarLib.LINE_SEPARATOR;
    for (int i = 0; i < _sr.pointsCotePression.length; i++) {
      _txt += _sr.pointsCotePression[i].cote + ";" + _sr.pointsCotePression[i].valeur + OscarLib.LINE_SEPARATOR;
    }
    OscarLib.writeFile(_f, _txt);
  }

  /**
   * imprime la fen�tre.
   */
  protected void imprimer() {
    getImplementation().actionPerformed(new ActionEvent(this, 15, "IMPRIMER"));
  }

  /**
   * retourne le nombre de couples (cote, pression) contenus dans le tableau.
   * 
   * @return un entier repr�sentant le nombre de lignes du tableau
   */
  public int getNbPointDiagrammePressions() {
    final int _n = tb_pressions_.getRowCount();
    return _n;
  }

  /**
   * retourne une structure de donn�es contenant les informations du couple (cote, pression) _n du tableau. retourne
   * null si le couple n'existe pas dans le tableau.
   * 
   * @param _n num�ro du couple (cote, pression) � retourner (commence � 1)
   * @return une structure <code>SPointCoteValeur</code> contenant les infos du point
   */
  public SPointCoteValeur getPointDiagrammePression(final int _n) {
    SPointCoteValeur _p = null;
    try {
      _p = new SPointCoteValeur();
      _p.cote = Double.parseDouble(dm_tb_pressions_.getValueAt(_n - 1, 0).toString());
      _p.valeur = Double.parseDouble(dm_tb_pressions_.getValueAt(_n - 1, 1).toString());
    } catch (final ArrayIndexOutOfBoundsException _e1) {
      WSpy.Error(OscarMsg.ERR030);
    }
    return _p;
  }

  /**
   * retourne une structure de donn�es contenant les informations de tous les couples (cote, pressions).
   * 
   * @return une structure de donn�es <code>SResultatsPressions</code>
   */
  public SResultatsPressions getResultatsPressions() {
    SPointCoteValeur[] _p = null;
    final int _i1 = 1;
    final int _i2 = getNbPointDiagrammePressions();
    try {
      _p = new SPointCoteValeur[_i2 - _i1 + 1];
    } catch (final NegativeArraySizeException _e2) {
      WSpy.Error(OscarMsg.ERR031);
    }
    for (int i = (_i1 - 1); i < _i2; i++) {
      try {
        _p[i - (_i1 - 1)] = getPointDiagrammePression(i + 1);
      } catch (final NullPointerException _e1) {
        WSpy.Error(OscarMsg.ERR032);
      } catch (final ArrayIndexOutOfBoundsException _e2) {
        WSpy.Error(OscarMsg.ERR033);
        return null;
      }
    }
    final SResultatsPressions _srp = new SResultatsPressions();
    _srp.pointsCotePression = _p;
    return _srp;
  }

  /**
   * charge les couples (cote, pression) sp�cifi�es dans le tableau des couples
   * 
   * @param _p structure <code>SResultatsPressions</code> contenant les couples (cote, pression) � importer.
   */
  public void setResultatsPressions(SResultatsPressions _p) {
    viderTableau();
    if (_p == null) {
      _p = (SResultatsPressions) OscarLib.createIDLObject(SResultatsPressions.class);
    }
    int _n = 0;
    try {
      _n = _p.pointsCotePression.length;
    } catch (final NullPointerException _e1) {
      WSpy.Error(OscarMsg.ERR034);
    }
    for (int i = 0; i < _n; i++) {
      dm_tb_pressions_.addRow(OscarLib.EmptyRow(dm_tb_pressions_.getColumnCount()));
      tb_pressions_.setValueAt(OscarLib.formatterNombre(_p.pointsCotePression[i].cote), dm_tb_pressions_.getRowCount() - 1, 0);
      tb_pressions_.setValueAt(OscarLib.formatterNombre("pression", _p.pointsCotePression[i].valeur), dm_tb_pressions_.getRowCount() - 1, 1);
      
    }
  }

  /**
   * supprime tous les couples (cote, pression) du tableau des couples
   */
  private void viderTableau() {
    if (tb_pressions_.isEditing()) {
      tb_pressions_.getCellEditor().stopCellEditing();
    }
    final int _max = tb_pressions_.getRowCount();
    for (int i = 0; i < _max; i++) {
      dm_tb_pressions_.removeRow(0);
    }
  }

  /**
   *
   */
  public Double getCoteMin() {
    SPointCoteValeur _point = null;
    Double _min = null;
    if (getNbPointDiagrammePressions() > 0) {
      _min = new Double(Double.MAX_VALUE);
      for (int i = 0; i < getNbPointDiagrammePressions(); i++) {
        _point = getPointDiagrammePression(i + 1);
        if ((_point != null) && (_point.cote != OscarLib.DOUBLE_NULL) && (_point.cote < _min.doubleValue())) {
          _min = new Double(_point.cote);
        }
      }
    }
    return _min;
  }

  /**
   *
   */
  public Double getCoteMax() {
    SPointCoteValeur _point = null;
    Double _max = null;
    if (getNbPointDiagrammePressions() > 0) {
      _max = new Double(Double.MIN_VALUE);
      for (int i = 0; i < getNbPointDiagrammePressions(); i++) {
        _point = getPointDiagrammePression(i + 1);
        if ((_point != null) && (_point.cote != OscarLib.DOUBLE_NULL) && (_point.cote > _max.doubleValue())) {
          _max = new Double(_point.cote);
        }
      }
    }
    return _max;
  }

  /**
   *
   */
  public Double getValeurMin() {
    SPointCoteValeur _point = null;
    Double _min = null;
    if (getNbPointDiagrammePressions() > 0) {
      _min = new Double(Double.MAX_VALUE);
      for (int i = 0; i < getNbPointDiagrammePressions(); i++) {
        _point = getPointDiagrammePression(i + 1);
        if ((_point != null) && (_point.valeur != OscarLib.DOUBLE_NULL) && (_point.valeur < _min.doubleValue())) {
          _min = new Double(_point.valeur);
        }
      }
    }
    return _min;
  }

  /**
   *
   */
  public Double getValeurMax() {
    SPointCoteValeur _point = null;
    Double _max = null;
    if (getNbPointDiagrammePressions() > 0) {
      _max = new Double(Double.MIN_VALUE);
      for (int i = 0; i < getNbPointDiagrammePressions(); i++) {
        _point = getPointDiagrammePression(i + 1);
        if ((_point != null) && (_point.valeur != OscarLib.DOUBLE_NULL) && (_point.valeur > _max.doubleValue())) {
          _max = new Double(_point.valeur);
        }
      }
    }
    return _max;
  }

  /**
   *
   */
  protected void dessin() {
    changeOnglet((ChangeEvent) null);
    OscarLib.showDialogGraphViewer(getApplication(), "R�sultats bruts : Diagramme des pressions",
        getDiagrammePressionsGrapheScript());
  }

  /**
   *
   */
  public String getDiagrammePressionsGrapheScript() {
    double _min;
    double _max;
    double _xmin;
    double _xmax;
    final Double _dmin = getCoteMin();
    final Double _dmax = getCoteMax();
    final Double _dxmin = getValeurMin();
    final Double _dxmax = getValeurMax();
    if (_dmin == null) {
      _min = -1.0;
    } else {
      _min = _dmin.doubleValue();
    }
    if (_dmax == null) {
      _max = 1.0;
    } else {
      _max = _dmax.doubleValue() + 1.0;
    }
    if (_dxmin == null) {
      _xmin = 0.0;
    } else {
      _xmin = _dxmin.doubleValue();
    }
    if (_dxmax == null) {
      _xmax = 0.0;
    } else {
      _xmax = _dxmax.doubleValue();
    }
    return getDiagrammePressionsGrapheScript(_xmin, _xmax, _min, _max);
  }

  /**
   *
   */
  public String getDiagrammePressionsGrapheScript(final double _xmin, final double _xmax, final double _min,
      final double _max) {
    final StringBuffer _s = new StringBuffer(4096);
    _s.append("graphe \n{\n");
    _s.append("  titre \"Diagramme des pressions\"\n");
    _s.append("  sous-titre \"R�sultats\"\n");
    _s.append("  legende oui\n");
    _s.append("  animation non\n");
    _s.append("  marges\n{\n");
    _s.append("    gauche 80\n");
    _s.append("    droite 120\n");
    _s.append("    haut 50\n");
    _s.append("    bas 30\n");
    _s.append("  }\n");
    _s.append("  \n");
    _s.append("  axe\n{\n");
    _s.append("    titre \"Pression\"\n");
    _s.append("    unite \"kPa\"\n");
    _s.append("    orientation horizontal\n");
    _s.append("    graduations oui\n");
    _s.append("    minimum " + _xmin + "\n");
    _s.append("    maximum " + _xmax + "\n");
    _s.append("  }\n");
    _s.append("  axe\n{\n");
    _s.append("    titre \"Cote\"\n");
    _s.append("    unite \"m\"\n");
    _s.append("    orientation vertical\n");
    _s.append("    graduations oui\n");
    _s.append("    minimum " + _min + "\n");
    _s.append("    maximum " + _max + "\n");
    _s.append("  }\n");
    _s.append(getDiagrammePressionsCourbeGrapheScript(_xmin, _xmax, _min, _max));
    _s.append("}\n");
    return _s.toString();
  }

  /**
   *
   */
  public String getDiagrammePressionsCourbeGrapheScript(final double _xmin, final double _xmax, final double _ymin,
      final double _ymax) {
    final StringBuffer _s = new StringBuffer(4096);
    SPointCoteValeur _point;
    int i;
    if (getNbPointDiagrammePressions() > 0) {
      _s.append("  courbe\n{\n");
      _s.append("    marqueurs non\n");
      _s.append("    type lineaire\n");
      _s.append("    valeurs\n{\n");
      for (i = 0; i < getNbPointDiagrammePressions(); i++) {
        _point = getPointDiagrammePression(i + 1);
        if (_point != null) {
          _s.append("      " + _point.valeur + " " + _point.cote + "\n");
        }
      }
      _s.append("    }\n");
      _s.append("  }\n");
      _s.append("  courbe\n{\n");
      _s.append("    marqueurs non\n");
      _s.append("    type lineaire\n");
      _s.append("    valeurs\n{\n");
      _s.append("      0.0 " + _ymin + "\n");
      _s.append("      0.0 " + _ymax + "\n");
      _s.append("    }\n");
      _s.append("    aspect\n{\n");
      _s.append("      contour.couleur 000000\n");
      _s.append("    }\n");
      _s.append("  }\n");
    }
    return _s.toString();
  }
  /**
   *
   */
  class OscarDiagrammesPressionsResultatsBrutsTableCellRenderer extends BuTableCellRenderer {
    /**
     *
     */
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
        final boolean hasFocus, final int row, final int column) {
      Component _r = null;
      BuLabel _lb = null;
      _lb = (BuLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      _r = _lb;
      if (row >= 0) {
        if (value instanceof Double) {}
      }
      return _r;
    }
  }
  /**
   *
   */
  public class OscarDiagrammesPressionsResultatsBrutsTableModel extends DefaultTableModel {
    /**
     *
     */
    public OscarDiagrammesPressionsResultatsBrutsTableModel(final Object[] columnNames) {
      super(columnNames, 0);
    }

    /**
     *
     */
    public boolean isCellEditable(final int row, final int column) {
      return false;
    }

    /**
     *
     */
    public Object getValueAt(final int row, final int column) {
      Object _o = null;
      _o = super.getValueAt(row, column);
      return _o;
    }

    /**
     *
     */
    public void setValueAt(final Object _o, final int row, final int column) {
      super.setValueAt(_o, row, column);
    }
  }
}
