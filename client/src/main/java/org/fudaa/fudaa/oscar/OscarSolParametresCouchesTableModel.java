/*
 * @file         OscarSolParametresCouchesTableModel.java
 * @creation     2000-11-08
 * @modification $Date: 2007-05-04 13:59:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.table.DefaultTableModel;


/**
 * Mod�le de donn�es pour le tableau des couches de sol (pouss�e et but�e)
 * 
 * @version $Revision: 1.7 $ $Date: 2007-05-04 13:59:31 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarSolParametresCouchesTableModel extends DefaultTableModel {
  /**
   * cr�e un mod�le de donn�es utilis� pour les tableaux de couches de sol en pouss�e et but�e. rempli le mod�le de
   * donn�es avec des ent�tes de colonnes apropri�es suivant le mode choisi.
   * 
   * @param _mode mode OscarLib.POUSSEE ou OscarLib.BUTEE
   */
  public OscarSolParametresCouchesTableModel(final int _mode) {
    this((_mode == OscarLib.POUSSEE) ? new Object[] { "header-numero", "header-epaisseur", "header-gamma-h",
        "header-gamma-prime", "header-ka", "header-c-prime", "header-phi-prime" } : new Object[] { "header-numero",
        "header-epaisseur", "header-gamma-h", "header-gamma-prime", "header-kp", "header-c-prime", "header-phi-prime" });
  }

  /**
   * cr�e un mod�le de donn�es utilis� pour les tableaux de couches de sol en pouss�e et but�e.
   * 
   * @param columnNames tableau d'objet contenant les noms des colonnes � cr�er
   */
  public OscarSolParametresCouchesTableModel(final Object[] columnNames) {
    super(columnNames, 0);
  }

  /**
   * d�finit si la cellule situ�e � l'emplacement sp�cifi�e est �ditable par l'utilisateur ou non.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public boolean isCellEditable(final int row, final int column) {
    boolean _b = false;
    if ((row >= 0) && (column >= 1)) {
      _b = true;
    }
    return _b;
  }

  /**
   * retourne l'objet valeur de la cellule sp�cifi�e.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public Object getValueAt(final int row, final int column) {
    Object _o = null;
    Object _v = null;
    _v = super.getValueAt(row, column);
    if (_v == null) {
      //pas la peine de faire new String(String)
      return "non-valide";
    }
    try {
      if ((row >= 0) && (column > 0) && (column < 7)) {
        _o = new Double(_v.toString());
      } else if ((row >= 0) && (column == 0)) {
        _o = new Integer(_v.toString());
      } else {
        _o = _v;
      }
    } catch (final NumberFormatException _e1) {} catch (final NullPointerException _e2) {}
    return _o;
  }

  /**
   * modifie la valeur de la cellule sp�cifi�e.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public void setValueAt(final Object _o, final int row, final int column) {
    Double _v;
    try {
      if ((row >= 0) && (column > 0) && (column < 7)) {
        if (!(_o instanceof String)) {
          _v = new Double(_o.toString());
          super.setValueAt(_v, row, column);
        }
      } else if ((row >= 0) && (column == 0)) {
        super.setValueAt(new Integer(_o.toString()), row, column);
      }
    } catch (final NullPointerException _e1) {}
    super.setValueAt(_o, row, column);
  }
}
