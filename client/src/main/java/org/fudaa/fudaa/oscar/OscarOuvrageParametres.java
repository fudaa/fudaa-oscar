/*
 * @file         OscarOuvrageParametres.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.beans.PropertyChangeEvent;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.oscar.SOuvrage;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;


/**
 * Description de l'onglet des parametres du ouvrage.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarOuvrageParametres extends OscarAbstractOnglet {
  /**
   *
   */
  OscarOuvrageParametresDonneesObligatoires oblig_;
  /**
   *
   */
  OscarOuvrageParametresDonneesFacultatives facul_;
  /**
   *
   */
  BuPanel pn2;
  /**
   * Set d'onglet de l'onglet Ouvrage
   */
  JTabbedPane tpMain;

  /**
   *
   */
  public OscarOuvrageParametres(final OscarFilleParametres _fp, final String _helpfile) {
    super(_fp.getApplication(), _fp, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    tpMain = new JTabbedPane();
    pn2 = OscarLib.InfoAidePanel(OscarMsg.LAB001, this);
    // Set d'onglets 'Type de Donn�es'
    oblig_ = new OscarOuvrageParametresDonneesObligatoires(this, OscarMsg.URL022);
    facul_ = new OscarOuvrageParametresDonneesFacultatives(this, OscarMsg.URL023);
    tpMain.addTab(OscarMsg.TABLAB022, null, oblig_, OscarMsg.TIPTEXT013);
    tpMain.addTab(OscarMsg.TABLAB023, null, facul_, OscarMsg.TIPTEXT014);
    setCurrentTab(0);
    tpMain.addChangeListener(this);
    oblig_.addPropertyChangeListener("NAPPE_TIRANTS_SELECTIONNEE", this);
    _p.add(pn2);
    _p.add(tpMain);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  public void propertyChange(final PropertyChangeEvent _evt) {
    if (_evt.getPropertyName().equals("NAPPE_TIRANTS_SELECTIONNEE")) {
      selectionnerNappeTirants(((Boolean) _evt.getNewValue()).booleanValue());
    }
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT014);
    _mv.addMessages(validation().getMessages());
    ((OscarAbstractOnglet) tpMain.getComponentAt(getCurrentTab())).stateChanged(_evt);
    setCurrentTab(tpMain.getSelectedIndex());
  }

  /**
   * affiche / masque la partie "Nappe de tirants" dans le sous-onglet 'Donn�es Facultatives'
   */
  private void selectionnerNappeTirants(final boolean _etat) {
    oblig_.toggleNappeTirants(_etat);
    facul_.toggleNappeTirants(_etat);
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans l'onglet ouvrage
   */
  public SOuvrage getParametresOuvrage() {
    final SOuvrage _p = (SOuvrage) OscarLib.createIDLObject(SOuvrage.class);
    _p.presenceNappeTirants = (oblig_.getPresenceNappeTirants()) ? OscarLib.IDL_OUVRAGE_CHOIXTIRANTS_OUI
        : OscarLib.IDL_OUVRAGE_CHOIXTIRANTS_NON;
    if (oblig_.getPresenceNappeTirants()) {
      try {
        _p.coteNappeTirants = (oblig_.getCoteNappeTirants()).doubleValue();
      } catch (final NullPointerException _e1) {
        _p.coteNappeTirants = VALEUR_NULLE.value;
      }
      try {
        _p.pourcentageEncastrementNappe = (oblig_.getPourcentageEncastrementNappe()).doubleValue();
      } catch (final NullPointerException _e1) {
        _p.pourcentageEncastrementNappe = VALEUR_NULLE.value;
      }
      try {
        _p.espaceEntreDeuxTirants = (facul_.getEspaceEntreDeuxTirants()).doubleValue();
      } catch (final NullPointerException _e1) {
        _p.espaceEntreDeuxTirants = VALEUR_NULLE.value;
      }
      try {
        _p.sectionTirants = (facul_.getSectionTirants()).doubleValue();
      } catch (final NullPointerException _e1) {
        _p.sectionTirants = VALEUR_NULLE.value;
      }
      try {
        _p.limiteElastiqueAcierTirants = (facul_.getLimiteElastiqueAcierTirants()).doubleValue();
      } catch (final NullPointerException _e1) {
        _p.limiteElastiqueAcierTirants = VALEUR_NULLE.value;
      }
    }
    try {
      _p.moduleYoungAcier = (facul_.getModuleYoungAcier()).doubleValue();
    } catch (final NullPointerException _e1) {
      _p.moduleYoungAcier = VALEUR_NULLE.value;
    }
    try {
      _p.limiteElastiqueAcier = (facul_.getLimiteElastiqueAcier()).doubleValue();
    } catch (final NullPointerException _e1) {
      _p.limiteElastiqueAcier = VALEUR_NULLE.value;
    }
    try {
      _p.inertiePalplanches = (facul_.getInertiePalplanches()).doubleValue();
    } catch (final NullPointerException _e1) {
      _p.inertiePalplanches = VALEUR_NULLE.value;
    }
    try {
      _p.demieHauteur = (facul_.getDemieHauteur()).doubleValue();
    } catch (final NullPointerException _e1) {
      _p.demieHauteur = VALEUR_NULLE.value;
    }
    return _p;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans les champs de l'onglet ouvrage
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) � importer
   */
  public synchronized void setParametresOuvrage(SOuvrage _p) {
    if (_p == null) {
      _p = (SOuvrage) OscarLib.createIDLObject(SOuvrage.class);
    }
    try {
      selectionnerNappeTirants((_p.presenceNappeTirants.equals(OscarLib.IDL_OUVRAGE_CHOIXTIRANTS_OUI)) ? true : false);
    } catch (final NullPointerException _e1) {
      selectionnerNappeTirants(false);
    }
    if (oblig_.getPresenceNappeTirants()) {
      if (_p.coteNappeTirants != VALEUR_NULLE.value) {
        oblig_.setCoteNappeTirants(_p.coteNappeTirants);
      }
      if (_p.pourcentageEncastrementNappe != VALEUR_NULLE.value) {
        oblig_.setPourcentageEncastrementNappe(_p.pourcentageEncastrementNappe);
      } else {
        oblig_.setPourcentageEncastrementNappe(OscarMsg.INITIALVALUE009);
      }
      if (_p.espaceEntreDeuxTirants != VALEUR_NULLE.value) {
        facul_.setEspaceEntreDeuxTirants(_p.espaceEntreDeuxTirants);
      }
      if (_p.sectionTirants != VALEUR_NULLE.value) {
        facul_.setSectionTirants(_p.sectionTirants);
      }
      if (_p.limiteElastiqueAcierTirants != VALEUR_NULLE.value) {
        facul_.setLimiteElastiqueAcierTirants(_p.limiteElastiqueAcierTirants);
      }
    }
    //oblig_.setPourcentageEncastrementNappe(OscarMsg.INITIALVALUE009); Supprim� par FARGEIX
    if (_p.moduleYoungAcier != VALEUR_NULLE.value) {
      facul_.setModuleYoungAcier(_p.moduleYoungAcier);
    }
    if (_p.limiteElastiqueAcier != VALEUR_NULLE.value) {
      facul_.setLimiteElastiqueAcier(_p.limiteElastiqueAcier);
    }
    if (_p.inertiePalplanches != VALEUR_NULLE.value) {
      facul_.setInertiePalplanches(_p.inertiePalplanches);
    }
    if (_p.demieHauteur != VALEUR_NULLE.value) {
      facul_.setDemieHauteur(_p.demieHauteur);
    }
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    _vm.add(oblig_.validation());
    _vm.add(facul_.validation());
    return _vm;
  }
}
