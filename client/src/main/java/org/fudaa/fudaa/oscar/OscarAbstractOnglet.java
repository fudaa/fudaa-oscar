/*
 * @file         OscarAbstractOnglet.java
 * @creation     2000-11-13
 * @modification $Date: 2007-11-15 14:07:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuPanel;




/**
 * Classe abstraite de base pour les onglets des classeurs d'onglets.
 * 
 * @version $Revision: 1.7 $ $Date: 2007-11-15 14:07:22 $ by $Author: hadouxad $
 * @author Olivier Hoareau
 */
public class OscarAbstractOnglet extends BuPanel implements ActionListener, ChangeListener, PropertyChangeListener {
  /**
   * application
   */
  private BuCommonInterface appli_;
  /**
   * composant conteneur parent principal de l'onglet.
   */
  private JComponent parent_;
  /**
   * lien vers l'aide contextuelle concernant cet onglet
   */
  private String helpfile_;
  /**
   * use only if needed (tabbedpane in this tab)
   */
  private int currenttab_;

  /**
   * construit un onglet d�pendant du classeur d'onglet <code>_parent</code> n'ayant aucune aide associ�e.
   * 
   * @param _parent composant parent conteneur de cet onglet
   */
  protected OscarAbstractOnglet(final JComponent _parent) {
    this(null, _parent, null);
  }

  /**
   * construit un onglet d�pendant du classeur d'onglet <code>_parent</code> et dont l'aide associ� peut �tre acc�der
   * par le mot cl�s <code>_helpfile</code>.
   * 
   * @param _parent composant parent conteneur de cet onglet
   */
  protected OscarAbstractOnglet(final BuCommonInterface _appli, final JComponent _parent, final String _helpfile) {
    super();
    parent_ = _parent;
    currenttab_ = 0;
    helpfile_ = _helpfile;
    appli_ = _appli;
    setLayout(new BorderLayout());
    setBorder(OscarLib.EmptyBorder());
    add(construireGUI(), BorderLayout.CENTER);
    if (helpfile_ != null) {
      getImplementation().installContextHelp(this, OscarLib.helpUrl(helpfile_));
    }
  }

  /**
   *
   */
  protected void setCurrentTab(final int _index) {
    currenttab_ = _index;
  }

  /**
   *
   */
  protected int getCurrentTab() {
    return currenttab_;
  }

  /**
   * m�thode appell�e lors d'une action �cout�e par ce composant.
   * 
   * @param _evt �v�nement qui a engendr� l'action
   */
  public void actionPerformed(final ActionEvent _evt) {
    // try{
    if (_evt.getActionCommand().equals("AIDE")) {
    	//System.out.println("je vais dans aide");
    	aide();
    } else {
      action(_evt.getActionCommand());
    }
    // }catch(NullPointerException _e1){
    // WSpy.Error("invalid action command (null)");
    // }
  }

  /**
   * m�thode appell�e lors d'un changement de propri�t� espionn�e par ce composant.
   * 
   * @param evt �v�nement qui d�crit le changement de propri�t�
   */
  public void propertyChange(final PropertyChangeEvent evt) {}

  /**
   * m�thode appell�e lors de la construction de l'objet pour construire l'interface GUI de l'onglet.
   * 
   * @return doit retourner un composant contenant l'interface
   */
  protected JComponent construireGUI() {
    return new JPanel();
  }

  /**
   * m�thode appell�e lors d'une action utilisateur �cout�e.
   * 
   * @param _a action � traiter
   */
  protected void action(final String _a) {}

  /**
   * m�thode appell�e � l'initialisation de l'objet pour importer les donn�es �ventuelles dans les champs de l'onglet.
   */
  protected void importerDonnees() {}

  /**
   * affiche l'aide concernant l'onglet courant dans le navigateur.
   */
  protected void aide() {
	  
	  System.out.println("getHelpFile: "+getHelpFile()+"\n Le tout: "+OscarLib.helpUrl(getHelpFile()));
    getApplication().getImplementation().displayURL(/*OscarLib.helpUrl(*/"aide"+File.separator+"oscar"+File.separator+getHelpFile()/*)*/);
  }

  /**
   * retourn le mot cl� permettant d'acc�der � l'aide contextuelle
   */
  protected String getHelpFile() {
	  if((helpfile_.substring(helpfile_.length()-5, helpfile_.length())).equals(".html"))
		  return helpfile_.substring(helpfile_.lastIndexOf(File.separatorChar)+1, helpfile_.length());
	  //-- cas ou il n'y a pas le suffixe .html afin de charger la bonne page --//
		  return helpfile_.substring(helpfile_.lastIndexOf(File.separatorChar)+1, helpfile_.length())+".html";
  }

  /**
   * imprime le contenu de l'onglet.
   */
  protected void imprimer() {}

  /**
   *
   */
  protected void sauvegarder() {}

  /**
   * m�thode appell�e lorsque l'utilisateur arrive ou quitte l'onglet.
   * 
   * @param _et �v�nement d'onglet qui a engendr� l'action
   */
  protected void changeOnglet(final ChangeEvent _e) {}

  /**
   * m�thode appell�e lorsque l'utilisateur quitte ou arrive dans l'onglet.
   * 
   * @param _evt �v�nement d'onglet qui a engendr� l'action
   */
  public void stateChanged(final ChangeEvent _evt) {
    // if(helpfile_!=null)
    // appli_.getImplementation().installContextHelp(this,OscarLib.helpUrl(helpfile_));
    changeOnglet(_evt);
  }

  /**
   * retourne le composant parent de cet onglet.
   * 
   * @return le conteneur <code>parent_</code>
   */
  protected JComponent getParentComponent() {
    return parent_;
  }

  /**
   *
   */
  protected BuCommonInterface getApplication() {
    return appli_;
  }

  /**
   *
   */
  protected OscarImplementation getImplementation() {
    return (OscarImplementation) appli_.getImplementation();
  }

  /**
   *
   */
  protected OscarFilleParametres getFilleParametres() {
    return getImplementation().getFilleParametres();
  }

  /**
   *
   */
  public ValidationMessages validation() {
    return null;
  }
}
