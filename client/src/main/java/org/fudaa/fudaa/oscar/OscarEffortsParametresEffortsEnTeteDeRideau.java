/*
 * @file         OscarEffortsParametresEffortsEnTeteDeRideau.java
 * @creation     2000-10-15
 * @modification $Date: 2007-01-19 11:13:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.oscar.SEffortEnTeteDeRideau;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;


/**
 * Description du sous-onglet 'Efforts en T�te de Rideau' de l'onglet des parametres des efforts.
 * 
 * @version $Revision: 1.7 $ $Date: 2007-01-19 11:13:21 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public class OscarEffortsParametresEffortsEnTeteDeRideau extends OscarAbstractOnglet {
  /**
   * Champs Force en tete (effort en tete de rideau)
   */
  BuTextField tf_efftr_ft;
  /**
   * Champs Moment en tete (effort en tete de rideau)
   */
  BuTextField tf_efftr_mt;
  /**
   *
   */
  BuPanel pn1;
  /**
   *
   */
  BuPanel pn2;

  /**
   *
   */
  public OscarEffortsParametresEffortsEnTeteDeRideau(final OscarEffortsParametres _efforts, final String _helpfile) {
    super(_efforts.getApplication(), _efforts, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    tf_efftr_ft = OscarLib.DoubleField("#0.00");
    tf_efftr_mt = OscarLib.DoubleField("#0.00");
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    // Bloc 'Info'
    pn1 = OscarLib.InfoPanel(OscarMsg.LAB057);
    // Bloc 'Effort'
    pn2 = new BuPanel();
    pn2.setLayout(OscarLib.GridLayout(5));
    pn2.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB022));
    final BuLabel lb_efftr_ft = OscarLib.Label(OscarMsg.LAB058);
    final BuLabel lb_efftr_ft_symb = OscarLib.Label("F");
    final BuLabel lb_efftr_ft_equal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_efftr_ft_unit = OscarLib.Label(OscarMsg.UNITLAB_KILO_NEWTON_PAR_METRE_LINEAIRE);
    pn2.add(lb_efftr_ft);
    pn2.add(lb_efftr_ft_symb);
    pn2.add(lb_efftr_ft_equal);
    pn2.add(tf_efftr_ft);
    pn2.add(lb_efftr_ft_unit);
    final BuLabel lb_efftr_mt = OscarLib.Label(OscarMsg.LAB059);
    final BuLabel lb_efftr_mt_symb = OscarLib.Label("M");
    final BuLabel lb_efftr_mt_equal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel lb_efftr_mt_unit = OscarLib.Label(OscarMsg.UNITLAB_KILO_NEWTON_METRE_PAR_METRE_LINEAIRE);
    pn2.add(lb_efftr_mt);
    pn2.add(lb_efftr_mt_symb);
    pn2.add(lb_efftr_mt_equal);
    pn2.add(tf_efftr_mt);
    pn2.add(lb_efftr_mt_unit);
    _p.add(pn1);
    _p.add(pn2);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {}

  /**
   *
   */
  public SEffortEnTeteDeRideau getEffort() {
    final SEffortEnTeteDeRideau _p = (SEffortEnTeteDeRideau) OscarLib.createIDLObject(SEffortEnTeteDeRideau.class);
    try {
      _p.valeurForceEnTete = ((Double) tf_efftr_ft.getValue()).doubleValue();
    } catch (final NullPointerException _e1) {
      _p.valeurForceEnTete = VALEUR_NULLE.value;
    }
    try {
      _p.valeurMomentEnTete = ((Double) tf_efftr_mt.getValue()).doubleValue();
    } catch (final NullPointerException _e2) {
      _p.valeurMomentEnTete = VALEUR_NULLE.value;
    }
    return _p;
  }

  /**
   *
   */
  public synchronized void setEffort(SEffortEnTeteDeRideau _p) {
    if (_p == null) {
      _p = (SEffortEnTeteDeRideau) OscarLib.createIDLObject(SEffortEnTeteDeRideau.class);
    }
    if (_p.valeurForceEnTete != VALEUR_NULLE.value) {
      tf_efftr_ft.setValue(new Double(_p.valeurForceEnTete));
    }
    if (_p.valeurMomentEnTete != VALEUR_NULLE.value) {
      tf_efftr_mt.setValue(new Double(_p.valeurMomentEnTete));
    }
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _e) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT012);
    _mv.addMessages(validation().getMessages());
  }

  /**
   *
   */
  public ValidationMessages validation() {
    final ValidationMessages _vm = new ValidationMessages();
    final SEffortEnTeteDeRideau _etr = getEffort();
    if (_etr.valeurForceEnTete == VALEUR_NULLE.value) {
      _vm.add(OscarMsg.VMSG038);
    } else if (_etr.valeurForceEnTete < 0.0) {
      _vm.add(OscarMsg.VMSG039);
    }
    if (_etr.valeurMomentEnTete == VALEUR_NULLE.value) {
      _vm.add(OscarMsg.VMSG040);
    } /*else if (_etr.valeurMomentEnTete < 0.0) {
    _vm.add(OscarMsg.VMSG041);
    }*/
    return _vm;
  }
}
