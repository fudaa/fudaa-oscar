/*
 * @file         OscarEffortsParametresFichiersDeDiagrammes.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;

import org.fudaa.dodico.corba.oscar.SFichierDiagramme;
import org.fudaa.dodico.corba.oscar.SPointCoteValeur;

/**
 * Description du sous-onglet 'Fichiers de Diagrammes' de l'onglet des parametres des efforts.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarEffortsParametresFichiersDeDiagrammes extends OscarAbstractOnglet {
  /**
   * tableau contenant les informations sur les fichiers de diagrammes suppl�mentaires
   */
  private BuTable tb_fdiag;
  /**
   * mod�le de donn�es du tableau contenant les informations sur les fichiers de diagrammes suppl�mentaires
   */
  private DefaultTableModel dm_tb_fdiag;
  /**
   * bouton d'ajout d'un fichier de diagrammes
   */
  BuButton bt_add;
  /**
   * bouton de visualisation d'un fichier de diagrammes
   */
  BuButton bt_show;
  /**
   * bouton de recherche d'un fichier de diagrammes
   */
  BuButton bt_search;
  /**
   * bouton de suppression d'un fichier de diagrammes
   */
  BuButton bt_del;
  /**
   *
   */
  BuPanel pn1;
  /**
   *
   */
  BuPanel pn2;

  // M�thodes du sous onglet Fichiers de Diagrammes
  public OscarEffortsParametresFichiersDeDiagrammes(final OscarEffortsParametres _efforts, final String _helpfile) {
    super(_efforts.getApplication(), _efforts, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    bt_add = OscarLib.Button("Ajouter", "OUI", "AJOUTER");
    bt_show = OscarLib.Button("Visualiser", "VOIR", "VISUALISER");
    bt_search = OscarLib.Button("Modifier", "OUVRIR", "PARCOURIR");
    bt_del = OscarLib.Button("Supprimer", "NON", "SUPPRIMER");
    pn1 = OscarLib.InfoPanel(OscarMsg.LAB060);
    pn2 = new BuPanel();
    pn2.setLayout(OscarLib.VerticalLayout());
    pn2.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB023));
    dm_tb_fdiag = new OscarEffortsParametresFichiersDeDiagrammesTableModel();
    tb_fdiag = OscarLib.Table(dm_tb_fdiag);
    tb_fdiag.getColumnModel().getColumn(0).setCellRenderer(
        new OscarEffortsParametresFichiersDeDiagrammesTableCellRenderer());
    bt_add.addActionListener(this);
    bt_show.addActionListener(this);
    bt_search.addActionListener(this);
    bt_del.addActionListener(this);
    tb_fdiag.addMouseListener(new MouseAdapter() {
      public void mouseReleased(final MouseEvent _evt) {
        clickOnRow();
      }
    });
    // Bloc 'Contraintes'
    final JScrollPane sp1 = new JScrollPane(tb_fdiag);
    final BuPanel pn3 = new BuPanel();
    final BoxLayout lo4 = new BoxLayout(pn3, BoxLayout.X_AXIS);
    pn3.setBorder(OscarLib.EmptyBorder());
    pn3.setLayout(lo4);
    pn3.add(bt_add);
    pn3.add(bt_show);
    pn3.add(bt_search);
    pn3.add(bt_del);
    pn2.add(sp1);
    pn2.add(pn3);
    _p.add(pn1);
    _p.add(pn2);
    return _p;
  }

  /**
   * execut�e lorsque l'utilisateur clique sur une ligne du tableau
   */
  void clickOnRow() {
    if (tb_fdiag.getSelectedRowCount() > 0) {
      bt_del.setEnabled(true);
      final String _f = (String) dm_tb_fdiag.getValueAt(tb_fdiag.getSelectedRow(), 0);
      if ((_f == null) || (_f.equals(""))) {
        bt_show.setEnabled(false);
      } else {
        bt_show.setEnabled(true);
      }
      bt_search.setEnabled(true);
    } else {
      bt_del.setEnabled(false);
      bt_show.setEnabled(false);
      bt_search.setEnabled(false);
    }
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {
    if (_action.equals("AJOUTER")) {
      parcourir("AJOUTER");
    } else if (_action.equals("SUPPRIMER")) {
      supprimerFichier();
    } else if (_action.equals("VISUALISER")) {
      visualiserFichier();
    } else if (_action.equals("PARCOURIR")) {
      parcourir(null);
    }
  }

  /**
   *
   */
  private void ajouterFichier() {
    if (tb_fdiag.getRowCount() >= OscarLib.EFFORTS_NOMBRE_FICHIERS_DIAGRAMMES_MAXI) {
      WSpy.Error(OscarMsg.ERR018);
      return;
    }
    if (tb_fdiag.isEditing()) {
      tb_fdiag.getCellEditor().stopCellEditing();
    }
    dm_tb_fdiag.addRow(OscarLib.EmptyRow(dm_tb_fdiag.getColumnCount()));
    tb_fdiag.setRowSelectionInterval(tb_fdiag.getRowCount() - 1, tb_fdiag.getRowCount() - 1);
    if (tb_fdiag.getSelectedRowCount() <= 0) {
      bt_show.setEnabled(false);
      bt_search.setEnabled(false);
      bt_del.setEnabled(false);
    } else {
      bt_show.setEnabled(false);
      bt_search.setEnabled(false);
      bt_del.setEnabled(false);
    }
    if (tb_fdiag.getRowCount() >= OscarLib.EFFORTS_NOMBRE_FICHIERS_DIAGRAMMES_MAXI) {
      bt_add.setEnabled(false);
    } else {
      bt_add.setEnabled(true);
    }
  }

  /**
   *
   */
  private void supprimerFichier() {
    if (tb_fdiag.getSelectedRow() <= -1) {
      return;
    }
    if (tb_fdiag.isEditing()) {
      tb_fdiag.getCellEditor().stopCellEditing();
    }
    final int i = OscarLib.DialogConfirmation(getApplication(), OscarImplementation.informationsSoftware(), OscarLib
        .replaceKeyWordBy("file", (String) dm_tb_fdiag.getValueAt(tb_fdiag.getSelectedRow(), 0), OscarMsg.DLG015));
    if (i == JOptionPane.YES_OPTION) {
      dm_tb_fdiag.removeRow(tb_fdiag.getSelectedRow());
      if (tb_fdiag.getSelectedRowCount() <= 0) {
        bt_show.setEnabled(false);
        bt_search.setEnabled(false);
        bt_del.setEnabled(false);
      } else {
        final String _f = (String) dm_tb_fdiag.getValueAt(tb_fdiag.getSelectedRow(), 0);
        if ((_f == null) || (_f.equals(""))) {
          bt_show.setEnabled(false);
        } else {
          bt_show.setEnabled(true);
        }
        bt_search.setEnabled(true);
        bt_del.setEnabled(true);
      }
    }
  }

  /**
   *
   */
  private void visualiserFichier() {
    if (tb_fdiag.getSelectedRow() <= -1) {
      return;
    }
    if (tb_fdiag.isEditing()) {
      tb_fdiag.getCellEditor().stopCellEditing();
    }
    final int _n = tb_fdiag.getSelectedRow();
    final String _f = (String) dm_tb_fdiag.getValueAt(_n, 0);
    if (!OscarLib.ShowCsvFileWindow(getApplication(), _f, "Fichier de diagramme")) {
      OscarLib.DialogMessage(getApplication(), OscarImplementation.informationsSoftware(),
          "Le fichier de diagramme sp�cifi� n'existe pas.");
    }
  }

  /**
   *
   */
  private void parcourir(final String _mode) {
    // if(tb_fdiag.getSelectedRow()<=-1) return;
    if (tb_fdiag.isEditing()) {
      tb_fdiag.getCellEditor().stopCellEditing();
    }
    final String _f = OscarLib.getFileChoosenByUser(this, "Importer un fichier de diagramme", "Importer", null);
    try {
      final File _file = new File(_f);
      if (!_file.exists()) {
        OscarLib.DialogMessage(getApplication(), OscarImplementation.informationsSoftware(),
            "Le fichier de diagramme sp�cifi� n'existe pas.");
      } else {
        if ((tb_fdiag.getRowCount() <= -1) || ((_mode != null) && (_mode.equals("AJOUTER")))) {
          ajouterFichier();
        }
        if (tb_fdiag.getSelectedRow() <= -1) {
          tb_fdiag.setRowSelectionInterval(tb_fdiag.getRowCount() - 1, tb_fdiag.getRowCount() - 1);
        }
        final int _n = tb_fdiag.getSelectedRow();
        dm_tb_fdiag.setValueAt(_f, _n, 0);
        bt_show.setEnabled(true);
        bt_search.setEnabled(true);
        bt_del.setEnabled(true);
      }
    } catch (final NullPointerException _e1) {}
  }

  /**
   * retourne le nombre de fichiers contenues dans le tableau
   */
  public int getNbFichiers() {
    return tb_fdiag.getRowCount();
  }

  /**
   * retourne une structure de donn�es contenant les informations du fichier de diagramme _n du tableau. retourne null
   * si le fichier n'existe pas dans le tableau.
   * 
   * @param _n num�ro du fichier � retourner (commence � 1)
   */
  public SFichierDiagramme getFichier(final int _n) {
    SFichierDiagramme _c = null;
    try {
      _c = (SFichierDiagramme) OscarLib.createIDLObject(SFichierDiagramme.class);
      _c.fichier = (String) dm_tb_fdiag.getValueAt(_n - 1, 0);
      if (_c.fichier == null) {
        _c.fichier = "";
        _c.pointsCotePression = null;
      } else {
        final Vector _data = OscarLib.readCsvFile(_c.fichier);
        if (_data == null) {
          _c.pointsCotePression = null;
        } else {
          final Object[] _lines = _data.toArray();
          _c.pointsCotePression = new SPointCoteValeur[_lines.length - 1];
          // la premi�re ligne contient les titres de colonnes
          // on ne s'en occupe donc pas : i=1
          for (int i = 1; i < _lines.length; i++) {
            final Vector _v = (Vector) _lines[i];
            final Object[] _o = _v.toArray();
            for (int j = 0; j < _o.length; j = j + 2) {
              _c.pointsCotePression[i - 1] = new SPointCoteValeur();
              _c.pointsCotePression[i - 1].cote = ((Double) _o[j]).doubleValue();
              _c.pointsCotePression[i - 1].valeur = ((Double) _o[j + 1]).doubleValue();
            }
          }
        }
      }
    } catch (final Exception _e1) {
      WSpy.Error(OscarMsg.ERR019);
    }
    return _c;
  }

  /**
   * retourne une structure de donn�es contenant les informations de tous les fichiers.
   */
  public SFichierDiagramme[] getFichiers() {
    if (tb_fdiag.isEditing()) {
      tb_fdiag.getCellEditor().stopCellEditing();
    }
    SFichierDiagramme[] _c = null;
    final int _i1 = 1;
    final int _i2 = getNbFichiers();
    try {
      _c = new SFichierDiagramme[_i2 - _i1 + 1];
    } catch (final NegativeArraySizeException _e2) {
      WSpy.Error(OscarMsg.ERR020);
    }
    for (int i = (_i1 - 1); i < _i2; i++) {
      try {
        _c[i - (_i1 - 1)] = getFichier(i + 1);
      } catch (final NullPointerException _e1) {
        WSpy.Error(OscarMsg.ERR021);
      } catch (final ArrayIndexOutOfBoundsException _e2) {
        WSpy.Error(OscarMsg.ERR022);
        return null;
      }
    }
    return _c;
  }

  /**
   * charge les fichiers dans le tableau
   * 
   * @param _c tableau contenant les fichiers de diagrammes suppl�mentaires � importer
   */
  public synchronized void setFichiers(final SFichierDiagramme[] _c) {
    if (tb_fdiag.isEditing()) {
      tb_fdiag.getCellEditor().stopCellEditing();
    }
    viderTableau();
    int _n = 0;
    try {
      _n = _c.length;
    } catch (final NullPointerException _e1) {
      WSpy.Error(OscarMsg.ERR023);
    }
    for (int i = 0; i < _n; i++) {
      ajouterFichier();
      if (!_c[i].fichier.equals("")) {
        tb_fdiag.setValueAt(new String(_c[i].fichier), dm_tb_fdiag.getRowCount() - 1, 0);
      }
    }
  }

  /**
   * supprime tous les fichiers du tableau
   */
  private void viderTableau() {
    if (tb_fdiag.getRowCount() <= -1) {
      return;
    }
    if (tb_fdiag.isEditing()) {
      tb_fdiag.getCellEditor().stopCellEditing();
    }
    final int _max = tb_fdiag.getRowCount();
    for (int i = 0; i < _max; i++) {
      dm_tb_fdiag.removeRow(0);
    }
    if ((dm_tb_fdiag.getRowCount() == 0) || (tb_fdiag.getSelectedRowCount() <= 0)) {
      bt_del.setEnabled(false);
      bt_show.setEnabled(false);
      bt_search.setEnabled(false);
    } else if (tb_fdiag.getSelectedRowCount() > 0) {
      bt_del.setEnabled(true);
      bt_show.setEnabled(true);
      bt_search.setEnabled(true);
    } else {
      bt_del.setEnabled(false);
      bt_show.setEnabled(false);
      bt_search.setEnabled(false);
    }
    if (tb_fdiag.getRowCount() >= OscarLib.EFFORTS_NOMBRE_FICHIERS_DIAGRAMMES_MAXI) {
      bt_add.setEnabled(false);
    } else {
      bt_add.setEnabled(true);
    }
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT013);
    _mv.addMessages(validation().getMessages());
  }

  public ValidationMessages validation() {
    if (tb_fdiag.isEditing()) {
      tb_fdiag.getCellEditor().stopCellEditing();
    }
    final ValidationMessages _vm = new ValidationMessages();
    String _v;
    boolean _emptycell = false;
    boolean _dontexists = false;
    boolean _notordered = false;
    boolean _notenough = false;
    boolean _notwocols = false;
    final int _max = tb_fdiag.getRowCount();
    for (int i = 0; i < _max; i++) {
      _v = (String) dm_tb_fdiag.getValueAt(i, 0);
      if (_v == null) {
        _emptycell = true;
      } else if (!(new File(_v)).exists()) {
        _dontexists = true;
      } else {
        if (!OscarLib.verifierDeuxCotesMinimumDansFichierCsv(_v)) {
          _notenough = true;
        }
        if (!OscarLib.verifierCotesDecroissantesDansFichierCsv(_v)) {
          _notordered = true;
        }
        if (!OscarLib.verifierDeuxColonnesDansFichierCsv(_v)) {
          _notwocols = true;
        }
      }
    }
    if (_dontexists) {
      _vm.add(OscarMsg.VMSG021);
    }
    if (_emptycell) {
      _vm.add(OscarMsg.VMSG022);
    }
    if (_notenough) {
      _vm.add(OscarMsg.VMSG032);
    }
    if (_notordered) {
      _vm.add(OscarMsg.VMSG033);
    }
    if (_notwocols) {
      _vm.add(OscarMsg.VMSG051);
    }
    return _vm;
  }
}
