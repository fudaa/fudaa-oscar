/*
 * @file         OscarValidationParametres.java
 * @creation     2000-10-15
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.JPanel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;

/**
 * Description de l'onglet des parametres du validation.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarValidationParametres extends OscarAbstractOnglet {
  /**
   *
   */
  BuButton bt_valider;
  /**
   *
   */
  BuButton bt_fermer;

  /**
   *
   */
  public OscarValidationParametres(final OscarFilleParametres _fp, final String _helpfile) {
    super(_fp.getApplication(), _fp, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    // Bloc 'Informations'
    final BuPanel pn1 = OscarLib.InfoAidePanel(OscarMsg.LAB010, this);
    // Bloc Validation
    final BuPanel pn2 = new BuPanel();
    pn2.setLayout(OscarLib.VerticalLayout());
    pn2.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB025));
    final BuLabelMultiLine lb_val = OscarLib.LabelMultiLine(OscarMsg.LAB011);
    final BuPanel pn3 = OscarLib.FlowPanel();
    bt_valider = OscarLib.Button("Valider", "VALIDER", "VALIDER");
    bt_valider.addActionListener(this);
    bt_fermer = OscarLib.Button("Fermer", "FERMER", "FERMER");
    bt_fermer.addActionListener(this);
    pn3.add(bt_valider);
    pn3.add(bt_fermer);
    pn2.add(lb_val);
    pn2.add(pn3);
    // --
    _p.add(pn1);
    _p.add(pn2);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {
    if (_action.equals("VALIDER")) {
      valider();
    } else if (_action.equals("FERMER")) {
      fermer();
    }
  }

  /**
   *
   */
  private void valider() {
    ((OscarFilleParametres) getParentComponent()).valider();
  }

  /**
   *
   */
  private void fermer() {
    getImplementation().actionPerformed(new ActionEvent(this, 11, "FERMER"));
  }
}
