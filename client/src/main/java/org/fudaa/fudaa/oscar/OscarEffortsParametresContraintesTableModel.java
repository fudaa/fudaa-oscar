/*
 * @file         OscarEffortsParametresContraintesTableModel.java
 * @creation     2000-11-08
 * @modification $Date: 2007-01-19 11:13:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import javax.swing.table.DefaultTableModel;


/**
 * Mod�le de donn�es pour le tableau des contraintes (verticales et horizontales)
 * 
 * @version $Revision: 1.7 $ $Date: 2007-01-19 11:13:21 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public class OscarEffortsParametresContraintesTableModel extends DefaultTableModel {
  /**
   *
   */
  public OscarEffortsParametresContraintesTableModel(final int _mode) {
    this((_mode == OscarLib.EFFORTS_CVS) ? new Object[] { "Cote (m)", "Contrainte Verticale (kPa)" } : new Object[] {
        "Cote (m)", "Contrainte Horizontale (kPa)" });
  }

  /**
   * cr�e un mod�le de donn�es utilis� pour les tableaux de contraintes suppl�mentaires verticales et horizontales.
   * 
   * @param columnNames tableau d'objet contenant les noms des colonnes � cr�er
   */
  public OscarEffortsParametresContraintesTableModel(final Object[] columnNames) {
    super(columnNames, 0);
  }

  /**
   * d�finit si la cellule situ�e � l'emplacement sp�cifi�e est �ditable par l'utilisateur ou non.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public boolean isCellEditable(final int row, final int column) {
    return true;
  }

  /**
   * retourne l'objet valeur de la cellule sp�cifi�e.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public Object getValueAt(final int row, final int column) {
     
      return super.getValueAt(row, column);
  }

  /**
   * modifie la valeur de la cellule sp�cifi�e.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */
  public void setValueAt(final Object _o, final int row, final int column) {
    super.setValueAt(_o, row, column);
  }
}
