/*
 * @file         OscarEffortsTranchantsResultatsBruts.java
 * @creation     2000-10-15
 * @modification $Date: 2007-11-15 15:11:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableCellRenderer;

import org.fudaa.dodico.corba.oscar.SPointCoteValeur;
import org.fudaa.dodico.corba.oscar.SResultatsEffortsTranchants;


/**
 * Description de l'onglet 'Efforts Tranchants' de la fen�tre 'r�sultats bruts'.
 * 
 * @version $Revision: 1.7 $ $Date: 2007-11-15 15:11:49 $ by $Author: hadouxad $
 * @author Olivier Hoareau
 */
public class OscarEffortsTranchantsResultatsBruts extends OscarAbstractOnglet {
  /**
   *
   */
  BuTable tb_efforts_;
  /**
   *
   */
  BuButton bt_imprimer_;
  /**
   *
   */
  BuButton bt_sauvegarder_;
  /**
   *
   */
  BuButton bt_courbe_;
  /**
   *
   */
  DefaultTableModel dm_tb_efforts_;

  /**
   *
   */
  public OscarEffortsTranchantsResultatsBruts(final OscarFilleResultatsBruts _resultats, final String _helpfile) {
    super(_resultats.getApplication(), _resultats, _helpfile);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final BuPanel _p = new BuPanel();
    _p.setLayout(new BorderLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    bt_imprimer_ = OscarLib.Button("Imprimer", "IMPRIMER", "IMPRIMER");
    bt_sauvegarder_ = OscarLib.Button("Exporter", "ENREGISTRER", "SAUVEGARDER");
    bt_courbe_ = OscarLib.Button("Courbe", "VOIR", "COURBE");
    dm_tb_efforts_ = new OscarEffortsTranchantsResultatsBrutsTableModel(new Object[] { "Cote (m)",
        "Effort tranchant (kN)" });
    tb_efforts_ = OscarLib.Table(dm_tb_efforts_);
    // tb_efforts_.setPreferredScrollableViewportSize(new Dimension(OscarLib.TABLE_PREFERRED_WIDTH,187));
    final BuTableCellRenderer _cellrenderer = new OscarEffortsTranchantsResultatsBrutsTableCellRenderer();
    for (int i = 0; i < tb_efforts_.getColumnModel().getColumnCount(); i++) {
      tb_efforts_.getColumnModel().getColumn(i).setCellRenderer(_cellrenderer);
      tb_efforts_.getColumnModel().getColumn(i).setPreferredWidth((i == 0) ? 50 : 100);
    }
    final JScrollPane _sp = new JScrollPane(tb_efforts_);
    final BuPanel pn1 = OscarLib.InfoAidePanel(OscarMsg.LAB015, this);
    // Bloc 'Efforts Tranchants'
    final BuPanel pn2 = new BuPanel();
    pn2.setLayout(new BorderLayout());
    pn2.setBorder(OscarLib.TitledBorder(OscarMsg.TITLELAB027));
    final BuPanel pn3 = OscarLib.FlowPanel();
    bt_imprimer_.addActionListener(this);
    bt_sauvegarder_.addActionListener(this);
    bt_courbe_.addActionListener(this);
    pn3.add(bt_imprimer_);
    pn3.add(bt_sauvegarder_);
    pn3.add(bt_courbe_);
    pn2.add(pn3, BorderLayout.NORTH);
    pn2.add(_sp, BorderLayout.CENTER);
    _p.add(pn1, BorderLayout.NORTH);
    _p.add(pn2, BorderLayout.CENTER);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   */
  protected void action(final String _action) {
    if (_action.equals("IMPRIMER")) {
      imprimer();
    } else if (_action.equals("SAUVEGARDER")) {
      sauvegarder();
    } else if (_action.equals("COURBE")) {
      dessin();
    }
  }

  /**
   *
   */
  protected void sauvegarder() {
    final String _f = OscarLib.getFileChoosenByUser(this, "Exporter les donn�es brutes", "Exporter", OscarLib.USER_HOME
        + "efforts.csv");
    String _txt = "";
    if (_f == null) {
      return;
    }
    final SResultatsEffortsTranchants _sr = getResultatsEffortsTranchants();
    _txt += "cote;effort" + OscarLib.LINE_SEPARATOR;
    for (int i = 0; i < _sr.pointsCoteEffort.length; i++) {
      _txt += _sr.pointsCoteEffort[i].cote + ";" + _sr.pointsCoteEffort[i].valeur + OscarLib.LINE_SEPARATOR;
    }
    OscarLib.writeFile(_f, _txt);
  }

  /**
   * imprime la fen�tre.
   */
  protected void imprimer() {
    getImplementation().actionPerformed(new ActionEvent(this, 15, "IMPRIMER"));
  }

  /**
   * retourne le nombre de couples (cote, effort) contenus dans le tableau.
   * 
   * @return un entier repr�sentant le nombre de lignes du tableau
   */
  public int getNbPointEffortsTranchants() {
    return tb_efforts_.getRowCount();
  }

  /**
   * retourne une structure de donn�es contenant les informations du couple (cote, effort) _n du tableau. retourne null
   * si le couple n'existe pas dans le tableau.
   * 
   * @param _n num�ro du couple (cote, effort) � retourner (commence � 1)
   * @return une structure <code>SPointCoteValeur</code> contenant les infos du point
   */
  public SPointCoteValeur getPointEffortTranchant(final int _n) {
    SPointCoteValeur _p = null;
    try {
      _p = new SPointCoteValeur();
      _p.cote = Double.parseDouble(dm_tb_efforts_.getValueAt(_n - 1, 0).toString());
      _p.valeur = Double.parseDouble(dm_tb_efforts_.getValueAt(_n - 1, 1).toString());
    } catch (final ArrayIndexOutOfBoundsException _e1) {
      WSpy.Error(OscarMsg.ERR035);
    }
    return _p;
  }

  /**
   * retourne une structure de donn�es contenant les informations de tous les couples (cote, d�form�e).
   * 
   * @return une structure de donn�es <code>SResultatsEffortsTranchants</code>
   */
  public SResultatsEffortsTranchants getResultatsEffortsTranchants() {
    SPointCoteValeur[] _p = null;
    final int _i1 = 1;
    final int _i2 = getNbPointEffortsTranchants();
    try {
      _p = new SPointCoteValeur[_i2 - _i1 + 1];
    } catch (final NegativeArraySizeException _e2) {
      WSpy.Error(OscarMsg.ERR036);
    }
    for (int i = (_i1 - 1); i < _i2; i++) {
      try {
        _p[i - (_i1 - 1)] = getPointEffortTranchant(i + 1);
      } catch (final NullPointerException _e1) {
        WSpy.Error(OscarMsg.ERR037);
      } catch (final ArrayIndexOutOfBoundsException _e2) {
        WSpy.Error(OscarMsg.ERR038);
        return null;
      }
    }
    final SResultatsEffortsTranchants _srd = new SResultatsEffortsTranchants();
    _srd.pointsCoteEffort = _p;
    return _srd;
  }

  /**
   * charge les couples (cote, d�form�e) sp�cifi�es dans le tableau des couples
   * 
   * @param _p structure <code>SResultatsDeformees</code> contenant les couples (cote, d�form�e) � importer.
   */
  public void setResultatsEffortsTranchants(SResultatsEffortsTranchants _p) {
    viderTableau();
    if (_p == null) {
      _p = (SResultatsEffortsTranchants) OscarLib.createIDLObject(SResultatsEffortsTranchants.class);
    }
    int _n = 0;
    try {
      _n = _p.pointsCoteEffort.length;
    } catch (final NullPointerException _e1) {
      WSpy.Error(OscarMsg.ERR039);
    }
    for (int i = 0; i < _n; i++) {
      dm_tb_efforts_.addRow(OscarLib.EmptyRow(dm_tb_efforts_.getColumnCount()));
      tb_efforts_.setValueAt(OscarLib.formatterNombre(_p.pointsCoteEffort[i].cote), dm_tb_efforts_.getRowCount() - 1, 0);
      tb_efforts_.setValueAt(OscarLib.formatterNombre("effort", _p.pointsCoteEffort[i].valeur), dm_tb_efforts_.getRowCount() - 1, 1);
    }
  }

  /**
   * supprime tous les couples (cote, d�form�e) du tableau des couples
   */
  private void viderTableau() {
    if (tb_efforts_.isEditing()) {
      tb_efforts_.getCellEditor().stopCellEditing();
    }
    final int _max = tb_efforts_.getRowCount();
    for (int i = 0; i < _max; i++) {
      dm_tb_efforts_.removeRow(0);
    }
  }

  /**
   *
   */
  public Double getCoteMin() {
    SPointCoteValeur _point = null;
    Double _min = null;
    if (getNbPointEffortsTranchants() > 0) {
      _min = new Double(Double.MAX_VALUE);
      for (int i = 0; i < getNbPointEffortsTranchants(); i++) {
        _point = getPointEffortTranchant(i + 1);
        if ((_point != null) && (_point.cote != OscarLib.DOUBLE_NULL) && (_point.cote < _min.doubleValue())) {
          _min = new Double(_point.cote);
        }
      }
    }
    return _min;
  }

  /**
   *
   */
  public Double getCoteMax() {
    SPointCoteValeur _point = null;
    Double _max = null;
    if (getNbPointEffortsTranchants() > 0) {
      _max = new Double(Double.MIN_VALUE);
      for (int i = 0; i < getNbPointEffortsTranchants(); i++) {
        _point = getPointEffortTranchant(i + 1);
        if ((_point != null) && (_point.cote != OscarLib.DOUBLE_NULL) && (_point.cote > _max.doubleValue())) {
          _max = new Double(_point.cote);
        }
      }
    }
    return _max;
  }

  /**
   *
   */
  public Double getValeurMin() {
    SPointCoteValeur _point = null;
    Double _min = null;
    if (getNbPointEffortsTranchants() > 0) {
      _min = new Double(Double.MAX_VALUE);
      for (int i = 0; i < getNbPointEffortsTranchants(); i++) {
        _point = getPointEffortTranchant(i + 1);
        if ((_point != null) && (_point.valeur != OscarLib.DOUBLE_NULL) && (_point.valeur < _min.doubleValue())) {
          _min = new Double(_point.valeur);
        }
      }
    }
    return _min;
  }

  /**
   *
   */
  public Double getValeurMax() {
    SPointCoteValeur _point = null;
    Double _max = null;
    if (getNbPointEffortsTranchants() > 0) {
      _max = new Double(Double.MIN_VALUE);
      for (int i = 0; i < getNbPointEffortsTranchants(); i++) {
        _point = getPointEffortTranchant(i + 1);
        if ((_point != null) && (_point.valeur != OscarLib.DOUBLE_NULL) && (_point.valeur > _max.doubleValue())) {
          _max = new Double(_point.valeur);
        }
      }
    }
    return _max;
  }

  /**
   *
   */
  protected void dessin() {
    changeOnglet((ChangeEvent) null);
    OscarLib.showDialogGraphViewer(getApplication(), "R�sultats bruts : Efforts tranchants",
        getEffortsTranchantsGrapheScript());
  }

  /**
   *
   */
  public String getEffortsTranchantsGrapheScript() {
    double _min;
    double _max;
    double _xmin;
    double _xmax;
    final Double _dmin = getCoteMin();
    final Double _dmax = getCoteMax();
    final Double _dxmin = getValeurMin();
    final Double _dxmax = getValeurMax();
    if (_dmin == null) {
      _min = -1.0;
    } else {
      _min = _dmin.doubleValue();
    }
    if (_dmax == null) {
      _max = 1.0;
    } else {
      _max = _dmax.doubleValue();
    }
    if (_dxmin == null) {
      _xmin = 0.0;
    } else {
      _xmin = _dxmin.doubleValue();
    }
    if (_dxmax == null) {
      _xmax = 0.0;
    } else {
      _xmax = _dxmax.doubleValue();
    }
    return getEffortsTranchantsGrapheScript(_xmin, _xmax, _min, _max);
  }

  /**
   *
   */
  public String getEffortsTranchantsGrapheScript(final double _xmin, final double _xmax, final double _min,
      final double _max) {
    final StringBuffer _s = new StringBuffer(4096);
    _s.append("graphe \n{\n");
    _s.append("  titre \"Efforts tranchants\"\n");
    _s.append("  sous-titre \"R�sultats\"\n");
    _s.append("  legende oui\n");
    _s.append("  animation non\n");
    _s.append("  marges\n{\n");
    _s.append("    gauche 80\n");
    _s.append("    droite 120\n");
    _s.append("    haut 50\n");
    _s.append("    bas 30\n");
    _s.append("  }\n");
    _s.append("  \n");
    _s.append("  axe\n{\n");
    _s.append("    titre \"Effort\"\n");
    _s.append("    unite \"kN\"\n");
    _s.append("    orientation horizontal\n");
    _s.append("    graduations oui\n");
    _s.append("    minimum " + _xmin + "\n");
    _s.append("    maximum " + _xmax + "\n");
    _s.append("  }\n");
    _s.append("  axe\n{\n");
    _s.append("    titre \"Cote\"\n");
    _s.append("    unite \"m\"\n");
    _s.append("    orientation vertical\n");
    _s.append("    graduations oui\n");
    _s.append("    minimum " + _min + "\n");
    _s.append("    maximum " + _max + "\n");
    _s.append("  }\n");
    _s.append(getEffortsTranchantsCourbeGrapheScript(_xmin, _xmax, _min, _max));
    _s.append("}\n");
    return _s.toString();
  }

  /**
   *
   */
  public String getEffortsTranchantsCourbeGrapheScript(final double _xmin, final double _xmax, final double _ymin,
      final double _ymax) {
    final StringBuffer _s = new StringBuffer(4096);
    SPointCoteValeur _point;
    int i;
    if (getNbPointEffortsTranchants() > 0) {
      _s.append("  courbe\n{\n");
      _s.append("    marqueurs non\n");
      _s.append("    type lineaire\n");
      _s.append("    valeurs\n{\n");
      for (i = 0; i < getNbPointEffortsTranchants(); i++) {
        _point = getPointEffortTranchant(i + 1);
        if (_point != null) {
          _s.append("      " + _point.valeur + " " + _point.cote + "\n");
        }
      }
      _s.append("    }\n");
      _s.append("  }\n");
      _s.append("  courbe\n{\n");
      _s.append("    marqueurs non\n");
      _s.append("    type lineaire\n");
      _s.append("    valeurs\n{\n");
      _s.append("      0.0 " + _ymin + "\n");
      _s.append("      0.0 " + _ymax + "\n");
      _s.append("    }\n");
      _s.append("    aspect\n{\n");
      _s.append("      contour.couleur 000000\n");
      _s.append("    }\n");
      _s.append("  }\n");
    }
    return _s.toString();
  }
  /**
   *
   */
  class OscarEffortsTranchantsResultatsBrutsTableCellRenderer extends BuTableCellRenderer {
    /**
     *
     */
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
        final boolean hasFocus, final int row, final int column) {
      Component _r = null;
      BuLabel _lb = null;
      _lb = (BuLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      _r = _lb;
      if (row >= 0) {
        if (value instanceof Double) {}
      }
      return _r;
    }
  }
  /**
   *
   */
  public class OscarEffortsTranchantsResultatsBrutsTableModel extends DefaultTableModel {
    /**
     *
     */
    public OscarEffortsTranchantsResultatsBrutsTableModel(final Object[] columnNames) {
      super(columnNames, 0);
    }

    /**
     *
     */
    public boolean isCellEditable(final int row, final int column) {
      return false;
    }

    /**
     *
     */
    public Object getValueAt(final int row, final int column) {
      Object _o = null;
      _o = super.getValueAt(row, column);
      return _o;
    }

    /**
     *
     */
    public void setValueAt(final Object _o, final int row, final int column) {
      super.setValueAt(_o, row, column);
    }
  }
}
