/*
 * @file         OscarResultatsBrutsTableModel.java
 * @creation     2000-11-20
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import javax.swing.table.DefaultTableModel;

/**
 * objet mod�le de donn�es pour les tableaux des r�sultats bruts. d�finit un mod�le de donn�es non �ditable pour les
 * cellules des tableaux.
 * 
 * @version $Revision: 1.5 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarResultatsBrutsTableModel extends DefaultTableModel {
  /**
   * construit le mod�le de donn�es avec les ent�tes de colonnes apropri�es.
   */
  public OscarResultatsBrutsTableModel(final Object[] _headers, final int _numrows) {
    super(_headers, _numrows);
    WSpy.Enter("ctor" + " " + getClass().getName() + "::" + "OscarResultatsBrutsTableModel()");
    // ...
    WSpy.Exit("ctor" + " " + getClass().getName() + "::" + "OscarResultatsBrutsTableModel()");
  }

  /**
   * d�finit quels sont les cellules �ditable du tableau. aucune cellule �ditable dans ce mod�le de donn�es.
   */
  public boolean isCellEditable(final int row, final int col) {
    WSpy.EnterFonction("boolean" + " " + getClass().getName() + "::" + "isCellEditable(int,int)");
    final boolean _b = false;
    WSpy.ExitFonction("boolean" + " " + getClass().getName() + "::" + "isCellEditable(int,int)");
    return _b;
  }
} // class OscarResultatsBrutsTableModel
