/*
 * @file         OscarDialogNoteDeCalculsConfig.java
 * @creation     2002-10-23
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuInformationsSoftware;

import org.fudaa.fudaa.commun.projet.FudaaProjet;


/**
 * une bo�te de dialogue pour l'�dition des pr�f�rences pour la g�n�ration de la note de calculs.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarDialogNoteDeCalculsConfig extends BuDialog implements ActionListener {
  WTree listconfig_;
  boolean close_;
  OscarFilleNoteDeCalculs.Settings settings_;

  /**
   * constructeur de la boite de dialogue
   */
  public OscarDialogNoteDeCalculsConfig(final BuCommonInterface _parent, final BuInformationsSoftware _isoft) {
    super(_parent, _isoft, "Personnalisation de la note de calculs");
    close_ = false;
    setSize(700, 400);
    setLocation(50, 100);
    settings_ = OscarFilleNoteDeCalculs.createDefaultSettings();
    pack();
  }

  public boolean needsScrollPane() {
    return false;
  }

  /**
   * lorsque l'utilisateur clic sur un des boutons
   */
  public void actionPerformed(final ActionEvent _evt) {
  // String action = _evt.getActionCommand();
  }

  /**
   *
   */
  protected void setFichier(final String _f) {
    settings_.stocke("NOM_DU_FICHIER_HTML", _f);
  }

  /**
   * retourne le composant
   */
  public JComponent getComponent() {
    final JPanel _p = new JPanel();
    _p.setLayout(new BorderLayout());
    _p.setBorder(new EmptyBorder(5, 5, 5, 5));
    listconfig_ = new WTree((FudaaProjet) null);
    final JScrollPane _sp = new JScrollPane(listconfig_);
    _p.add(_sp, BorderLayout.CENTER);
    final JPanel _buttons = new JPanel();
    _buttons.setLayout(new FlowLayout());
    final JButton _bt_save = OscarLib.Button("Enregistrer et afficher", "ENREGISTRER", "ENREGISTRER");
    final JButton _bt_display = OscarLib.Button("Afficher", "VOIR", "AFFICHER");
    final JButton _bt_close = OscarLib.Button("Fermer", "ANNULER", "FERMER");
    _buttons.add(_bt_display);
    _buttons.add(_bt_save);
    _buttons.add(_bt_close);
    _p.add(_buttons, BorderLayout.SOUTH);
    _bt_close.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        closeDialog(true);
      }
    });
    _bt_save.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        final String _f = obtainFileFromUser();
        if (_f != null) {
          setFichier(_f);
        }
        setVisible(false);
      }
    });
    _bt_display.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _evt) {
        closeDialog(false);
      }
    });
    return _p;
  }

  /**
   *
   */
  protected String obtainFileFromUser() {
    return OscarLib.getFileChoosenByUser(this);
  }

  /**
   *
   */
  protected void closeDialog(final boolean _v) {
    close_ = _v;
    setVisible(false);
  }

  /**
   * renvoie une structure contenant les pr�f�rences choisies par l'utilisateur pour la g�n�ration de la note de
   * calculs.
   */
  public OscarFilleNoteDeCalculs.Settings getConfig() {
    setVisible(true);
    OscarFilleNoteDeCalculs.Settings _s = null;
    if (!close_) {
      WSpy.Spy("settings_ = " + settings_);
      WSpy.Spy("listconfig_ = " + listconfig_);
      WSpy.Spy("listconfig_.getConfig() = " + listconfig_.getConfig());
      settings_.add(listconfig_.getConfig());
      _s = settings_;
    }
    return _s;
  }
} // class OscarDialogNoteDeCalculsConfig
