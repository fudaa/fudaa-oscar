/*
 * @file         OscarResource.java
 * @creation     2002-10-07
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import com.memoire.bu.BuResource;

/**
 * Le gestionnaire de ressources de Oscar.
 * 
 * @version $Revision: 1.5 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarResource extends BuResource {
  public final static String PARAMETRES = "Parametres";
  public final static String RESULTATS = "Resultats";
  public final static String NOTEDECALCULS = "NoteDeCalculs";
  // Renvoie un nouvel objet OscarResource
  public final static OscarResource OSCAR = new OscarResource();
}
