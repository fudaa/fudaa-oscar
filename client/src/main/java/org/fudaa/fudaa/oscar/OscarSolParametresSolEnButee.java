/*
 * @file         OscarSolParametresSolEnButee.java
 * @creation     2000-10-15
 * @modification $Date: 2007-05-04 13:59:30 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.oscar.SCoucheSol;
import org.fudaa.dodico.corba.oscar.VALEUR_NULLE;


/**
 * Impl�mentation du sous-onglet 'Sol en but�e' de l'onglet des parametres du sol.
 * 
 * @version $Revision: 1.12 $ $Date: 2007-05-04 13:59:30 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarSolParametresSolEnButee extends OscarAbstractOnglet {
  /**
   * champs de saisie de la cote du toit du sol c�t� but�e
   */
  private BuTextField tf_cot_sol_;
  /**
   * bouton d'ajout d'une couche dans le tableau de but�e
   */
  private BuButton bt_add_;
  /**
   * bouton de suppression d'une couche dans le tableau de but�e
   */
  private BuButton bt_del_;
  /**
   * bouton d'affichage de la l�gende du tableau de but�e
   */
  private BuButton bt_legend_;
  /**
   * bouton d'affichage de la calculatrice pour Kp (but�e)
   */
  private BuButton bt_calc_;
  /**
   * tableau contenant les informations sur les couches en but�e
   */
  private BuTable tb_couches_;
  /**
   * mod�le de donn�es du tableau contenant les informations sur les couches en but�e
   */
  private DefaultTableModel dm_tb_couches_;
  /**
   * bloc 1 : cote du sol
   */
  private BuPanel b1_;
  /**
   * bloc 2 : informations sur les couches
   */
  private BuPanel b2_;

  /**
   * construit un sous-onglet de l'onglet sol permettant d'ajouter des couches en but�e dans un tableau
   * 
   * @param _sol r�f�rence vers l'onglet sol parent
   */
  public OscarSolParametresSolEnButee(final OscarSolParametres _sol, final String _helpfile) {
    super(_sol.getApplication(), _sol, _helpfile);
  }

  protected final void firePropertyChange(final String _propertyName, final Object _oldValue, final Object _newValue) {
    super.firePropertyChange(_propertyName, _oldValue, _newValue);
  }

  /**
   *
   */
  protected JComponent construireGUI() {
    final JPanel _p = new JPanel();
    _p.setLayout(OscarLib.VerticalLayout());
    _p.setBorder(OscarLib.EmptyBorder());
    tf_cot_sol_ = OscarLib.DoubleField(OscarMsg.FORMAT010);
    bt_add_ = OscarLib.Button("Ajouter", "OUI", "AJOUTER");
    bt_del_ = OscarLib.Button("Supprimer", "NON", "SUPPRIMER");
    bt_legend_ = OscarLib.Button("L�gende", "AIDE", "LEGENDE");
    bt_calc_ = OscarLib.Button("Calculatrice Kp", "EXECUTER", "CALCULATRICE");
    dm_tb_couches_ = new OscarSolParametresCouchesTableModel(OscarLib.BUTEE);
    tb_couches_ = OscarLib.Table(dm_tb_couches_);
    tb_couches_.getTableHeader().setDefaultRenderer(new OscarSolParametresCouchesTableCellRenderer());
    // tb_couches_.getColumnModel().getColumn(1).setCellEditor(new OscarDoubleValueCellEditor( "#0.00" , 0.01 ,
    // Double.MAX_VALUE ));
    // tb_couches_.getColumnModel().getColumn(2).setCellEditor(new OscarDoubleValueCellEditor( "#0.0" , 0.0 ,
    // Double.MAX_VALUE ));
    // tb_couches_.getColumnModel().getColumn(3).setCellEditor(new OscarDoubleValueCellEditor( "#0.0" , 0.0 ,
    // Double.MAX_VALUE ));
    // tb_couches_.getColumnModel().getColumn(4).setCellEditor(new OscarDoubleValueCellEditor( "#0.000" , 0.0 ,
    // Double.MAX_VALUE ));
    // tb_couches_.getColumnModel().getColumn(5).setCellEditor(new OscarDoubleValueCellEditor( "#0.0" , 0.0 ,
    // Double.MAX_VALUE ));
    // tb_couches_.getColumnModel().getColumn(6).setCellEditor(new OscarDoubleValueCellEditor( "#0.0" , 0.0 ,
    // Double.MAX_VALUE ));
    // tb_couches_.getColumnModel().getColumn(0).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(0));
    // tb_couches_.getColumnModel().getColumn(1).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(2));
    // tb_couches_.getColumnModel().getColumn(2).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(1));
    // tb_couches_.getColumnModel().getColumn(3).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(1));
    // tb_couches_.getColumnModel().getColumn(4).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(3));
    // tb_couches_.getColumnModel().getColumn(5).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(1));
    // tb_couches_.getColumnModel().getColumn(6).setCellRenderer(new OscarSolParametresCouchesTableCellRenderer(1));
    tb_couches_.getColumnModel().getColumn(0).setPreferredWidth(20);
    b2_ = OscarLib.TitledPanel(OscarMsg.TITLELAB009);
    bt_add_.addActionListener(this);
    bt_del_.addActionListener(this);
    bt_legend_.addActionListener(this);
    bt_calc_.addActionListener(this);
    tb_couches_.addMouseListener(new MouseAdapter() {
      public void mouseReleased(final MouseEvent _evt) {
        clickedOnTable();
      }
    });
    tf_cot_sol_.addKeyListener(new KeyAdapter() {
      public void keyReleased(final KeyEvent _evt) {
        firePropertyChange("COTE_TERRE_PLEIN_BUTEE", (Double) null, getCoteTerrePleinButee());
      }
    });
    bt_del_.setEnabled(false);
    bt_calc_.setEnabled(false);
    // Bloc Cote du sol
    b1_ = OscarLib.TitledPanel(OscarMsg.TITLELAB008);
    b1_.setLayout(OscarLib.GridLayout(4));
    final BuLabel _lb_cot_sol = OscarLib.Label(OscarMsg.LAB030);
    final BuLabel _lb_cot_sol_egal = OscarLib.Label(OscarMsg.MATHLAB_EQUAL);
    final BuLabel _lb_cot_sol_unit = OscarLib.Label(OscarMsg.UNITLAB_METRE);
    b1_.add(_lb_cot_sol);
    b1_.add(_lb_cot_sol_egal);
    b1_.add(tf_cot_sol_);
    b1_.add(_lb_cot_sol_unit);
    // Bloc Informations sur les couches
    final JScrollPane _sp1 = new JScrollPane(tb_couches_);
    final BuPanel _b2a = OscarLib.HFlowPanel();
    _b2a.add(bt_add_);
    _b2a.add(bt_del_);
    _b2a.add(bt_legend_);
    _b2a.add(bt_calc_);
    b2_.add(_sp1);
    b2_.add(_b2a);
    // Ajout des blocs au conteneur principal
    _p.add(b1_);
    _p.add(b2_);
    return _p;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action
   * 
   * @param _evt �v�nement qui a engendr� l'action
   */
  protected void action(final String _action) {
    if (_action.equals("AJOUTER")) {
      ajouterCouche();
    } else if (_action.equals("SUPPRIMER")) {
      supprimerCouche();
    } else if (_action.equals("LEGENDE")) {
      legendeCouche();
    } else if (_action.equals("CALCULATRICE")) {
      calculatriceKp();
    }
  }

  /**
   *
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    final OscarParamEventView _mv = getImplementation().getParamEventView();
    _mv.removeAllMessagesInCategory(OscarMsg.VMSGCAT003);
    _mv.addMessages(validation().getMessages());
  }

  /**
   * m�thode appell�e lors d'un changement de propri�t� espionn�e par ce composant.
   * 
   * @param evt �v�nement qui d�crit le changement de propri�t�
   */
  public void propertyChange(final PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("SOL_TYPE_CALCUL")) {
      // int _old= ((Integer)evt.getOldValue()).intValue();
      final int _new = ((Integer) evt.getNewValue()).intValue();
      switch (_new) {
      case OscarLib.SOL_CALCUL_EFFECTIF: {
        tb_couches_.getColumnModel().getColumn(3).setHeaderValue("header-gamma-prime");
        tb_couches_.getColumnModel().getColumn(5).setHeaderValue("header-c-prime");
        tb_couches_.getColumnModel().getColumn(6).setHeaderValue("header-phi-prime");
        break;
      }
      case OscarLib.SOL_CALCUL_TOTAL: {
        tb_couches_.getColumnModel().getColumn(3).setHeaderValue("header-gamma-sat");
        tb_couches_.getColumnModel().getColumn(5).setHeaderValue("header-c-u");
        tb_couches_.getColumnModel().getColumn(6).setHeaderValue("header-phi-u");
        break;
      }
      }
      // dm_tb_couches_.fireTableDataChanged();
      // tb_couches_.repaint();
    }
    repaint();
  }

  /**
   * ajoute une couche sol en but�e vierge dans le tableau des couches
   */
  private void ajouterCouche() {
    if (tb_couches_.isEditing()) {
      tb_couches_.getCellEditor().stopCellEditing();
    }
    dm_tb_couches_.addRow(OscarLib.EmptyRow(dm_tb_couches_.getColumnCount()));
    tb_couches_.setValueAt(new Integer(dm_tb_couches_.getRowCount()), dm_tb_couches_.getRowCount() - 1, 0);
    if (dm_tb_couches_.getRowCount() > 0) {
      // bt_del_.setEnabled(true);
      // tb_couches_.setRowSelectionInterval(tb_couches_.getRowCount()-1,tb_couches_.getRowCount()-1);
      // tb_couches_.editCellAt(dm_tb_couches_.getRowCount()-1,1);
    }
    clickedOnTable();
    if (dm_tb_couches_.getRowCount() == 1) {
      firePropertyChange("COUCHES_BUTEE_DISPONIBLE", false, true);
    }
  }

  /**
   * supprime la couche sol en but�e s�lectionn�e dans le tableau des couches. Ne supprime rien du tableau si aucune
   * couche n'est s�lectionn� dans celui-ci.
   */
  private void supprimerCouche() {
    if (tb_couches_.getSelectedRow() <= -1) {
      return;
    }
    if (tb_couches_.isEditing()) {
      tb_couches_.getCellEditor().stopCellEditing();
    }
    dm_tb_couches_.removeRow(tb_couches_.getSelectedRow());
    for (int i = 0; i < dm_tb_couches_.getRowCount(); i++) {
      tb_couches_.setValueAt(CtuluLibString.getString(i + 1), i, 0);
    }
    if (tb_couches_.getSelectedRowCount() <= 0) {
      bt_del_.setEnabled(false);
      bt_calc_.setEnabled(false);
    }
    if (dm_tb_couches_.getRowCount() == 0) {
      firePropertyChange("COUCHES_BUTEE_DISPONIBLE", true, false);
    }
  }

  /**
   * affiche la l�gende des colonnes du tableau couches de sol en but�e
   */
  private void legendeCouche() {
    if (tb_couches_.isEditing()) {
      tb_couches_.getCellEditor().stopCellEditing();
    }
    ((OscarSolParametres) getParentComponent()).getFilleParametres().getApplication().getImplementation().displayURL(
        OscarLib.helpUrl(OscarMsg.URL004));
  }

  /**
   * affiche la calculatrice Kp et stocke la valeur calcul�e par l'utilisateur dans la cellule Kp de la ligne de la
   * couche s�lectionn�e du tableau.
   */
  private void calculatriceKp() {
    if (tb_couches_.isEditing()) {
      tb_couches_.getCellEditor().stopCellEditing();
    }
    final int _mode = (getTypeCalcul() == OscarLib.SOL_CALCUL_EFFECTIF) ? OscarLib.CALCULATRICEK_BUTEE_EFFECTIVE
        : ((getTypeCalcul() == OscarLib.SOL_CALCUL_TOTAL) ? OscarLib.CALCULATRICEK_BUTEE_TOTALE : 0);
    final Double _kp = OscarLib.CalculatriceK((OscarSolParametres) getParentComponent(), _mode);
    if(_kp!=null){
      tb_couches_.setValueAt(_kp, tb_couches_.getSelectedRow(), 4);
    }
    //fre equals(null) est faux ....
/*    try {
      _kp.equals(null);
      tb_couches_.setValueAt(_kp, tb_couches_.getSelectedRow(), 4);
    } catch (final NullPointerException _e1) {}*/
  }

  /**
   * retourne le type de calcul en cours (en contraintes effectives ou en contraintes totales). les valeurs retourn�es
   * peuvent �tre : OscarLib.SOL_CALCUL_EFFECTIF ou OscarLib.SOL_CALCUL_TOTAL.
   */
  private int getTypeCalcul() {
    return ((OscarSolParametres) getParentComponent()).getTypeCalcul();
  }

  /**
   * execut�e lorsque l'utilisateur clique sur une ligne du tableau des couches de sol en pouss�e.
   */
  protected void clickedOnTable() {
    if (tb_couches_.getSelectedRowCount() > 0) {
      bt_calc_.setEnabled(true);
      bt_del_.setEnabled(true);
    } else {
      bt_calc_.setEnabled(false);
      bt_del_.setEnabled(false);
    }
  }

  /**
   * retourne le nombre de couches contenues dans le tableau
   */
  public int getNbCouches() {
    return tb_couches_.getRowCount();
  }

  /**
   * retourne une structure de donn�es contenant les informations sur la cote du toit du sol c�t� but�e.
   */
  public Double getCoteTerrePleinButee() {
    Double _v = null;
    try {
      _v = (Double) tf_cot_sol_.getValue();
    } catch (final NumberFormatException _e1) {}
    return _v;
  }

  /**
   * importe une structure de donn�es contenant les informations sur la cote du toit du sol c�t� but�e.
   * 
   * @param _c valeur de la cote � importer
   */
  public void setCoteTerrePleinButee(final Double _c) {
    try {
      tf_cot_sol_.setValue(_c);
    } catch (final NullPointerException _e1) {}
  }

  /**
   *
   */
  public Double getEpaisseurTotale() {
    Double _epaisseur = null;
    Double _v;
    if (getNbCouches() > 0) {
      _epaisseur = new Double(0.0);
      for (int i = 0; i < getNbCouches(); i++) {
        try {
          _v = (Double) dm_tb_couches_.getValueAt(i, 1);
        } catch (final Exception _e1) {
          _v = null;
        }
        if (_v != null) {
          _epaisseur = new Double(_epaisseur.doubleValue() + _v.doubleValue());
        }
      }
    }
    return _epaisseur;
  }

  /**
   * retourne une structure de donn�es contenant les informations de la couche _n du tableau. retourne null si la couche
   * n'existe pas dans le tableau.
   * 
   * @param _n num�ro de la couche � retourner (commence � 1)
   */
  public SCoucheSol getCouche(final int _n) {
    SCoucheSol _c = null;
    try {
      _c = (SCoucheSol) OscarLib.createIDLObject(SCoucheSol.class);
      try {
        _c.epaisseur = ((Double) dm_tb_couches_.getValueAt(_n - 1, 1)).doubleValue();
      } catch (final Exception _e2) {
        _c.epaisseur = VALEUR_NULLE.value;
      }
      try {
        _c.poidsVolumiqueHumide = ((Double) dm_tb_couches_.getValueAt(_n - 1, 2)).doubleValue();
      } catch (final Exception _e2) {
        _c.poidsVolumiqueHumide = VALEUR_NULLE.value;
      }
      try {
        _c.poidsVolumique = ((Double) dm_tb_couches_.getValueAt(_n - 1, 3)).doubleValue();
      } catch (final Exception _e1) {
        _c.poidsVolumique = VALEUR_NULLE.value;
      }
      try {
        _c.coefficient = ((Double) dm_tb_couches_.getValueAt(_n - 1, 4)).doubleValue();
      } catch (final Exception _e2) {
        _c.coefficient = VALEUR_NULLE.value;
      }
      try {
        _c.cohesion = ((Double) dm_tb_couches_.getValueAt(_n - 1, 5)).doubleValue();
      } catch (final Exception _e2) {
        _c.cohesion = VALEUR_NULLE.value;
      }
      try {
        _c.angleFrottement =

        ((Double) dm_tb_couches_.getValueAt(_n - 1, 6)).doubleValue();

      } catch (final Exception _e2) {
        _c.angleFrottement = VALEUR_NULLE.value;
      }
    } catch (final ArrayIndexOutOfBoundsException _e1) {
      WSpy.Error(OscarMsg.ERR004);
    }
    return _c;
  }

  /**
   * retourne une structure de donn�es contenant les informations de toutes les couches.
   */
  public SCoucheSol[] getCouches() {
    if (tb_couches_.isEditing()) {
      tb_couches_.getCellEditor().stopCellEditing();
    }
    SCoucheSol[] _c = null;
    final int _i1 = 1;
    final int _i2 = getNbCouches();
    try {
      _c = new SCoucheSol[_i2 - _i1 + 1];
    } catch (final NegativeArraySizeException _e2) {
      WSpy.Error(OscarMsg.ERR005);
    }
    for (int i = (_i1 - 1); i < _i2; i++) {
      try {
        _c[i - (_i1 - 1)] = getCouche(i + 1);
      } catch (final NullPointerException _e1) {
        WSpy.Error(OscarMsg.ERR006);
      } catch (final ArrayIndexOutOfBoundsException _e2) {
        WSpy.Error(OscarMsg.ERR007);
        return null;
      }
    }
    return _c;
  }

  /**
   * charge les couches de sol sp�cifi�es dans le tableau des couches de sol
   * 
   * @param _c tableau contenant les couches de sol � importer
   */
  public synchronized void setCouches(final SCoucheSol[] _c) {
    if (tb_couches_.isEditing()) {
      tb_couches_.getCellEditor().stopCellEditing();
    }
    viderTableau();
    int _n = 0;
    try {
      _n = _c.length;
      for (int i = 0; i < _n; i++) {
        ajouterCouche();
        if (_c[i].epaisseur != VALEUR_NULLE.value) {
          tb_couches_.setValueAt(new Double(_c[i].epaisseur), dm_tb_couches_.getRowCount() - 1, 1);
        }
        if (_c[i].poidsVolumiqueHumide != VALEUR_NULLE.value) {
          tb_couches_.setValueAt(new Double(_c[i].poidsVolumiqueHumide), dm_tb_couches_.getRowCount() - 1, 2);
        }
        if (_c[i].poidsVolumique != VALEUR_NULLE.value) {
          tb_couches_.setValueAt(new Double(_c[i].poidsVolumique), dm_tb_couches_.getRowCount() - 1, 3);
        }
        if (_c[i].coefficient != VALEUR_NULLE.value) {
          tb_couches_.setValueAt(new Double(_c[i].coefficient), dm_tb_couches_.getRowCount() - 1, 4);
        }
        if (_c[i].cohesion != VALEUR_NULLE.value) {
          tb_couches_.setValueAt(new Double(_c[i].cohesion), dm_tb_couches_.getRowCount() - 1, 5);
        }
        if (_c[i].angleFrottement != VALEUR_NULLE.value) {
          tb_couches_.setValueAt(new Double(_c[i].angleFrottement), dm_tb_couches_.getRowCount() - 1, 6);
        }
      }
    } catch (final NullPointerException _e1) {
      WSpy.Error(OscarMsg.ERR008);
    }
  }

  /**
   * supprime toutes les couches du tableau des couches
   */
  private void viderTableau() {
    if (tb_couches_.getRowCount() <= -1) {
      return;
    }
    if (tb_couches_.isEditing()) {
      tb_couches_.getCellEditor().stopCellEditing();
    }
    final int _max = tb_couches_.getRowCount();
    for (int i = 0; i < _max; i++) {
      dm_tb_couches_.removeRow(0);
    }
    if ((dm_tb_couches_.getRowCount() == 0) || (tb_couches_.getSelectedRowCount() <= 0)) {
      bt_del_.setEnabled(false);
      bt_calc_.setEnabled(false);
    }
  }

  /**
   *
   */
  public ValidationMessages validation() {
    if (tb_couches_.isEditing()) {
      tb_couches_.getCellEditor().stopCellEditing();
    }
    final ValidationMessages _vm = new ValidationMessages();
    Double _v;
    boolean _emptycell = false;
    boolean _epaisseur = false;
    boolean _gh = false;
    boolean _gsat = false;
    boolean _ka = false;
    boolean _cprime = false;
    boolean _phiprime = false;
    final int _max = tb_couches_.getRowCount();
    for (int i = 0; i < _max; i++) {
      try {
        _v = (Double) dm_tb_couches_.getValueAt(i, 1);
      } catch (final ClassCastException _e1) {
        _v = null;
      }
      if (_v == null) {
        _emptycell = true;
      } else if (_v.doubleValue() <= 0) {
        _epaisseur = true;
      }
      try {
        _v = (Double) dm_tb_couches_.getValueAt(i, 2);
      } catch (final ClassCastException _e1) {
        _v = null;
      }
      if (_v == null) {
        _emptycell = true;
      } else if (_v.doubleValue() < 0) {
        _gh = true;
      }
      try {
        _v = (Double) dm_tb_couches_.getValueAt(i, 3);
      } catch (final ClassCastException _e1) {
        _v = null;
      }
      if (_v == null) {
        _emptycell = true;
      } else if (_v.doubleValue() < 0) {
        _gsat = true;
      }
      try {
        _v = (Double) dm_tb_couches_.getValueAt(i, 4);
      } catch (final ClassCastException _e1) {
        _v = null;
      }
      if (_v == null) {
        _emptycell = true;
      } else if (_v.doubleValue() < 0) {
        _ka = true;
      }
      try {
        _v = (Double) dm_tb_couches_.getValueAt(i, 5);
      } catch (final ClassCastException _e1) {
        _v = null;
      }
      if (_v == null) {
        _emptycell = true;
      } else if (_v.doubleValue() < 0) {
        _cprime = true;
      }
      try {
        _v = (Double) dm_tb_couches_.getValueAt(i, 6);
      } catch (final ClassCastException _e1) {
        _v = null;
      }
      if (_v == null) {
        _emptycell = true;
      } else if (_v.doubleValue() < 0) {
        _phiprime = true;
      }
    }
    if (getNbCouches() <= 0) {
      _vm.add(OscarMsg.VMSG011);
    }
    if (getCoteTerrePleinButee() == null) {
      _vm.add(OscarMsg.VMSG014);
    }
    if (_epaisseur) {
      _vm.add(OscarMsg.VMSG018);
    }
    if (_emptycell) {
      _vm.add(OscarMsg.VMSG019);
    }
    if (_gh || _gsat || _ka || _cprime || _phiprime) {
      _vm.add(OscarMsg.VMSG020);
    }
    return _vm;
  }
}
