/*
 * @file         OscarUnitView.java
 * @creation     2002-12-10
 * @modification $Date: 2007-04-27 16:16:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuEmptyList;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuLib;


/**
 * Unit view. sert � afficher dans le panneau unit�s, une liste de conversions entre unit�s
 * 
 * @version $Revision: 1.7 $ $Date: 2007-04-27 16:16:01 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public class OscarUnitView extends BuEmptyList {
  /**
   *
   */
  DefaultListModel model_;
  /**
   *
   */
  Vector unites_;

  /**
   *
   */
  public OscarUnitView() {
    super();
    setName("oscarUNITVIEW");
    model_ = new DefaultListModel();
    setModel(model_);
    setEmptyText("");
    setCellRenderer(new CellRenderer());
    setFont(BuLib.deriveFont("List", Font.PLAIN, -2));
    unites_ = new Vector();
  }

  /**
   *
   */
  public void addConversion(final String _u1, final String _u2) {
    boolean found = false;
    final Object[] _ma = unites_.toArray();
    Conversion _c;
    for (int i = 0; i < _ma.length; i++) {
      _c = (Conversion) _ma[i];
      if ((_c.getUnit1().equals(_u1) && _c.getUnit2().equals(_u2))
          || (_c.getUnit1().equals(_u2) && _c.getUnit2().equals(_u1))) {
        found = true;
        break;
      }
    }
    if (found) {
      return;
    }
    final Conversion _tmpc = new Conversion(_u1, _u2);
    unites_.add(_tmpc);
    model_.addElement(_tmpc);
    revalidate();
    repaint(200);
  }

  /**
   *
   */
  public void removeConversion(final String _u1, final String _u2) {
    int index = -1;
    boolean found = false;
    final Object[] _ma = unites_.toArray();
    Conversion _c;
    for (int i = 0; i < _ma.length; i++) {
      _c = (Conversion) _ma[i];
      if ((_c.getUnit1().equals(_u1) && _c.getUnit2().equals(_u2))
          || (_c.getUnit1().equals(_u2) && _c.getUnit2().equals(_u1))) {
        index = i;
        found = true;
        break;
      }
    }
    if (!found) {
      return;
    }
    final Conversion _dc = (Conversion) unites_.remove(index);
    model_.removeElement(_dc);
    revalidate();
    repaint(200);
  }

  /**
   *
   */
  public void removeAllConversions() {
    final Object[] _ma = unites_.toArray();
    Conversion _c;
    for (int i = 0; i < _ma.length; i++) {
      _c = (Conversion) _ma[i];
      removeConversion(_c.getUnit1(), _c.getUnit2());
    }
    revalidate();
    repaint(200);
  }
  class Conversion {
    String unit1_;
    String unit2_;

    public Conversion(final String _u1, final String _u2) {
      unit1_ = _u1;
      unit2_ = _u2;
    }

    public String getUnit1() {
      return unit1_;
    }

    public String getUnit2() {
      return unit2_;
    }
  }
  /**
   *
   */
  class Cell extends JPanel {
    /**
     *
     */
    BuLabelMultiLine unit1_;
    BuLabelMultiLine unit2_;

    /**
     * Constructor Cell.
     */
    public Cell() {
      this.setLayout(new FlowLayout());
      this.setOpaque(false);
      this.setBorder(new EmptyBorder(1, 3, 1, 3));
      unit1_ = new BuLabelMultiLine();
      unit1_.setText("");
      //unit1_.setWrapMode(BuLabelMultiLine.WORD);
      unit1_.setFont(BuLib.deriveFont("List", Font.PLAIN, -2));
      unit2_ = new BuLabelMultiLine();
      unit2_.setText("");
      //unit2_.setWrapMode(BuLabelMultiLine.WORD);
      unit2_.setFont(BuLib.deriveFont("List", Font.PLAIN, -2));
      final BuLabel _lb = new BuLabel("=");
      this.add(unit1_);
      this.add(_lb);
      this.add(unit2_);
    }

    public void setUnit1(final String _m) {
      unit1_.setText(_m);
      unit1_.setPreferredSize(unit1_.computeHeight((OscarLib.COLUMN_SCROLLPANE_WIDTH )-30 / 2));
    }

    public void setUnit2(final String _m) {
      unit2_.setText(_m);
      unit2_.setPreferredSize(unit2_.computeHeight((OscarLib.COLUMN_SCROLLPANE_WIDTH )-30 / 2));
    }
  }
  /**
   *
   */
  class CellRenderer implements ListCellRenderer {
    /**
     *
     */
    private final Cell r_ = new Cell();

    /**
     *
     */
    public Component getListCellRendererComponent(final JList list, final Object value, final int index,
        final boolean isSelected, final boolean cellHasFocus) {
      final Conversion _v = (Conversion) value;
      r_.setUnit1(_v.getUnit1());
      r_.setUnit2(_v.getUnit2());
      return r_;
    }
  }
}
