/*
 * @file         OscarEffortsParametresContraintesTableCellRenderer.java
 * @creation     2000-11-08
 * @modification $Date: 2007-01-19 11:13:21 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTableCellRenderer;


/**
 * Gestionnaire de rendu de cellule pour le tableau des contraintes suppl�mentaires (verticales et horizontales)
 * 
 * @version $Revision: 1.7 $ $Date: 2007-01-19 11:13:21 $ by $Author: clavreul $
 * @author Olivier Hoareau
 */
public class OscarEffortsParametresContraintesTableCellRenderer extends BuTableCellRenderer {
  public OscarEffortsParametresContraintesTableCellRenderer() {
    super();
  }

  public OscarEffortsParametresContraintesTableCellRenderer(final int _digit) {
    super();
    setNumberFormat(OscarLib.NumberFormat(_digit));
  }

  /**
   * renvoi un objet "rendu" du contenu de la cellule sp�cifi� par rapport � la donn�e fournie.
   * 
   * @param table r�f�rence vers le tableau contenant les donn�es
   * @param value valeur de la cellule � afficher/traiter
   * @param isSelected sp�cifie si la cellule est actuellement selectionn�e
   * @param hasFocus sp�cifie si la cellule a actuellement le focus
   * @param row sp�cifi� le num�ro de ligne de la cellule
   * @param column sp�cifie le num�ro de colonne de la cellule
   */
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
      final boolean hasFocus, final int row, final int column) {
    Component _r = null;
    BuLabel _lb = null;
    _lb = (BuLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    if (value instanceof Double) {}else if (value instanceof Integer) {
	  _lb.setHorizontalAlignment(SwingConstants.CENTER);
	  _lb.setText(((Integer) value).toString());
	  _lb.setForeground(Color.blue);
      } else if (value instanceof String) {
	  _lb.setHorizontalAlignment(SwingConstants.CENTER);
	  _lb.setText((String) value);
	  _lb.setForeground(Color.red);
      }
    _r = _lb;
    return _r;
  }
}
