/*
 * @file         OscarPreferences.java
 * @creation     2002-10-07
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.oscar;

import com.memoire.bu.BuPreferences;

/**
 * Preferences pour Oscar.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public class OscarPreferences extends BuPreferences {
  // Renvoie un nouvel objet OscarPreferences
  public final static OscarPreferences OSCAR = new OscarPreferences();

  /**
   * applyOn() d�clenche une erreur si _o n'est pas une instance de OscarImplementation.
   */
  public void applyOn(final Object _o) {
    WSpy.EnterFonction("void OscarImplementation::applyOn()");
    if (!(_o instanceof OscarImplementation)) {
      throw new RuntimeException("" + _o + " is not a OscarImplementation.");
    }
    WSpy.ExitFonction("void OscarImplementation::applyOn()");
  }
}
